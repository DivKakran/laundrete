<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package clean-blogging
 */

?>

    </div><!-- .container -->
    </div><!-- .wrap -->
    </div><!-- #content -->

    <footer id="colophon" class="site-footer">
		
        <div class="container">
          <div class="row footer-top text-center">
          
            <div class="col-md-12">
              <p class="fadeInDown wow" data-wow-duration="1s" data-wow-delay="0.5s"><a href="/blog"><img src="wp-content/uploads/2017/12/logo.png" alt="Launderette" title="Launderette" class="img-responsive" /></a></p>
				<div class="row support">
					<div class="col-xs-6 support-left fadeInDown wow" data-wow-duration="1s" data-wow-delay="0.7s"><a href="callto:011-71558882" ><i class="fa fa-phone fa-lg" aria-hidden="true"></i> 011-71558882</a></div>
					<div class="col-xs-6 support-right fadeInDown wow" data-wow-duration="1s" data-wow-delay="0.9s"><a href="mailto:info@launderette.in"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i> info@launderette.in</a></div>
				</div>
              <div class="footer-link"><a href="../terms.html" class="fadeInLeft wow" data-wow-duration="1s" data-wow-delay="1.1s">Terms &amp; Conditions</a><a href="../privacy.html" class="fadeInLeft wow" data-wow-duration="1s" data-wow-delay="1.3s">Privacy Policy</a><a class="fadeInLeft wow" href="../faq.html" data-wow-duration="1s" data-wow-delay="1.5s">faq'<span style="text-transform:none;">s</span></a><a class="fadeInLeft wow" href="../schedule" data-wow-duration="1s" data-wow-delay="1.7s">Schedule</a><a class="active fadeInLeft wow" href="<?php echo site_url(); ?>/" data-wow-duration="1s" data-wow-delay="1.9s">Blog</a></div>
            </div>
          </div>
        </div>
        <div class="copyright">
			<div class="wrap clearfix">
				<div class="site-info">
					<div class="left footer_left col-sm-5 footer-social"><a href="https://plus.google.com/105987729922701861721" target="_blank"><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i></a><a href="https://www.facebook.com/LaunderetteApp/" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a><a href="https://twitter.com/LaunderetteApp" target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a><a href="https://www.instagram.com/launderetteapp/" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a><a href="https://youtu.be/_xeldNo_DWg/" target="_blank"><i class="fa fa-youtube-play fa-lg" aria-hidden="true"></i></a></div>
					<div class="footer_right right col-sm-7">
						<?php 
							esc_html_e( 'Copyright &copy;', 'clean-blogging' ); echo bloginfo(); ?>
						<?php
							/* translators: 1: Theme name, 2: Theme author. */
							//printf( esc_html__( 'Theme: %1$s', 'clean-blogging' ), '<a href="https://wpvkp.com/minimal-blogging-theme/">Minimal Blogging Theme</a>' );
						?>
					</div>
					</div>
				</div><!-- .site-info -->
			</div>	
    </footer><!-- #colophon -->
</div><!-- #page -->
<!-- JavaScript for Gallery --> 
<script src="<?php echo home_url(); ?>../../js/jquery.min.js"></script> 
<script src="<?php echo home_url(); ?>../../js/bootstrap.min.js"></script> 
<script src="<?php echo home_url(); ?>../../js/scrolling-nav.js"></script> 
<script>
$(document).ready(function() {
    "use strict"; 
    new WOW().init();
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 80){  
        $('header#masthead').addClass("sticky");
		 $('body').addClass("stick_body");
		
    }
    else{
        $('header#masthead').removeClass("sticky");
		$('body').removeClass("stick_body");
    }
});
</script> 
<!-- Custom JavaScript for the Menu Toggle --> 
<script>
  $("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("active");
  });
</script>
<?php wp_footer(); ?>

</body>
</html>