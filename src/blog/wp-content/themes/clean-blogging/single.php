<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clean-blogging
 */

get_header(); ?>

	<div id="primary" class="content-area col-sm-8">		
		<div class="breadcrumbs">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			
		endwhile; // End of the loop.
		?>
		<?php the_post_thumbnail( 'single-post-thumbnail' );?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
