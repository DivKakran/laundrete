<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package clean-blogging
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
	<base href="<?php echo site_url(); ?>/"/>
	<link href="<?php echo home_url(); ?>../../css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo home_url(); ?>../../css/style.css" rel="stylesheet">
	<link href="<?php echo home_url(); ?>../../css/inner-page.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="<?php echo home_url(); ?>../../js/html5shiv.js"></script>
  <script src="<?php echo home_url(); ?>../../js/respond.js"></script>
<![endif]-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M9KGFCT');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9KGFCT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'clean-blogging' ); ?></a>

    <header id="masthead" class="site-header header-new clearfix striky common-header">
        <div class="wrap">
			<div class="col-md-10 pull-right col-sm-9">
				<nav id="site-navigation" class="main-navigation navbar fadeInDown wow animated">
					<button class="menu-toggle navbar-toggler collapsed" data-toggle="collapse" data-target="#primary-menu" aria-controls="primary-menu" aria-expanded="false" aria-label="Toggle navigation"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span><?php //esc_html_e( 'Primary Menu', 'clean-blogging' ); ?></button>
					<div id="primary-menu" class="navbar-collapse collapse"><?php
						wp_nav_menu( array(
							'theme_location' => 'primary-menu',
							'menu_id'        => 'primary-menu'
						) );
					?>						
					</div>
				</nav>				
			</div>
			<!-- #site-navigation -->
            <div class="site-branding col-md-2 col-sm-3 pull-left logo-inner text-center">
				<a href="../blog"><img class="img-responsive" src="wp-content/uploads/2017/12/logo-inner.png"  alt="Launderette" /></a>
				 <?php
               // the_custom_logo();
               // if ( is_front_page() && is_home() ) : ?>
                    <h1 class="site-title"><a href="<?php //echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php //bloginfo( 'name' ); ?></a></h1>
                <?php //else : ?>
                    <p class="site-title"><a href="<?php //echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php //bloginfo( 'name' ); ?></a></p>
                <?php
                //endif;

                //$description = get_bloginfo( 'description', 'display' );
                //if ( $description || is_customize_preview() ) : ?>
                    <p class="site-description"><?php //echo $description; /* WPCS: xss ok. */ ?></p>
                <?php
                //endif; ?>
            </div><!-- .site-branding -->
        </div>
    </header><!-- #masthead -->

    <div id="content" class="site-content">
        <div class="wrap">
			<div class="container">
				
			</div>
