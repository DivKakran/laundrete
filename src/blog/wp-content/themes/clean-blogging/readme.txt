=== clean-blogging ===

Contributors: automattic
Tags: custom-background, custom-menu, featured-images, threaded-comments, translation-ready, right-sidebar, two-columns, sticky-post, blog

Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.1.3
License: GNU General Public License v2 or later
License URI: LICENSE

Clean Blogging WordPress Theme, Copyright 2017 Designgrande
Clean Blogging is distributed under the terms of the GNU GPL v2.0.

== Description ==

It's a professionally designed minimal wordpress theme for Bloggers, Writers & Content Marketers. If you are looking for a template which is designed to offer great readability, at the same time is mobile responsive and SEO friendly, then try Clean Blogging.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Inorder to generate the Featured image you can install Ajax Thumbnail Rebuild.
This theme has integrated support for Infinite Scroll in Jetpack.

Additionally it supports almost every single plugin. If you get any compatibility issues with any of the plugin installed on your site then please create support ticket.

== Changelog ==

= 1.1.3 - Nov 24 2017 =
* Everything now complies GPL license.

= 1.1.2 - Nov 24 2017 =
* License Updates

= 1.1.1 - Nov 24 2017 =
* CSS improvements for search form.

= 1.1 - Nov 24 2017 =
* Now compatible with WordPress 4.9
* Various changes - required for theme approval.

= 1.0.1 - Oct 4 2017 =
* Minor changes

= 1.0.0 - Oct 4 2017 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
* minblog.ttf (C) 2016-2017 Designgrande, [MIT](http://opensource.org/licenses/MIT)
* All images are based on Creative Commons Zero (CC0) license. This means the pictures are completely free to be used for any legal purpose.