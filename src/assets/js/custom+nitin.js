// Slide Media
$(document).ready(function(){
  $('.media-logo').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 4000,
	  arrows: false,
	  dots: false,
	  pauseOnHover: true,
	  responsive: [{		 
		  breakpoint: 767,
		  settings: {
			  slidesToShow: 2
		  }
	  }, {
		  breakpoint: 480,
		  settings: {
			  slidesToShow: 1
		  }
	  }]
  });
});

// Slide Service
$(document).ready(function(){
  $('.service-slide').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 4000,
	  arrows: false,
	  dots: false,
	  pauseOnHover: true,
	  responsive: [{		 
		  breakpoint: 767,
		  settings: {
			  slidesToShow: 1
		  }
	  }, {
		  breakpoint: 480,
		  settings: {
			  slidesToShow: 1
		  }
	  }]
  });
});
$(document).ready(function(){
  $('.laundry-banner').slick({
	  slidesToShow: 2.35,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 5000,
	  arrows: false,
	  dots: true,
	  pauseOnHover: true,
	  responsive: [{		 
		  breakpoint: 767,
		  settings: {
	  slidesToShow: 2.35,
		  }
	  }, {
		  breakpoint: 480,
		  settings: {
	  slidesToShow: 2.35,
		  }
	  }]
  });
});
// Close Insta-Order 
$(document).ready(mob);
function mob() {
	$('#closeInsta').click(function(e) {
		$('.insta-order').hide('slow');
	});
}