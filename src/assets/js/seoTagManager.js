(function(){
  $.ajax({
    type:"POST",
    url: "http://cms.launderette.staging.birdapps.org/api-web-v3/order/orderdetails",
    data: "user_id="+localStorage.getItem('userId')+"&recent=1",
    success: function(response){
      var transactionTax = parseFloat(response.data[0].IGST) + 
      parseFloat(response.data[0].CGST) + parseFloat(response.data[0].SGST);

      let placedOrderDetail = {tShiping:parseFloat(response.data[0].express_Delivery) + 
        parseFloat(response.data[0].deliveryCharges) , tTax: transactionTax };
      // script start 
      window.dataLayer = window.dataLayer || [];
      dataLayer.push({
        'event':'ecommerce_event',
        'transactionId': response.data[0].order_id,
        'transactionAffiliation': response.data[0].items[0].name,
        'transactionTotal': parseFloat(response.data[0].total_payable_price),
        'transactionTax': placedOrderDetail.tTax,
        'transactionShipping': placedOrderDetail.tShiping,
        'transactionProducts': response.data[0].items[0]
      });
    }
  });
}());
