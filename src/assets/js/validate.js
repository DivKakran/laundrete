//******Form Validation*********
(function($,W,D)
{
    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //form validation rules
      //Name validation
      $.validator.addMethod("nameRegex", function(value, element) {
              return this.optional(element) || /^[a-zA-Z\' ']+$/i.test(value);
      }, "Only letters, or spaces allowed");
	  
      //Mobile validation	  
	  $.validator.addMethod("telRegex", function(value, element) {
              return this.optional(element) || /^[0-9]{10,10}$/i.test(value);
      }, "Only Numbers are allowed");
	  
      //Email validation
	  $.validator.addMethod("emailRegex", function(value, element) {
              return this.optional(element) || /^^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,})$/i.test(value);
      }, "Enter valid email address");
      $("#contact-form2").validate({
         rules: {
            txt_name: {
            required: true,
            nameRegex: true           
          },
          txt_city: {
            required: true,
            nameRegex: true
          },
          txt_email: {
            required: true,
            emailRegex: true
          },		  
          txt_tel: {
            required: true,
			minlength: 10,
			maxlength: 10,
			telRegex: true
          },         
       },
       submitHandler: function(form) {
           submit_post();
          }
      });
    }
  }//when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });
})(jQuery, window, document);


function submit_post(){ 
//alert('Hello');
var dataString = $("#contact-form2").serialize();
      $.ajax({
      type: "POST",
		url:  "cms/api-web-v1/webpage/pickup_schedule",  // for production environment
      //url:  "http://localhost/launderette/api-web-v1/webpage/pickup_schedule", // for local environment
      data: dataString,
      cache: false,    

      beforeSend: function()
      {
		$('#btn_cont2').attr("disabled", true);
      },      
      success: function(msg){
  	   $(".msg_span1").html(msg['message']).hide()
         .fadeIn(1500, function() { $('.msg_span1'); });
        setTimeout(resetAll,4000);
 
         $("#msg_span").html(msg['message']).hide()
            .fadeIn(1500, function() { $('#msg_span'); });
           setTimeout(resetAll,4000);

      },
	  
      error: function(){
      alert("failure");
      }
	  
      });
}


	function resetAll(){
    $('.msg_span1').remove(); 
		$('#msg_span').remove(); 
    $('#msg_span3').remove();
        $('#btn_cont2').attr("disabled", false); 
		document.getElementById("contact-form2").reset();		

	}
