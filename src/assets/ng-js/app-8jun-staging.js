//var app = angular.module("myApp", ['ngRoute', 'angular-flexslider', 'ngAnimate', 'ui.bootstrap','angular-matchmedia']);

app.directive("ngMobileClick", [function () {
    return function (scope, elem, attrs) {
        elem.bind("touchstart click", function (e) {
            e.preventDefault();
            e.stopPropagation();

            scope.$apply(attrs["ngMobileClick"]);
        });
    }
}])


app.directive('starRating',
    function() {
        return {
          restrict: 'EA',
          template:
            '<ul class="star-rating" ng-class="{readonly: readonly}">' +
            '  <li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' +
            '    <i class="fa fa-star"></i>' + // or &#9733
            '  </li>' +
            '</ul>',
          scope: {
            ratingValue: '=ngModel',
            max: '=?', // optional (default is 5)
            onRatingSelect: '&?',
            readonly: '=?'
          },
          link: function(scope, element, attributes) {
            if (scope.max == undefined) {
              scope.max = 5;
            }
            function updateStars() {
              scope.stars = [];
              for (var i = 0; i < scope.max; i++) {
                scope.stars.push({
                  filled: i < scope.ratingValue
                });
              }
            };
            scope.toggle = function(index) {
              if (scope.readonly == undefined || scope.readonly === false){
                scope.ratingValue = index + 1;
                scope.onRatingSelect({
                  rating: index + 1
                });
              }
            };
            scope.$watch('ratingValue', function(oldValue, newValue) {
              if (newValue || newValue === 0) {
                updateStars();
              }
            });
          }
        };
    }
);

app.service("save", function ($window) {
    this.setob = function (key, value) {
        $window.localStorage.setItem(key, JSON.stringify(value).replace(/\\/g, '\\'))
    }
    this.setvar = function (key, value) {
        $window.localStorage.setItem(key, value)
    }
    this.getob = function (key) {
        if ($window.localStorage.hasOwnProperty(key)) {
            return JSON.parse($window.localStorage.getItem(key))
        }
        else {
            return []
        }
    }

    this.getvar = function (key) {
        if ($window.localStorage.hasOwnProperty(key)) {
            return ($window.localStorage.getItem(key))
        }
        else {
            return "" || 0
        }
    }
})

app.config(function ($routeProvider,$locationProvider) {
    $routeProvider.
        when("/", {
            templateUrl: "templates/home.html",
            controller: "homeCtrl"
        })
        .when("/logout", {
            template: "templates/logout",
            controller: "logoutCtrl"
        })
        .when("/service-select", {
            templateUrl: "templates/service-select.html",
            controller: "serviceCtrl"
        })
        .when("/myorder", {
            templateUrl: "templates/my-order.html",
            controller: "myOrderCtrl"
        })
        .when("/trackorder", {
            templateUrl: "templates/track-order.html",
            controller: "trackOrderCtrl"
        })
        .when("/login", {
            templateUrl: "templates/login.html",
            controller: "loginCtrl"
        })
        .when("/login-detail", {
            templateUrl: "templates/login-details.html",
            controller: "loginDetailsCtrl"
        })
        .when("/login-otp", {
            templateUrl: "templates/login-otp.html",
            controller: "verifyOTPCtrl"
        })
        .when("/login-sucessfully", {
            templateUrl: "templates/login-sucessfully.html",
            controller: "loginSucessfullyCtrl"
        })
        .when("/view-address", {
            templateUrl: "templates/view-address.html",
            controller: "viewAddressCtrl"
        })
        .when("/add-address", {
            templateUrl: "templates/add-address.html",
            controller: "addAddressCtrl"
        })
        .when("/date-time", {
            templateUrl: "templates/date-time.html",
            controller: "dateTimeCtrl"
        })
        .when("/payment-option", {
            templateUrl: "templates/payment-option.html",
            controller: "placeOrderCtrl"
        })
        .when("/thank-you", {
            templateUrl: "templates/thank-you.html",
            controller: "thankYouCtrl"
        })
        .when("/error-404", {
            templateUrl: "templates/error-404.html",
        })
        .when("/refer-earn",{
            templateUrl : "templates/refer-earn.html",
            controller : "referEarnCtrl"
        })  
        /*.when("/user-profile",{
            templateUrl : "templates/user-profile.html",
            controller : "userProfileCtrl"
        })   */    
        .otherwise({
            redirectTo: '/'
        }); 
        // use the HTML5 History API
        $locationProvider.html5Mode(true).hashPrefix('!');
});

var object = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "122001", referral_code: "OTgxMDkyOTU4Mg==", user_id: "1", deliveryDetails: {} };
var totalItems = 0;
var services = [];


app.factory('WSCALLFACTORY', function () {
    return {
        requestOTP: function ($http, $window, path, data, config, successpage, errorpage) {
            $http.post(path, data, config).
                then(function successCallback(response) {
                    var status = response.data.status;
                    if (status === "1") {
                        $window.location.href = successpage;
                        return true;
                    }
                    else {
                        //$window.location.href = errorpage;
                        alert(response.data.message);
                        return false;
                    }

                }, function errorCallback(response) {
                    console.log("Unable to perform Post request");
                    return false;
                });
            return false;
        }
    }
});


app.factory("orderDetails", function(){ //orderDetails service Start
    var orderPriceDetails = [];
    var order = [];
    orderPriceDetails.orderCalculation = function (orders,$scope) {
        order = orders;
        //console.log(order);
        var discount = "0.00";
        if(order[0].coupon_type!="") {
            if (order[0].coupon_type == "PERCENTAGE") {
                discount = (parseFloat(order[0].total_item_price) * parseFloat(order[0].coupon_value)) / 100;
                discount =(discount).toFixed(2);
                if (parseFloat(discount) > parseFloat(order[0].coupon_discount_upto)) {
                    discount = parseFloat(order[0].coupon_discount_upto);
                }
            }
            if (order[0].coupon_type === "FLAT") {
                discount = parseFloat(order[0].coupon_value);
            }
        } 
        if(order[0].coupon_type=="" && order[0].coupon_value!=""){
            discount = order[0].coupon_value;
        }
        if(discount=="0"){
            discount = "0.00";
        }
        netTotal = parseFloat(order[0].total_item_price) - parseFloat(discount);  // calculating Net Total after discount
        netTotal = (netTotal).toFixed(2);
        if (order[0].express_Delivery != "0.00") {
            deliveryType = "Express Delivery";
            delivery_charges = parseFloat(order[0].deliveryCharges) + parseFloat(order[0].express_Delivery);
        }
        else {
            deliveryType = "Delivery";
            delivery_charges = parseFloat(order[0].deliveryCharges);
        }
        
        delivery_charges = (delivery_charges).toFixed(2);

        total = parseFloat(netTotal) + parseFloat(delivery_charges); // sum of Net Total + delivery_amt
        total = parseFloat(total).toFixed(2);

        cgstPercentage = order[0].CGST;
        sgstPercentage = order[0].SGST;
        igstPercentage = order[0].IGST;
        if (order[0].CGST > 0) {
            cgst = (parseFloat(total) * parseFloat(order[0].CGST)) / 100;
            cgst = (cgst).toFixed(2);
        }
        else {
            cgst = parseFloat(order[0].CGST);
        }
        if (order[0].SGST > 0) {
            sgst = (parseFloat(total) * parseFloat(order[0].SGST)) / 100;
            sgst =(sgst).toFixed(2);
        }
        else {
            sgst = parseFloat(order[0].SGST);
        }
        if (order[0].IGST > 0) {
            igst = (parseFloat(total) * parseFloat(order[0].IGST)) / 100;
            igst =(igst).toFixed(2);
        }
        else {
            igst = parseFloat(order[0].IGST);
        }
        grandTotal = parseFloat(total) + parseFloat(cgst) + parseFloat(sgst) + parseFloat(igst);
        grandTotal = Math.round(grandTotal * 100) / 100;

        credit = order[0].credit;
        credit = Math.round(credit * 100) / 100;

        amount_payable = parseFloat(order[0].total_payable_price);
        var orderPriceDetails = [{
            "price": order[0].total_item_price,
            "discount": discount,
            "deliveryType": deliveryType,
            "deliveryCharges": delivery_charges,
            "cgst": cgst,
            "sgst": sgst,
            "igst": igst,
            "cgstPercentage": cgstPercentage,
            "sgstPercentage": sgstPercentage,
            "igstPercentage": igstPercentage,
            "credit": order[0].credit,
            "amountPayable": order[0].total_payable_price
        }];
        return orderPriceDetails;
    }
    return orderPriceDetails;
}); //orderDetails service end

app.controller("logoutCtrl", function ($window, $location) {
    $window.sessionStorage.clear(), 
    $window.localStorage.clear() 
    $location.url("/");
})

/* Home Pincode Controller */
app.controller("homeCtrl", ['$scope', '$http', '$window', '$rootScope', 'save', function ($scope, $http, $window, $rootScope, save) {
    $rootScope.popupLoginClicked = false;
    $rootScope.loginUser = true;
    $rootScope.statusClick = false;             //variable to check user want to see recent or specific order on track-order.html
    $rootScope.fbExist = false;     //variable to check user is login through fb 1st time or not & used in fblogin   
    $scope.continueLoginClicked = false;    
    save.setvar("baseApiURl",$rootScope.baseApiURl);
    save.setvar("baseSiteUrl",$rootScope.baseSiteUrl);   
    // point1 starts    
    
    // code for removeing index page content on all angular pages
    var path = window.location.pathname;   
    if(path==$rootScope.baseSiteUrl || path=='/'){
    document.getElementById("contents-area").style.display="block";
    }
    // end code for removeing index page content on all angular pages

    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.userLogin = save.getvar("userLogin")
    $rootScope.loginOtp = save.getvar("loginOtp")
    $rootScope.loginSucess = save.getvar("loginSucess")
    $rootScope.userId = save.getvar("userId")
    $rootScope.userName = save.getvar("userName")
    $rootScope.referralCode = save.getvar("referralCode")
    $rootScope.userDetails = save.getob("userDetails")
    $rootScope.addressDetails = save.getob("addressDetails")
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.popupLogin = function () {
        $rootScope.loginOtp = false;
        $rootScope.loginSucess = false;
        $rootScope.loginRegister = false;
        $scope.otp1 = "";
        $scope.otp2 = "";
        $scope.otp3 = ""; 
        $scope.otp4 = "";
        $scope.otp5 = "";
        $scope.phonenumber = "";// by default setting login related textfield values blank       
        $rootScope.loginUser = true;
        $rootScope.popupLoginClicked = true;
    }
    $scope.sorryPopup = false;
    $scope.sorryClose = function () {
        $scope.sorryPopup = false;
        $scope.serviceAreas = false;
    }

    $scope.serviceAreas = false;
    $scope.serviceAvail = function () {
        $scope.sorryPopup = false;
        $scope.serviceAreas = true;
        var data1 = "";
        var path = $rootScope.baseApiURl + "location/serviceArea";
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
        $http.post(path,data1,config).then(function sucessCallback(response){
            if(response.data.status== "1") {
                $scope.serviceList = response.data.serviceList;                
            }
            else {
                alert(response.data.message);
            }
        },
        function errorCallback(response){
            console.log("serviceList response not get");
        });
    }

    
    $scope.pickServiceArea = function (pincode) {
        $scope.serviceAreas = false;
        $scope.txt_pincode = pincode;        
        $scope.chk_pincode();
        $scope.sorryPopup = false;
        $scope.serviceAreas = false;
    }

    $scope.appFormSubmission= function(){ // App form submission function
        var path = $rootScope.baseApiURl + "webpage/email";
        var data = "txt_email="+ $scope.txt_get_app;
        config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
       $http.post(path,data,config).then(function sucessCallback(response){ 
            $scope.appresponse = response.data.message; 
            $scope.txt_get_app = "";
       },
       function errorCallback(response){
           alert(response.data);
       });
    } // App form submission function end 
    

    $rootScope.totalItems = 0;
    $rootScope.services = [];
    $scope.chk_pincode = function () {  //chk_pincode function start
        if($scope.txt_pincode=="" || $scope.txt_pincode==undefined){
            $scope.sorryPopup = true;
            return;
        }
        var data = "postcode=" + $scope.txt_pincode;
        config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
        var response = $http.post($rootScope.baseApiURl + "location/locationbypostcode", data, config).
            then(function sucessCallback(response) {
                if (response.data.status == "1") {
                    $rootScope.cityName = response.data.cityName;
                    $rootScope.pincodeVal = $scope.txt_pincode;
                    save.setvar("pincodeVal", $rootScope.pincodeVal);
                    save.setvar("cityName", $rootScope.cityName)
                    save.setob("services", [])
                    save.setvar("totalItems","0")
                    save.setob("itemList",[])
                    save.setvar("price", "0")
                    save.setob("laundry_settings",{})

                    save.setvar("popupLoginClicked", "$rootScope.popupLoginClicked")
                    save.setvar("loginUser", "$rootScope.loginUser")
                    $window.location.href = 'service-select';
                }
                else {
                    $scope.sorryPopup = true;
                }
            }, function errorCallback(response) {
            });

    }   //chk_pincode function ends


}]);


/* Service Controller */
app.controller("serviceCtrl", function ($scope, $http, $rootScope, save, $window) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.hideContentArea();
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal==""){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {
        $rootScope.itemList = [];
        $rootScope.totalItems = "0";
        //point3 starts 
        $rootScope.data = { 

            paymentDetails: { price: 0 }, 
            itemList: [], 
            serviceComment: [], 
            postcode: "", 
            referral_code: "", 
            user_id: "", 
            deliveryDetails: {} 
            };
        // $rootScope.data = object;
       
       save.setob("services", [])
        save.setvar("totalItems","0")
        save.setob("itemList",[])
        save.setvar("price", "0")
        save.setob("laundry_settings",{})

        $rootScope.userLogin = save.getvar("userLogin")
        $rootScope.loginOtp = save.getvar("loginOtp")
        $rootScope.loginSucess = save.getvar("loginSucess")
        $rootScope.popupLoginClicked = save.getvar("popupLoginClicked");
        $rootScope.loginUser = save.getvar("loginUser");
        $rootScope.userId = save.getvar("userId")
        $rootScope.userName = save.getvar("userName")
        $rootScope.referralCode = save.getvar("referralCode")
        $rootScope.userDetails = save.getob("userDetails")
        $rootScope.addressDetails = save.getob("addressDetails")
        $rootScope.cityName = save.getvar("cityName")
        $rootScope.services = save.getob("services")
        $rootScope.totalItems = save.getvar("totalItems")
        $rootScope.data.itemList = save.getob("itemList")
        $rootScope.data.paymentDetails.price = save.getob("price")        

        //point4 ends 
        $rootScope.totalCardOrder = 0;
        $scope.showcart = function(x){  // function to show & hide proceed button
            if(x==0){
                $scope.zeroCart = true;
            }
            else {
                $scope.zeroCart = false;
            }
        }
        var data0 = "postcode=" + $rootScope.pincodeVal;
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
        var path2 = $rootScope.baseApiURl + "home/home";
        if ($scope.serviceList == undefined) {
            $http.post(path2, data0, config).
                then(function successCallback(response) {
                    $scope.serviceList = response.data.serviceList;
                    $rootScope.serviceid = response.data.serviceList[0].id;  // to show default service details
                    $rootScope.laundry_settings = response.data.laundry_settings;
                    save.setob("laundry_settings",$rootScope.laundry_settings);

                    var data1 = "postcode=" + $rootScope.pincodeVal + "&service_id=" + $rootScope.serviceid;

                    $scope.isCollapsed = false;
                    $scope.selectedTab = 0;
                    $scope.selectedSubTab = 0;

                    $http.post($rootScope.baseApiURl + "serviceItems/service", data1, config).
                        then(function successCallback(response) {
                            if ($rootScope.itemList == undefined) {
                                $rootScope.itemList = response.data.itemList;
                            } else {
                                var newItemList = response.data.itemList;

                                for (i = 0; i < $rootScope.itemList.length; i++) {
                                    for (j = 0; j < $rootScope.itemList[i].sub_category.length; j++) {
                                        for (k = 0; k < $rootScope.itemList[i].sub_category[j].item_list.length; k++) {

                                            if ($rootScope.itemList[i].service_name == newItemList.itemList[i].service_name && $rootScope.itemList[i].name == newItemList.itemList[i].name
                                                && $rootScope.itemList[i].sub_category[j].name == newItemList.itemList[i].sub_category[j].name && $rootScope.itemList[i].sub_category[j].item_list[k].name == newItemList.itemList[i].sub_category[j].item_list[k].name) {

                                                newItemList.itemList[i].sub_category[j].item_list[k].weight = $rootScope.itemList[i].sub_category[j].item_list[k].weight
                                            }
                                        }
                                    }

                                }
                                $rootScope.itemList = newItemList;              
                            }

                        }, function errorCallback(response) {
                            console.log("Unable to perform get request");
                        });

                }, function errorCallback(response) {
                    console.log("Unable to perform get request");
                });


        }

        /** When user click on   **/
        $scope.selectSubTab = function (id, index) {
            $rootScope.serviceid = id;
            var data1 = "postcode=" + $rootScope.pincodeVal + "&service_id=" + $rootScope.serviceid;
            $scope.isCollapsed = false;
            $scope.selectedTab = 0;
            $scope.selectedSubTab = index;

            $http.post($rootScope.baseApiURl + "serviceItems/service", data1, config).
                then(function successCallback(response) {
                    $rootScope.itemList = response.data.itemList;
                    for (i = 0; i < $rootScope.itemList.length; i++) {
                        for (j = 0; j < $rootScope.itemList[i].sub_category.length; j++) {
                            for (k = 0; k < $rootScope.itemList[i].sub_category[j].item_list.length; k++) {

                                for (z = 0; z < $rootScope.data.itemList.length; z++) {
                                    if ($rootScope.itemList[i].service_name == $rootScope.data.itemList[z].serviceName && $rootScope.itemList[i].name == $rootScope.data.itemList[z].categoryName && $rootScope.itemList[i].sub_category[j].name == $rootScope.data.itemList[z].subCategoryName
                                        && $rootScope.itemList[i].sub_category[j].item_list[k].name == $rootScope.data.itemList[z].itemName) {

                                        var itemCount = $rootScope.data.itemList[z].itemCount;
                                        $rootScope.itemList[i].sub_category[j].item_list[k].weight = itemCount;
                                    }
                                }
                            }
                        }                    
                    }
                }, function errorCallback(response) {
                    console.log("Unable to perform get request");
                });

        }
        $scope.selectTab = function (index) {
            $scope.isCollapsed = true;
            $scope.selectedTab = index;
        }


        /**  when user click on + button this method called **/
        //                      item_list.rate , item_list.id , item_sub.name , item_list.name , items.service_name , items.service_id , service_item.id , service_item.name , item_sub.id
        $scope.addOrder = function (itemRate, itemId, subCategoryName, itemName, serviceName, serviceId, categoryId, categoryName, subCategoryId) {
            for (i = 0; i < $rootScope.itemList.length; i++) {
                for (j = 0; j < $rootScope.itemList[i].sub_category.length; j++) {
                    for (k = 0; k < $rootScope.itemList[i].sub_category[j].item_list.length; k++) {

                        if ($rootScope.itemList[i].service_name == serviceName && $rootScope.itemList[i].name == categoryName
                            && $rootScope.itemList[i].sub_category[j].name == subCategoryName && $rootScope.itemList[i].sub_category[j].item_list[k].name == itemName) {

                            $rootScope.itemList[i].sub_category[j].item_list[k].weight = parseInt($rootScope.itemList[i].sub_category[j].item_list[k].weight) + 1;
                        }
                    }
                }
            }
            var matched = true;
            for (i = 0; i < $rootScope.data.itemList.length; i++) {
                if ($rootScope.data.itemList[i].itemId == itemId && $rootScope.data.itemList[i].itemRate == itemRate && $rootScope.data.itemList[i].subCategoryName == subCategoryName && $rootScope.data.itemList[i].itemName == itemName && $rootScope.data.itemList[i].serviceName == serviceName && $rootScope.data.itemList[i].serviceId == serviceId && $rootScope.data.itemList[i].categoryId == categoryId && $rootScope.data.itemList[i].categoryName == categoryName && $rootScope.data.itemList[i].subCategoryId == subCategoryId) {
                    $rootScope.data.itemList[i].itemCount = parseInt($rootScope.data.itemList[i].itemCount) + 1;
                    matched = false;
                }
            }
            if (matched) {
                var temp = {
                    "itemId": itemId,               // we need to set
                    "itemRate": itemRate,       // we need to set 
                    "itemCount": "1",       // we need to set 
                    "subCategoryName": subCategoryName, // we need to set 
                    "itemName": itemName,   // we need to set 
                    "categoryId": categoryId,
                    "itemType": "",
                    "itemWeight": "0",
                    "serviceName": serviceName,     // we need to set
                    "categoryName": categoryName,               // we need to set 
                    "serviceId": serviceId,
                    "subCategoryId": subCategoryId
                }
                $rootScope.data.itemList.push(temp);
            }

            $rootScope.data.paymentDetails.price = parseInt($rootScope.data.paymentDetails.price) + parseInt(itemRate);
            $rootScope.totalItems = parseInt($rootScope.totalItems) + 1;
            if ($rootScope.services.length > 0) {
                var unique_array = []
                for (i = 0; i < $rootScope.services.length; i++) {
                    if ($rootScope.services.indexOf(serviceName) == -1) {
                        $rootScope.services.push(serviceName);
                    }
                }
            } 
            else {
                $rootScope.services.push(serviceName);
            }           

        }

        /**  when user click on - button this method called **/
        $scope.removeOrder = function (itemRate, itemId, subCategoryName, itemName, serviceName, serviceId, categoryId, categoryName, subCategoryId) {
         
            for (i = 0; i < $rootScope.itemList.length; i++) {
                for (j = 0; j < $rootScope.itemList[i].sub_category.length; j++) {
                    for (k = 0; k < $rootScope.itemList[i].sub_category[j].item_list.length; k++) {
                        if (parseInt($rootScope.itemList[i].sub_category[j].item_list[k].id) == parseInt(itemId) && $rootScope.itemList[i].sub_category[j].item_list[k].name == itemName) {
                            
                            if(parseInt($rootScope.itemList[i].sub_category[j].item_list[k].weight)==0)
                            return;
                            $rootScope.itemList[i].sub_category[j].item_list[k].weight = parseInt($rootScope.itemList[i].sub_category[j].item_list[k].weight) - 1;
                            $rootScope.totalItems = parseInt($rootScope.totalItems) - 1;
                            for (index = 0;index < $rootScope.data.itemList.length; index++) {
                                if (parseInt($rootScope.data.itemList[index].itemId) == parseInt(itemId) && parseInt($rootScope.data.itemList[index].serviceId) == parseInt(serviceId) && parseInt($rootScope.data.itemList[index].itemCount)>1){
                                    $rootScope.data.itemList[index].itemCount = parseInt($rootScope.data.itemList[index].itemCount) - 1;
                                    $rootScope.data.paymentDetails.price = parseInt($rootScope.data.paymentDetails.price) - parseInt(itemRate);
                                 }
                                else if(parseInt($rootScope.data.itemList[index].itemId) == parseInt(itemId) && parseInt($rootScope.data.itemList[index].serviceId) == parseInt(serviceId)){ 
                                    $rootScope.data.paymentDetails.price = parseInt($rootScope.data.paymentDetails.price) - parseInt(itemRate);
                                    $rootScope.data.itemList.splice(index,1);                                    
                                    break;
                                }
                            } 
                        }
                    }
                }
            }
            for(i=0;i<$rootScope.services.length;i++){               // loop to delete service if count==0
                    count=$rootScope.data.itemList.filter(function(z){
                        return z.serviceName==$rootScope.services[i]
                    }).length
                if(count==0)
                {
                    $rootScope.services.splice(i,1)
                }
            }
        }

        $scope.deleteOrder = function (serviceName, itemName, categoryName, subCategoryName, itemRate, itemCount) {
            for (i = 0; i < $rootScope.itemList.length; i++) {
                for (j = 0; j < $rootScope.itemList[i].sub_category.length; j++) {
                    for (k = 0; k < $rootScope.itemList[i].sub_category[j].item_list.length; k++) {

                        if ($rootScope.itemList[i].service_name == serviceName && $rootScope.itemList[i].name == categoryName
                            && $rootScope.itemList[i].sub_category[j].name == subCategoryName && $rootScope.itemList[i].sub_category[j].item_list[k].name == itemName) {

                            $rootScope.itemList[i].sub_category[j].item_list[k].weight = 0;
                        }

                    }

                }
            }

            var index;
            var serviceNameCount = 0;
            for (i = 0; i < $rootScope.data.itemList.length; i++) {
                if ($rootScope.data.itemList[i].itemRate == itemRate && $rootScope.data.itemList[i].subCategoryName == subCategoryName && $rootScope.data.itemList[i].itemName == itemName && $rootScope.data.itemList[i].serviceName == serviceName && $rootScope.data.itemList[i].categoryName == categoryName) {
                    index = i;
                }
                if ($rootScope.data.itemList[i].serviceName == serviceName) {
                    serviceNameCount = serviceNameCount + 1;
                }
            }

            var serviceIndex;
            if (serviceNameCount == 1) {
                for (i = 0; i < $rootScope.services.length; i++) {
                    if ($rootScope.services[i] == serviceName) { serviceIndex = i; }
                }
                $rootScope.services.splice(serviceIndex, 1);
            }

            $rootScope.data.itemList.splice(index, 1);
            $rootScope.totalItems = parseInt($rootScope.totalItems) - parseInt(itemCount);      // total no of items in bag. 
            $rootScope.data.paymentDetails.price = parseInt($rootScope.data.paymentDetails.price) - (parseInt(itemRate) * parseInt(itemCount));  // total item price change. 
            if($rootScope.totalItems<=0){
                $scope.zeroCart=true;
            }
        }


        $scope.removeOrderByModel = function (serviceName, itemName, categoryName, subCategoryName, itemRate, itemCount, popup) {
            
            if (popup) { if (itemCount <= 1) return; }
            var zerocall;
            for (i = 0; i < $rootScope.itemList.length; i++) {
                for (j = 0; j < $rootScope.itemList[i].sub_category.length; j++) {
                    for (k = 0; k < $rootScope.itemList[i].sub_category[j].item_list.length; k++) {

                        if ($rootScope.itemList[i].service_name == serviceName && $rootScope.itemList[i].name == categoryName
                            && $rootScope.itemList[i].sub_category[j].name == subCategoryName && $rootScope.itemList[i].sub_category[j].item_list[k].name == itemName) {

                            if ($rootScope.itemList[i].sub_category[j].item_list[k].weight != 0) {
                                $rootScope.itemList[i].sub_category[j].item_list[k].weight = parseInt($rootScope.itemList[i].sub_category[j].item_list[k].weight) - 1;
                            } else {
                                zerocall = 1;
                                $rootScope.itemList[i].sub_category[j].item_list[k].weight = 0;
                                $rootScope.itemList[i].sub_category[j].item_list[k].weight = 0;
                            }
                        }

                    }

                }

            }

            var index;
            if (zerocall != 1) {
                for (i = 0; i < $rootScope.data.itemList.length; i++) {
                    if ($rootScope.data.itemList[i].itemRate == itemRate && $rootScope.data.itemList[i].subCategoryName == subCategoryName && $rootScope.data.itemList[i].itemName == itemName && $rootScope.data.itemList[i].serviceName == serviceName && $rootScope.data.itemList[i].categoryName == categoryName) {
                        index = i;
                    }
                }

                $rootScope.data.itemList[index].itemCount = parseInt($rootScope.data.itemList[index].itemCount) - 1;
                $rootScope.totalItems = parseInt($rootScope.totalItems) - 1;        // total no of items in bag. 
                $rootScope.data.paymentDetails.price = parseInt($rootScope.data.paymentDetails.price) - parseInt(itemRate);  // total item price change. 
            }
        }

        $scope.addOrderByModel = function (serviceName, itemName, categoryName, subCategoryName, itemRate, itemCount) {
            // console.log("addOrderByModel itemWeight : " + itemWeight);
            for (i = 0; i < $rootScope.itemList.length; i++) {
                for (j = 0; j < $rootScope.itemList[i].sub_category.length; j++) {
                    for (k = 0; k < $rootScope.itemList[i].sub_category[j].item_list.length; k++) {

                        if ($rootScope.itemList[i].service_name == serviceName && $rootScope.itemList[i].name == categoryName
                            && $rootScope.itemList[i].sub_category[j].name == subCategoryName && $rootScope.itemList[i].sub_category[j].item_list[k].name == itemName) {

                            $rootScope.itemList[i].sub_category[j].item_list[k].weight = parseInt($rootScope.itemList[i].sub_category[j].item_list[k].weight) + 1;
                        }

                    }
                }

            }
            for (i = 0; i < $rootScope.data.itemList.length; i++) {
                if ($rootScope.data.itemList[i].itemRate == itemRate && $rootScope.data.itemList[i].subCategoryName == subCategoryName && $rootScope.data.itemList[i].itemName == itemName && $rootScope.data.itemList[i].serviceName == serviceName && $rootScope.data.itemList[i].categoryName == categoryName) {
                    $rootScope.data.itemList[i].itemCount = parseInt($rootScope.data.itemList[i].itemCount) + 1;
                }
            }

            $rootScope.data.paymentDetails.price = parseInt($rootScope.data.paymentDetails.price) + parseInt(itemRate);
            $rootScope.totalItems = parseInt($rootScope.totalItems) + 1;
        }

        /*----------------- ProceedOrder Start -----------------*/
        $scope.proceedOrder = function () {
            save.setob("services", $rootScope.services)
            save.setvar("totalItems", $rootScope.totalItems)
            save.setob("itemList", $rootScope.data.itemList)
            save.setvar("price", $rootScope.data.paymentDetails.price);
            var myModalOpen = angular.element(document.querySelector('body.modal-open')).removeClass('modal-open');
            if ($rootScope.userLogin == true) { // checking user is loginned or Not
                if($rootScope.addressDetails.HOME.pincode=="" && $rootScope.addressDetails.WORK.pincode=="" && $rootScope.addressDetails.OTHER.pincode=="" || $rootScope.addressDetails.length=='0') {
                    $window.location.href = "add-address";
                }
                else {
                    $window.location.href = "view-address";
                }
            }
            else {
                $window.location.href = "login";
            }
        }
        /*----------------- ProceedOrder End -----------------*/
    }
});

/* Login Controller */

app.controller("loginCtrl", function ($scope, $http, $rootScope, $window, WSCALLFACTORY,save) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    $scope.btnLoginClicked = false;
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="",$rootScope.totalItems==0){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {
        $scope.phonenumber = "";
        //point3 starts 
        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "122001", referral_code: "OTgxMDkyOTU4Mg==", user_id: "1", deliveryDetails: {} };
        $rootScope.cityName = save.getvar("cityName");        
        $rootScope.services = save.getob("services");        
        $rootScope.data.itemList = save.getob("itemList");        
        $rootScope.data.paymentDetails.price = save.getvar("price");
        $rootScope.laundry_settings = save.getob("laundry_settings");        

        var myModalBackdrop = angular.element(document.querySelector('.modal-backdrop.fade.in'));
        myModalBackdrop.removeClass('in modal-backdrop');
        $rootScope.userExist = false;        
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
        
        $scope.login = function () {
            if($scope.phonenumber.length!="10"){
                alert("Please Enter 10 Digits Phone Number")
                return;
            }
            $scope.btnLoginClicked = true;
            var pnumber = $scope.phonenumber;
            $rootScope.phonenumber = pnumber;

            var data = "login_type=MOBILE&uuid_value=1234&login_value=" + $rootScope.phonenumber;
            var path = $rootScope.baseApiURl + "login/login";

            $http.post(path, data, config).
                then(function successCallback(response) {                
                    var result = response.data.message;
                    //var message = "User Does Not Exist";                    
                    if (response.data.status === "0") {       // if user is not registered as of now. take him to registration page.   
                        save.setvar("phonenumber",$rootScope.phonenumber); 
                        save.setvar("userExist",$rootScope.userExist);                           
                        $window.location.href = "login-detail";
                    }
                    else {  //  if user is already regrestred. generate otp and take him to verify otp page.                               
                        $rootScope.userExist = true;      
                        save.setvar("phonenumber",$rootScope.phonenumber);
                        save.setvar("userExist",$rootScope.userExist);                  
                        $window.location.href = "login-otp";
                    }                    
                    $scope.btnLoginClicked = false;        
                });
        }
        
        $scope.facebooklogin = function () {
            //console.log("facebooklogin method called");
            FB.getLoginStatus(function (response) {
                if (response.status == 'connected') {   // if user is already connected.
                    alert("user already connected ");
                    $scope.LoginMethod(response);

                } else {                // if user is not connected . 
                    FB.login(function (response) {
                        $scope.LoginMethod(response);

                    });
                }

            });
        }

        $rootScope.LoginMethod = function (response) {
            if (response.authResponse) {
                console.log('Welcome!  Fetching your information.... ');

                FB.api('/me', { fields: 'id ,name ,email' }, function (response) {
                    var accessToken = FB.getAuthResponse();

                   
                    $rootScope.name = response.name;
                    $rootScope.emailaddress = response.email;
                    $rootScope.fbuserid = response.id;

                    // after facbook looking we are calling out login functionality  with faceook details.  
                    var data = "login_type=FB&uuid_value=1234&login_value=" + response.id;
                    var path = $rootScope.baseApiURl + "login/login";

                    $http.post(path, data, config).
                        then(function successCallback(response) {
                           
                            var result = response.data.message;
                            var message = "User Does Not Exist";
                            if (result === message) {       // if user is not registered as of now. take him to registration page. 
                                $rootScope.fbExist = true;                                
                                $window.location.href = "login-detail";
                            }
                            else {          // if user is registered then calll  request otp services.                                  
                                //  if user is already regrestred. generate otp and take him to verify otp page.                
                                var path = $rootScope.baseApiURl + "login/requestotp";
                                var data = "social_id=" + response.id + "&uuid_value=1234";
                                var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
                                var successpage = 'login-otp';
                                var errorpage = 'error-404';
                                WSCALLFACTORY.requestOTP($http, $window, path, data, config, successpage, errorpage);
                            }
                        });
                });
            }
            else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }
    }    
    
});

/* Login Details Controller */

app.controller("loginDetailsCtrl", function ($rootScope,$scope, $http ,$window, WSCALLFACTORY,save) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $scope.name = null;
    $scope.emailaddress = null;

    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="",$rootScope.totalItems==0){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else{        
        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "122001", referral_code: "OTgxMDkyOTU4Mg==", user_id: "1", deliveryDetails: {} };
        
        $rootScope.cityName = save.getvar("cityName");        
        $rootScope.services = save.getob("services");        
        $rootScope.data.itemList = save.getob("itemList");        
        $rootScope.data.paymentDetails.price = save.getvar("price");
        $rootScope.laundry_settings = save.getob("laundry_settings"); 
        $rootScope.phonenumber = save.getvar("phonenumber"); 

        $scope.requestotp = function () {
            if($scope.name=="" || $scope.name == null || $scope.emailaddress=="" || $scope.emailaddress == null) {
                alert("Please provide valid Name & Address details.")
                return;
            }                                       
            $rootScope.name = $scope.name;
            //$rootScope.phonenumber = $scope.phonenumber;
            $rootScope.emailaddress = $scope.emailaddress;
            var path = $rootScope.baseApiURl + "login/requestotp";
            if($rootScope.fbExist == true) {
                var data = "mobile=" + $rootScope.phonenumber + "&name=" + $rootScope.name + "&email=" + $rootScope.emailaddress + "&uuid=1234&login_type=FB&uuid_value=1234&social_id="+$rootScope.fbuserid +"&device_type=web";
            }
            else {
                var data = "mobile=" + $rootScope.phonenumber + "&name=" + $rootScope.name + "&email=" + $rootScope.emailaddress + "&uuid=1234&login_type=MOBILE&uuid_value=1234&social_id=MOBILE&device_type=web";
            }
            var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
            //var successpage = 'login-otp';
            //var errorpage = 'error-404';
            //WSCALLFACTORY.requestOTP($http, $window, path, data, config, successpage, errorpage);
            $http.post(path, data, config).then(function successCallback(response) {
                if(response.data.status === "1") {
                    $window.location.href = "login-otp";                    
                }
                else {
                    alert(response.data.message);
                }
            }, function errorCallback(response) {
                console.log("Unable to perform Post request");
                return false;
            });

            save.setvar("name",$rootScope.name);
            save.setvar("emailaddress",$rootScope.emailaddress);
            $scope.name = null;
            $scope.emailaddress = null;
        }

    }
});


/* Verify OTP Controller */

app.controller("verifyOTPCtrl", function ($rootScope, $scope, $http, $window, save, WSCALLFACTORY) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    $scope.submitVerifyOtp = false;
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="",$rootScope.totalItems==0){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {
        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "122001", referral_code: "OTgxMDkyOTU4Mg==", user_id: "1", deliveryDetails: {} };
        $rootScope.cityName = save.getvar("cityName");        
        $rootScope.services = save.getob("services");        
        $rootScope.data.itemList = save.getob("itemList");        
        $rootScope.data.paymentDetails.price = save.getvar("price");
        $rootScope.laundry_settings = save.getob("laundry_settings");
        $rootScope.phonenumber = save.getvar("phonenumber"); 
        $rootScope.userExist = save.getvar("userExist"); 

        /* Verify OTP Function */
        $scope.verifyotp = function () {
            var otp = $scope.otp1 + $scope.otp2 + $scope.otp3 + $scope.otp4 + $scope.otp5;
            if (otp.length == 5) { // If OTP length is 5 then Call verifyOtp
                $scope.submitVerifyOtp = true;
                if($rootScope.userExist == "true") { //if user exist then Calling VerifyOtp
                    var path = $rootScope.baseApiURl + "login/verifyotp"
                    var data = "login_type=MOBILE&uuid_value=1234&login_value=" + $rootScope.phonenumber + "&otp=" + otp;
                    var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
                    $http.post(path, data, config).
                        then(function successCallback(response) {
                            if(response.data.status==="0"){
                                alert(response.data.message);
                                $scope.otp1 = "";
                                $scope.otp2 = "";
                                $scope.otp3 = "";
                                $scope.otp4 = "";
                                $scope.otp5 = "";
                                $scope.submitVerifyOtp = false;
                                return;
                            }
                            if (response.data.message === "OTP Not Matched.Please click on resend OTP to generate new OTP.") {
                                alert("OTP Not Matched.Please click on resend OTP to generate new OTP.");
                                $scope.submitVerifyOtp = false;
                            }
                            else {
                                $rootScope.userDetails = response.data.userDetails;                                
                                $rootScope.addressDetails = response.data.addressDetails;
                                $rootScope.userId = response.data.userDetails.user_id;
                                $rootScope.userName = response.data.userDetails.name;
                                $rootScope.emailaddress = response.data.userDetails.email;
                                $rootScope.referralCode = response.data.userDetails.referral_code;
                                $rootScope.userLogin = true;
                                $window.location.href = "login-sucessfully";

                                save.setvar('userId', response.data.userDetails.user_id);
                                save.setvar('userName', response.data.userDetails.name);
                                save.setvar('referralCode', response.data.userDetails.referral_code);
                                save.setob('userDetails', response.data.userDetails);
                                save.setob('addressDetails', response.data.addressDetails);
                                $scope.submitVerifyOtp = false;
                            }

                        });
                }
                else { //if user does not exist then Calling Register
                    //$rootScope.phonenumber = save.getvar("phonenumber");
                    $rootScope.name = save.getvar("name");
                    $rootScope.emailaddress = save.getvar("emailaddress");
                    var path = $rootScope.baseApiURl + "login/register";

                    var data = "otp=" + otp + "&postcode=" + $rootScope.pincodeVal + "&uuid_value=1234&name=" + $rootScope.name + "&email=" + $rootScope.emailaddress + "&mobile=" + $rootScope.phonenumber + "&login_type=MOBILE&social_id=MOBILE&device_type=WEB";
                    var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
                    $http.post(path, data, config).
                        then(function successCallback(response) {
                            if(response.data.status=="1") {
                                $rootScope.userDetails = response.data.userDetails;
                                $rootScope.addressDetails = response.data.addressDetails;
                                $rootScope.userId = response.data.userDetails.user_id;
                                $rootScope.userName = response.data.userDetails.name;

                                $rootScope.emailaddress = response.data.userDetails.email;
                                $rootScope.referralCode = response.data.userDetails.referral_code;
                                $rootScope.userLogin = true;
                                $window.location.href = "login-sucessfully";
                                save.setvar('userId', response.data.userDetails.user_id);
                                save.setvar('userName', response.data.userDetails.name);
                                save.setvar('referralCode', response.data.userDetails.referral_code);
                                save.setob('userDetails', response.data.userDetails);
                                save.setob('addressDetails', response.data.addressDetails);
                            }
                            else{
                                alert(response.data.message)
                            }
                            $scope.submitVerifyOtp = false;                        
                        });
                }
            }
        }

        /* Resend OTP Function */
        $scope.resendotp = function () {

            var path = $rootScope.baseApiURl + "login/requestotp";
            var data = "mobile=" + $rootScope.phonenumber + "&uuid=1234&login_type=WEB&uuid_value=1234";
            var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
           $http.post(path, data, config).then(function successCallback(response) {
                alert(response.data.message);
                if (response.data.status === "1") {
                    $scope.otp1 = "";
                    $scope.otp2 = "";
                    $scope.otp3 = "";
                    $scope.otp4 = "";
                    $scope.otp5 = "";
                } 

            }, function errorCallback(response) {
                console.log("Unable to perform Post request");
                return false;
            });

            $scope.otp1 = "";
            $scope.otp2 = "";
            $scope.otp3 = "";
            $scope.otp4 = "";
            $scope.otp5 = "";
            alert(response.data.message);
        }
    }
    

});


/* Login Sucessfully */

app.controller("loginSucessfullyCtrl", function ($scope, $window, $rootScope,save) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="",$rootScope.totalItems==0){
        $window.location.href = $rootScope.baseSiteUrl;
    }

    else{
        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "122001", referral_code: "OTgxMDkyOTU4Mg==", user_id: "1", deliveryDetails: {} };
        
        $rootScope.userId = save.getvar("userId");
        $rootScope.userName = save.getvar("userName");
        $rootScope.userDetails = save.getob("userDetails");
        $rootScope.addressDetails = save.getob("addressDetails");
        $rootScope.cityName = save.getvar("cityName");        
        $rootScope.services = save.getob("services");        
        $rootScope.data.itemList = save.getob("itemList");        
        $rootScope.data.paymentDetails.price = save.getvar("price");
        $rootScope.laundry_settings = save.getob("laundry_settings");  
        $rootScope.userExist = save.getvar("userExist");     
        if ($rootScope.userExist == "true") {
            $scope.loginSucessfully = function () {
                if($rootScope.addressDetails.HOME.pincode=="" && $rootScope.addressDetails.WORK.pincode=="" && $rootScope.addressDetails.OTHER.pincode=="") {
                    $window.location.href = "add-address";
                }
                else {
                    $window.location.href = "view-address";
                }
            }
        }
        else {
            $scope.loginSucessfully = function () {
                $window.location.href = "add-address";
            }
        }
    }

});

/* Login Include Controller */

app.controller("loginIncludeCtrl", function ($scope, $http, $rootScope, $window, save) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $scope.showErrorMsg = false;
    $scope.continueLoginClicked = false;    

    var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
    $scope.loginInclude = function () {
        
        var pnumber = $scope.phonenumber;
        if (pnumber == "" || pnumber.length > 10 || pnumber.length < 10 ) {
            alert("Please Enter 10 Digits Mobile Number!");
        }
        else {
            $scope.continueLoginClicked = true;
            
            $rootScope.phonenumber = pnumber;
            var data = "login_type=MOBILE&uuid_value=1234&login_value=" + $rootScope.phonenumber;
            var path = $rootScope.baseApiURl + "login/login";
            $http.post(path, data, config).
                then(function successCallback(response) {                    
                    $rootScope.result = response.data.message;
                    var message = "User Does Not Exist";
                    if (response.data.status === "0") {        // if user is not registered as of now. take him to registration page. 
                        $rootScope.loginUser = false;
                        $scope.name = null;
                        $scope.emailaddress = null;
                        $scope.continueLoginClicked = false;
                        $rootScope.loginRegister = true;                        
                    }
                    else {  
                        $rootScope.loginUser = false;
                        $scope.otp1 = "";
                        $scope.otp2 = "";
                        $scope.otp3 = "";
                        $scope.otp4 = "";
                        $scope.otp5 = "";
                        $scope.continueLoginClicked = false;
                        $rootScope.loginOtp = true;
                    }                    
                },function errorCallback(response){
                    $scope.continueLoginClicked = false;
                    alert(response.data.message);
                });  
        }
    }

    $rootScope.onSignIn = function (googleUser) {
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());
        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);

    }

    $rootScope.signOut = function () {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }

});

/* Login Details Include Controller */

app.controller("loginDetailsIncludeCtrl", function ($scope, $http, $rootScope, $window, WSCALLFACTORY, save) {     /*$rootScope.baseURl = save.getvar("baseURl");*/    
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $scope.registerClose = function(){
        $scope.name = null;
        $scope.emailaddress = null; 
        $scope.pnumber = "";
    }
    $scope.requestotp = function () {
        if($scope.name=="" || $scope.name == null || $scope.emailaddress=="" || $scope.emailaddress == null) {
            alert("Please provide valid Name & Address details.")
            return;
        }
        $rootScope.name = $scope.name;
        $rootScope.emailaddress = $scope.emailaddress;
        var path = $rootScope.baseApiURl + "login/requestotp";

        var data = "mobile=" + $rootScope.phonenumber + "&name=" + $rootScope.name + "&email=" + $rootScope.emailaddress + "&uuid=1234&login_type=MOBILE&uuid_value=1234&social_id=MOBILE&device_type=ios";
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
        $http.post(path, data, config).then(function sucessCallback(response) {
            if (response.data.status === "1") {  
                $scope.otp1 = "";
                $scope.otp2 = "";
                $scope.otp3 = "";
                $scope.otp4 = "";
                $scope.otp5 = "";              
                $rootScope.loginUser = false;
                $rootScope.loginRegister = false;
                $rootScope.loginOtp = true;   
                $scope.name = null;
                $scope.emailaddress = null;  
            }
            else {
                alert(response.data.message);
            }
        });
    }

});


/* Verify OTP Include Controller */

app.controller("verifyOtpIncludeCtrl", function ($scope, $http, save, $rootScope, $window) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $scope.submitVerifyOtp = false;

    if ($rootScope.userId == "") {
        $rootScope.userLogin = false;
    }
    else {
        $rootScope.userLogin = true;
    }

    $scope.otpClose = function(){
        $scope.otp1 = "";
        $scope.otp2 = "";
        $scope.otp3 = "";
        $scope.otp4 = "";
        $scope.otp5 = "";
        $scope.pnumber = "";
        $rootScope.loginUser = true;
        $rootScope.loginOtp = false;
        $scope.submitVerifyOtp = false;
    }

    /* Verify OTP Function */
    $scope.verifyOtpInclude = function () {
        var otp = ($scope.otp1) + $scope.otp2 + $scope.otp3 + $scope.otp4 + $scope.otp5;
        if (otp.length == 5) {
            $scope.submitVerifyOtp = true;
            if ($rootScope.result === "User Exists") {
                var path = $rootScope.baseApiURl + "login/verifyotp";
                var data = "login_type=MOBILE&uuid_value=1234&login_value=" + $rootScope.phonenumber + "&otp=" + otp;
                var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
                $http.post(path, data, config).
                    then(function successCallback(response) {
                        if(response.data.status==="0"){
                            alert(response.data.message);
                            $scope.otp1 = "";
                            $scope.otp2 = "";
                            $scope.otp3 = "";
                            $scope.otp4 = "";
                            $scope.otp5 = "";
                            $scope.submitVerifyOtp = false;
                            return;
                        }
                        if (response.data.message === "OTP Not Matched.Please click on resend OTP to generate new OTP.") {
                            alert("OTP Not Matched.Please click on resend OTP to generate new OTP.");
                            $scope.otp1 = "";
                            $scope.otp2 = "";
                            $scope.otp3 = "";
                            $scope.otp4 = "";
                            $scope.otp5 = "";
                            $scope.submitVerifyOtp = false;
                            return;
                        }
                        else { //(response.data.status==="1")
                            $rootScope.userLogin = true;
                            $rootScope.loginOtp = false;                            
                            $rootScope.loginSucess = true;
                            $rootScope.userId = response.data.userDetails.user_id;

                            $rootScope.userName = response.data.userDetails.name;
                            $rootScope.referralCode = response.data.userDetails.referral_code;
                            $rootScope.userDetails = response.data.userDetails;
                            $rootScope.addressDetails = response.data.addressDetails;                            
                            save.setvar('loginOtp', false);
                            save.setvar('userLogin', true);
                            save.setvar('loginSucess', true);
                            save.setvar('userId', response.data.userDetails.user_id);
                            save.setvar('userName', response.data.userDetails.name);
                            save.setvar('referralCode', response.data.userDetails.referral_code);
                            save.setob('userDetails', response.data.userDetails);
                            save.setob('addressDetails', response.data.addressDetails);
                            $scope.submitVerifyOtp = false;
                        }
                    });
            }
            else {
                // if(response.data.message == message){        call registration web service here. 
                var path =$rootScope.baseApiURl + "login/register";
                var data = "otp=" + otp + "&uuid_value=1234&name=" + $rootScope.name + "&email=" + $rootScope.emailaddress + "&mobile=" + $rootScope.phonenumber + "&login_type=MOBILE&social_id=MOBILE&device_type=ANDROID"; // postcode is entered static bcoz in registered api it required
                var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
                $http.post(path, data, config).
                    then(function successCallback(response) {
                        if (response.data.status === "1") {
                            $rootScope.userExist = true;                            
                            $rootScope.userLogin = true;
                            $rootScope.loginOtp = false;
                            $rootScope.loginSucess = true;
                            $rootScope.userDetails = response.data.userDetails;
                            $rootScope.addressDetails = response.data.addressDetails;
                            $rootScope.userId = response.data.userDetails.user_id;
                            $rootScope.userName = response.data.userDetails.name;
                            $rootScope.referralCode = response.data.userDetails.referral_code;
                            console.log("$rootScope.userDetails :", $rootScope.userDetails)
                            console.log("$rootScope.addressDetails :", $rootScope.addressDetails)
                            $rootScope.loginRegister = false;
                            $rootScope.loginOtp = false;                            
                            $rootScope.loginSucess = true;
                            save.setvar('userId', response.data.userDetails.user_id);
                            save.setvar('userName', response.data.userDetails.name);
                            save.setvar('referralCode', response.data.userDetails.referral_code);
                            save.setob('userDetails', response.data.userDetails);
                            save.setob('addressDetails', response.data.addressDetails);
                            $scope.submitVerifyOtp = false;
                        }
                        else {
                            $scope.submitVerifyOtp = false;
                            alert(response.data.message);
                        }
                    });

                //}
            }
        }
        else {
            //when user does not enter OTP;
            alert("Please Enter Five digit OTP");
        }

    }


    /* Resend OTP Include Function */
    $scope.resendOtpInclude = function () {
        var path = $rootScope.baseApiURl + "login/requestotp";
        var data = "mobile=" + $rootScope.phonenumber + "&name=" + $rootScope.name + "&email=" + $rootScope.emailaddress + "&uuid=1234&login_type=MOBILE&uuid_value=1234&social_id=MOBILE&device_type=WEB";
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
        $http.post(path, data, config).then(function sucessCallback(response) {
            alert(response.data.message);
            if (response.data.status === "1") {
                $scope.otp1 = "";
                $scope.otp2 = "";
                $scope.otp3 = "";
                $scope.otp4 = "";
                $scope.otp5 = "";
                $rootScope.loginUser = false;
                $rootScope.loginRegister = false;
                $rootScope.loginOtp = true;
            }            
        });
    }

});

/* Login Sucessfully Include */
app.controller("loginSucessIncludeCtrl", function ($scope, $window, $rootScope,save) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $scope.loginSucessClose = function () {
        $rootScope.popupLoginClicked = false;
        $rootScope.loginOtp = false;
        $rootScope.loginSucess = false;

        $rootScope.loginRegister = false;
        $scope.otp1 = "";
        $scope.otp2 = "";
        $scope.otp3 = "";
        $scope.otp4 = "";
        $scope.otp5 = "";
        var myModalBackdrop = angular.element(document.querySelector('.modal-backdrop.fade.in'));
        myModalBackdrop.removeClass('in modal-backdrop');
        var myModalOpen = angular.element(document.querySelector('body.modal-open')).removeClass('modal-open');
    }
});

/* View Address Controller */
app.controller("viewAddressCtrl", function ($scope, $rootScope, $http, $window, save) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    $rootScope.userName = save.getvar("userName");
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="" || $rootScope.totalItems==0 ||$rootScope.userName=="" || $rootScope.userName==undefined){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {
        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "", referral_code: "", user_id: "", deliveryDetails: {} };
        $rootScope.altNumber = save.getvar("altNumber");
        $rootScope.userId = save.getvar("userId");
        //$rootScope.userName = save.getvar("userName");
        $rootScope.referralCode = save.getvar("referralCode");
        $rootScope.userDetails = save.getob("userDetails");
        //$rootScope.addressDetails = save.getob("addressDetails");
        $rootScope.pincodeVal = save.getvar("pincodeVal");
        $rootScope.cityName = save.getvar("cityName");
        $rootScope.data.itemList = save.getob("itemList");    
        $rootScope.totalItems = save.getob("totalItems");
        $rootScope.services = save.getob("services");
        $rootScope.data.paymentDetails.price = save.getvar("price");

        var myModalBackdrop = angular.element(document.querySelector('.modal-backdrop.fade.in'));
        myModalBackdrop.removeClass('in modal-backdrop');
        $rootScope.selectAddressDetails = {
            "address_type": "",
            "address": "",
            "landmark": "",
            "pincode": "",
            "cgst": "",
            "sgst": "",
            "igst": ""
        }

        var path = $rootScope.baseApiURl + "profile/profile";
        var data = { user_id: $rootScope.userId };    
        var config = { headers: { 'Content-Type': 'application/json;charset=utf-8;', 'Authorization': 'Basic YWRtaW46MTIzNA==' } };

        $http.post(path, data, config).
            then(function successCallback(response) {
                $rootScope.selectedAddress = "";
                
                $rootScope.addressDetails = response.data.addressDetails;
                save.setob("addressDetails",response.data.addressDetails);
                if (response.data.addressDetails != undefined) {
                    var showaddressoff = '';
                    $rootScope.HOME = response.data.addressDetails.HOME;
                    $rootScope.WORK = response.data.addressDetails.WORK;
                    $rootScope.OTHER = response.data.addressDetails.OTHER;
                    
                    if ($rootScope.OTHER.landmark != "") {
                        showaddressoff = 'other';
                        $scope.showaddressoff = showaddressoff;
                        $rootScope.selectedAddress = showaddressoff;
                        $scope.viewadd = showaddressoff;
                    }
                    if ($rootScope.WORK.landmark != "") {
                        showaddressoff = 'work';
                        $scope.showaddressoff = showaddressoff;
                        $rootScope.selectedAddress = showaddressoff;
                        $scope.viewadd = showaddressoff;
                    }
                    if ($rootScope.HOME.landmark != "") {
                        showaddressoff = 'home';
                        $scope.showaddressoff = showaddressoff;
                        $rootScope.selectedAddress = showaddressoff;
                        $scope.viewadd = showaddressoff;
                    }                
                }
            });

        $scope.changeshowaddressoff = function (value) {
            $scope.showaddressoff = value;
            $rootScope.selectedAddress = value;
        }

        $scope.addnew = function () { location.href = "add-address"; }
        $scope.viewAddressDone = function () {
            $rootScope.altNumber = $scope.alt_number;
            // point7 starts
            save.setvar("altNumber", $rootScope.altNumber);
            save.setvar("selectedAddress",$rootScope.selectedAddress);
            // point7 ends
            if ($rootScope.selectedAddress == "") { // if user forgot to select address
                alert("Please Select Address for PickUp/Delivery.");
            }
            else {
                switch ($rootScope.selectedAddress) {  // switch case to set pickup/delivery address
                    case 'home':
                        $rootScope.selectAddressDetails.address_type = $rootScope.selectedAddress;
                        $rootScope.selectAddressDetails.address = $rootScope.HOME.address;
                        $rootScope.selectAddressDetails.landmark = $rootScope.HOME.landmark;
                        $rootScope.selectAddressDetails.pincode = $rootScope.HOME.pincode;
                        $rootScope.selectAddressDetails.cgst = $rootScope.HOME.cgst;
                        $rootScope.selectAddressDetails.sgst = $rootScope.HOME.sgst;
                        $rootScope.selectAddressDetails.igst = $rootScope.HOME.igst;
                        break;
                    case 'work':
                        $rootScope.selectAddressDetails.address_type = $rootScope.selectedAddress;
                        $rootScope.selectAddressDetails.address = $rootScope.WORK.address;
                        $rootScope.selectAddressDetails.landmark = $rootScope.WORK.landmark;
                        $rootScope.selectAddressDetails.pincode = $rootScope.WORK.pincode;
                        $rootScope.selectAddressDetails.cgst = $rootScope.WORK.cgst;
                        $rootScope.selectAddressDetails.sgst = $rootScope.WORK.sgst;
                        $rootScope.selectAddressDetails.igst = $rootScope.WORK.igst;
                        break;
                    case 'other':
                        $rootScope.selectAddressDetails.address_type = $rootScope.selectedAddress;
                        $rootScope.selectAddressDetails.address = $rootScope.OTHER.address;
                        $rootScope.selectAddressDetails.landmark = $rootScope.OTHER.landmark;
                        $rootScope.selectAddressDetails.pincode = $rootScope.OTHER.pincode;
                        $rootScope.selectAddressDetails.cgst = $rootScope.OTHER.cgst;
                        $rootScope.selectAddressDetails.sgst = $rootScope.OTHER.sgst;
                        $rootScope.selectAddressDetails.igst = $rootScope.OTHER.igst;
                        break;
                }
                save.setob("selectAddressDetails",$rootScope.selectAddressDetails);

                var data = "postcode=" + $rootScope.selectAddressDetails.pincode;
                config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'} };
                $http.post($rootScope.baseApiURl + "location/locationbypostcode", data, config).
                    then(function sucessCallback(response) {
                        if(response.data.status == "1") {            
                            $window.location.href = "date-time";
                        }
                        else{
                            alert("Laundry service is not available in " + $rootScope.selectAddressDetails.pincode + " postcode for Pickup/Delivery.");
                            return;
                        }
                    }, function errorCallback(response) {
                });
            }
        }
    }
});

/* Add Address Controller */
app.controller("addAddressCtrl", function ($scope, $rootScope, $http, $window, save) {
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    $rootScope.userName = save.getvar("userName");
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="" || $rootScope.totalItems==0 ||$rootScope.userName=="" || $rootScope.userName==undefined){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {

        var myModalBackdrop = angular.element(document.querySelector('.modal-backdrop.fade.in'));
        myModalBackdrop.removeClass('in modal-backdrop');

        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "", referral_code: "", user_id: "", deliveryDetails: {} };
        $rootScope.altNumber = save.getvar("altNumber");
        $rootScope.userId = save.getvar("userId");
        $rootScope.referralCode = save.getvar("referralCode");
        $rootScope.userDetails = save.getob("userDetails");
        $rootScope.addressDetails = save.getob("addressDetails");
        $rootScope.cityName = save.getvar("cityName");
        $rootScope.data.itemList = save.getob("itemList");
        $rootScope.services = save.getob("services");
        $rootScope.data.paymentDetails.price = save.getvar("price");
        $rootScope.HOME = $rootScope.addressDetails.HOME;
        $rootScope.OTHER = $rootScope.addressDetails.OTHER;
        $rootScope.WORK = $rootScope.addressDetails.WORK;

        var showaddressoff = 'home';
        $scope.showaddressoff = showaddressoff;

        $scope.home_address = $rootScope.HOME.address;
        $scope.home_landmark = $rootScope.HOME.landmark;
        $scope.home_pincode = $rootScope.HOME.pincode;

        $scope.work_address = $rootScope.WORK.address;
        $scope.work_landmark =$rootScope.WORK.landmark;
        $scope.work_pincode = $rootScope.WORK.pincode;

        $scope.other_address = $rootScope.OTHER.address;
        $scope.other_landmark = $rootScope.OTHER.landmark;
        $scope.other_pincode = $rootScope.OTHER.pincode;

        $scope.changeshowaddressoff = function (value) {
            this.showaddressoff = value;
        }

        $scope.changeText = function (type,value) { // start changeText function : update values in $scope.home_address on change 
            if(type=="HomeAddress") {
                $scope.home_address = value;
            }
            else if(type=="HomeLandmark") {
                $scope.home_landmark = value;
            }
            else if(type=="HomePincode") {
                $scope.home_pincode = value;
            }
            if(type=="WorkAddress") {
                $scope.work_address = value;
            }
            else if(type=="WorkLandmark") {
                $scope.work_landmark = value;
            }
            else if(type=="WorkPincode") {
                $scope.work_pincode = value;
            }
            if(type=="OtherAddress") {
                $scope.other_address = value;
            }
            else if(type=="OtherLandmark") {
                $scope.other_landmark = value;
                
            }
            else if(type=="OtherPincode") {
                $scope.other_pincode = value;             
            }
        }   // end changeText function

        $scope.save = function () {

            if($scope.home_pincode=="" && $scope.home_address!=""){  
                alert("Please Enter Pincode for Home.");
                return;
            }
            if($scope.home_address=="" && $scope.home_pincode!=""){  
                alert("Please Enter Address for Home.");
                return;
            }

            if($scope.work_pincode=="" && $scope.work_address!=""){  
                alert("Please Enter Pincode for Work.");
                return;
            }
            if($scope.work_address=="" && $scope.work_pincode!=""){  
                alert("Please Enter Address for Work.");
                return;
            }

            if($scope.other_pincode=="" && $scope.other_address!=""){  
                alert("Please Enter Pincode for Other.");
                return;
            }
            if($scope.other_address=="" && $scope.other_pincode!=""){  
                alert("Please Enter Address for Other.");
                return;
            }
            if($scope.work_pincode=="" && $scope.other_pincode=="" && $scope.home_pincode==""){  
                alert("Please Enter Address to Proceed.");
                return;
            }
            var config1 = { headers: { 'Content-Type': 'application/json', 'Authorization': 'Basic YWRtaW46MTIzNA==' } }

            var data1 = {
                "name": $rootScope.userDetails.name,
                "addressDetails": {
                    "HOME": { "pincode": $scope.home_pincode, "landmark": $scope.home_landmark, "address": $scope.home_address },
                    "WORK": { "pincode": $scope.work_pincode, "landmark": $scope.work_landmark, "address": $scope.work_address },
                    "OTHER": { "pincode": $scope.other_pincode, "landmark": $scope.other_landmark, "address": $scope.other_address }
                },
                "user_id": $rootScope.userId,
                "email": $rootScope.userDetails.email,
                "mobile": $rootScope.userDetails.mobile
            };
            $http.post($rootScope.baseApiURl + "profile/addressupdate", data1, config1).
                then(function successCallback(response) {
                    if(response.data.status == "1") {
                        $window.location.href = "view-address";
                    }
                    else {
                        alert(response.data.message);
                    }
                }, function errorCallback(response) {
            });
        }
    }
});



/*------------  dateTimeCtrl Start --------------*/
app.controller("dateTimeCtrl", function ($scope, $rootScope, $window, $http, matchMedia, save) { //dateTimeCtrl Start  
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl"); 
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    $rootScope.userName = save.getvar("userName");
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="" || $rootScope.totalItems==0 ||$rootScope.userName=="" || $rootScope.userName==undefined){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else{   
        $rootScope.selectedFinalDeliveryDate = "";
        $rootScope.selected_deliveryslot_from = "";
        $rootScope.selected_deliveryslot_to = "";

        $scope.selectedDateTab = 0;
        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "122001", referral_code: "OTgxMDkyOTU4Mg==", user_id: "1", deliveryDetails: {} };
        $rootScope.altNumber = save.getvar("altNumber")
        $rootScope.userId = save.getvar("userId");
        $rootScope.referralCode = save.getvar("referralCode");
        $rootScope.userDetails = save.getob("userDetails");
        $rootScope.addressDetails = save.getob("addressDetails");
        $rootScope.cityName = save.getvar("cityName");
        $rootScope.data.itemList = save.getob("itemList");
        $rootScope.services = save.getob("services");
        $rootScope.data.paymentDetails.price = save.getvar("price")
        $rootScope.HOME = $rootScope.addressDetails.HOME;
        $rootScope.OTHER = $rootScope.addressDetails.OTHER;
        $rootScope.WORK = $rootScope.addressDetails.WORK;
        $rootScope.deliveryType = save.getvar("deliveryType");
        $rootScope.laundry_settings = save.getob("laundry_settings");
        $rootScope.selectAddressDetails = save.getob("selectAddressDetails");
        $rootScope.cartPrice=save.getvar("cartPrice");
        $rootScope.expressDelivery = save.getvar("expressDelivery");
        $rootScope.normalDelivery = save.getvar("normalDelivery");
        $rootScope.express_percent = save.getvar("express_percent");
        $rootScope.calc_delivery_amt = save.getvar("calc_delivery_amt");
        $rootScope.delivery_amt = save.getvar("delivery_amt");
        $rootScope.netTotal=save.getvar("netTotal");
        $rootScope.cartPrice=save.getvar("cartPrice");
        $rootScope.discount=save.getvar("discount");
        $rootScope.total=save.getvar("total");
        $rootScope.grandTotal=save.getvar("grandTotal");
        $rootScope.amount_payable=save.getvar("amount_payable");
        $rootScope.credit_used=save.getvar("credit_used");

        $rootScope.$on("pickup_ev",function(){
            $rootScope.selected_deliveryslot_from=0
            $rootScope.selected_deliveryslot_to=0
            $rootScope.selectedFinalDeliveryDate=0
        })

        $scope.desktopView = true;
        $scope.tablet = false;
        $scope.mobileView = false;

        $scope.desktop = matchMedia.isDesktop();
        $scope.tablet = matchMedia.isTablet();
        $scope.mobile = matchMedia.isPhone();

        if($scope.desktop==true){
            $scope.desktopView = true;
            $scope.tablet = false;
            $scope.mobileView = false;
        }

        if($scope.tablet || $scope.mobile==true){
            $scope.mobileView = true;
            $scope.tablet = true;
            $scope.desktopView = false;
        }

        $scope.checkSlotEnable = function(checkArray,slot_to){    // start of checkSlotEnable function
            $scope.dateEnableCheck = [];
            $scope.dateEnableCheck = checkArray[0];  

            for (var k = 0; k < checkArray.length; k++) { // adding EnableActive field to every Slot of array
                $scope.temp = checkArray[k];
                if ($scope.temp.MORNING) { 
                    for (var l = 0; l < $scope.temp.MORNING.length; l++) {
                        $scope.temp.MORNING[l].EnableActive = "1";
                    }
                }
                if ($scope.temp.AFTERNOON) {
                    for (var l = 0; l < $scope.temp.AFTERNOON.length; l++) {
                        $scope.temp.AFTERNOON[l].EnableActive = "1";
                    }
                }
                if ($scope.temp.EVENING) {
                    for (var l = 0; l < $scope.temp.EVENING.length; l++) {
                        $scope.temp.EVENING[l].EnableActive = "1";
                    }
                }
            }   // end adding EnableActive field to every Slot of array

            
            $scope.morArray = $scope.dateEnableCheck.MORNING;
            $scope.noonArray = $scope.dateEnableCheck.AFTERNOON;
            $scope.eveArray = $scope.dateEnableCheck.EVENING;        
            for (var j = 0; j < $scope.morArray.length; j++) {
                if (parseInt(slot_to) > parseInt($scope.morArray[j].service_from)) {                
                    $scope.morArray[j].EnableActive = '0';
                }
                else {
                    break;
                }
            }
            if (j == $scope.morArray.length) {
                for (var j = 0; j < $scope.noonArray.length; j++) {
                    if (parseInt(slot_to) > parseInt($scope.noonArray[j].service_from)) {
                        $scope.noonArray[j].EnableActive = '0';
                    }
                    else {
                        break;
                    }

                }
                if (j == $scope.noonArray.length) {
                    for (var j = 0; j < $scope.eveArray.length; j++) {
                        if (parseInt(slot_to) > parseInt($scope.eveArray[j].service_from)) {
                            $scope.eveArray[j].EnableActive = '0';
                        }
                        else {
                            break;
                        }
                    }
                }
            }
            return checkArray;
        }   // end of checkSlotEnable function
       
        var data= {"postcode": $rootScope.pincodeVal};
        path_date = $rootScope.baseApiURl + "slot/slot";
        config1 = { headers: { 'Content-Type': 'application/json;charset=utf-8', 'Authorization': 'Basic YWRtaW46MTIzNA==' } }

        $http.post(path_date, data, config1).then(function (response) {
                if (response.data.status == "1") {
                    var pickupSlot = [];
                    
                    $scope.currentHour = new Date().getHours();
                    $scope.currentHour += (new Date().getMinutes()>0)?1:0;
                    $scope.pickupSlot = response.data.PICK_UP;

                    if($rootScope.deliveryType=='normal'){
                        $scope.pickupSlotValue = parseInt($scope.currentHour) + parseInt($rootScope.laundry_settings.normal_hours_gap);
                    }
                    else {  
                        $scope.pickupSlotValue = parseInt($scope.currentHour) + parseInt($rootScope.laundry_settings.express_hours_gap);
                    }
                    
                    $scope.pickupSlot = $scope.checkSlotEnable($scope.pickupSlot,$scope.pickupSlotValue);
                    $rootScope.selected_pickup_date = $scope.pickupSlot[0].date;       // by Default current date is set as pickup date
                    $scope.deliverySlot = response.data.DELIVERY;
                    $scope.laundry_settings = response.data.laundry_settings;
                    $scope.express_hours = $rootScope.laundry_settings.express_hours;
                    $scope.normal_hours = $rootScope.laundry_settings.normal_hours;
                    $scope.currentDate = $scope.formatDate(new Date());
                   
                    $scope.pickup_date_index = 0;       // by Default current date is set as pickup date                    

                    $scope.showDelivery = false;
                    $scope.selectedDateTabDelivery = '0';
                    if (!save.getvar("deliveryType")) {
                        $rootScope.deliveryType = 'normal';
                    }
                    save.setvar("deliveryType", $rootScope.deliveryType)
                    /*Code to calculate days gaps for delivery */
                    $scope.normal_delivery_gap = $scope.normal_hours / 24;  // converting normal_hours into days for delivery
                    $scope.express_delivery_gap = $scope.express_hours / 24; // converting express_hours into days for delivery
                }
            });

        $scope.getTimeSlots=function(){
            $window.location.href="date-time";
            $http.post(path_date, data, config1).then(function (response) {
                if (response.data.status == "1") {
                    var pickupSlot = [];                    
                    $scope.currentHour = new Date().getHours();
                    $scope.currentHour += (new Date().getMinutes()>0)?1:0;
                    $scope.pickupSlot = response.data.PICK_UP;
                    if($rootScope.deliveryType=='normal'){
                        $scope.pickupSlotValue = parseInt($scope.currentHour) + parseInt($rootScope.laundry_settings.normal_hours_gap);
                    }
                    else { 
                        $scope.pickupSlotValue = parseInt($scope.currentHour) + parseInt($rootScope.laundry_settings.express_hours_gap);
                    }
                    $scope.pickupSlot = $scope.checkSlotEnable($scope.pickupSlot,$scope.pickupSlotValue);
                    $rootScope.selected_pickup_date = $scope.pickupSlot[0].date;       // by Default current date is set as pickup date
                    $scope.deliverySlot = response.data.DELIVERY;
                    $scope.laundry_settings = response.data.laundry_settings;
                    $scope.express_hours = $rootScope.laundry_settings.express_hours;
                    $scope.normal_hours = $rootScope.laundry_settings.normal_hours;
                    $scope.currentDate = $scope.formatDate(new Date());
                   
                    $scope.pickup_date_index = 0;       // by Default current date is set as pickup date                    

                    $scope.showDelivery = false;
                    $scope.selectedDateTabDelivery = '0';
                    if (!save.getvar("deliveryType")) {
                        $rootScope.deliveryType = 'normal';
                    }
                    save.setvar("deliveryType", $rootScope.deliveryType)
                    /*Code to calculate days gaps for delivery */
                    $scope.normal_delivery_gap = $scope.normal_hours / 24;  // converting normal_hours into days for delivery
                    $scope.express_delivery_gap = $scope.express_hours / 24; // converting express_hours into days for delivery
                }
            });
        }

        $scope.formatDate = function (date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();        
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
    
            return [year, month, day].join('-');
        }

        $scope.selectDateTab = function(index, da) {
            $scope.showDelivery = false;
            $scope.selectedDateTab = index;
            $scope.pickup_date_index = $scope.selectedDateTab;
            
            var date = new Date(da);
            var pdate = new Date(da);
            $rootScope.selected_pickup_date = $scope.formatDate(pdate)  // Selected date is set as pickup date
            $scope.selected_pickupslot_from = '';
            $scope.selected_pickupslot_to = '';
        }


        $scope.selectDateTabDelivery = function (index, da) {
            $scope.selectedDateTabDelivery = index;
            var date = new Date(da);
            $rootScope.selectedFinalDeliveryDate = $scope.formatDate(date);

        }
        $scope.setDeliveryType = function (value) {
            $rootScope.deliveryType = value;
            save.setvar("deliveryType", $rootScope.deliveryType);
            console.log($rootScope.deliveryType)
        }

        $scope.slotSelected = function (ev, slot_from, slot_to, da, index) {
            $("a").removeClass("date-select");
            $(ev.target).addClass("date-select");
            
        
            if($rootScope.selected_pickup_date && $rootScope.selected_pickupslot_from && $rootScope.selected_pickupslot_from){
                $rootScope.$broadcast("pickup_ev")
            }   
            
            if($rootScope.selected_deliveryslot_from!="" || $rootScope.selected_deliveryslot_to!="" || $rootScope.selectedFinalDeliveryDate!=""){
                $rootScope.selected_deliveryslot_from = "";
                $rootScope.selected_deliveryslot_to = "";
                $rootScope.selectedFinalDeliveryDate = "";
            }   

            $rootScope.selected_pickupslot_from = slot_from;
            $rootScope.selected_pickupslot_to = slot_to;           
            
            var date = new Date(da);
            if ($rootScope.deliveryType == 'normal') {      
                date.setDate(date.getDate() + $scope.normal_delivery_gap);
                $rootScope.selected_delivery_date = $scope.formatDate(date);
            }
            else {
                date.setDate(date.getDate() + $scope.express_delivery_gap)
                $rootScope.selected_delivery_date = $scope.formatDate(date);      
            }  
            $scope.delivery_dates = $scope.deliverySlot;
            
            for(i=0 ; i < $scope.deliverySlot.length; i++){
                if($scope.deliverySlot[i].date <= $rootScope.selected_delivery_date){                    
                    $scope.delivery_dates =$scope.deliverySlot.slice(i);
                }
            }            
            
            $scope.delEnableCheck = [];
            $scope.delEnableCheck = $scope.delivery_dates[0];  
            
            for (var k = 0; k < $scope.delivery_dates.length; k++) { // adding EnableActive field to every Slot of array
            $scope.temp = $scope.delivery_dates[k];
            if ($scope.temp.MORNING) { 
                for (var l = 0; l < $scope.temp.MORNING.length; l++) {
                    $scope.temp.MORNING[l].EnableActive = "1";
                }
            }
            if ($scope.temp.AFTERNOON) {
                for (var l = 0; l < $scope.temp.AFTERNOON.length; l++) {
                    $scope.temp.AFTERNOON[l].EnableActive = "1";
                }
            }
            if ($scope.temp.EVENING) {
                for (var l = 0; l < $scope.temp.EVENING.length; l++) {
                    $scope.temp.EVENING[l].EnableActive = "1";
                }
            }
        }   // end adding EnableActive field to every Slot of array
        
        $scope.morArray = $scope.delEnableCheck.MORNING;
        $scope.noonArray = $scope.delEnableCheck.AFTERNOON;
        $scope.eveArray = $scope.delEnableCheck.EVENING;   
        var canShift=true;
        
        var j = 0;     
        for (; j < $scope.morArray.length; j++) {
            if (parseInt(slot_to) > parseInt($scope.morArray[j].service_to)) {                
                $scope.morArray[j].EnableActive = '0';
                canShift=true;
            }
            else {
                canShift=false;
                break;
            }
        }
        if (canShift) {
            j = 0;   
            for (; j < $scope.noonArray.length; j++) {
                if (parseInt(slot_to) > parseInt($scope.noonArray[j].service_to)) {
                    $scope.noonArray[j].EnableActive = '0';
                canShift=true;
                }
                else {
                canShift=false;
                    break;
                }        
            }
        if (canShift) {
            j = 0;   
                for (; j < $scope.eveArray.length; j++) {
                    if (parseInt(slot_to) > parseInt($scope.eveArray[j].service_to)) {
                        $scope.eveArray[j].EnableActive = '0';
                canShift=true;
                    }
                    else {
                canShift=false;
                        break;
                    }
                }
            }
        }
        
        $rootScope.selectedFinalDeliveryDate =  $scope.delivery_dates[0].date;
        $scope.showDelivery = true;
        //console.log("********* $rootScope.selected_pickup_date ******", $rootScope.selected_pickup_date)
        }       // slotSelected End


        $scope.deliverySlotSelected = function (ev, slot_from, slot_to) {
            $(".del-slider a").removeClass("date-select");
            $(ev.target).addClass("date-select");
            $rootScope.selected_deliveryslot_from = slot_from;
            $rootScope.selected_deliveryslot_to = slot_to;
        }


        $scope.submited = function () {
            if(!$rootScope.selected_pickup_date || $rootScope.selected_pickupslot_from=="" || $rootScope.selected_pickupslot_to=="" || !$rootScope.selectedFinalDeliveryDate || $rootScope.selected_deliveryslot_from=="" || $rootScope.selected_deliveryslot_to==""){
                alert("Please Select Delivery Date and Time Slots");
                return;
            }

            save.setvar("selectedFinalDeliveryDate", $rootScope.selectedFinalDeliveryDate);
            save.setvar("selected_deliveryslot_from", $rootScope.selected_deliveryslot_from);
            save.setvar("selected_deliveryslot_to", $rootScope.selected_deliveryslot_to);
            save.setvar("selected_pickup_date", $rootScope.selected_pickup_date);
            save.setvar("selected_pickupslot_from", $rootScope.selected_pickupslot_from);
            save.setvar("selected_pickupslot_to", $rootScope.selected_pickupslot_to); 

            $rootScope.cgstExist = false;
            $rootScope.sgstExist = false;
            $rootScope.igstExist = false;
            $rootScope.creditExist = false;
            $rootScope.normalDelivery = true;
            $rootScope.discountExist = false;
            $rootScope.expressDelivery = false;
            $rootScope.cgst = 0;
            $rootScope.sgst = 0;
            $rootScope.igst = 0;
            $rootScope.discount = 0;
            $rootScope.express_percent = 0;
            $rootScope.cartPrice = parseFloat($rootScope.data.paymentDetails.price);

            $rootScope.calcPercentage = function (total, per) {    // function to calculate %
                $scope.calcPer = parseFloat(total) * parseFloat(per) / 100;
                $scope.calcPer = ($scope.calcPer).toFixed(2);
                return $scope.calcPer;
            }

                

            // point8 starts        // 
            save.setvar("igst", $rootScope.igst);
            save.setvar("igstExist", $rootScope.igstExist);
            save.setvar("cgstExist", $rootScope.cgstExist);
            save.setvar("cgst", $rootScope.cgst);
            save.setvar("sgst", $rootScope.sgst);
            save.setvar("sgstExist", $rootScope.sgstExist);
            save.setvar("expressDelivery", $rootScope.expressDelivery);
            save.setvar("normalDelivery", $rootScope.normalDelivery);
            save.setvar("express_percent", $rootScope.express_percent);
            save.setvar("calc_delivery_amt", $rootScope.calc_delivery_amt);
            save.setvar("delivery_amt", $rootScope.delivery_amt);
            save.setvar("creditExist",$rootScope.creditExist);
            save.setvar("selectedFinalDeliveryDate", $rootScope.selectedFinalDeliveryDate);
            save.setvar("cartPrice",$rootScope.cartPrice);

            // point8 ends      // 
            
            save.setvar("netTotal",$rootScope.netTotal);
            save.setvar("cartPrice",$rootScope.cartPrice);
            save.setvar("discount",$rootScope.discount);
            save.setvar("total",$rootScope.total);
            save.setvar("grandTotal",$rootScope.grandTotal);
            save.setvar("amount_payable",$rootScope.amount_payable);
            save.setvar("credit_used",$rootScope.credit_used);
            $window.location.href = "payment-option";
        }
    }    

});  //dateTimeCtrl End

app.controller("placeOrderCtrl", function ($scope, $http, $window, $rootScope, save) {   //placeOrderCtrl Start
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.pincodeVal = save.getvar("pincodeVal");
    $rootScope.totalItems = save.getvar("totalItems");
    $rootScope.userName = save.getvar("userName");
    if($rootScope.pincodeVal==undefined || $rootScope.pincodeVal=="" || $rootScope.totalItems==0 ||$rootScope.userName=="" || $rootScope.userName==undefined){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    
    else {
        $rootScope.data = { paymentDetails: { price: 0 }, itemList: [], serviceComment: [], postcode: "122001", referral_code: "OTgxMDkyOTU4Mg==", user_id: "1", deliveryDetails: {} };
        $rootScope.altNumber = save.getvar("altNumber")
        $rootScope.userId = save.getvar("userId");
        $rootScope.userName = save.getvar("userName");
        $rootScope.referralCode = save.getvar("referralCode");
        $rootScope.userDetails = save.getob("userDetails");
        $rootScope.addressDetails = save.getob("addressDetails");
        $rootScope.pincodeVal = save.getvar("pincodeVal");
        $rootScope.cityName = save.getvar("cityName");
        $rootScope.data.itemList = save.getob("itemList");
        $rootScope.totalItems = save.getob("totalItems");
        $rootScope.services = save.getob("services");
        $rootScope.data.paymentDetails.price = save.getvar("price");
        $rootScope.HOME = $rootScope.addressDetails.HOME;
        $rootScope.OTHER = $rootScope.addressDetails.OTHER;
        $rootScope.WORK = $rootScope.addressDetails.WORK;
        $rootScope.deliveryType = save.getvar("deliveryType");
        $rootScope.igst = save.getvar("igst");
        $rootScope.igstExist = save.getvar("igstExist");
        $rootScope.cgstExist = save.getvar("cgstExist");
        $rootScope.cgst = save.getvar("cgst");
        $rootScope.sgst = save.getvar("sgst");
        $rootScope.sgstExist = save.getvar("sgstExist");
        $rootScope.creditExist = save.getvar("creditExist");
        $rootScope.expressDelivery = save.getvar("expressDelivery");
        $rootScope.normalDelivery = save.getvar("normalDelivery");
        $rootScope.express_percent = save.getvar("express_percent");
        $rootScope.calc_delivery_amt = save.getvar("calc_delivery_amt");
        $rootScope.delivery_amt = save.getvar("delivery_amt");
        $rootScope.laundry_settings = save.getob("laundry_settings");
        $rootScope.amount_payable=save.getvar("amount_payable");
        $rootScope.netTotal=save.getvar("netTotal");
        $rootScope.cartPrice=save.getvar("cartPrice");
        $rootScope.discount=save.getvar("discount");
        $rootScope.total=save.getvar("total");
        $rootScope.grandTotal=save.getvar("grandTotal");
        $rootScope.amount_payable=save.getvar("amount_payable");
        $rootScope.credit_used=save.getvar("credit_used");
        $rootScope.selectAddressDetails = save.getob("selectAddressDetails");
        $rootScope.selectedFinalDeliveryDate = save.getvar("selectedFinalDeliveryDate");
        $rootScope.selected_deliveryslot_from = save.getvar("selected_deliveryslot_from");
        $rootScope.selected_deliveryslot_to = save.getvar("selected_deliveryslot_to");
        $rootScope.selected_pickup_date = save.getvar("selected_pickup_date");
        $rootScope.selected_pickupslot_from = save.getvar("selected_pickupslot_from");
        $rootScope.selected_pickupslot_to = save.getvar("selected_pickupslot_to");        

        // point10 ends

        $scope.placeOrderClicked = false;   // variable used to set Loader vissibility
        $rootScope.inputread = false;
        $rootScope.paymentMode = "COD";
        $scope.discountBtnText = "Apply"; // discount button text  which change after discount to Cancel


        $rootScope.amountPayable = function () {  //amountPayable function Start
            $rootScope.netTotal = $rootScope.cartPrice - $rootScope.discount;  // calculating Net Total after discount
            // calculate delivery charges start
            if (parseFloat($rootScope.cartPrice) >= parseFloat($rootScope.laundry_settings.min_cart_amount)) {  // cart value is more than mini_cart_value so delivery is free
                $rootScope.delivery_amt = "Free";
                $rootScope.calc_delivery_amt = "0.00";    
            }
            else {    // cart value is less than mini_cart_value so delivery is chargeable
                $rootScope.delivery_amt = $rootScope.laundry_settings.delivery_amt;
                $rootScope.calc_delivery_amt = $rootScope.laundry_settings.delivery_amt;
            }
            if ($rootScope.deliveryType == "express") {
                $rootScope.express_percent = $rootScope.calcPercentage($rootScope.data.paymentDetails.price, $rootScope.laundry_settings.express_percent);
                $rootScope.expressDelivery = true;
                $rootScope.normalDelivery = false;
                $rootScope.calc_delivery_amt = parseFloat($rootScope.calc_delivery_amt) + $rootScope.express_percent;
                $rootScope.calc_delivery_amt = ($rootScope.calc_delivery_amt).toFixed(2);
            }
            else {
                $rootScope.expressDelivery = false;
                $rootScope.normalDelivery = true;
            }
            // calculate delivery charges end

            $rootScope.total = $rootScope.netTotal + parseFloat($rootScope.calc_delivery_amt); // sum of Net Total + delivery_amt
            
            // calculate Taxes start
            if ($rootScope.selectAddressDetails.cgst > 0) {
                $rootScope.cgst = $rootScope.calcPercentage($rootScope.total, $rootScope.selectAddressDetails.cgst);
                $rootScope.cgstExist = true;
                $rootScope.cgst = ($rootScope.cgst).toFixed(2);
            }
            if ($rootScope.selectAddressDetails.sgst > 0) {
                $rootScope.sgst = $rootScope.calcPercentage($rootScope.total, $rootScope.selectAddressDetails.sgst);
                $rootScope.sgstExist = true;
                $rootScope.sgst = ($rootScope.sgst).toFixed(2);
            }
            if ($rootScope.selectAddressDetails.igst > 0) {
                $rootScope.igst = $rootScope.calcPercentage($rootScope.total, $rootScope.selectAddressDetails.igst);
                $rootScope.igstExist = true;
                $rootScope.igst = ($rootScope.igst).toFixed(2);
            }
            // calculate Taxes end

            $rootScope.grandTotal = parseFloat($rootScope.total) + parseFloat($rootScope.cgst) + parseFloat($rootScope.sgst) + parseFloat($rootScope.igst);
            $rootScope.grandTotal = Math.round($rootScope.grandTotal * 100) / 100;
            $rootScope.creditCheck();
            $rootScope.amount_payable = parseFloat($rootScope.grandTotal) - parseFloat($rootScope.credit_used);
            $rootScope.amount_payable = Math.round($rootScope.amount_payable * 100) / 100;
        }  //amountPayable function end     

        $rootScope.calcPercentage = function (total, per) {    // function to calculate %
            $scope.calcPer = parseFloat(total) * parseFloat(per) / 100;
            $scope.calcPer = Math.round($scope.calcPer * 100) / 100;
            return $scope.calcPer;
        }            

        $rootScope.creditCheck = function () {   //creditcheck function Start
            if ($scope.chk_credit) {
                var data_credit = "user_id=" + $rootScope.userDetails.user_id;
                config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
                $http.post($rootScope.baseApiURl + "Referral/creditCheck", data_credit, config).then(function sucessCallback(response) {
                    
                    $rootScope.credit_points = parseFloat(response.data.credit_points);                    
                    if ($rootScope.credit_points > 0) {    // perform function when user has credit_points more than 0
                        $scope.Credit_max_percent = parseFloat(response.data.credit_max_percent);
                        $rootScope.credit_calc = $rootScope.calcPercentage($rootScope.grandTotal, $scope.Credit_max_percent);
                        $rootScope.creditExist = true;
                        if ($scope.credit_calc > $rootScope.credit_points) {  // check user has more/less credit points in his account as compare to calculated credit points
                            $rootScope.credit_used = Math.round($rootScope.credit_points * 100) / 100;
                        }
                        else {
                            $rootScope.credit_used = $scope.credit_calc;
                        }                        
                        $rootScope.amount_payable = parseFloat($rootScope.grandTotal) - parseFloat($rootScope.credit_used);
                        $rootScope.amount_payable = Math.round($rootScope.amount_payable * 100) / 100;
                    }
                    else{
                        alert(response.data.message);
                        $scope.chk_credit = !$scope.chk_credit;
                    }

                }, function errorCallback(response) {
                    alert(response.data.message);
                });
            }
            else {
                $rootScope.creditExist = false;
                $rootScope.credit_used = 0;
                $rootScope.amount_payable = $rootScope.grandTotal;
            }
        }  //creditcheck function End

        $rootScope.amountPayable();        

        $rootScope.couponApply = function () {  //couponApply function Start
            if (!$scope.txt_coupon || $scope.txt_coupon == null ) {  // checking coupon textfeild has value or not  // $scope.discountBtnText == "Cancel"
                $rootScope.discount = 0;
                $scope.discountBtnText = "Apply";
                $rootScope.discountExist = false;
                $rootScope.coupon_type = ""; 
                $rootScope.discount_value = "";
                $rootScope.discount_upto = "";      
                $scope.txt_coupon = "";    
                $rootScope.inputread = false;  
            }
            else {
                var data = "user_id=" + $rootScope.userDetails.user_id + "&postcode=" + $rootScope.pincodeVal + "&referral_code=" + $scope.txt_coupon + "&cart_amount=" + $rootScope.data.paymentDetails.price;
               config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
                path = $rootScope.baseApiURl + "referral/referralCheck";
                $http.post(path, data, config).then(function sucessCallback(response) {
                    if(response.data.status == "0") {
                        alert(response.data.message);
                        $rootScope.discount = 0;
                        $scope.discountBtnText = "Apply";
                        $rootScope.discountExist = false;
                        $rootScope.coupon_type = ""; 
                        $rootScope.discount_value = "";
                        $rootScope.discount_upto = "";    
                        $scope.txt_coupon = null;  
                        $rootScope.inputread = false;   
                    }
                    else {
                        var discount_mini_cart = response.data.min_cart_value;                        
                        $rootScope.coupon_type = response.data.coupon_type;
                        $rootScope.discount_upto = response.data.discount_upto;
                        $rootScope.discount_value = response.data.discount_value;
                        if (parseFloat($rootScope.cartPrice) >= parseFloat(discount_mini_cart)) {  // checking Cart value is equal or greater than min_cart_value.
                            if(response.data.coupon_type === "PERCENTAGE") {
                                $rootScope.discount = $rootScope.calcPercentage($rootScope.cartPrice, response.data.discount_value);                                
                                if (parseFloat($rootScope.discount) > parseFloat($rootScope.discount_upto)) {
                                    $rootScope.discount = $rootScope.discount_upto;
                                }
                            }
                            if(response.data.coupon_type === "FLAT") {
                                $rootScope.discount = response.data.discount_value;
                            }
                            if(response.data.coupon_type == "") {
                                $rootScope.discount = response.data.credit_points;   
                                $rootScope.discount_value = response.data.credit_points;    
                            }
                            $rootScope.discountExist = true;
                            $rootScope.inputread = true;
                            //$scope.discountBtnText = "Cancel"; // uncomment when cancell functionality add
                        }
                        else {
                            alert("To get discount Cart Value should be greater than" + response.data.min_cart_value + ".");
                        }
                        $rootScope.amountPayable();
                    }
                },
                function errorCallback(response) {
                    alert(response.data.message);
                });
            }
        }  //couponApply function end

       

        $scope.setPaymentMode = function (value) {
            $rootScope.paymentMode = value;
        }

        $scope.placeorder = function () {
            $scope.placeOrderClicked = true;
            if($scope.txt_coupon == null){
                $scope.txt_coupon = "";   
            }
            if($rootScope.coupon_type == null){
                $rootScope.coupon_type = "";   
            }
            $rootScope.discount = $rootScope.discount.toString();
            config1 = { headers: { 'Content-Type': 'application/json;charset=utf-8', 'Authorization': 'Basic YWRtaW46MTIzNA==' } }
            var data1 = {
                "paymentDetails":
                    {
                        "coupon_value": $rootScope.discount_value,
                        "SGST": $rootScope.selectAddressDetails.sgst,
                        "delivery": $rootScope.delivery_amt,
                        "credit": $rootScope.credit_used,
                        "coupon_type": $rootScope.coupon_type,
                        "discount": $rootScope.discount,
                        "IGST": $rootScope.selectAddressDetails.igst,
                        "express_delivery": $rootScope.express_percent,
                        "price": $rootScope.cartPrice,
                        "CGST": $rootScope.selectAddressDetails.cgst,
                        "min_cart_value": $rootScope.laundry_settings.min_cart_amount,
                        "amtPayable": $rootScope.amount_payable,
                        "couponCode": $scope.txt_coupon,
                        "discount_upto": $rootScope.discount_upto,
                        "paymentMode": $rootScope.paymentMode
                    },
                "itemList": $rootScope.data.itemList,
                "serviceComment": [],
                "postcode": $rootScope.pincodeVal,
                "order_timestamp": "",
                "user_id": $rootScope.userDetails.user_id,
                "deliveryDetails": {
                    "deliveryLandmark": $rootScope.selectAddressDetails.landmark,
                    "deliveryPincode": $rootScope.selectAddressDetails.pincode,
                    "deliverToSlot": $rootScope.selected_deliveryslot_to,
                    "pickupLandmark": $rootScope.selectAddressDetails.landmark,
                    "pickupAddress": $rootScope.selectAddressDetails.address,
                    "deliverFromSlot": $rootScope.selected_deliveryslot_from,
                    "alternateNumber": parseInt($rootScope.altNumber),
                    "pickupPincode": $rootScope.selectAddressDetails.pincode,
                    "deliveryMode": $rootScope.deliveryType,
                    "pickupTime": $rootScope.selected_pickup_date,
                    "deliveryTime": $rootScope.selectedFinalDeliveryDate,
                    "deliveryAddress": $rootScope.selectAddressDetails.address,
                    "pickupFromSlot": $rootScope.selected_pickupslot_from,
                    "pickupToSlot": $rootScope.selected_pickupslot_to
                }
            }    
            $http.post($rootScope.baseApiURl + "order/order", JSON.stringify(data1), config1).
                then(function successCallback(response) {
                    if (response.data.status == "1") {                        
                        $scope.placeOrderClicked = false;
                        console.log(response.data)
                        //$scope.placedOrderId = response.data.orderId;
                        save.setvar("placedOrderId",response.data.orderId)
                        $window.location.href = "thank-you";
                    }
                    else {
                        $scope.placeOrderClicked = false;
                        alert(response.data.message);
                    }

                }, function errorCallback(response) {
                    alert("Unable to perform  order place order");
            });

        }
        save.setvar("netTotal",$rootScope.netTotal);
        save.setvar("cartPrice",$rootScope.cartPrice);
        save.setvar("total",$rootScope.total) ;
        save.setvar("grandTotal",$rootScope.grandTotal);
        save.setvar("amount_payable",$rootScope.amount_payable);
    }
});  //placeOrderCtrl End 


app.controller("myOrderCtrl", function ($scope, $http, $window, $rootScope,save,orderDetails) {   //myOrderCtrl Start   
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $scope.hasOrder = true;
    $rootScope.userId = save.getvar("userId");
    $rootScope.userName = save.getvar("userName");
    if($rootScope.userId==undefined || $rootScope.userId=="" || $rootScope.userName==undefined){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {
        $rootScope.referralCode = save.getvar("referralCode");
        $rootScope.userDetails = save.getob("userDetails");
        $rootScope.addressDetails = save.getob("addressDetails");

        $scope.rateSelected = 0;   // default value of rate Selected
        $scope.rateOrderId = "";    // default value of rate orderId   
        $scope.rateOrderClicked = false;    // default value of rate order screen set false to hide the screen
        $scope.rating = 5;        

        var data = "user_id=" + $rootScope.userDetails.user_id + "&recent=0";
        var path1 = $rootScope.baseApiURl + "order/orderdetails";
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
        $http.post(path1, data, config).then(function sucessCallback(response) {
            $scope.myorders = response.data.data;
            if($scope.myorders.length==0){
                $scope.hasOrder = false;
            }
            else{
                $scope.hasOrder = true;
            }
        }, function errorCallback(response) {
            console.log("Order details received");
        });
        

        var data2 = "user_id=" + $rootScope.userDetails.user_id + "&recent=1";
        $http.post(path1, data2, config).then(function sucessCallback(response) {
            var orderId = response.data.data[0].order_id;
            //console.log("orderId--",orderId);
            $scope.singleOrderDetail(orderId, $rootScope.userId);
        }, 
        function errorCallback(response) {
        });


        $scope.orderTrack = function (orderId, userID) { // function implemented on status to trackOrder
            $rootScope.statusClick = true;            
            $rootScope.trackOrderId = orderId;
            $rootScope.trackUserId = userID;        
            $window.location.href = "trackorder";
            save.setvar("statusClick",$rootScope.statusClick);
            save.setvar("trackOrderId",$rootScope.trackOrderId);
        }

        $scope.singleOrderDetail = function (orderId, userID) { //  function applied on View detail
            var data1 = "user_id=" + userID + "&order_id=" + orderId;
            $http.post($rootScope.baseApiURl + "order/orderitemdetails", data1, config).then(function sucessCallback(response) {
                $scope.singleorder = response.data.data;

                //process to calculate totalitems of single order
                $scope.totalOrderItems = 0;
                $scope.orderItems = [];     // array used to calculate totalItems                
                for(var j=0;j<$scope.singleorder.length;j++){
                    $scope.orderItems = $scope.singleorder[j].items;
                    for(var i=0; i<$scope.orderItems.length;i++){                    
                        $scope.totalOrderItems += parseInt($scope.orderItems[i].quantity);
                    }
                }

                $scope.orderPayment = [];
                $scope.orderPayment[0] = response.data.payment_details;
                $rootScope.orderPriceDetails = orderDetails.orderCalculation($scope.orderPayment);
                
            }, function errorCallback(response) {
                console.log("Order details received");
            });
        }

        $scope.rateOrderClick = function(rateOrderId){ // function called on Rate Order button
            $scope.rateOrderClicked = true;
            $scope.rateOrderId = rateOrderId;
        }//rateOrderClick End


        $scope.rating1 = 0;
        $scope.rating2 = 2;
        $scope.isReadonly = true;
        $scope.rateFunction = function(rating) {
          $scope.rateSelected = rating;
        };

        $scope.closeRate = function(){
            $scope.rateOrderClicked = false;
        }//closeRate End
        $scope.orderRateSubmit = function(){    // function called on rating submission
            var path = $rootScope.baseApiURl + "order/orderRating";
            var config = { headers: { 'Content-Type': 'application/json;charset=utf-8;'} };
            var data = {
                "order_id" : $scope.rateOrderId,
                "user_id"  : $rootScope.userId,
                "rating_points" : $scope.rateSelected,
                "rating_comment" : $scope.rateComment
            }
            //console.log(data);
            $http.post(path,data,config).then(function sucessCallback(response){
                //console.log("appi called")
                $scope.rateOrderClicked = false;
                alert(response.data.message);
                $window.location.href = "myorder";
                //console.log(response.data);                
            },
            function errorCallback(response){
                alert(response.data.message);
                return false;
            });
            $scope.rating_comment = "";
        }//orderRateSubmit End
    }
});  //myOrderCtrl End

app.controller("trackOrderCtrl", function ($scope, $http, $rootScope, $filter, save,orderDetails, $window) {   //trackOrderCtrl Start
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $window.localStorage.removeItem("pincodeVal");
    $window.localStorage.removeItem("cityName"); 

    
    $rootScope.userId = save.getvar("userId");
    $rootScope.userName = save.getvar("userName");  
    $rootScope.statusClick = save.getvar("statusClick"); 
    $rootScope.trackOrderId = save.getvar("trackOrderId");  

    if($rootScope.userId==undefined || $rootScope.userId=="" || $rootScope.userName==undefined){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {
        $scope.hasOrder = true;
        $scope.processStatus = false;
        $scope.deliveryStatus = false;
        var orderStatus = "";
        $scope.lastDeliverStatus = "Out For Delivery";
        $rootScope.referralCode = save.getvar("referralCode");
        $rootScope.userDetails = save.getob("userDetails");
        $rootScope.addressDetails = save.getob("addressDetails");
        if($rootScope.statusClick == "true") {  // if user want to see specific order status
            var data2 = "user_id=" + $rootScope.userId + "&recent=0";
            config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
            $http.post($rootScope.baseApiURl + "order/orderdetails", data2, config).then(function sucessCallback(response) { 
                $scope.orderData = [];
                $scope.orderData = response.data.data;
                for (var i = 0; i < $scope.orderData.length; i++) {
                    $scope.trackOrder = [];
                    if ($scope.orderData[i].order_id == $rootScope.trackOrderId) {
                        $scope.trackOrder[0] = $scope.orderData[i]
                        $rootScope.trackOrder = $scope.trackOrder;

                        orderStatus = $rootScope.trackOrder[0].order_status;
                        if(orderStatus=='Processing'){
                            $scope.processStatus = true;
                        }
                        if(orderStatus=='Out For Delivery' || orderStatus=='Delivered' || orderStatus=='Ready to Dispatch'){
                            $scope.processStatus = true;
                            $scope.deliveryStatus = true;
                            $scope.lastDeliverStatus = orderStatus;
                        }
                        if($rootScope.trackOrder.length==0){
                            $scope.hasOrder = false;
                        }

                        $scope.totalOrderItems = 0;
                        $scope.orderItems = [];     // array used to calculate totalItems                
                        for(var j=0;j<$rootScope.trackOrder.length;j++){
                            $scope.orderItems = $rootScope.trackOrder[j].items;
                            for(var i=0; i<$scope.orderItems.length;i++){                    
                                $scope.totalOrderItems += parseInt($scope.orderItems[i].quantity);
                            }
                        }
                        $scope.orderStatusComment = $scope.trackOrder[0].orderStatusComment;                        
                        var brExp = /<br\s*\/?>/i;
                        $scope.orderStatusComment = $scope.orderStatusComment.split(brExp);
                        $rootScope.orderPriceDetails =  orderDetails.orderCalculation($scope.trackOrder);                        
                        $rootScope.statusClick = false;
                        $rootScope.trackOrderId = "";
                        save.setvar("statusClick",$rootScope.statusClick);
                        save.setvar("trackOrderId",$rootScope.trackOrderId);
                        break;
                    }
                }
            }, function errorCallback(response) {
                alert("clicked Order details not received");
                return false;
            });
        }
        else {          // otherwise show recent order status
            var data2 = "user_id=" + $rootScope.userId + "&recent=1";
            config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
            $http.post($rootScope.baseApiURl + "order/orderdetails", data2, config).then(function sucessCallback(response) {
                if (response.data.status == "1") {
                    $rootScope.trackOrder = response.data.data;                      
                    
                    if($rootScope.trackOrder.length==0){
                        $scope.hasOrder = false;
                    }
                    else{
                        $scope.hasOrder = true;
                        orderStatus = $rootScope.trackOrder[0].order_status;
                        if(orderStatus=='Processing'){
                            $scope.processStatus = true;
                        }
                        if(orderStatus=='Out For Delivery' || orderStatus=='Delivered' || orderStatus=='Ready to Dispatch'){
                            $scope.processStatus = true;
                            $scope.deliveryStatus = true;
                            $scope.lastDeliverStatus = orderStatus;
                        }
                        $scope.totalOrderItems = 0;
                        $scope.orderItems = [];     // array used to calculate totalItems                
                        for(var j=0;j<$rootScope.trackOrder.length;j++){
                            $scope.orderItems = $rootScope.trackOrder[j].items;
                            for(var i=0; i<$scope.orderItems.length;i++){                    
                                $scope.totalOrderItems += parseInt($scope.orderItems[i].quantity);
                            }
                        }
                        $scope.orderStatusComment = $scope.trackOrder[0].orderStatusComment;                        
                        var brExp = /<br\s*\/?>/i;
                        $scope.orderStatusComment = $scope.orderStatusComment.split(brExp);    
                        $rootScope.orderPriceDetails =  orderDetails.orderCalculation($scope.trackOrder);
                    }                    
                }

            }, function errorCallback(response) {
                alert("Order details not received");
                return false;

            });
        }
    }
});  //trackOrderCtrl End

//referEarnCtrl Start
app.controller("referEarnCtrl", function($scope, $http, $rootScope, save, $window){ 
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.userId = save.getvar("userId");
    $rootScope.userName = save.getvar("userName");      
    
    if($rootScope.userId==undefined || $rootScope.userId=="" || $rootScope.userName==undefined || $rootScope.userName==""){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else {
        $rootScope.userDetails = save.getob("userDetails")
        $rootScope.addressDetails = save.getob("addressDetails")
        $rootScope.pincodeVal = save.getvar("pincodeVal");
        $scope.referCode = $rootScope.userDetails.referCode;
        $scope.referralAmountText = $rootScope.userDetails.referralAmountText;
        $scope.referralToLink = $rootScope.userDetails.referralToLink;   

        $scope.referInvite = function(){
            var data = "sender_name="+$rootScope.userName+"&referral_code="+$scope.referCode+"&email_ids="+$scope.referEmailInvites+"&text_contents="+$scope.referralToLink;
            var path = $rootScope.baseApiURl + "webpage/refer_and_earn";
            var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } };
            //console.log(data)
            $http.post(path, data, config).then( function sucessCallback(response){
                alert(response.data.message);
                $scope.referEmailInvites = "";
            }, function errorCallback(resonse){
                alert(response.data.message);
                return false;
            });
        }  //referInvite End

        $scope.copyToClipboard = function(){ // function called to copy referText on click
            // create temp element
            var copyElement = document.createElement("span");
            copyElement.appendChild(document.createTextNode($scope.referCode));
            copyElement.id = 'tempCopyToClipboard';
            angular.element(document.body.append(copyElement));
            // select the text
            var range = document.createRange();
            range.selectNode(copyElement);
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);
            // copy & cleanup
            document.execCommand('copy');
            window.getSelection().removeAllRanges();
            copyElement.remove();
            alert("copied to your clipboard");
        }//copyToClipboard End
    } 
});
//referEarnCtrl End

//thankYouCtrl Start
app.controller("thankYouCtrl",function($rootScope,$scope,$http,$window,save){
    $rootScope.baseSiteUrl = save.getvar("baseSiteUrl");
    $rootScope.baseApiURl = save.getvar("baseApiURl");
    $rootScope.hideContentArea();
    $rootScope.userId = save.getvar("userId");
    $rootScope.userName = save.getvar("userName");  
    if($rootScope.userId==undefined || $rootScope.userId=="" || $rootScope.userName==undefined || $rootScope.userName==""){
        $window.location.href = $rootScope.baseSiteUrl;
    }
    else { 
        $window.localStorage.removeItem("itemList");
        $window.localStorage.removeItem("services");
        $window.localStorage.removeItem("totalItems");
        $window.localStorage.removeItem("price");
        $window.localStorage.removeItem("laundry_settings");

        $window.localStorage.removeItem("selectAddressDetails");

        $window.localStorage.removeItem("expressDelivery");
        $window.localStorage.removeItem("normalDelivery");
        $window.localStorage.removeItem("deliveryType");
        $window.localStorage.removeItem("selectedFinalDeliveryDate");
        $window.localStorage.removeItem("selected_deliveryslot_from");
        $window.localStorage.removeItem("selected_deliveryslot_to");
        $window.localStorage.removeItem("selected_pickup_date");
        $window.localStorage.removeItem("selected_pickupslot_from");
        $window.localStorage.removeItem("selected_pickupslot_to");

        $window.localStorage.removeItem("igst");
        $window.localStorage.removeItem("igstExist");
        $window.localStorage.removeItem("cgstExist");
        $window.localStorage.removeItem("cgst");
        $window.localStorage.removeItem("sgst");
        $window.localStorage.removeItem("sgstExist");
        $window.localStorage.removeItem("creditExist");
        $window.localStorage.removeItem("calc_delivery_amt");
        $window.localStorage.removeItem("delivery_amt");

        $window.localStorage.removeItem("netTotal");
        $window.localStorage.removeItem("cartPrice");
        $window.localStorage.removeItem("discount");
        $window.localStorage.removeItem("total") ;
        $window.localStorage.removeItem("grandTotal");
        $window.localStorage.removeItem("amount_payable");
        $window.localStorage.removeItem("credit_used");

        $scope.placedOrderId = save.getvar("placedOrderId");
        $scope.placeMoreOrder = function() {
            $window.location.href = 'service-select';
            $window.localStorage.removeItem("placedOrderId");
        }; 
        $scope.thanksTrackOrder = function() {
            $window.location.href = "trackorder";
            $window.localStorage.removeItem("placedOrderId");
        };  
    }  
})
//thankYouCtrl End


app.filter("smalldt",function(){
    return function(z){
        if(z<12)
        {
            return z+" AM"
        }
        else if(z==12)
        {
            return z+" PM"
        }
        else if(z>12)
        {
            return z-12+" PM"
        }
        else if(z==24)
        {
            return z-12+" AM"
        }
    }
})

app.filter("myround",function(){
    return function(z){
        return (z).toFixed(2)
    }
})





 