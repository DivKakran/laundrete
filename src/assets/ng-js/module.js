
var app = angular.module("myApp", ['ngRoute', 'angular-flexslider', 'ngAnimate', 'ui.bootstrap','angular-matchmedia']);
app.run(function($rootScope){
	$rootScope.baseApiURl = "http://testing.birdapps.org/launderette/cms/api-web-v1/"; // variable used to set Api base url for link
    $rootScope.baseSiteUrl = "/launderette/"; // variable used to set base url for link

	$rootScope.hideContentArea = function(){	// code for removeing index page content on all angular pages
		var path = window.location.pathname;  //alert(path);
		if(path== $rootScope.baseSiteUrl+'user-profile' || path== $rootScope.baseSiteUrl+'thank-you' || path== $rootScope.baseSiteUrl+'service-select' || path==$rootScope.baseSiteUrl+'view-address' || path==$rootScope.baseSiteUrl+'add-address' || path==$rootScope.baseSiteUrl+'login' || path==$rootScope.baseSiteUrl+'login-otp' || path==$rootScope.baseSiteUrl+'login-detail'  || path==$rootScope.baseSiteUrl+'login-sucessfully' || path==$rootScope.baseSiteUrl+'date-time' || path==$rootScope.baseSiteUrl+'payment-option' || path==$rootScope.baseSiteUrl+'trackorder' || path==$rootScope.baseSiteUrl+'myorder' || path==$rootScope.baseSiteUrl+'refer-earn'){
			document.getElementById("contents-area").style.display="none";
		}	
	} // end code for removeing index page content on all angular pages  
})