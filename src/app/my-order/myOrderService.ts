import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class MyOrderService {
    private myOrdersDetailUrl = environment.baseApiURl + 'order/orderdetails';
    private orderItemDetailUrl = environment.baseApiURl + 'order/orderitemdetails';
    constructor(private http: HttpClient) { }

    myOrderDetailService(orderDetail) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }).append('Authorization', 'Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.myOrdersDetailUrl,
            orderDetail,
            {
                headers: headers
            }
        ))
    }

    trackSingleOrderService(trackOrderData) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }).append('Authorization', 'Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.orderItemDetailUrl,
            trackOrderData,
            {
                headers: headers
            }
        ))
    }
}