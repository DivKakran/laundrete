import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject , Renderer} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import { MyOrderDetail, TrackSingleOrder } from './myOrderModel';
import { MyOrderService } from './myOrderService';
import { Router } from '@angular/router';
@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.css'],
  providers: [MyOrderService]
})
export class MyOrderComponent implements OnInit {

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private header: ChangeHeaderService, private myOrderService: MyOrderService,
    private router: Router , private render: Renderer) { }
  headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: false, dropdownHeader: true };

  allOrdersDetail;
  orderExist = false;
  firstOrderId;
  orderViewDetail;
  singleOrderDetail;
  paymentDetails;
  totalOrderItems = 0;
  itemQuantity = 0;
  grandTotal: number;
  cgstAmount;
  totalCgstAmount;
  sgstAmount;
  totalSgstAmount;
  igstAmount;
  totalIgstAmount;
  discountValue: Number;
  totalDiscountValue;
  totalPayableAmount;

  selectedViewDetail = 500;
  myOrdersDetail() {
    let myOrdersData = new MyOrderDetail(this.localStorage.getItem('userId'), 0);
    this.myOrderService.myOrderDetailService(myOrdersData).subscribe(response => {
      this.orderExist = true;
      if (response && response["body"] && response["body"].data.length > 0) {
        this.orderExist = true;
        this.allOrdersDetail = response["body"].data;
        this.firstOrderId = response["body"].data[0].order_id;
        this.trackSingleOrder(this.firstOrderId , 500);
      }
      else {
        this.orderExist = false;
      }
    })
  }

  trackSingleOrder(orderId , i) { 
    if( i == this.selectedViewDetail){
      this.selectedViewDetail = 500;
    }else{
      this.selectedViewDetail = i;
    }
    // this.render.setElementClass(event.target,"show-order",true);
    this.orderViewDetail = orderId;
    if (orderId != '') {
      this.totalOrderItems = 0;
    }
    let singleOrderData = new TrackSingleOrder(this.localStorage.getItem('userId'), orderId);
    this.myOrderService.trackSingleOrderService(singleOrderData).subscribe(response => {
      if (response && response["body"] && response["body"].data.length > 0) {
        this.singleOrderDetail = response["body"].data;
        this.paymentDetails = response["body"].payment_details;
        for (let i = 0; i < response["body"].data.length; i++) {
          let trackOrderItems = response["body"].data[i].items;
          for (let j = 0; j < trackOrderItems.length; j++) {
            let totalNum = trackOrderItems[j].quantity;
            this.totalOrderItems += Number(trackOrderItems[j].quantity);
            // this.itemQuantity +=Number(trackOrderItems[j].quantity);
          }
        }
        // ------------------------- Price Calculation Start-----------------------------------//
        // -----------------------------------------------------------------------------------//
        if (this.paymentDetails.deliveryCharges != '0.00') {
          if (this.paymentDetails.credit != '0' && this.paymentDetails.coupon_value == '0') {
            if (this.paymentDetails.credit_before_gst == "1") {
              this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.paymentDetails.credit)
              this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
              this.totalCgstAmount = this.cgstAmount.toFixed(2)
              this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
              this.totalSgstAmount = this.sgstAmount.toFixed(2)
              this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
              this.totalIgstAmount = this.igstAmount.toFixed(2)
            }
            // ---------------------------- Credit before GST 0----------------------------------------//
            else if (this.paymentDetails.credit_before_gst == "0") {
              this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges)
              this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
              this.totalCgstAmount = this.cgstAmount.toFixed(2)
              this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
              this.totalSgstAmount = this.sgstAmount.toFixed(2)
              this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
              this.totalIgstAmount = this.igstAmount.toFixed(2)
              if (this.paymentDetails.IGST == "0.00") {
                this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
              }
              else {
                this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
              }
            }
          }
          else if (this.paymentDetails.coupon_value != '0' && this.paymentDetails.credit == '0') {
            if (this.paymentDetails.coupon_type == "PERCENTAGE") {
              let discountPercentage: number = this.paymentDetails.coupon_value;
              let discountUpto: number = this.paymentDetails.coupon_discount_upto;
              let cartValuePercentage = (Number(this.paymentDetails.total_item_price) * discountPercentage) / 100;
              if (discountUpto > cartValuePercentage) {
                this.discountValue = cartValuePercentage;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }
              else if (discountUpto < cartValuePercentage) {
                this.discountValue = discountUpto;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }

            }
            else if (this.paymentDetails.coupon_type == "FLAT") {
              this.discountValue = this.paymentDetails.coupon_value;
              this.totalDiscountValue = Number(this.discountValue).toFixed(2)
              this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue)
              this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
              this.totalCgstAmount = this.cgstAmount.toFixed(2)
              this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
              this.totalSgstAmount = this.sgstAmount.toFixed(2)
              this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
              this.totalIgstAmount = this.igstAmount.toFixed(2)
            }
          }
          else if (this.paymentDetails.coupon_value != '0' && this.paymentDetails.credit != '0') {
            if (this.paymentDetails.credit_before_gst == "1") {
              if (this.paymentDetails.coupon_type == "PERCENTAGE") {
                let discountPercentage: number = this.paymentDetails.coupon_value;
                let discountUpto: number = this.paymentDetails.coupon_discount_upto;
                let cartValuePercentage = (Number(this.paymentDetails.total_item_price) * discountPercentage) / 100;
                if (discountUpto > cartValuePercentage) {
                  this.discountValue = cartValuePercentage;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
                else if (discountUpto < cartValuePercentage) {
                  this.discountValue = discountUpto;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }

              }
              else if (this.paymentDetails.coupon_type == "FLAT") {
                this.discountValue = this.paymentDetails.coupon_value;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }
            }
            // ---------------------------- Credit before GST 0----------------------------------------//
            else if (this.paymentDetails.credit_before_gst == "0") {
              if (this.paymentDetails.coupon_type == "PERCENTAGE") {
                let discountPercentage: number = this.paymentDetails.coupon_value;
                let discountUpto: number = this.paymentDetails.coupon_discount_upto;
                let cartValuePercentage = (Number(this.paymentDetails.total_item_price) * discountPercentage) / 100;
                if (discountUpto > cartValuePercentage) {
                  this.discountValue = cartValuePercentage;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                  if (this.paymentDetails.IGST == "0.00") {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
                  }
                  else {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
                  }
                  //  this.totalPayableAmount = Number(this.grandTotal) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                }
                else if (discountUpto < cartValuePercentage) {
                  this.discountValue = discountUpto;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                  if (this.paymentDetails.IGST == "0.00") {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
                  }
                  else {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
                  }
                  //  this.totalPayableAmount = Number(this.grandTotal) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                }

              }
              else if (this.paymentDetails.coupon_type == "FLAT") {
                this.discountValue = this.paymentDetails.coupon_value;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
                if (this.paymentDetails.IGST == "0.00") {
                  this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
                }
                else {
                  this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
                }
                //  this.totalPayableAmount = Number(this.grandTotal) + Number(this.paymentDetails.deliveryCharges) - Number(this.discountValue) - Number(this.paymentDetails.credit)
              }
            }
          }
          else {
            this.grandTotal = Number(this.paymentDetails.total_item_price) + Number(this.paymentDetails.deliveryCharges)
            this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
            this.totalCgstAmount = this.cgstAmount.toFixed(2)
            this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
            this.totalSgstAmount = this.sgstAmount.toFixed(2)
            this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
            this.totalIgstAmount = this.igstAmount.toFixed(2)
          }

        }
        else if (this.paymentDetails.deliveryCharges == '0.00') {
          if (this.paymentDetails.credit != '0' && this.paymentDetails.coupon_value == '0') {
            if (this.paymentDetails.credit_before_gst == "1") {
              this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.paymentDetails.credit)
              this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
              this.totalCgstAmount = this.cgstAmount.toFixed(2)
              this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
              this.totalSgstAmount = this.sgstAmount.toFixed(2)
              this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
              this.totalIgstAmount = this.igstAmount.toFixed(2)
            }
            // ---------------------------- Credit before GST 0----------------------------------------//
            else if (this.paymentDetails.credit_before_gst == "0") {
              this.grandTotal = Number(this.paymentDetails.total_item_price)
              this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
              this.totalCgstAmount = this.cgstAmount.toFixed(2)
              this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
              this.totalSgstAmount = this.sgstAmount.toFixed(2)
              this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
              this.totalIgstAmount = this.igstAmount.toFixed(2)
              if (this.paymentDetails.IGST == "0.00") {
                this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
              }
              else {
                this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
              }
              // this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.paymentDetails.credit)
            }
          }
          else if (this.paymentDetails.coupon_value != '0' && this.paymentDetails.credit == '0') {
            if (this.paymentDetails.coupon_type == "PERCENTAGE") {
              let discountPercentage: number = this.paymentDetails.coupon_value;
              let discountUpto: number = this.paymentDetails.coupon_discount_upto;
              let cartValuePercentage = (Number(this.paymentDetails.total_item_price) * discountPercentage) / 100;
              if (discountUpto > cartValuePercentage) {
                this.discountValue = cartValuePercentage;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }
              else if (discountUpto < cartValuePercentage) {
                this.discountValue = discountUpto;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }
            }
            else if (this.paymentDetails.coupon_type == "FLAT") {
              this.discountValue = this.paymentDetails.coupon_value;
              this.totalDiscountValue = Number(this.discountValue).toFixed(2)
              this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue)
              this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
              this.totalCgstAmount = this.cgstAmount.toFixed(2)
              this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
              this.totalSgstAmount = this.sgstAmount.toFixed(2)
              this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
              this.totalIgstAmount = this.igstAmount.toFixed(2)
            }
          }
          else if (this.paymentDetails.coupon_value != '0' && this.paymentDetails.credit != '0') {
            if (this.paymentDetails.credit_before_gst == "1") {
              if (this.paymentDetails.coupon_type == "PERCENTAGE") {
                let discountPercentage: number = this.paymentDetails.coupon_value;
                let discountUpto: number = this.paymentDetails.coupon_discount_upto;
                let cartValuePercentage = (Number(this.paymentDetails.total_item_price) * discountPercentage) / 100;
                if (discountUpto > cartValuePercentage) {
                  this.discountValue = cartValuePercentage;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
                else if (discountUpto < cartValuePercentage) {
                  this.discountValue = discountUpto;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }

              }
              else if (this.paymentDetails.coupon_type == "FLAT") {
                this.discountValue = this.paymentDetails.coupon_value;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }
            }
            // ---------------------------- Credit before GST 0----------------------------------------//
            else if (this.paymentDetails.credit_before_gst == "0") {
              if (this.paymentDetails.coupon_type == "PERCENTAGE") {
                let discountPercentage: number = this.paymentDetails.coupon_value;
                let discountUpto: number = this.paymentDetails.coupon_discount_upto;
                let cartValuePercentage = (Number(this.paymentDetails.total_item_price) * discountPercentage) / 100;
                if (discountUpto > cartValuePercentage) {
                  this.discountValue = cartValuePercentage;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                  if (this.paymentDetails.IGST == "0.00") {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
                  }
                  else {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
                  }
                  //  this.totalPayableAmount = Number(this.grandTotal) - Number(this.discountValue) - Number(this.paymentDetails.credit)
                }
                else if (discountUpto < cartValuePercentage) {
                  this.discountValue = discountUpto;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue)
                  this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                  if (this.paymentDetails.IGST == "0.00") {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
                  }
                  else {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
                  }
                  //  this.totalPayableAmount = Number(this.grandTotal)  - Number(this.discountValue) - Number(this.paymentDetails.credit)
                }

              }
              else if (this.paymentDetails.coupon_type == "FLAT") {
                this.discountValue = this.paymentDetails.coupon_value;
                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                this.grandTotal = Number(this.paymentDetails.total_item_price) - Number(this.discountValue)
                this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
                if (this.paymentDetails.IGST == "0.00") {
                  this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.paymentDetails.credit)
                }
                else {
                  this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.paymentDetails.credit)
                }
                //  this.totalPayableAmount = Number(this.grandTotal)  - Number(this.discountValue) - Number(this.paymentDetails.credit)
              }
            }
          }
          else {
            this.grandTotal = Number(this.paymentDetails.total_item_price)
            this.cgstAmount = (this.grandTotal * this.paymentDetails.CGST) / 100;
            this.totalCgstAmount = this.cgstAmount.toFixed(2)
            this.sgstAmount = (this.grandTotal * this.paymentDetails.SGST) / 100;
            this.totalSgstAmount = this.sgstAmount.toFixed(2)
            this.igstAmount = (this.grandTotal * this.paymentDetails.IGST) / 100;
            this.totalIgstAmount = this.igstAmount.toFixed(2)
          }
        }
        // ------------------------- Price Calculation End------------------------------------//
        // -----------------------------------------------------------------------------------//       
      }
    })
  }

  orderTrack(orderId) {
    this.localStorage.setItem('myOrderStatus', "true");
    this.localStorage.setItem('trackOrderId', orderId);
    this.router.navigate(['/trackorder'])
  }

  ngOnInit() {
    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
    this.myOrdersDetail();
  }
}
