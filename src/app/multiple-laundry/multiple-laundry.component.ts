import { Component, OnInit, Inject , AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';
import { LaundryClicked } from '../multiple-laundry/laundry-clicked';
import { ToastrService } from 'ngx-toastr';
import {HomeService} from  '../home/homeService';
import { HeaderService } from '../header/headerService';
import { LocationByPostcode, LoginModel, RequestOtpModel, VerifyOtpModel, RequestOtpFBModel, FbRegistrationModel, RegitrataionModel,GetNotification,DeleteNotification } from '../header/headerServicesModel';
import { FormGroup } from '@angular/forms';
import {AuthService,FacebookLoginProvider,GoogleLoginProvider} from 'angular5-social-login';
import {ChangeHeaderService} from '../changeHeaderService';
import { localeData } from 'moment';
//import { routerNgProbeToken } from '@angular/router/src/router_module';

import { Meta , Title} from '@angular/platform-browser';

@Component({
  selector: 'app-multiple-laundry',
  templateUrl: './multiple-laundry.component.html',
  styleUrls: ['./multiple-laundry.component.css'],
  providers: [HomeService, HeaderService]
})
export class MultipleLaundryComponent implements OnInit , AfterViewInit{
  // inLatitude: number = 21.7679;
  // inLongitude: number = 78.8718;
    requestCallDetail = {'fullName':'' , 'mobile':''};
    disableButton = false;
    serviceList : any = {};  
    selectedPostcode = localStorage.getItem("pincode");  
    //cityName = localStorage.getItem("cityName");
    serviceDetail: any;    
    //Search Pincode related variable
    sorryPopup = false;
    serviceAreas = false;
    locationByPostcodeResponse: any;
    searchText: any = '';
    // login related variable
    popupLoginClicked = true;
    loginUser = true;
    loginOtp = false;
    LoginResponse;
    model = { mobile: "" };
    submittedLoginData = false;
    checkUserExistence=false;
    userMobileNumber;
    registrationForm = false;
    facebookResponse;
    googleResponse;
    facebookModel = {fullName:'',emailAddress:'',mobile:'' };
    googleModel = {fullName:'',emailAddress:'',mobile:''};
    facebookId = false;
    googleId = false;
    fbOtpResponse = false;
    googleOtpResponse = false;
    FbLoginResponse;
    googleLoginResponse;
    socialLoginResponse;
    submittedOtp = false;
    otp1 = '';
    otp2 = '';
    otp3 = '';
    otp4 = '';
    otp5 = '';
    otpConcat = '';
    checkUserLogin = '';
    creditPoints   = '';
    registeredUserName = '';
    verifyOtpResponse;
    verifyOtpError;
    loginSuccess = false;
    loggedInUser = false;
    registerModel = {fullName:'',emailAddress:''};
    submittedRegisteredData  =false;

    // Notification variable
    notifyClicked = false;
    notificationExist = false
    notificationList;

    lat: number;
    lng: number;
    laundryServeCordinates = [];
    
    totalItems;
    itemNameArray=[];
    serviceNameArray=[];

    // cart variables
    totalCloths: any = 0;
    retrieveServiceFromCartArray: any;
    retrieveItemsFromCartArray: any;
    totalPrice: any = 0;
    loadAPI: Promise<any>;

    headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: false, dropdownHeader: false,processHeader:false};
    constructor(private myService: LaundryClicked, private router: Router, private homeService : HomeService, private toastr: ToastrService,
      private headerService: HeaderService,private socialAuthService: AuthService,private changeHeader:ChangeHeaderService , private meta: Meta , private title: Title) { 
        this.title.setTitle("Find Multiple Laundry & Dry cleaning Store  - Launderette");
        this.meta.addTags([{name:"description", content: 'launderette - Find Multiple Laundry & Dry cleaning Store in your near locations' } ,
        { name:"keywords" ,content: 'Multiple laundry, Multiple Dry Cleaning store' }]);
      }
    
    private loadScript() {
      var dynamicScripts = ["assets/js/custom.js" , "assets/js/slick.js"];
      for (var i = 0; i < dynamicScripts .length; i++) {
          let node = document.createElement('script');
          node.src = dynamicScripts [i];
          node.type = 'text/javascript';
          node.async = false;
          node.charset = 'utf-8';
          document.getElementsByTagName('head')[0].appendChild(node);
      }
    }
   ngOnInit() { 
      // for cart
      this.creditPoints       = localStorage.getItem('creditPoint'); 
      this.totalCloths = localStorage.getItem('totalCloths');
      this.retrieveServiceFromCartArray = JSON.parse(localStorage.getItem('serviceName'));
      this.retrieveItemsFromCartArray   = JSON.parse(localStorage.getItem('itemList'));
      this.totalPrice = localStorage.getItem('totalPrice');

      // exitance changing
      this.changeHeader.updateExistance.subscribe((res) => {
        // this.checkUserLogin = <string>res;
      });
      this.changeHeader.rUserName.subscribe((res) => { 
        this.registeredUserName = <string>res; 
        this.checkUserLogin = "true";
      });
      this.changeHeader.changingHeaderCredit.subscribe((res) => {
        this.creditPoints = <string>res;
        console.log('ff' , this.creditPoints);
      });
      this.checkUserLogin = localStorage.getItem('userLogin');
      this.registeredUserName = localStorage.getItem('userName');
      this.changeHeader.changeHeader(this.headerValue);
      this.getLaundry(localStorage.getItem("postcode"));

      // cart functionality
      this.changeHeader.countItem.subscribe(res => {
        this.totalItems = res; 
        this.itemNameArray.filter((res,i)=>{
          if(res.weight<=0){
            this.serviceNameArray=[]
            this.itemNameArray.splice(i,1);
            this.itemNameArray.filter((res)=>{
              this.serviceNameArray.push(res.service);
            })
          }
        })
      }); 
    } 
    laundryList: any;
  // function to get laundryList
     async getLaundry(postcode){
        //console.log("selectedPostcode:",this.selectedPostcode);
      await this.myService.getLaundry(postcode).subscribe(async (response) => {
          //console.log('sssssssssssss', response)
         await this.getting(response);
         this.loadAPI = new Promise((resolve) => {
          this.loadScript();
          resolve(true);
        });
      });
    }
    getting(response){
      if(response && response["body"]){
        if(response["body"].status=="1"){
          this.serviceDetail = response["body"];
          this.laundryList   = response["body"].standardLaundaryList; 
          this.putInitilLatLng(response["body"]);
          this.laundryServeCordinates =  response["body"].coordinates;
          this.changeHeader.headerCredit(response["body"].creditPoints);
          localStorage.setItem('creditPoint' ,response["body"].creditPoints )
          console.log(this.serviceDetail)
        }
      }
    }
    ngAfterViewInit(){ 
      // this.loadAPI = new Promise((resolve) => {
      //   this.loadScript();
      //   resolve(true);
      // });
    }
    putInitilLatLng(data){
      this.lat =  data.coordinates[0].lat;
      this.lng =  data.coordinates[0].lng;
      console.log(this.lat , this.lng);
    }

    // function execute when user click on specific laundry
    laundryClicked(laundryId , pin , type){ 
      if(type == 'premium'){ 
        localStorage.setItem('laundryTypeForGst' , "true");
        localStorage.removeItem('laundryIdForTimeSlot');
        localStorage.setItem('laundryIdMain' , '');
        localStorage.setItem('typeOfLaundryOnSelect', 'PREMIUM');

        localStorage.setItem('laundryPincodeList' , 'Premium');
        
        localStorage.setItem('forSlotId' , '');
      } else{
        localStorage.setItem('laundryIdMain' , laundryId);
        localStorage.setItem('laundryId' , laundryId);
        localStorage.setItem('laundryIdForTimeSlot' , laundryId);
        
        localStorage.setItem('forSlotId' , laundryId);

        localStorage.setItem('laundryPincodeList' , 'Standard');
        
      }
      localStorage.setItem('postcode', pin);
        this.myService.getServices(laundryId , pin).subscribe((response) => {
            if (response && response["body"]) {
                if(response["body"].status=="1"){
                    localStorage.setItem("laundryId",laundryId);
                    // localStorage.setItem("cityName" , response["body"].name);
                    // this.changeHeader.getUpdatedCityName(response["body"].name);
                    this.router.navigate(["/service-select"]);
                }
            }
        });
    } 

    showError(Error) {
        this.toastr.error(Error);
    }
    showSuccess(success) {
        this.toastr.success(success);
    }
    // insta order form submission
    onSubmitRequestForm(){
        if(this.requestCallDetail.fullName=="" && this.requestCallDetail.mobile==""){
          this.showError('Please Enter Details');
        }
        else if(this.requestCallDetail.fullName=="" && this.requestCallDetail.mobile.length<10){
          this.showError('Please Enter Valid Mobile No.');
        }
        else if(this.requestCallDetail.fullName!="" && this.requestCallDetail.mobile.length<10){
          this.showError('Please Enter Valid Mobile No.');
        }
        else{
          // this.requestBtnText = "THANKS FOR CONNECTING!"
          this.disableButton = true;
          let requestModelData = {"mobile_number":this.requestCallDetail.mobile,"name":this.requestCallDetail.fullName };
          this.homeService.requestCallService(requestModelData).subscribe(response=>{
            if(response && response["body"]){
              if(response["body"].status=="1"){
                this.showSuccess(response["body"].message);
                // this.requestBtnText = "REQUEST A CALL";
                this.requestCallDetail.mobile = "";
                this.requestCallDetail.fullName = "";
                this.disableButton = false;
              }
              else{
                this.showError(response["body"].message);
                // this.requestBtnText = "REQUEST A CALL";
                this.requestCallDetail.mobile = "";
                this.requestCallDetail.fullName = "";
                this.disableButton = false;
              }
            }
          });
        }
    }

    placeOrder(number){
        // this.submitted = true;
        if (number == "" || number == undefined) {
            this.sorryPopup = true;
        } 
        if (number) {    
          this.headerService.checkLocationByPostcode(number).subscribe(response => {
            this.locationByPostcodeResponse = response;
            if(response && response["body"]){

                // removing cart when click new pincode
                localStorage.removeItem('itemList');
                localStorage.removeItem('serviceName');
                localStorage.removeItem('totalPrice');
                localStorage.removeItem('totalCloths');
                localStorage.removeItem('serviceComment');


                if(response["body"].status=="1"){
                  if(response["body"].laundaryList.length!= 1){  
                    localStorage.setItem("postcode" , number);
                    this.getLaundry(number);
                    localStorage.setItem("cityName" , response["body"].cityName);
                    this.changeHeader.getUpdatedCityName(response["body"].cityName);
                    localStorage.setItem("serviceArea" , "false");
                    //this.router.navigate(["/multiple-laundry"]);
                  }else{ 
                    this.changeHeader.getUpdatedCityName(response["body"].cityName);
                    localStorage.setItem('laundryPincodeList' , response["body"].laundaryList[0].type);
                   
                    localStorage.setItem("postcode" , number);
                    if(response["body"].laundaryList[0].type == 'Standard'){ 
                      localStorage.setItem('forSlotId' , response["body"].laundaryList[0].id);
                      localStorage.setItem('laundryIdMain' , response["body"].laundaryList[0].id );

                      localStorage.setItem('typeOfLaundryOnSelect', 'Standard');
                    }else{
                      localStorage.setItem('typeOfLaundryOnSelect', 'PREMIUM');
                      
                      localStorage.setItem('laundryIdMain' , '' );
                    }
                    localStorage.setItem("cityName" , response["body"].cityName);
                    localStorage.setItem("serviceArea" , "true");
                    this.router.navigate(["/service-select"]);
                  }
                }else{
                this.sorryPopup = true;
              }
            }
          },
            (error) => {
              // this.locationByPostcodeError = error;
            })
        }
      }
      serviceAreaResponse =[];
      serviceAvail() {
        this.sorryPopup   = false;
        this.serviceAreas = true;
        let data1 = "";
        this.headerService.checkServiceArea(data1).subscribe(response => {
          if (response && response["body"]) {
            this.serviceAreaResponse = response["body"].serviceList;
          }
        },
        (error) => { 
          // this.serviceAreaError = error; 
        })
      }

    serviceAreasPopup(postcode){
        this.serviceAreas = false;
        let locationByPostcode = new LocationByPostcode(postcode);
        this.headerService.checkLocationByPostcode(postcode).subscribe(response => {
           if (response && response["body"]) {

            // removing cart when click new pincode
            localStorage.removeItem('itemList');
            localStorage.removeItem('serviceName');
            localStorage.removeItem('totalPrice');
            localStorage.removeItem('totalCloths');
            localStorage.removeItem('serviceComment');


            //this.cityName = response["body"].cityName;
             localStorage.setItem("cityName",response["body"].cityName);
             localStorage.setItem("postcode" ,postcode);
            if(response && response["body"]){
                if(response["body"].status=="1"){
                    if(response["body"].laundaryList.length!= 1){
                        localStorage.setItem("postcode" , postcode);
                        this.getLaundry(postcode);
                    }
                    else{
                        localStorage.setItem("postcode" , postcode);
                        this.router.navigate(["/service-select"]);
                    }
                }
            }
            
             //this.router.navigate(["/service-select"]);
           }
        })  
    }

    // function for login popUp data
    loginComplete;
    onSubmitLoginData(loginFormData:FormGroup) { alert();
        this.loginComplete = loginFormData;
        this.submittedLoginData = true;
        if(this.model.mobile=="0000000000" || this.model.mobile==""){
          this.showError('Mobile number is required');
        }
        else if(this.model.mobile.length>10 || this.model.mobile.length<10){
          this.showError('Mobile number should be 10 digit');
        }
        else{
        let loginData = new LoginModel('MOBILE', '1234', this.model.mobile);
        this.headerService.UserloginService(loginData).subscribe(response => {
          this.disableButton = true;
          this.LoginResponse = response;
          if(response && response["body"]) {
            if(response["body"].message == 'User Exists') {
              this.loginUser = false;
              this.loginOtp = true;
              this.checkUserExistence = true;
              this.userMobileNumber = response["body"].mobile;
              this.disableButton = false;
            }
            else{
              this.loginUser = false;
              this.registrationForm = true;
              this.disableButton = false;
            }
          }
        },
        )
        } 
    }
  // function for Otp submission or Otp submit
  onSubmitOtp(otp:FormGroup) { 
    if(this.checkUserExistence){
      if(this.facebookId){
      this.submittedOtp = true; 
     // this.otpConcat = otp.otp1 + otp.otp2 + otp.otp3 + otp.otp4 + otp.otp5;
      this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
      let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "FB", "1234", this.facebookResponse.id);
      if (this.facebookResponse.id) {
          this.headerService.verifyOtpService(verifyOtpModel).subscribe(response => {
          this.verifyOtpResponse = response;
          if (response && response["body"]) {
            if (response["body"].status == '1') {
              localStorage.setItem('userLogin','true');
              this.loginOtp = false;
              this.loginSuccess = true;
              this.loggedInUser = true;
              this.checkUserLogin = "true"; 
              localStorage.setItem('userName',response["body"].userDetails.name);
              localStorage.setItem('userId', response["body"].userDetails.user_id);
              this.registeredUserName=localStorage.getItem('userName');
              localStorage.setItem('mobileNumber',response["body"].userDetails.mobile);
              localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
              this.creditPoints = response["body"].userDetails.creditPoints;
              localStorage.setItem('referCode',response["body"].userDetails.referCode);
              localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
              localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
            }
            if(response["body"].status == "0"){
              this.showError(response["body"].message);
              setTimeout(()=>{ 
                otp.reset();
              }, 5000);
            }
          }
        },
          (error) => { this.verifyOtpError = error}
        )
      }
      }
     else if(this.googleId){
        this.submittedOtp = true; 
       // this.otpConcat = otp.otp1 + otp.otp2 + otp.otp3 + otp.otp4 + otp.otp5;
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "GOOGLE_PLUS", "1234", this.googleResponse.id);
        if (this.googleResponse.id) {
            this.headerService.verifyOtpService(verifyOtpModel).subscribe(response => {
            this.verifyOtpResponse = response;
            if (response && response["body"]) {
              if (response["body"].status == '1') {
                localStorage.setItem('userLogin','true');
                this.loginOtp = false;
                this.loginSuccess = true;
                this.loggedInUser = true;
                this.checkUserLogin = "true"; 
                localStorage.setItem('userName',response["body"].userDetails.name);
                localStorage.setItem('userId', response["body"].userDetails.user_id);
                this.registeredUserName=localStorage.getItem('userName');
                localStorage.setItem('mobileNumber',response["body"].userDetails.mobile);
                localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                this.creditPoints = localStorage.getItem('creditPoint');
                localStorage.setItem('referCode',response["body"].userDetails.referCode);
                localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
              }
              if(response["body"].status == "0"){
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                  otp.reset();
                }, 5000);
              }
            }
          },
            (error) => { this.verifyOtpError = error}
          )
        }
        }
      else{
        this.submittedOtp = true;
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "MOBILE", "1234", this.userMobileNumber);
        if (this.userMobileNumber) {
          this.headerService.verifyOtpService(verifyOtpModel).subscribe(response => {
            this.verifyOtpResponse = response;
            if (response && response["body"]) {
              if (response["body"].status == '1') {
                localStorage.setItem('userLogin','true');
                this.loginOtp = false;
                this.loginSuccess = true;
                this.loggedInUser = true;
                this.checkUserLogin = "true"; 
                localStorage.setItem('userName',response["body"].userDetails.name);
                localStorage.setItem('userId', response["body"].userDetails.user_id);
                this.registeredUserName=localStorage.getItem('userName');
                localStorage.setItem('mobileNumber',response["body"].userDetails.mobile);
                localStorage.setItem('email',response["body"].userDetails.email);
                localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                localStorage.setItem('referCode',response["body"].userDetails.referCode);
                localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                this.creditPoints = localStorage.getItem('creditPoint');
              }
              if(response["body"].status == "0"){
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                  otp.reset();
             }, 5000);
              }
            }
          })
        }
      }
    }
    else{
     // this.fullRegistration();
    // ======================== Registration Process================================
     if(this.fbOtpResponse){ 
      this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
      let registration = new FbRegistrationModel(this.otpConcat ,1234, this.facebookModel.mobile,
      this.facebookModel.fullName , this.facebookModel.emailAddress , 'WEB',this.facebookResponse.id,
        "FB");
          this.headerService.registerUser(registration).subscribe(response =>{
            if(response && response["body"]){
              if(response["body"].status=="1"){
                this.loggedInUser = true;
                this.loginOtp = false;
                this.loginSuccess = true;
                this.registrationForm = false;
                localStorage.setItem('userLogin','true');
                localStorage.setItem('userId', response["body"].userDetails.user_id);
                localStorage.setItem('userName',response["body"].userDetails.name);
                this.registeredUserName = localStorage.getItem("userName");
                localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                this.creditPoints = localStorage.getItem('creditPoint');
                localStorage.setItem('referCode',response["body"].userDetails.referCode);
                localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
              }
              else{
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                otp.reset();
                }, 5000);
              }
            }
          })
        }
      else if(this.googleOtpResponse){  
          this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
          let registration = new FbRegistrationModel(this.otpConcat ,1234, this.googleModel.mobile,
          this.googleModel.fullName , this.googleModel.emailAddress , 'WEB',this.googleResponse.id,
            "GOOGLE_PLUS");
              this.headerService.registerUser(registration).subscribe(response =>{
                if(response && response["body"]){
                  if(response["body"].status=="1"){
                    this.loggedInUser = true;
                    this.loginOtp = false;
                    this.loginSuccess = true;
                    this.registrationForm = false;
                    localStorage.setItem('userLogin','true');
                    localStorage.setItem('userId', response["body"].userDetails.user_id);
                    localStorage.setItem('userName',response["body"].userDetails.name);
                    this.registeredUserName = localStorage.getItem("userName");
                    localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                    this.creditPoints = localStorage.getItem('creditPoint');
                    localStorage.setItem('referCode',response["body"].userDetails.referCode);
                    localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                    localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                    }
                  else{
                    this.showError(response["body"].message);
                    setTimeout(()=>{ 
                    otp.reset();
                    }, 5000);
                  }  
                }
              })
            }
    else{
      this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
      let registration = new RegitrataionModel(this.otpConcat ,1234, this.model.mobile,
        this.registerModel.fullName , this.registerModel.emailAddress , 'MOBILE');
        this.headerService.registerUser(registration).subscribe(response =>{
          if(response && response["body"]){
            if(response["body"].status=="1"){
            this.loggedInUser = true;
            this.loginOtp = false;
            this.loginSuccess = true;
            this.registrationForm = false;
            localStorage.setItem('userLogin','true');
            localStorage.setItem('userId', response["body"].userDetails.user_id);
            localStorage.setItem('userName',response["body"].userDetails.name);
            this.registeredUserName = localStorage.getItem("userName");
            localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
            this.creditPoints = localStorage.getItem('creditPoint');
            localStorage.setItem('referCode',response["body"].userDetails.referCode);
            localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
            localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
            }
            else{
              this.showError(response["body"].message);
              setTimeout(() => { 
                otp.reset();
              }, 5000);
            }
          }
        });
      }
    }
  }

  generatedOtp;
  onSubmitRegisterData(otp){
    if(this.facebookResponse){
      this.submittedRegisteredData = true;
      this.loginOtp = true;
    if(this.facebookModel.mobile){
      if(this.facebookModel.mobile=="0000000000"){
        this.showError('Invalid mobile number');
      }
      else if(this.facebookModel.mobile.length<10){
        this.showError('Mobile number should be 10 digit');
      }
      else{
        let requestOtp = new RequestOtpFBModel(this.facebookModel.mobile,this.facebookModel.fullName,
        this.facebookModel.emailAddress,"1234","1234","FB",this.facebookResponse.id,"WEB");
        this.headerService.requestOtpService(requestOtp).subscribe(response=>{
          if(response && response["body"]){
            this.generatedOtp = response["body"].otp;
              if(response["body"].status=="1"){
                this.fbOtpResponse = true;
                this.showSuccess(response["body"].message);
              }
              else{
                this.showError(response["body"].message);
              }
          }
        })
      }
    }
  else{
    let requestOtp = new RequestOtpModel(this.FbLoginResponse.mobile,"1234");
    this.headerService.requestOtpService(requestOtp).subscribe(response=>{
      if(response && response["body"]){
        this.generatedOtp = response["body"].otp;
          if(response["body"].status=="1"){
            this.fbOtpResponse = true;
            this.showSuccess(response["body"].message);
          }
          else{
            this.showError(response["body"].message);
          }
      }
    })
  }
}
else if(this.googleResponse){
  this.submittedRegisteredData = true;
  this.loginOtp = true;
  if(this.googleModel.mobile){
    if(this.googleModel.mobile=="0000000000"){
      this.showError('Invalid mobile number');
    }
    else if(this.googleModel.mobile.length<10){
      this.showError('Mobile number should be 10 digit');
    }
    else{
      let requestOtp = new RequestOtpFBModel(this.googleModel.mobile,this.googleModel.fullName,
      this.googleModel.emailAddress,"1234","1234","GOOGLE_PLUS",this.googleResponse.id,"WEB");
      this.headerService.requestOtpService(requestOtp).subscribe(response=>{
      if(response && response["body"]){
        this.generatedOtp = response["body"].otp;
        if(response["body"].status=="1"){
          this.googleOtpResponse = true;
          this.showSuccess(response["body"].message);
        }
        else{
          this.showError(response["body"].message);
        }
      }
    })
  }
}
else{
  let requestOtp = new RequestOtpModel(this.googleLoginResponse.mobile,"1234");
  this.headerService.requestOtpService(requestOtp).subscribe(response=>{
  if(response && response["body"]){
    this.generatedOtp = response["body"].otp;
    if(response["body"].status=="1"){
      this.googleOtpResponse = true;
      this.showSuccess(response["body"].message);
    }
    else{
      this.showError(response["body"].message);
    }
  }
  })
}
}
  else{
      this.submittedRegisteredData = true;
      this.loginOtp = true;
      let requestOtp = new RequestOtpModel(this.model.mobile , 1234);
      this.headerService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
          this.generatedOtp = response["body"].otp;  
          if(response["body"].status=="1"){
            this.showSuccess(response["body"].message);
          }   
          else{
            this.showError(response["body"].message);
          }     
        }
      })  
  }
}

public socialSignIn(socialPlatform : string) {
  let socialPlatformProvider;
  if(socialPlatform == "facebook"){
    socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  }else if(socialPlatform == "google"){
    socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  } 
  this.socialAuthService.signIn(socialPlatformProvider).then(
    (userData) => {
      this.socialLoginResponse = userData;
      if(userData.provider=="facebook"){
      this.facebookResponse = userData;
      let loginData = new LoginModel('FB', '1234', userData.id);
      this.headerService.UserloginService(loginData).subscribe(
      response=>{            
        if(response && response["body"]){
          this.FbLoginResponse = response["body"];
          if(response["body"].status=="0"){
            this.loginUser = false;
            this.registrationForm = true;
            this.facebookModel.fullName = userData.name;
            this.facebookModel.emailAddress = userData.email;              
          }
          else{
            this.checkUserExistence = true;
            this.loginUser = false;
            this.loginOtp = true;
            this.facebookId = true;
          }
        }}         
      )
    }
    else{
      this.googleResponse = userData;
      let loginData = new LoginModel('GOOGLE_PLUS', '1234', userData.id);
      this.headerService.UserloginService(loginData).subscribe(
      response=>{
        if(response && response["body"]){
          this.googleLoginResponse = response["body"];
          if(response["body"].status=="0"){
            this.loginUser = false;
            this.registrationForm = true;
            this.googleModel.fullName = userData.name;
            this.googleModel.emailAddress = userData.email;               
          }
          else{
            this.checkUserExistence = true;
            this.loginUser = false;
            this.loginOtp = true;
            this.googleId = true;
          }
        }
      }
      )
    }
    }
  );
}

movetoNext(current: KeyboardEvent, nextFieldID) {
  if (current.keyCode === 32 || current.keyCode == 9) {
      current.preventDefault();
  }
  else {
    document.getElementById(nextFieldID).focus();
  }
}

  // function called on Login Icon 
  loginModelIssue(){
    this.loginOtp = false;
    this.loginUser = true;
    this.model.mobile = '';
    this.disableButton = false;
    this.registrationForm = false;
    this.registerModel.fullName = '';
    this.registerModel.emailAddress = '';
  }

  // Logout function 
  logOutUser(){
    localStorage.clear();
    this.loggingUserOut();
  }
  loggingUserOut(){
    this.registeredUserName = localStorage.getItem('userName');
    this.checkUserLogin = localStorage.getItem('userLogin');
    this.loggedInUser = false;
  }
  // Resend Otp function
  resendOtp(otp:FormGroup){
    this.onSubmitRegisterData(otp);
  }

  // notification show 
  notificationClick(){
    this.notifyClicked = true;
    this.getNotification();
  }
  // get notification function
  getNotification(){
    let getNotificationModel = new GetNotification(localStorage.getItem('userId'));
    this.headerService.getNotificationService(getNotificationModel).subscribe(response=>{
     if(response && response["body"]){
       if(response["body"].status=="1"){
         this.notificationList = response["body"].list;
         this.notificationExist = true;
       }
       else if(response["body"].status=="0"){
         this.notificationExist = false;
       }     
     }
    })
  }
  // close notification section
  notifyClose(){
    this.notifyClicked = false;
  }
  proceedOrder(){
    // this.changeHeader.applyOpacity(false);
    if(localStorage.getItem('userLogin')){
      this.router.navigate(['/view-address']);
      window.location.reload(true);
    } 
    else{ 
      this.router.navigate(['/login']);
      window.location.reload(true);
    }
  }
}

