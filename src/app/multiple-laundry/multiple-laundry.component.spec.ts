import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleLaundryComponent } from './multiple-laundry.component';

describe('MultipleLaundryComponent', () => {
  let component: MultipleLaundryComponent;
  let fixture: ComponentFixture<MultipleLaundryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleLaundryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleLaundryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
