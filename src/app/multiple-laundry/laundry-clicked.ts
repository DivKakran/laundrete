import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class LaundryClicked {
    private laundryDetailsUrl = environment.baseApiURl + 'home/laundryDetails';
    private homeApiUrl = environment.baseApiURl + 'home/home';
    constructor(private http: HttpClient) {}

    // Calling Home Api to get Laundry List & Details
    // getLaundry(pincode){ 
    //     let pin = {'postcode': pincode};
    //     let headers = new HttpHeaders({ "Content-Type":"application/json"}).append('Authorization', 'Basic YWRtaW46MTIzNA==');
    //     return this.http.request(new HttpRequest(
    //         'POST',
    //         this.homeApiUrl,
    //         pin,
    //         {
    //             headers: headers
    //         }
    //     ));
    // }
    getLaundry(pincode ) {
        let _id = localStorage.getItem('userId');
        let _temp = {'postcode':pincode , 'user_id': _id};
        let headers = new HttpHeaders({ "Content-Type": "application/json" }).append('Authorization', 'Basic YWRtaW46MTIzNA==');
        return this.http.request(new HttpRequest(
            'POST',
            this.homeApiUrl,
            _temp,
            {
                headers: headers
            }
        ));
    }
    getServices(id , pincode) {
        let _temp = {'postcode':pincode , 'laundry_id':id};
        let headers = new HttpHeaders({ "Content-Type": "application/json" }).append('Authorization', 'Basic YWRtaW46MTIzNA==');
        return this.http.request(new HttpRequest(
            'POST',
            this.laundryDetailsUrl,
            _temp,
            {
                headers: headers
            }
        ));
    }

}