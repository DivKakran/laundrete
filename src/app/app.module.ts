import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import {AgmCoreModule} from '@agm/core';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { FilterPipe }from './header/filter.pipe';
import {TimeConvertorPipe} from './date-and-time/date-format-pipe'
import { ServicePageComponent } from './service-page/service-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { PricingComponent } from './pricing/pricing.component';
import { TermsAndConditionComponent } from './terms-and-condition/terms-and-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { FaqComponent } from './faq/faq.component';
import {ChangeHeaderService} from './changeHeaderService';
import { TrackOrderComponent } from './track-order/track-order.component';
import { MyOrderComponent } from './my-order/my-order.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {SocialLoginModule,AuthServiceConfig,GoogleLoginProvider,FacebookLoginProvider} from "angular5-social-login";
import { ViewAddressComponent } from './view-address/view-address.component';
import {LocationFilter} from './pricing/filter.pipe';
import { AddAddressComponent } from './add-address/add-address.component';
import { DateAndTimeComponent } from './date-and-time/date-and-time.component';
import { LoginComponent } from './login/login.component';
import { LoginOtpComponent } from './login-otp/login-otp.component';
import { LoginSuccessComponent } from './login-success/login-success.component';
import { PaymentOptionComponent } from './payment-option/payment-option.component';
import { RegistrationComponent } from './registration/registration.component';
import { ReferAndEarnComponent } from './refer-and-earn/refer-and-earn.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { MyHomeComponent } from './my-home/my-home.component';
import { MultipleLaundryComponent } from './multiple-laundry/multiple-laundry.component';
import { LaundryClicked } from './multiple-laundry/laundry-clicked';
import { NewHomeComponent } from './new-home/new-home.component';
import { AuthGuard } from "./guards/auth-guard.service";
import { BecomeAPartnerComponent } from './become-apartner/become-apartner.component';
import { BlogRedirectComponent } from './blog-redirect/blog-redirect.component';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { RedirectionWebComponent } from './redirection-web/redirection-web.component';
import { RedirectionWebFaqComponent } from './redirection-web-faq/redirection-web-faq.component';
import { RedirectionWebPrivacyComponent } from './redirection-web-privacy/redirection-web-privacy.component';  
export function getAuthServiceConfigs() {
  var fb =`https://www.facebook.com/v3.0/dialog/oauth?
  client_id: {"284358278986915"}
  &redirect_uri={"https://www.facebook.com/connect/login_success.html"}
 &state={adfdfdghghjkdfghfrtvccrtedfgcsss}`

  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("284358278986915")    
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("136366551553-hm38c4h7sqrqm5pmqfh8i0b7t5udkcf0.apps.googleusercontent.com")
        },
      ]
  );
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    FilterPipe,
    TimeConvertorPipe,
    ServicePageComponent,
    PricingComponent,
    TermsAndConditionComponent,
    PrivacyPolicyComponent,
    FaqComponent,
    TrackOrderComponent,
    MyOrderComponent,
    UserProfileComponent,
    ViewAddressComponent,
    LocationFilter,
    AddAddressComponent,
    DateAndTimeComponent,
    LoginComponent,
    LoginOtpComponent,
    LoginSuccessComponent,
    PaymentOptionComponent,
    RegistrationComponent,
    ReferAndEarnComponent,
    ThankYouComponent,
    MyHomeComponent,
    MultipleLaundryComponent,
    NewHomeComponent,
    BecomeAPartnerComponent,
    BlogRedirectComponent,
    RedirectionWebComponent,
    RedirectionWebFaqComponent,
    RedirectionWebPrivacyComponent
  ],
  imports:[
    CommonModule,
    BrowserModule,
    NgtUniversalModule, 
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot({preventDuplicates: true,timeOut: 1000}), 
    AppRoutingModule,
    SocialLoginModule,
    Ng2ImgMaxModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCQ275l2nxna9WhbXnzCxsu6HQLKOWC2tY'
    })
  ],
  providers: [ChangeHeaderService,{
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs
  } , LaundryClicked , AuthGuard],
})
export class AppModule { }
