import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';


@Injectable()
export class HeaderService {
    private serviceAreasUrl = environment.baseApiURl + 'location/serviceArea';
    private locationByPostcodeUrl = environment.baseApiURl + 'location/locationbypostcode';
    private loginServiceUrl = environment.baseApiURl + 'login/login';
    private verifyOtpUrl = environment.baseApiURl + 'login/verifyotp';
    private requestOtp = environment.baseApiURl + 'login/requestotp';
    private registerServiceUrl = environment.baseApiURl + 'login/register';
    private subscriptionUrl = environment.baseApiURl + 'webpage/email';
    private scheduleFormUrl = environment.baseApiURl + 'Webpage/pickup_schedule';
    private getNotificationUrl = environment.baseApiURl + 'PushNotification/getNotificationList';
    private deleteNotificationUrl = environment.baseApiURl + 'PushNotification/deleteNotification';
    constructor(private http: HttpClient) { }

    checkServiceArea(pincode) {
        var formdata = new FormData();
        formdata.append("postcode", pincode);
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' })
        return this.http.request(new HttpRequest(
            'POST',
            this.serviceAreasUrl,
            formdata, {
                headers: headers
            }
        ))
    }
    checkLocationByPostcode(postcode) {
        var formdata = new FormData();
        formdata.append("postcode", postcode);
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.locationByPostcodeUrl,
            formdata,
            {
                headers: headers
            }
        ))
    }
    UserloginService(userData) {
        var formdata = new FormData();
        formdata.append("login_type", userData.login_type);
        formdata.append("uuid_value", "1234");
        formdata.append("login_value", userData.login_value);
        return this.http.request(new HttpRequest(
            'POST',
            this.loginServiceUrl,
            formdata
        ))
    }
    verifyOtpService(userOtp) {
        var formdata = new FormData();
        formdata.append("otp", userOtp.otp);
        formdata.append("login_type", userOtp.login_type);
        formdata.append("uuid_value", "1234");
        formdata.append("login_value", userOtp.login_value);
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.verifyOtpUrl,
            formdata,
            {
                headers: headers
            }
        ))
    }
    requestOtpService(requestOtpModel) {
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.requestOtp,
            requestOtpModel,
            {
                headers: headers
            }
        ))
    }
    registerUser(registerModel) {
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.registerServiceUrl,
            registerModel,
            {
                headers: headers
            }
        ))
    }

    subscribeUser(email) {
        return this.http.request(new HttpRequest(
            'POST',
            this.subscriptionUrl,
            email

        ))
    }

    scheduleFormService(scheduleData) {
        JSON.stringify(scheduleData);
        return this.http.request(new HttpRequest(
            'POST',
            this.scheduleFormUrl,
            scheduleData
        ))
    }

    getNotificationService(userId){
        let headers = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.getNotificationUrl,
            userId,
            {
                headers:headers
            }
        ))
    }

    deleteNotificationService(notificationDetail){
        let headers = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.deleteNotificationUrl,
            notificationDetail,
            {
                headers:headers
            }
        ))
    }
}