import { LOCAL_STORAGE , WINDOW} from '@ng-toolkit/universal';
import { Component, OnInit,OnChanges  , Inject} from '@angular/core';
import { HeaderService } from './headerService';
import {Router} from '@angular/router';
import { FilterPipe } from './filter.pipe';
import { Directive, forwardRef, Attribute, SimpleChanges, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { LocationByPostcode, LoginModel, VerifyOtpModel , RequestOtpModel , 
RegitrataionModel, SubscriptionFormModel, RequestOtpFBModel,FbRegistrationModel,ScheduleFormModel,
GetNotification,DeleteNotification } from './headerServicesModel';
import { HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {ChangeHeaderService} from '../changeHeaderService';
import {AuthService,FacebookLoginProvider,GoogleLoginProvider} from 'angular5-social-login';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [HeaderService]
})
export class HeaderComponent implements OnInit ,OnChanges{
  pincodeText = '';
  submitted = false;
  submittedLoginData = false;
  submittedRegisteredData  =false;
  sorryPopup = false;
  serviceAreas = false;
  serviceAreaResponse;
  serviceAreaError;
  locationByPostcodeResponse;
  locationByPostcodeError;
  verifyOtpResponse;
  verifyOtpError;
  searchText = "";
  model = { mobile: "" };
  popupLoginClicked = true;
  loginOtp = false;
  loginUser = true;
  registrationForm = false;
  loginSuccess = false;
  submittedOtp = false;
  userMobileNumber;
  disableButton = false;
  loggedInUser = false;
  firstHeader = true;
  notifyClicked = false;
  fbOtpResponse = false;
  googleOtpResponse = false;
  FbLoginResponse;
  googleLoginResponse;
  socialLoginResponse;

  disableButtonOtp = false;

  clickProceedButton = false;
  // registeredUserName = localStorage.getItem('userName');
  // checkUserLogin     = localStorage.getItem('userLogin');
  // creditPoints       = localStorage.getItem('creditPoint');

  registeredUserName = '';
  checkUserLogin     = '';
  creditPoints   = '';


  otp1 = '';
  otp2 = '';
  otp3 = '';
  otp4 = '';
  otp5 = '';
  check="asa";
  otpConcat = '';
  subscribeModel = {emailAddress:''};
  LoginResponse;
  LoginError;
  pincodeTextModel = { pincodeText: '' };
  registerModel = {fullName:'',emailAddress:''};
  checkUserExistence=false;
  submittedSubscribeData = false;
  cityName = "";
  facebookResponse;
  googleResponse;
  facebookModel = {fullName:'',emailAddress:'',mobile:'' };
  googleModel = {fullName:'',emailAddress:'',mobile:''};
  facebookId = false;
  googleId = false;
  retrieveServiceFromCartArray;
  retrieveItemsFromCartArray;
  displaySavedCartData;
  totalCloths;
  totalPrice;
  // alreadyComment='';
  notificationExist = false
  notificationList;
  constructor(@Inject(WINDOW) private window: Window, @Inject(LOCAL_STORAGE) private localStorage: any, private headerService: HeaderService,private changeHeader:ChangeHeaderService , 
    private toastr: ToastrService, private router:Router,private socialAuthService: AuthService) {
      this.changeHeader.updateCityName.subscribe((res) => {
        this.cityName = <string>res;
      });  
  }

  onSubmit(pincode) {
    this.submitted = true;
    if (pincode.pincodeText == "" || pincode.pincodeText == undefined) {
      this.sorryPopup = true;
    }
    if (pincode.pincodeText) {    
      let locationByPostcode = new LocationByPostcode(pincode.pincodeText);
      this.headerService.checkLocationByPostcode(pincode.pincodeText).subscribe(response => {
        this.locationByPostcodeResponse = response;
        if (response && response["body"]) {
          if(response["body"].status=="1"){
            this.cityName = response["body"].cityName;
            this.localStorage.setItem("cityName",this.cityName);
            this.localStorage.setItem("postcode" ,pincode.pincodeText);
           // this.cityName = localStorage.getItem("cityName");
           this.router.navigate(["/service-select"]);
          }
        else{
          this.sorryPopup = true;
        }
        }
      },
        (error) => {
          this.locationByPostcodeError = error;
        })
    }
  }

  sorryClose() {
    this.sorryPopup = false;
    this.serviceAreas = false;
  }

  chk_pincode(pincode) {}
  serviceAvail() {
    this.sorryPopup = false;
    this.serviceAreas = true;
    let data1 = "";
    this.headerService.checkServiceArea(data1).subscribe(response => {
      if (response && response["body"]) {
        this.serviceAreaResponse = response["body"].serviceList;
      }
    },
      (error) => { this.serviceAreaError = error; })

  }
  loginComplete;
  onSubmitLoginData(loginFormData:FormGroup) { 
    this.loginComplete = loginFormData;
    this.submittedLoginData = true;
    if(this.model.mobile=="0000000000" || this.model.mobile==""){
      this.showError('Mobile number is required');
    }
    else if(this.model.mobile.length>10 || this.model.mobile.length<10){
      this.showError('Mobile number should be 10 digit');
    }
    else{
    let loginData = new LoginModel('MOBILE', '1234', this.model.mobile);
    this.headerService.UserloginService(loginData).subscribe(response => {
      this.disableButton = true;
      this.LoginResponse = response;
      if (response && response["body"] ) { 
        if (response["body"].message == 'User Exists') {
          this.loginUser = false;
          this.loginOtp = true;
          this.checkUserExistence = true;
          this.changeHeader.changingUserExistance('true');
          this.userMobileNumber = response["body"].mobile;
          this.disableButton = false;
        }
        else{
          this.loginUser = false;
          this.registrationForm = true;
          this.disableButton = false;
        }
      }
    },
    )
    } 
  }
  closeLoginPopup(){
    // this.loginOtp = false;
    // this.loginUser = true;
    // console.log('loginFormData',this.loginComplete);
    // this.loginComplete.reset();    
  }
  loginModelIssue(){
    this.loginOtp = false;
    this.loginUser = true;
    this.model.mobile = '';
    this.disableButton = false;
    this.registrationForm = false;
    this.registerModel.fullName = '';
    this.registerModel.emailAddress = '';
  }
  

  homeHeader = true;
  pricingHeader = false;
  serviceHeader = false;
  dropdownHeader = false;
  processHeader = false;
  totalItems;
  rate;
  serviceName;
  serviceNameArray=[];
  serviceNameCheck;
  itemName;
  itemNameArray=[];
  itemNameCheck = true;
  serviceItemData;
  genderName;
  zeroCart;
  clothName;
  rateById;
  subCategoryList;
  itemListArrayForCart=[];
  objAlreadyPresent = true;
  typeOfServices = [];
  itemComment={};
  storeCommentsToShow = [];
  idService;
  ngOnInit() {
    this.cityName = localStorage.getItem("cityName");
    this.registeredUserName = localStorage.getItem('userName');
    this.checkUserLogin     = localStorage.getItem('userLogin');
    this.creditPoints       = localStorage.getItem('creditPoint'); 

    this.changeHeader.changingHeaderCredit.subscribe((res) => {
      this.creditPoints = <string>res;
    });

    this.changeHeader.removeObj.subscribe(res => {
      let obj = res;
      if(this.itemListArrayForCart.length>0){
        this.itemListArrayForCart.filter((res,i) => {
          if(res.id == obj["id"]){
            this.itemListArrayForCart.splice(i,1);
          }
        });
      }
    })
    this.changeHeader.data.subscribe(res =>{
    if(res["homeHeader"] !=undefined ){ 
      this.homeHeader = res["homeHeader"];
      this.pricingHeader = res["pricingHeader"];
      this.serviceHeader = res["serviceHeader"];
      this.dropdownHeader = res["dropdownHeader"];
      this.processHeader = res["processHeader"];
    }}
    );
    this.changeHeader.countItem.subscribe(res => { 
      this.totalItems = res;  
      this.itemNameArray.filter((res,i)=>{
        if(res.weight<=0){
          this.serviceNameArray=[]
          this.itemNameArray.splice(i,1);
          this.itemNameArray.filter((res)=>{
            this.serviceNameArray.push(res.service);
          })
        }
      })
    });
    this.changeHeader.totalRate.subscribe(res => {
      this.rate = res;     
    }) 
    this.changeHeader.serviceName.subscribe((response)=>{ 
      if(response == null){
        this.serviceNameArray = [];
      }
      if(response){
          this.serviceName = response["service_name"];
          this.genderName = response["name"];
          this.idService = response["service_id"];
          if(this.serviceNameArray.length > 0){
            this.serviceNameCheck = true;
            this.serviceNameArray.filter((res)=>{
              if(res == this.serviceName){
                this.serviceNameCheck = false;
              }
            });
            // creating comment array
            if(this.serviceNameCheck){
              this.serviceNameArray.push(this.serviceName);
              this.itemComment[this.serviceName]='';
              let obj = {[this.serviceName]:'' , comment:'',serviceId:'',serviceName:''};
              this.storeCommentsToShow.push(obj);
            }
          }
          else{
            // creating comment array
            this.serviceNameArray.push(this.serviceName);
            this.itemComment[this.serviceName]='';
            let obj = {[this.serviceName]:'' , comment:'',serviceId:'',serviceName:''};
            this.storeCommentsToShow.push(obj);          
          }
        }
    })
    this.changeHeader.ItemName.subscribe(res => {
      if(res == null){
        this.itemNameArray=[];
      }
      if(res){
        this.itemName = res;
        this.rateById = res["rate"];
        if(this.itemNameArray.length > 0){  
          this.itemNameCheck = true;
          this.itemNameArray.filter(response => {
            if(response["id"] == this.itemName["id"] && response["service"] == this.itemName["service"]){
              this.itemNameCheck = false;
            }
          });
          if(this.itemNameCheck){
            this.itemNameArray.push(this.itemName);
          }
        }
        else{
          this.itemNameArray.push(this.itemName);
        }
      }
    })
    this.changeHeader.subcategoryName.subscribe(res => {
      this.clothName = res["name"];
      this.subCategoryList = res;
    });
    this.changeHeader.showUserName.subscribe(res=>{
      this.registeredUserName = <string>res;
    })
    this.changeHeader.showCart.subscribe(res=>{
      this.displaySavedCartData = res;
    })
    this.changeHeader.serviceList.subscribe(res=>{
      this.retrieveServiceFromCartArray = res;
      if(res == undefined){ 
        this.retrieveServiceFromCartArray = JSON.parse(localStorage.getItem('serviceName'));
      
      }
    });
    this.changeHeader.itemList.subscribe(res=>{
      this.retrieveItemsFromCartArray = res; 
      if(res == undefined){
        this.retrieveItemsFromCartArray = JSON.parse(localStorage.getItem('itemList'))
      }
    })
    this.changeHeader.totalPrice.subscribe(res=>{
      this.totalPrice = res;
    })
    this.changeHeader.totalCloths.subscribe(res=>{
      this.totalCloths = res;
    });
  }
  movetoNext(current: KeyboardEvent, nextFieldID) {
    if (current.keyCode === 32 || current.keyCode == 9) {
        current.preventDefault();
    }
    else {
      document.getElementById(nextFieldID).focus();
    }
  }
  onSubmitOtp(otp:FormGroup) { 
    this.disableButtonOtp = true;
    if(this.checkUserExistence){
      if(this.facebookId){
      this.submittedOtp = true;
      // this.otpConcat = otp.otp1 + otp.otp2 + otp.otp3 + otp.otp4 + otp.otp5;
      this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
      let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "FB", "1234", this.facebookResponse.id);
      if (this.facebookResponse.id) {
          this.headerService.verifyOtpService(verifyOtpModel).subscribe(response => {
          this.verifyOtpResponse = response;
          if (response && response["body"]) {
            if (response["body"].status == '1') {
              this.localStorage.setItem('userLogin','true'); 
              this.loginOtp = false;
              this.loginSuccess = true;
              this.loggedInUser = true;
              this.changeHeader.getUserNameOnLogin(response["body"].userDetails.name);
              this.changeHeader.headerCredit(response["body"].userDetails.creditPoints);
              this.localStorage.setItem('userName',response["body"].userDetails.name);
              this.localStorage.setItem('userId', response["body"].userDetails.user_id);
              this.registeredUserName=this.localStorage.getItem('userName');
              this.localStorage.setItem('mobileNumber',response["body"].userDetails.mobile);
              this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
              this.creditPoints = this.localStorage.getItem('creditPoint');
              this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
              this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
              this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
            }
            if(response["body"].status == "0"){
              this.showError(response["body"].message);
              setTimeout(()=>{ 
                otp.reset();
              }, 5000);
            }
          }
        },
          (error) => { this.verifyOtpError = error}
        )
      }
      }
     else if(this.googleId){
        this.submittedOtp = true;
       // this.otpConcat = otp.otp1 + otp.otp2 + otp.otp3 + otp.otp4 + otp.otp5;
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "GOOGLE_PLUS", "1234", this.googleResponse.id);
        if (this.googleResponse.id) {
            this.headerService.verifyOtpService(verifyOtpModel).subscribe(response => {
            this.verifyOtpResponse = response;
            if (response && response["body"]) {
              if (response["body"].status == '1') {
                this.localStorage.setItem('userLogin','true');
                this.loginOtp = false;
                this.loginSuccess = true;
                this.loggedInUser = true;
                this.changeHeader.getUserNameOnLogin(response["body"].userDetails.name);
                this.changeHeader.headerCredit(response["body"].userDetails.creditPoints);
                this.localStorage.setItem('userName',response["body"].userDetails.name);
                this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                this.registeredUserName=this.localStorage.getItem('userName');
                this.localStorage.setItem('mobileNumber',response["body"].userDetails.mobile);
                this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                this.creditPoints = this.localStorage.getItem('creditPoint'); 
                this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
              }
              if(response["body"].status == "0"){
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                  otp.reset();
                }, 5000);
              }
            }
          },
            (error) => { this.verifyOtpError = error}
          )
        }
      }
      else{ 
        this.submittedOtp = true;
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "MOBILE", "1234", this.userMobileNumber);
        if (this.userMobileNumber) {
          this.headerService.verifyOtpService(verifyOtpModel).subscribe(response => { 
            this.verifyOtpResponse = response;
            if (response && response["body"]) {
              if (response["body"].status == '1') { 
                this.localStorage.setItem('userLogin','true');
                this.loginOtp = false;
                this.loginSuccess = true;
                this.loggedInUser = true;
                // sending name for multiple laundry 
                
                this.changeHeader.getUserNameOnLogin(response["body"].userDetails.name);
                this.changeHeader.headerCredit(response["body"].userDetails.creditPoints);

                this.localStorage.setItem('userName',response["body"].userDetails.name);
                this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                this.registeredUserName=this.localStorage.getItem('userName');
                this.localStorage.setItem('mobileNumber',response["body"].userDetails.mobile);
                this.localStorage.setItem('email',response["body"].userDetails.email);
                this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                this.creditPoints = this.localStorage.getItem('creditPoint');
              }
              if(response["body"].status == "0"){
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                  otp.reset();
             }, 5000);
              }
            }
          })
        }
      }
    }
    else{
     // this.fullRegistration();
    // ======================== Registration Process================================
     if(this.fbOtpResponse){
      this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
      let registration = new FbRegistrationModel(this.otpConcat ,1234, this.facebookModel.mobile,
      this.facebookModel.fullName , this.facebookModel.emailAddress , 'WEB',this.facebookResponse.id,
        "FB");
          this.headerService.registerUser(registration).subscribe(response =>{
            if(response && response["body"]){
              if(response["body"].status=="1"){
                this.loggedInUser = true;
                this.loginOtp = false;
                this.loginSuccess = true;
                this.registrationForm = false;
                this.localStorage.setItem('userLogin','true');
                this.changeHeader.getUserNameOnLogin(response["body"].userDetails.name);
                this.changeHeader.headerCredit(response["body"].userDetails.creditPoints);
                this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                this.localStorage.setItem('userName',response["body"].userDetails.name);
                this.registeredUserName = this.localStorage.getItem("userName");
                this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                this.creditPoints = this.localStorage.getItem('creditPoint');
                this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
              }
              else{
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                otp.reset();
                }, 5000);
              }
            }
          })
        }
      else if(this.googleOtpResponse){
          this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
          let registration = new FbRegistrationModel(this.otpConcat ,1234, this.googleModel.mobile,
          this.googleModel.fullName , this.googleModel.emailAddress , 'WEB',this.googleResponse.id,
            "GOOGLE_PLUS");
              this.headerService.registerUser(registration).subscribe(response =>{
                if(response && response["body"]){
                  if(response["body"].status=="1"){
                    this.loggedInUser = true;
                    this.loginOtp = false;
                    this.loginSuccess = true;
                    this.registrationForm = false;
                    this.changeHeader.getUserNameOnLogin(response["body"].userDetails.name);
                    this.changeHeader.headerCredit(response["body"].userDetails.creditPoints);
                    this.localStorage.setItem('userLogin','true');
                    this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                    this.localStorage.setItem('userName',response["body"].userDetails.name);
                    this.registeredUserName = this.localStorage.getItem("userName");
                    this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                    this.creditPoints = this.localStorage.getItem('creditPoint');
                    this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                    this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                    this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                    }
                  else{
                    this.showError(response["body"].message);
                    setTimeout(()=>{ 
                    otp.reset();
                    }, 5000);
                  }  
                }
              })
            }
    else{
      this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
      let registration = new RegitrataionModel(this.otpConcat ,1234, this.model.mobile,
        this.registerModel.fullName , this.registerModel.emailAddress , 'MOBILE');
        this.headerService.registerUser(registration).subscribe(response =>{
          if(response && response["body"]){
            if(response["body"].status=="1"){
              this.disableButtonOtp = false;
              this.loggedInUser = true;
              this.loginOtp = false;
              this.loginSuccess = true;
              this.registrationForm = false;
              this.localStorage.setItem('userLogin','true');

              this.changeHeader.getUserNameOnLogin(response["body"].userDetails.name);
              this.changeHeader.headerCredit(response["body"].userDetails.creditPoints);
              
              this.localStorage.setItem('userId', response["body"].userDetails.user_id);
              this.localStorage.setItem('userName',response["body"].userDetails.name);
              this.registeredUserName = this.localStorage.getItem("userName");
              this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
              this.creditPoints = this.localStorage.getItem('creditPoint');
              this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
              this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
              this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
              
            }
            else{
              this.showError(response["body"].message);
              this.disableButtonOtp = false;
              setTimeout(() => { 
                otp.reset();
              }, 5000);
            }
          }
        });
      }
    }
  }
  addStickClass = false;
  @HostListener("window:scroll", [])
  onWindowScroll() {
    let window = this.window.innerHeight;
    // alert(windowHeight);
    let position = document.documentElement.scrollTop + window;
    // alert(position);
    let client =  document.documentElement.scrollTop;
    // const scrollHeight = 695;
    // const initialHeight = 694;
    // if (position >= scrollHeight) {
    //   this.addStickClass = true;
    // }
    // else if (position <= initialHeight) {
    //   this.addStickClass = false;
    // }
    const scrollHeight = 79.34;
    const initialHeight = 79.33;
    if(client >= scrollHeight){
      this.addStickClass = true;
    }
    else if(client <= initialHeight){
      this.addStickClass = false;
    }
  }

  showError(Error) {
    this.toastr.error(Error);
  }
  showWarning(Error) {
    this.toastr.warning(Error);
  }
  loginSucessClose(){
     this.loginSuccess  = false;
  }
  generatedOtp;
  onSubmitRegisterData(otp){
      if(this.facebookResponse){
        this.submittedRegisteredData = true;
        this.loginOtp = true;
      if(this.facebookModel.mobile){
        if(this.facebookModel.mobile=="0000000000"){
          this.showError('Invalid mobile number');
        }
        else if(this.facebookModel.mobile.length<10){
          this.showError('Mobile number should be 10 digit');
        }
        else{
          let requestOtp = new RequestOtpFBModel(this.facebookModel.mobile,this.facebookModel.fullName,
          this.facebookModel.emailAddress,"1234","1234","FB",this.facebookResponse.id,"WEB");
          this.headerService.requestOtpService(requestOtp).subscribe(response=>{
            if(response && response["body"]){
              this.generatedOtp = response["body"].otp;
                if(response["body"].status=="1"){
                  this.fbOtpResponse = true;
                  this.showSuccess(response["body"].message);

                  this.changeHeader.getUserNameOnLogin(response["body"].userDetails.name);
                  this.changeHeader.headerCredit(response["body"].userDetails.creditPoints);
                  this.localStorage.setItem('userName',response["body"].userDetails.name);
                  this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                }
                else{
                  this.showError(response["body"].message);
                }
            }
          })
        }
      }
    else{
      let requestOtp = new RequestOtpModel(this.FbLoginResponse.mobile,"1234");
      this.headerService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
          this.generatedOtp = response["body"].otp;
            if(response["body"].status=="1"){
              this.fbOtpResponse = true;
              this.showSuccess(response["body"].message);
            }
            else{
              this.showError(response["body"].message);
            }
        }
      })
    }
  }
  else if(this.googleResponse){
    this.submittedRegisteredData = true;
    this.loginOtp = true;
    if(this.googleModel.mobile){
      if(this.googleModel.mobile=="0000000000"){
        this.showError('Invalid mobile number');
      }
      else if(this.googleModel.mobile.length<10){
        this.showError('Mobile number should be 10 digit');
      }
      else{
        let requestOtp = new RequestOtpFBModel(this.googleModel.mobile,this.googleModel.fullName,
        this.googleModel.emailAddress,"1234","1234","GOOGLE_PLUS",this.googleResponse.id,"WEB");
        this.headerService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
          this.generatedOtp = response["body"].otp;
          if(response["body"].status=="1"){
            this.googleOtpResponse = true;
            this.showSuccess(response["body"].message);
          }
          else{
            this.showError(response["body"].message);
          }
        }
      })
    }
  }
  else{
    let requestOtp = new RequestOtpModel(this.googleLoginResponse.mobile,"1234");
    this.headerService.requestOtpService(requestOtp).subscribe(response=>{
    if(response && response["body"]){
      this.generatedOtp = response["body"].otp;
      if(response["body"].status=="1"){
        this.googleOtpResponse = true;
        this.showSuccess(response["body"].message);
      }
      else{
        this.showError(response["body"].message);
      }
    }
    })
  }
  }
    else{
      this.submittedRegisteredData = true;
      this.loginOtp = true;
      let requestOtp = new RequestOtpModel(this.model.mobile , 1234);
      this.headerService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
          this.generatedOtp = response["body"].otp;  
          if(response["body"].status=="1"){
            this.showSuccess(response["body"].message);
          }   
          else{
            this.showError(response["body"].message);
          }     
        }
      });  
    }
}
    loggingUserOut(){
      this.registeredUserName = this.localStorage.getItem('userName');
      this.checkUserLogin = this.localStorage.getItem('userLogin');
      this.loggedInUser = false;
    }
    logOutUser(){
      this.localStorage.clear();
      this.loggingUserOut();
    }
    logOutFromService(){
      this.router.navigate(['/home']);
      if( this.router.navigate(['/home'])){
        this.localStorage.clear();
        this.loggedInUser = false;
      }    
    }

    notifyClose(){
      this.notifyClicked = false;
    }
    notificationClick(){
      this.notifyClicked = true;
      this.getNotification();
    }
    notifyHomeClick(){
      this.router.navigate(['/']);
      this.notifyClicked = false;
    }
    resendOtp(otp:FormGroup){
      this.onSubmitRegisterData(otp);
    }
    showSuccess(success) {
      this.toastr.success(success);
    }
    showSecondHeader(){
      this.firstHeader = false;
    }
    notifyDelete(notificationId){
      let _temp = {user_id:this.localStorage.getItem('userId') , notification_id:notificationId};
      this.headerService.deleteNotificationService(_temp).subscribe((res) => {
        if(res && res["body"] && res["body"].status==1){
          let getNotificationModel = new GetNotification(this.localStorage.getItem('userId'));
          this.headerService.getNotificationService(getNotificationModel).subscribe(response=>{
          if(response && response["body"]){
            if(response["body"].status=="1"){
              this.notificationList = response["body"].list;
              this.notificationExist = true;
            }
            else if(response["body"].status=="0"){
              this.notificationExist = false;
            }     
          }
          });
        }
      });
    }

    onSubmitSubscribe(emailAddress){ 
      this.submittedSubscribeData = true;
           let subscribeModelData = new SubscriptionFormModel(this.subscribeModel.emailAddress);
           this.headerService.subscribeUser(subscribeModelData).subscribe(response=>{
            if(response["body"] && response["body"]){
              if(response["body"].status=="0"){
              this.showError(response["body"].message);
            }
            else{
              this.showSuccess(response["body"].message);
            }}
          },
          )
    }
    
    serviceAreasPopup(postcode){
       this.serviceAreas = false;
       let locationByPostcode = new LocationByPostcode(postcode);
       this.headerService.checkLocationByPostcode(postcode).subscribe(response => {
          if (response && response["body"]) {
            this.cityName = response["body"].cityName;
            this.localStorage.setItem("cityName",this.cityName);
            this.localStorage.setItem("postcode" ,postcode);
            this.router.navigate(["/service-select"]);
          }
       })  
    }

    public socialSignIn(socialPlatform : string) { 
      let socialPlatformProvider;
      if(socialPlatform == "facebook"){ 
        socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
      }else if(socialPlatform == "google"){
        socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
      } 
      this.socialAuthService.signIn(socialPlatformProvider).then(
        (userData) => {
          this.socialLoginResponse = userData;
          if(userData.provider=="facebook"){
          this.facebookResponse = userData;
          let loginData = new LoginModel('FB', '1234', userData.id);
          this.headerService.UserloginService(loginData).subscribe(
          response=>{            
            if(response && response["body"]){
              this.FbLoginResponse = response["body"];
              if(response["body"].status=="0"){
                this.loginUser = false;
                this.registrationForm = true;
                this.facebookModel.fullName = userData.name;
                this.facebookModel.emailAddress = userData.email;              
              }
              else{
                this.checkUserExistence = true;
                this.loginUser = false;
                this.loginOtp = true;
                this.facebookId = true;
              }
            }}         
          )
        }
        else{
          this.googleResponse = userData;
          let loginData = new LoginModel('GOOGLE_PLUS', '1234', userData.id);
          this.headerService.UserloginService(loginData).subscribe(
          response=>{
            if(response && response["body"]){
              this.googleLoginResponse = response["body"];
              if(response["body"].status=="0"){
                this.loginUser = false;
                this.registrationForm = true;
                this.googleModel.fullName = userData.name;
                this.googleModel.emailAddress = userData.email;               
              }
              else{
                this.checkUserExistence = true;
                this.loginUser = false;
                this.loginOtp = true;
                this.googleId = true;
              }
            }
          }
          )
        }
        }
      );
    }
    ngOnChanges(changes: SimpleChanges){
    }
    servicePlaceOrder(){
      this.router.navigate(['/service-select']);
      this.window.location.reload(true);
    }
    commentClicked = false;
    commentIconservice;
    commentValueCheck = true;
    oneServiceCommentValue;
    // saving service type to recognise comment from object
    serviceType:any;
    commentClick(commentServiceOnClick){
      this.serviceType = commentServiceOnClick;
      this.commentIconservice = commentServiceOnClick;
      this.commentClicked = true;
      let container = document.querySelector('.modal-open');
      container.classList.add("comm-model");
    }
    commentCancel(){
      this.commentClicked = false;
      let container1 = document.querySelector('.modal-open');
      container1.classList.remove("comm-model");
    }
    showcart(item){      
      // this.changeHeader.applyOpacity(true);
    }
    scheduleFormModel = {fullName:'',mobile:'',email:'',city:'',serviceableArea:''};
    submittedSchedule = false;
    onSubmitScheduleForm(scheduleFormData:FormGroup){
      this.submittedSchedule = true;
      let scheduleModel = new ScheduleFormModel(this.scheduleFormModel.fullName,this.scheduleFormModel.mobile,
      this.scheduleFormModel.email,this.scheduleFormModel.city
     );
      this.headerService.scheduleFormService(scheduleModel).subscribe(response => {
        this.disableButton = true;
        if(response && response["body"]){
          if(response["body"].status=="1"){
            this.showSuccess(response["body"].message);
            scheduleFormData.reset();
            setTimeout(()=>{
              this.disableButton = false;
            },2000)
          }
          else{
            this.showError(response["body"].message);
            scheduleFormData.reset();
            setTimeout(()=>{
              this.disableButton = false;
            },2000)
          }
        }
      })
    }
    minusItem(item){
      if(item.weight > 1){
        item.weight--;
        // for deleting the item set count
        item.count--;
        this.totalItems--;
         // if plus set rate agai for cart
          this.rate -= 1 * item.rate;
          this.changeHeader.getRate(this.rate);
        // end of set rate if item plus
        this.changeHeader.incCount(this.totalItems);      
      }
      else{
        this.showWarning('Atleast one item should be there');
      }
    }
    plusItem(item){
      item.weight++;
      // for deleting the item set count
      item.count++;
      // if plus set rate again for cart
      this.rate += 1 * item.rate; 
      this.changeHeader.getRate(this.rate);
      // end of set rate if item plus
      this.totalItems++;
      this.changeHeader.incCount(this.totalItems);
    }
    txtComment:'';
    commentBoxArray= [];
    commentObject = {commentText:'',commentService:''};
    // get saved comment
    showComment(i){
      for(let key in this.itemComment){
        if(key == i){
          return this.itemComment[key];
        }
      }
    }
    // end getting saved coment
    // saving comment
    commentSave(comment){
      this.storeCommentsToShow.filter((r) =>{
        for(let key in r){
          if(key == this.serviceType){
            r['comment'] = comment;
            r['serviceId'] = this.idService;
            r['serviceName'] = this.serviceName;
          }
        }
      });
      // saving comment to show;
      if(comment!='' && comment!=undefined){
        if(this.commentIconservice){   
          this.commentObject = {commentText:comment,commentService:this.commentIconservice};
          this.commentBoxArray.push(this.commentObject);
            if(this.commentBoxArray){
              this.commentClicked = false;
            }
        } 
      }
      else{
        this.showWarning('Please Enter Comment...');
      }
    }
    ch=false;
    count=0
    deletingService = false;
    deleteOrder(item , service,index){
      this.changeHeader.setItemCountOnDelete(item);
      // set counter for total item
      this.rate -= item.count*item.rate; 
      this.totalItems -= item.count; 
      this.changeHeader.getRate(this.rate);
      this.changeHeader.set(this.totalItems);
      // this.itemNameArray.filter((res,i)=>{  
      //   if(res.id==item.id && res.service==item.service){
      //     this.itemNameArray.splice(i,1);      
      //   }        
      // });
      this.serviceNameArray.filter((serve , i)=>{
        this.itemNameArray.filter((res,i)=>{
         if(this.itemNameArray.length > 0 && this.serviceNameArray.length > 0){
            if(res.service[i] == serve[i] && res.id == item.id && res.service == item.service){
              this.itemNameArray.splice(i,1); 
              // delete service 
                this.serviceNameArray.filter((r , i) =>{
                  this.itemNameArray.filter((res , index) =>{
                    if(res.service == r && this.itemNameArray.length){
                      this.deletingService = true;
                    } else{
                      // this.deletingService = false;
                    }
                  });
                  if(!this.deletingService){
                    this.serviceNameArray.splice(i , 1);
                  }
                  this.deletingService = false;
                });
              // delete service
            }
          }
        })      
      })
    }
    proceedOrder(){
      this.clickProceedButton = true;
      // this.changeHeader.applyOpacity(false);
      localStorage.setItem('serviceName',JSON.stringify(this.serviceNameArray));
      localStorage.setItem('itemList',JSON.stringify(this.itemNameArray));
      localStorage.setItem('totalPrice',this.rate);
      localStorage.setItem('totalCloths',this.totalItems);
      localStorage.setItem('serviceComment',JSON.stringify(this.storeCommentsToShow));

      if(localStorage.getItem('userLogin')){
        // this.router.navigate(['/view-address']);
        window.location.href = 'view-address';
        // this.window.location.reload(true);
      } 
      else{ 
        window.location.href = 'login'
        // this.router.navigate(['/login']);
        // this.window.location.reload(true);
      }
    }
      cartItemClicked = false;
      cartItemClick(){
        this.cartItemClicked = true;
        let container = document.querySelector('.modal-open');
        container.classList.add("cart-open");
      }
      cartItemCancel(){
        this.cartItemClicked = false;
        let container1 = document.querySelector('.modal-open');
        container1.classList.remove("cart-open");
      }
      showCommentsDoneOnServices(item , i){
        for(let key in item){
          if(key == i){
            return true;
          }
        }
      }
      deselectOpacity(){
        // this.changeHeader.applyOpacity(false);
      }

      getNotification(){ 
        let getNotificationModel = new GetNotification(this.localStorage.getItem('userId'));
        this.headerService.getNotificationService(getNotificationModel).subscribe(response=>{
         if(response && response["body"]){
           if(response["body"].status=="1"){
             this.notificationList = response["body"].list;
             this.notificationExist = true;
           }
           else if(response["body"].status=="0"){
             this.notificationExist = false;
           }     
         }
        });
      }
  }



 