export class LocationByPostcode {
    postcode: any;
    constructor(postcode: any) {
        this.postcode = postcode;
    }
}

export class LoginModel {
    login_type: any;
    uuid_value: any;
    login_value: any;
    constructor(login_type: any, uuid_value: any, login_value: any) {
        this.login_type = login_type;
        this.uuid_value = uuid_value;
        this.login_value = login_value;
    }
}

export class VerifyOtpModel {
    otp: any;
    login_type: any;
    uuid_value: any;
    login_value: any;
    constructor(otp: any, login_type: any, uuid_value: any, login_value: any) {
        this.otp = otp;
        this.login_type = login_type;
        this.uuid_value = uuid_value;
        this.login_value = login_value;
    }
}
export class RequestOtpModel {
    mobile: any;
    uuid_value: any;
    constructor(mobile, uuid_value) {
        this.mobile = mobile;
        this.uuid_value = uuid_value;
    }
}
export class RequestOtpFBModel {
    mobile: any;
    name: any;
    email: any;
    uuid_value: any;
    uuid: any;
    login_type: any;
    social_id: any;
    device_type: any;
    constructor(mobile: any, name: any, email: any, uuid_value: any, uuid: any, login_type: any,
        social_id: any, device_type: any) {
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.uuid_value = uuid_value;
        this.uuid = uuid;
        this.login_type = login_type;
        this.social_id = social_id;
        this.device_type = device_type;

    }
}
export class RegitrataionModel {
    otp: any;
    uuid_value: any;
    mobile: any;
    name: any;
    email: any;
    login_type: any;
    constructor(otp, uuid_value, mobile, name, email, login_type) {
        this.otp = otp;
        this.uuid_value = uuid_value;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.login_type = login_type
    }
}
export class FbRegistrationModel {
    otp: any;
    uuid_value: any;
    mobile: any;
    name: any;
    email: any;
    device_type: any;
    social_id: any;
    login_type: any;
    constructor(otp: any, uuid_value: any, mobile: any, name: any, email: any, device_type: any,
        social_id: any, login_type: any) {
        this.otp = otp;
        this.uuid_value = uuid_value;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.device_type = device_type;
        this.social_id = social_id;
        this.login_type = login_type;
    }

}

export class SubscriptionFormModel {
    txt_email: any;
    constructor(txt_email: any) {
        this.txt_email = txt_email;
    }
}

export class ScheduleFormModel {
    txt_name: any;
    txt_tel: any;
    txt_email: any;
    txt_city: any;
    constructor(txt_name: any, txt_tel: any, txt_email: any, txt_city: any) {
        this.txt_name = txt_name;
        this.txt_tel = txt_tel;
        this.txt_email = txt_email;
        this.txt_city = txt_city;
    }
}

export class GetNotification{
    user_id:any;
    constructor(user_id:any){
        this.user_id = user_id;
    }
}

export class DeleteNotification{
    user_id:any;
    notification_id:any;
    constructor(user_id:any,notification_id:any){
        this.user_id = user_id;
        this.notification_id = notification_id;
    }
}