export class TrackOrder {
    user_id: any;
    order_id: any;
    constructor(user_id: any, order_id: any) {
        this.user_id = user_id;
        this.order_id = order_id;
    }
}

export class RecentOrderDetail {
    user_id: any;
    recent: any;
    constructor(user_id: any, recent: any) {
        this.user_id = user_id;
        this.recent = recent;
    }
}