import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import { TrackOrder, RecentOrderDetail } from './trackOrderModel';
import { TrackService } from './trackOrderService';
@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.css'],
  providers: [TrackService]
})
export class TrackOrderComponent implements OnInit {

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private header: ChangeHeaderService, private trackService: TrackService) { }
  headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: false, dropdownHeader: true };
  orderExist = true;
  trackOrderResponse;
  itemQuantity = 0;
  orderDetailResponse;
  trackOrderItems;
  totalItemPrice;
  grandTotal: number;
  cgstAmount;
  totalCgstAmount;
  sgstAmount;
  totalSgstAmount;
  igstAmount;
  totalIgstAmount;
  discountValue: Number;
  totalDiscountValue;
  totalPayableAmount;
  myOrderData;
  trackOrderId;
  processStatus = false;
  deliveryStatus = false;
  lastDeliverStatus = "Out For Delivery";
  trackRecentOrder() {
    if (this.localStorage.getItem('myOrderStatus') == "true" && this.localStorage.getItem('trackOrderId')) {
      let recentOrderData = new RecentOrderDetail(this.localStorage.getItem('userId'), 0);
      this.trackService.trackRecentOrder(recentOrderData).subscribe(response => {
        if (response && response["body"] && response["body"].data.length > 0) {
          this.orderExist = true;
          this.myOrderData = response["body"].data;
          for (let i = 0; i < this.myOrderData.length; i++) {
            if (this.myOrderData[i].order_id == this.localStorage.getItem('trackOrderId')) {
              this.orderDetailResponse = this.myOrderData[i]
              this.trackOrderItems = this.orderDetailResponse.items;
              if(this.orderDetailResponse.order_status=="Processing"){
                this.processStatus = true;
              }
              else if(this.orderDetailResponse.order_status=='Out for Delivery' || this.orderDetailResponse.order_status=='Delivered' || this.orderDetailResponse.order_status=='Ready to Dispatch'){
                this.processStatus = true; 
                this.deliveryStatus = true;
                this.lastDeliverStatus = this.orderDetailResponse.order_status;
              }
              for (let j = 0; j < this.trackOrderItems.length; j++) {
                let totalNum = this.trackOrderItems[j].quantity;
                this.itemQuantity += Number(this.trackOrderItems[j].quantity);
              }
              // =================   Calculation start=========================//
              if (this.orderDetailResponse.deliveryCharges != '0.00') {
                if (this.orderDetailResponse.credit != '0' && this.orderDetailResponse.coupon_value == '0') {
                  if (this.orderDetailResponse.credit_before_gst == "1") {
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.orderDetailResponse.credit)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                  else if (this.orderDetailResponse.credit_before_gst == "0") {
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                    if (this.orderDetailResponse.IGST == "0.00") {
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                    else { 
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                  }
                }
                else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit == '0') {
                  if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                    let discountPercentage: number = this.orderDetailResponse.coupon_value;
                    let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                    let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                    if (discountUpto > cartValuePercentage) {
                      this.discountValue = cartValuePercentage;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                    else if (discountUpto < cartValuePercentage) {
                      this.discountValue = discountUpto;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                  }
                  else if (this.orderDetailResponse.coupon_type == "FLAT") {
                    this.discountValue = this.orderDetailResponse.coupon_value;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                }
                else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit != '0') {
                  if (this.orderDetailResponse.credit_before_gst == "1") {
                    if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                      let discountPercentage: number = this.orderDetailResponse.coupon_value;
                      let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                      let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                      if (discountUpto > cartValuePercentage) {
                        this.discountValue = cartValuePercentage;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                      }
                      else if (discountUpto < cartValuePercentage) {
                        this.discountValue = discountUpto;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                      }
                    }
                    else if (this.orderDetailResponse.coupon_type == "FLAT") {
                      this.discountValue = this.orderDetailResponse.coupon_value;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                  }
                  else if (this.orderDetailResponse.credit_before_gst == "0") {
                    if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                      let discountPercentage: number = this.orderDetailResponse.coupon_value;
                      let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                      let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                      if (discountUpto > cartValuePercentage) {
                        this.discountValue = cartValuePercentage;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                        if (this.orderDetailResponse.IGST == "0.00") {
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                        else {
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                      }
                      else if (discountUpto < cartValuePercentage) {
                        this.discountValue = discountUpto;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                        if (this.orderDetailResponse.IGST == "0.00") { 
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                        else {
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                      }
                    }
                    else if (this.orderDetailResponse.coupon_type == "FLAT") {
                      this.discountValue = this.orderDetailResponse.coupon_value;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                      if (this.orderDetailResponse.IGST == "0.00") { 
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      else {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                    }
                  }
                }
                else {
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges)
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
              }
              else if (this.orderDetailResponse.deliveryCharges == '0.00') {
                if (this.orderDetailResponse.credit != '0' && this.orderDetailResponse.coupon_value == '0') {
                  if (this.orderDetailResponse.credit_before_gst == "1") {
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.orderDetailResponse.credit)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                  else if (this.orderDetailResponse.credit_before_gst == "0") {
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                    if (this.orderDetailResponse.IGST == "0.00") {
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                    else {
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                  }
                }
                else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit == '0') {
                  if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                    let discountPercentage: number = this.orderDetailResponse.coupon_value;
                    let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                    let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                    if (discountUpto > cartValuePercentage) {
                      this.discountValue = cartValuePercentage;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                    else if (discountUpto < cartValuePercentage) {
                      this.discountValue = discountUpto;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                  }
                  else if (this.orderDetailResponse.coupon_type == "FLAT") {
                    this.discountValue = this.orderDetailResponse.coupon_value;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                }
                else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit != '0') {
                  if (this.orderDetailResponse.credit_before_gst == "1") {
                    if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                      let discountPercentage: number = this.orderDetailResponse.coupon_value;
                      let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                      let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                      if (discountUpto > cartValuePercentage) {
                        this.discountValue = cartValuePercentage;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                      }
                      else if (discountUpto < cartValuePercentage) {
                        this.discountValue = discountUpto;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                      }
                    }
                    else if (this.orderDetailResponse.coupon_type == "FLAT") {
                      this.discountValue = this.orderDetailResponse.coupon_value;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                  }
                  // ---------------------------- Credit before GST 0----------------------------------------//
                  else if (this.orderDetailResponse.credit_before_gst == "0") {
                    if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                      let discountPercentage: number = this.orderDetailResponse.coupon_value;
                      let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                      let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                      if (discountUpto > cartValuePercentage) {
                        this.discountValue = cartValuePercentage;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                        if (this.orderDetailResponse.IGST == "0.00") {
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                        else {
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                      }
                      else if (discountUpto < cartValuePercentage) {
                        this.discountValue = discountUpto;
                        this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                        this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
                        this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                        this.totalCgstAmount = this.cgstAmount.toFixed(2)
                        this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                        this.totalSgstAmount = this.sgstAmount.toFixed(2)
                        this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                        this.totalIgstAmount = this.igstAmount.toFixed(2)
                        if (this.orderDetailResponse.IGST == "0.00") {
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                        else {
                          this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                        }
                      }
                    }
                    else if (this.orderDetailResponse.coupon_type == "FLAT") {
                      this.discountValue = this.orderDetailResponse.coupon_value;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                      if (this.orderDetailResponse.IGST == "0.00") {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      else {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                    }
                  }
                }
                else {
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price)
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
              }
              // =================== Calculation End===========================//             
              this.localStorage.setItem('myOrderStatus', "false");
              this.localStorage.setItem('trackOrderId', "");
            }
          }
        }
      })
    }

    // ======================== Recent 1===================================================//
    else { 
      let recentOrderData = new RecentOrderDetail(this.localStorage.getItem('userId'), 1);
      this.trackService.trackRecentOrder(recentOrderData).subscribe(response => {
        if (response && response["body"]) { 
          if (response["body"].data.length > 0) {
            this.orderExist = true;
            this.trackOrderResponse = response["body"].data;
            this.orderDetailResponse = response["body"].data[0];
            for (let i = 0; i < response["body"].data.length; i++) {
              this.trackOrderItems = response["body"].data[i].items;
              for (let j = 0; j < this.trackOrderItems.length; j++) {
                let totalNum = this.trackOrderItems[j].quantity;
                this.itemQuantity += Number(this.trackOrderItems[j].quantity);
              }
            }
            if(this.orderDetailResponse.order_status=="Processing"){ 
              this.processStatus = true;
            }
            else if(this.orderDetailResponse.order_status=='Out for Delivery' || this.orderDetailResponse.order_status=='Delivered' || this.orderDetailResponse.order_status=='Ready to Dispatch'){
              this.processStatus = true; 
              this.deliveryStatus = true; 
              this.lastDeliverStatus = this.orderDetailResponse.order_status;
            }
            //  =--------------------- Credit before GST 1----------------------------------//
            if (this.orderDetailResponse.deliveryCharges != '0.00') { 
              if (this.orderDetailResponse.credit != '0' && this.orderDetailResponse.coupon_value == '0') {
                if (this.orderDetailResponse.credit_before_gst == "1") {
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.orderDetailResponse.credit)
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
                // ---------------------------- Credit before GST 0----------------------------------------//
                else if (this.orderDetailResponse.credit_before_gst == "0") {
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges)
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                  if (this.orderDetailResponse.IGST == "0.00") {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                  }
                  else {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                  }
                }
              }
              else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit == '0') {
                if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                  let discountPercentage: number = this.orderDetailResponse.coupon_value;
                  let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                  let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                  if (discountUpto > cartValuePercentage) { 
                    this.discountValue = cartValuePercentage;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = ((this.grandTotal * this.orderDetailResponse.IGST) / 100).toFixed(2);
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                  else if (discountUpto < cartValuePercentage) {
                    this.discountValue = discountUpto;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                }
                else if (this.orderDetailResponse.coupon_type == "FLAT") {
                  this.discountValue = this.orderDetailResponse.coupon_value;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
              }
              else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit != '0') {
                if (this.orderDetailResponse.credit_before_gst == "1") {
                  if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                    let discountPercentage: number = this.orderDetailResponse.coupon_value;
                    let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                    let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                    if (discountUpto > cartValuePercentage) {
                      this.discountValue = cartValuePercentage;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                    else if (discountUpto < cartValuePercentage) {
                      this.discountValue = discountUpto;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                  }
                  else if (this.orderDetailResponse.coupon_type == "FLAT") {
                    this.discountValue = this.orderDetailResponse.coupon_value;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                }
                // ---------------------------- Credit before GST 0----------------------------------------//
                else if (this.orderDetailResponse.credit_before_gst == "0") {
                  if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                    let discountPercentage: number = this.orderDetailResponse.coupon_value;
                    let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                    let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                    if (discountUpto > cartValuePercentage) {
                      this.discountValue = cartValuePercentage;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                      if (this.orderDetailResponse.IGST == "0.00") {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      else {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      //  this.totalPayableAmount = Number(this.grandTotal) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                    }
                    else if (discountUpto < cartValuePercentage) {
                      this.discountValue = discountUpto;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                      if (this.orderDetailResponse.IGST == "0.00") {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      else {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      //  this.totalPayableAmount = Number(this.grandTotal) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                    }
                  }
                  else if (this.orderDetailResponse.coupon_type == "FLAT") {
                    this.discountValue = this.orderDetailResponse.coupon_value;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                    if (this.orderDetailResponse.IGST == "0.00") {
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                    else {
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                    //  this.totalPayableAmount = Number(this.grandTotal) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                  }
                }
              }
              else {
                this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges)
                this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }
            }
            else if (this.orderDetailResponse.deliveryCharges == '0.00') { 
              if (this.orderDetailResponse.credit != '0' && this.orderDetailResponse.coupon_value == '0') {
                if (this.orderDetailResponse.credit_before_gst == "1") { 
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.orderDetailResponse.credit) + Number(this.orderDetailResponse.express_Delivery); 
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
                // ---------------------------- Credit before GST 0----------------------------------------//
                else if (this.orderDetailResponse.credit_before_gst == "0") {
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.express_Delivery);
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                  if (this.orderDetailResponse.IGST == "0.00") {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                  }
                  else {
                    this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                  }
                  // this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
                }
              }
              else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit == '0') {
                if (this.orderDetailResponse.coupon_type == "PERCENTAGE") { 
                  let discountPercentage: number = this.orderDetailResponse.coupon_value;
                  let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                  let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                  if (discountUpto > cartValuePercentage) {
                    this.discountValue = cartValuePercentage;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) + Number(this.orderDetailResponse.express_Delivery);
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                  else if (discountUpto < cartValuePercentage) {
                    this.discountValue = discountUpto;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) + Number(this.orderDetailResponse.express_Delivery);
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                }
                else if (this.orderDetailResponse.coupon_type == "FLAT") {
                  this.discountValue = this.orderDetailResponse.coupon_value;
                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                  this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) + Number(this.orderDetailResponse.express_Delivery);
                  this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
                  this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
                  this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                  this.totalIgstAmount = this.igstAmount.toFixed(2)
                }
              }
              else if (this.orderDetailResponse.coupon_value != '0' && this.orderDetailResponse.credit != '0') {
                if (this.orderDetailResponse.credit_before_gst == "1") {
                  if (this.orderDetailResponse.coupon_type == "PERCENTAGE") { 
                    let discountPercentage: number = this.orderDetailResponse.coupon_value;
                    let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;

                    let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                    
                    if (discountUpto > cartValuePercentage) { 
                      this.discountValue = cartValuePercentage;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2);

                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - 
                      Number(this.discountValue) - Number(this.orderDetailResponse.credit) +  Number(this.orderDetailResponse.express_Delivery);
                      // alert(this.grandTotal);   
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = ((this.grandTotal * this.orderDetailResponse.IGST) / 100).toFixed(2);
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                    else if (discountUpto < cartValuePercentage) {
                      this.discountValue = discountUpto;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) -
                       Number(this.orderDetailResponse.credit) + Number(this.orderDetailResponse.express_Delivery);
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                    }
                  }
                  else if (this.orderDetailResponse.coupon_type == "FLAT") {
                    this.discountValue = this.orderDetailResponse.coupon_value;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) -
                     Number(this.discountValue) - Number(this.orderDetailResponse.credit) + Number(this.orderDetailResponse.express_Delivery);
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                  }
                }
                // ---------------------------- Credit before GST 0----------------------------------------//
                else if (this.orderDetailResponse.credit_before_gst == "0") {
                  if (this.orderDetailResponse.coupon_type == "PERCENTAGE") {
                    let discountPercentage: number = this.orderDetailResponse.coupon_value;
                    let discountUpto: number = this.orderDetailResponse.coupon_discount_upto;
                    let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price) * discountPercentage) / 100;
                    if (discountUpto > cartValuePercentage) {
                      this.discountValue = cartValuePercentage;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) + Number(this.orderDetailResponse.express_Delivery)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                      if (this.orderDetailResponse.IGST == "0.00") { 
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      else {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      //  this.totalPayableAmount = Number(this.grandTotal) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                    }
                    else if (discountUpto < cartValuePercentage) {
                      this.discountValue = discountUpto;
                      this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                      this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
                      this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                      this.totalCgstAmount = this.cgstAmount.toFixed(2)
                      this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                      this.totalSgstAmount = this.sgstAmount.toFixed(2)
                      this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                      this.totalIgstAmount = this.igstAmount.toFixed(2)
                      if (this.orderDetailResponse.IGST == "0.00") {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      else {
                        this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                      }
                      //  this.totalPayableAmount = Number(this.grandTotal)  - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                    }
                  }
                  else if (this.orderDetailResponse.coupon_type == "FLAT") {
                    this.discountValue = this.orderDetailResponse.coupon_value;
                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue) + Number(this.orderDetailResponse.express_Delivery)
                    this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
                    this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
                    this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                    this.totalIgstAmount = this.igstAmount.toFixed(2)
                    if (this.orderDetailResponse.IGST == "0.00") {
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalCgstAmount) + Number(this.totalSgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                    else {
                      this.totalPayableAmount = Number(this.grandTotal) + Number(this.totalIgstAmount) - Number(this.orderDetailResponse.credit)
                    }
                    //  this.totalPayableAmount = Number(this.grandTotal)  - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
                  }
                }
              }
              else { 
                this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.express_Delivery); 
                this.cgstAmount = (this.grandTotal * this.orderDetailResponse.CGST) / 100;
                this.totalCgstAmount = this.cgstAmount.toFixed(2)
                this.sgstAmount = (this.grandTotal * this.orderDetailResponse.SGST) / 100;
                this.totalSgstAmount = this.sgstAmount.toFixed(2)
                this.igstAmount = (this.grandTotal * this.orderDetailResponse.IGST) / 100;
                this.totalIgstAmount = this.igstAmount.toFixed(2)
              }
            }
          }
          else {
            this.orderExist = false;
          }
        }
      });
    }
  }






  // trackSingleOrder(){
  //   let trackOrderData = new TrackOrder(localStorage.getItem('userId'),localStorage.getItem('trackOrderId'))
  //   this.trackService.trackSingleOrderService(trackOrderData).subscribe(response=>{
  //     console.log('single order response',response);
  //     if(response && response["body"]){
  //       if(response["body"].data.length > 0){
  //        this.orderExist = true;
  //        this.trackOrderResponse = response["body"].data;

  //        this.orderDetailResponse = response["body"].payment_details;
  //        this.orderDetailResponse.order_id = localStorage.getItem('trackOrderId')
  //        console.log('this.orderDetailResponse',this.orderDetailResponse);
  //        localStorage.setItem('myOrderStatus',"false");
  //        localStorage.setItem('trackOrderId',"");
  //        for(let i=0; i < response["body"].data.length;i++){
  //          this.trackOrderItems = response["body"].data[i].items;
  //          console.log('=================================================',this.trackOrderItems);
  //          for(let j=0; j < this.trackOrderItems.length;j++){
  //            let totalNum = this.trackOrderItems[j].quantity;        
  //            this.itemQuantity +=Number(this.trackOrderItems[j].quantity);
  //          }
  //        }

  //        if(this.orderDetailResponse.deliveryCharges!='0.00'){
  //           if(this.orderDetailResponse.credit!='0' && this.orderDetailResponse.coupon_value=='0'){
  //             if(this.orderDetailResponse.credit_before_gst=="1"){
  //             this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.orderDetailResponse.credit)
  //             this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //             this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //             this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //             this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //             this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //             this.totalIgstAmount = this.igstAmount.toFixed(2)
  //             }

  //             else if(this.orderDetailResponse.credit_before_gst=="0"){
  //               this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges)
  //               this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //               this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //               this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //               this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //               this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //               this.totalIgstAmount = this.igstAmount.toFixed(2)
  //               if(this.orderDetailResponse.IGST=="0.00"){
  //                 this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //               }
  //               else{
  //                 this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //               }
  //             }
  //           }
  //           else if(this.orderDetailResponse.coupon_value!='0' && this.orderDetailResponse.credit=='0'){
  //              if(this.orderDetailResponse.coupon_type=="PERCENTAGE"){
  //               let discountPercentage:number = this.orderDetailResponse.coupon_value;
  //               let discountUpto:number = this.orderDetailResponse.coupon_discount_upto;
  //                 let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price)*discountPercentage)/100;
  //                 if(discountUpto > cartValuePercentage){
  //                   this.discountValue = cartValuePercentage;
  //                   this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                   this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
  //                   this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                   this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                   this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                   this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                   this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                   this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                 }
  //                 else if(discountUpto < cartValuePercentage){
  //                   this.discountValue = discountUpto;
  //                   this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                   this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
  //                   this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                   this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                   this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                   this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                   this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                   this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                 }  

  //              }
  //              else if(this.orderDetailResponse.coupon_type=="FLAT"){
  //               this.discountValue = this.orderDetailResponse.coupon_value;
  //               this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //               this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
  //               this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //               this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //               this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //               this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //               this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //               this.totalIgstAmount = this.igstAmount.toFixed(2)
  //              }
  //           }
  //           else if(this.orderDetailResponse.coupon_value!='0' && this.orderDetailResponse.credit!='0'){
  //             if(this.orderDetailResponse.credit_before_gst=="1"){
  //              if(this.orderDetailResponse.coupon_type=="PERCENTAGE"){
  //               let discountPercentage:number = this.orderDetailResponse.coupon_value;
  //               let discountUpto:number = this.orderDetailResponse.coupon_discount_upto;
  //                 let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price)*discountPercentage)/100;
  //                 if(discountUpto > cartValuePercentage){
  //                   this.discountValue = cartValuePercentage;
  //                   this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                   this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
  //                   this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                   this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                   this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                   this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                   this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                   this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                 }
  //                 else if(discountUpto < cartValuePercentage){
  //                   this.discountValue = discountUpto;
  //                   this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                   this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
  //                   this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                   this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                   this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                   this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                   this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                   this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                 }  

  //              }
  //              else if(this.orderDetailResponse.coupon_type=="FLAT"){
  //               this.discountValue = this.orderDetailResponse.coupon_value;
  //               this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //               this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
  //               this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //               this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //               this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //               this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //               this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //               this.totalIgstAmount = this.igstAmount.toFixed(2)
  //              }
  //             } 

  //             else if(this.orderDetailResponse.credit_before_gst=="0"){
  //               if(this.orderDetailResponse.coupon_type=="PERCENTAGE"){
  //                let discountPercentage:number = this.orderDetailResponse.coupon_value;
  //                let discountUpto:number = this.orderDetailResponse.coupon_discount_upto;
  //                  let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price)*discountPercentage)/100;
  //                  if(discountUpto > cartValuePercentage){
  //                    this.discountValue = cartValuePercentage;
  //                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
  //                    this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                    this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                    this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                    this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                    if(this.orderDetailResponse.IGST=="0.00"){
  //                     this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //                   }
  //                   else{
  //                     this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //                   }

  //                  }
  //                  else if(discountUpto < cartValuePercentage){
  //                    this.discountValue = discountUpto;
  //                    this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                    this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue)
  //                    this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                    this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                    this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                    this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                    this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                    this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                    if(this.orderDetailResponse.IGST=="0.00"){
  //                     this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //                   }
  //                   else{
  //                     this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //                   }

  //                  }  

  //               }
  //               else if(this.orderDetailResponse.coupon_type=="FLAT"){
  //                this.discountValue = this.orderDetailResponse.coupon_value;
  //                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges) - Number(this.discountValue) 
  //                this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                if(this.orderDetailResponse.IGST=="0.00"){
  //                 this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //               }
  //               else{
  //                 this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //               }

  //               }
  //              } 
  //           }
  //           else{
  //             this.grandTotal = Number(this.orderDetailResponse.total_item_price) + Number(this.orderDetailResponse.deliveryCharges)
  //             this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //             this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //             this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //             this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //             this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //             this.totalIgstAmount = this.igstAmount.toFixed(2)
  //           }

  //        }
  //        else if(this.orderDetailResponse.deliveryCharges=='0.00'){
  //         if(this.orderDetailResponse.credit!='0' && this.orderDetailResponse.coupon_value=='0'){
  //           if(this.orderDetailResponse.credit_before_gst=="1"){
  //           this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.orderDetailResponse.credit)
  //           this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //           this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //           this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //           this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //           this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //           this.totalIgstAmount = this.igstAmount.toFixed(2)
  //           }

  //           else if(this.orderDetailResponse.credit_before_gst=="0"){
  //             this.grandTotal = Number(this.orderDetailResponse.total_item_price)
  //             this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //             this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //             this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //             this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //             this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //             this.totalIgstAmount = this.igstAmount.toFixed(2)
  //             if(this.orderDetailResponse.IGST=="0.00"){
  //               this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //             }
  //             else{
  //               this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //             }

  //           }
  //         }
  //         else if(this.orderDetailResponse.coupon_value!='0'  && this.orderDetailResponse.credit=='0'){
  //           if(this.orderDetailResponse.coupon_type=="PERCENTAGE"){
  //            let discountPercentage:number = this.orderDetailResponse.coupon_value;
  //            let discountUpto:number = this.orderDetailResponse.coupon_discount_upto;
  //              let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price)*discountPercentage)/100;
  //              if(discountUpto > cartValuePercentage){
  //                this.discountValue = cartValuePercentage;
  //                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
  //                this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                this.totalIgstAmount = this.igstAmount.toFixed(2)
  //              }
  //              else if(discountUpto < cartValuePercentage){
  //                this.discountValue = discountUpto;
  //                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
  //                this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                this.totalIgstAmount = this.igstAmount.toFixed(2)
  //              }             
  //           }
  //           else if(this.orderDetailResponse.coupon_type=="FLAT"){
  //            this.discountValue = this.orderDetailResponse.coupon_value;
  //            this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //            this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)
  //            this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //            this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //            this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //            this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //            this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //            this.totalIgstAmount = this.igstAmount.toFixed(2)
  //           }
  //         }
  //         else if(this.orderDetailResponse.coupon_value!='0' && this.orderDetailResponse.credit!='0'){
  //           if(this.orderDetailResponse.credit_before_gst=="1"){
  //           if(this.orderDetailResponse.coupon_type=="PERCENTAGE"){
  //            let discountPercentage:number = this.orderDetailResponse.coupon_value;
  //            let discountUpto:number = this.orderDetailResponse.coupon_discount_upto;
  //              let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price)*discountPercentage)/100;
  //              if(discountUpto > cartValuePercentage){
  //                this.discountValue = cartValuePercentage;
  //                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                this.grandTotal = Number(this.orderDetailResponse.total_item_price) - Number(this.discountValue)- Number(this.orderDetailResponse.credit)
  //                this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                this.totalIgstAmount = this.igstAmount.toFixed(2)
  //              }
  //              else if(discountUpto < cartValuePercentage){
  //                this.discountValue = discountUpto;
  //                this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                this.grandTotal = Number(this.orderDetailResponse.total_item_price)  - Number(this.discountValue)- Number(this.orderDetailResponse.credit)
  //                this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                this.totalIgstAmount = this.igstAmount.toFixed(2)
  //              }  

  //           }
  //           else if(this.orderDetailResponse.coupon_type=="FLAT"){
  //            this.discountValue = this.orderDetailResponse.coupon_value;
  //            this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //            this.grandTotal = Number(this.orderDetailResponse.total_item_price)  - Number(this.discountValue) - Number(this.orderDetailResponse.credit)
  //            this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //            this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //            this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //            this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //            this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //            this.totalIgstAmount = this.igstAmount.toFixed(2)
  //           }
  //         }

  //           else if(this.orderDetailResponse.credit_before_gst=="0"){
  //             if(this.orderDetailResponse.coupon_type=="PERCENTAGE"){
  //              let discountPercentage:number = this.orderDetailResponse.coupon_value;
  //              let discountUpto:number = this.orderDetailResponse.coupon_discount_upto;
  //                let cartValuePercentage = (Number(this.orderDetailResponse.total_item_price)*discountPercentage)/100;
  //                if(discountUpto > cartValuePercentage){
  //                  this.discountValue = cartValuePercentage;
  //                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                  this.grandTotal = Number(this.orderDetailResponse.total_item_price)  - Number(this.discountValue)
  //                  this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                  this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                  this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                  this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                  if(this.orderDetailResponse.IGST=="0.00"){
  //                   this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //                 }
  //                 else{
  //                   this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //                 }

  //                }
  //                else if(discountUpto < cartValuePercentage){
  //                  this.discountValue = discountUpto;
  //                  this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //                  this.grandTotal = Number(this.orderDetailResponse.total_item_price)  - Number(this.discountValue)
  //                  this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //                  this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //                  this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //                  this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //                  this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //                  this.totalIgstAmount = this.igstAmount.toFixed(2)
  //                  if(this.orderDetailResponse.IGST=="0.00"){
  //                   this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //                  }
  //                 else{
  //                   this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //                 }

  //                }  

  //             }
  //             else if(this.orderDetailResponse.coupon_type=="FLAT"){
  //              this.discountValue = this.orderDetailResponse.coupon_value;
  //              this.totalDiscountValue = Number(this.discountValue).toFixed(2)
  //              this.grandTotal = Number(this.orderDetailResponse.total_item_price)  - Number(this.discountValue) 
  //              this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //              this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //              this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //              this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //              this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //              this.totalIgstAmount = this.igstAmount.toFixed(2)
  //              if(this.orderDetailResponse.IGST=="0.00"){
  //               this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalCgstAmount)+Number(this.totalSgstAmount)-Number(this.orderDetailResponse.credit)
  //             }
  //             else{
  //               this.totalPayableAmount = Number(this.grandTotal)+ Number(this.totalIgstAmount)-Number(this.orderDetailResponse.credit)
  //             }

  //             }
  //            } 
  //        }
  //         else{
  //           this.grandTotal = Number(this.orderDetailResponse.total_item_price)
  //           this.cgstAmount = (this.grandTotal*this.orderDetailResponse.CGST)/100;
  //           this.totalCgstAmount = this.cgstAmount.toFixed(2)
  //           this.sgstAmount = (this.grandTotal*this.orderDetailResponse.SGST)/100;
  //           this.totalSgstAmount = this.sgstAmount.toFixed(2)
  //           this.igstAmount = (this.grandTotal*this.orderDetailResponse.IGST)/100;
  //           this.totalIgstAmount = this.igstAmount.toFixed(2)
  //         }  
  //        }
  //       }
  //       else{
  //         this.orderExist = false;
  //         localStorage.setItem('myOrderStatus',"false");
  //         localStorage.setItem('trackOrderId',"");
  //       }
  //     }
  //   })
  // }

  // myOrderIdDetail(){
  //   if(localStorage.getItem('myOrderStatus')=="true"){
  //     let recentOrderData = new RecentOrderDetail(localStorage.getItem('userId'),0);
  //     this.trackService.trackRecentOrder(recentOrderData).subscribe(response => {
  //       console.log('recent 0',response);
  //       if(response && response["body"] && response["body"].data.length>0){
  //         let myOrderData = response["body"].data;
  //         for(let i=0; i < myOrderData.length;i++){
  //           if(myOrderData[i].order_id == localStorage.getItem('trackOrderId')){
  //             let wholeData = myOrderData[i]
  //             console.log('new values required', wholeData);
  //             for(let i=0; i < wholeData.length;i++){
  //               let trackOrderData = response["body"].wholeData[i].items;
  //               for(let j=0; j < trackOrderData.length;j++){
  //                 let totalNum = trackOrderData[j].quantity;        
  //                 this.itemQuantity +=Number(trackOrderData[j].quantity);

  //               }
  //             }
  //           }
  //         }
  //       }

  //     })
  //   } 


  // }



  ngOnInit() {
    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
    this.trackRecentOrder();

  }

}
