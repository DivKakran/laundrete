import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class TrackService {
    private orderItemDetailUrl = environment.baseApiURl + 'order/orderitemdetails';
    private ordersDetailUrl = environment.baseApiURl + 'order/orderdetails';

    constructor(private http: HttpClient) { }

    trackSingleOrderService(trackOrderData) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }).append('Authorization', 'Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.orderItemDetailUrl,
            trackOrderData,
            {
                headers: headers
            }
        ))
    }

    trackRecentOrder(orderDetail) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' }).append('Authorization', 'Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.ordersDetailUrl,
            orderDetail,
            {
                headers: headers
            }
        ))
    }
}