import { Title, Meta } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import {ChangeHeaderService} from '../changeHeaderService';
import {Router , ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  constructor(private ac: ActivatedRoute, private route: Router,private header:ChangeHeaderService , private title: Title , private meta: Meta) {
    this.title.setTitle("Faq - Launderette");
    this.meta.addTags([{ name:"description" ,content: 'Faq - Launderette' },
    { name:"keywords" ,content: 'Faq- Launderette' }]);
   }
  headerValue = {homeHeader:false, pricingHeader:true, serviceHeader:false ,dropdownHeader:false};
  ngOnInit() {
    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
    this.ac.queryParams.subscribe(res => {
      if(window.location.hash.substr(1) == "payment-4"){
        document.getElementById('payment-4').click();
      }
    });
  }
}
