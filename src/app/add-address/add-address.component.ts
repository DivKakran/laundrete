import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import { ToastrService } from 'ngx-toastr';
import { AddAddressModel,AddressDetails,Home,Work,Other,ViewAddressModel} from './addAddressModel';
import { AddressService} from './addAddressService';
import {Router , ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css'],
  providers: [AddressService]
})
export class AddAddressComponent implements OnInit {
  userProfileData;
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false, processHeader:true};
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private serviceHeaderChange:ChangeHeaderService,private toastr: ToastrService,
   private addressService:AddressService, private router:Router , private activatedRoute: ActivatedRoute) { }
  
  homeAddressModel={home_address:'',home_landmark:'',home_pincode:''};
  workAddressModel={work_address:'',work_landmark:'',work_pincode:''};
  otherAddressModel={other_address:'',other_landmark:'',other_pincode:''};
  addAddressFormSubmitted = false;
  showAddressOff='';
  checkValidations(val){
    if(this.homeAddressModel.home_address=='' && this.homeAddressModel.home_pincode!=''){
      this.showWarning('Please Enter Address for Home');
      return false;
    }
    else if(this.homeAddressModel.home_address!=='' && this.homeAddressModel.home_pincode==''){
      this.showWarning('Please Enter Pincode for Home');
      return false;
    }
    else if(this.workAddressModel.work_address=='' && this.workAddressModel.work_pincode!=''){
      this.showWarning('Please Enter Address for Work');
      return false;
    }
    else if(this.workAddressModel.work_address!=='' && this.workAddressModel.work_pincode==''){
      this.showWarning('Please Enter Pincode for Work');
      return false;
    }
    else if(this.otherAddressModel.other_address=='' && this.otherAddressModel.other_pincode!=''){
      this.showWarning('Please Enter Address for Other');
      return false;
    }
    else if(this.otherAddressModel.other_address!=='' && this.otherAddressModel.other_pincode==''){
      this.showWarning('Please Enter Pincode for Other');
      return false;
    }
    else if(this.homeAddressModel.home_pincode=='' && this.workAddressModel.work_pincode=='' &&
     this.otherAddressModel.other_pincode==''){
      this.showWarning('Please Enter Address to Proceed');
      return false;
    }else{
      return true;
    }
  }
  addAddressFieldsData(val){
    this.addAddressFormSubmitted = true;
    if(this.checkValidations(val)){
      let addressModel = new AddAddressModel(this.localStorage.getItem('userName'),new AddressDetails(
        new Home(this.homeAddressModel.home_pincode,this.homeAddressModel.home_landmark,
          this.homeAddressModel.home_address),new Work(this.workAddressModel.work_pincode,this.workAddressModel.work_landmark,
            this.workAddressModel.work_address),new Other(this.otherAddressModel.other_pincode,this.otherAddressModel.other_landmark,
              this.otherAddressModel.other_address)
        ),this.localStorage.getItem('userId'),this.localStorage.getItem('email'),this.localStorage.getItem('mobileNumber')
      );
      this.addressService.addressUpdateService(addressModel).subscribe(response=>{
        if(response&&response["body"]){
          if(response["body"].status=="1"){
            this.router.navigate(['/view-address']);
          }
          else{
            this.showError(response["body"].message);
          }
        }
      });
    }
  }
  getUserProfileService() {
    let userProfileModelData = new ViewAddressModel(this.localStorage.getItem("userId"));
    this.addressService.getUserProfile(userProfileModelData).subscribe(response => {
      if (response && response["body"]) {
        this.userProfileData = response["body"].addressDetails;
        this.homeAddressModel.home_address = response["body"].addressDetails.HOME.address;
        this.homeAddressModel.home_landmark = response["body"].addressDetails.HOME.landmark;
        this.homeAddressModel.home_pincode = response["body"].addressDetails.HOME.pincode;
        this.workAddressModel.work_address = response["body"].addressDetails.WORK.address;
        this.workAddressModel.work_landmark = response["body"].addressDetails.WORK.landmark;
        this.workAddressModel.work_pincode = response["body"].addressDetails.WORK.pincode;
        this.otherAddressModel.other_address = response["body"].addressDetails.OTHER.address;
        this.otherAddressModel.other_landmark = response["body"].addressDetails.OTHER.landmark;
        this.otherAddressModel.other_pincode = response["body"].addressDetails.OTHER.pincode;
      }
    })
  }
  changeShowAddressOff(val){
    this.showAddressOff=val;
  }
  showWarning(Error) {
    this.toastr.warning(Error);
  }
  showError(Error) {
    this.toastr.error(Error);
  }
  retrieveServiceFromCart = '';
  retrieveServiceFromCartArray;
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveItemsFromCart = '';
  retrieveItemsFromCartArray;
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);

  typeOfAddressEditing: any;
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.typeOfAddressEditing = res.type; 
      this.showAddressOff       = res.type;
    });  
    this.retrieveServiceFromCart = localStorage.getItem('serviceName');
    this.retrieveItemsFromCart = localStorage.getItem('itemList');
    this.retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
    this.retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
    this.serviceHeaderChange.changeHeader(this.headerValue);
    this.serviceHeaderChange.setGetACallCSS(false);
    if (this.localStorage.getItem("userId")) {
      this.getUserProfileService();
    }
    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCartArray);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCartArray);
    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
  }
}
