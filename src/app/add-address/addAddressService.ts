import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class AddressService {
    private addressUpdateUrl = environment.baseApiURl+'profile/addressupdate';
    private getProfileUrl = environment.baseApiURl + 'profile/profile';
    constructor(private http: HttpClient) { }

    addressUpdateService(addressUpdateData){
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' }).append('Content-Type','application/json')
       return this.http.request(new HttpRequest(
           'POST',
           this.addressUpdateUrl,
           addressUpdateData,
           {
               headers:headers
           }
       ))
    }
    getUserProfile(userId) {
        let headers = new HttpHeaders({ "Content-Type": "application/json" }).append('Authorization', 'Basic YWRtaW46MTIzNA==');
        return this.http.request(new HttpRequest(
            'POST',
            this.getProfileUrl,
            userId,
            {
                headers: headers
            }
        ))
    }

}