export class AddAddressModel{
    name:any;
    addressDetails:AddressDetails;
    user_id:any;
    email:any;
    mobile:any;
    constructor(name:any,addressDetails:AddressDetails,user_id:any,email:any,mobile:any){
        this.name=name;
        this.addressDetails=addressDetails;
        this.user_id=user_id;
        this.email=email;
        this.mobile=mobile;
    }
}
export class AddressDetails{
    HOME:Home;
    WORK:Work;
    OTHER:Other;
    constructor(HOME:Home,WORK:Work, OTHER:Other){
        this.HOME=HOME;
        this.WORK=WORK;
        this.OTHER=OTHER;
    }
}
export class Home{
    pincode:any;
    landmark:any;
    address:any;
    constructor(pincode:any,landmark:any,address:any){
        this.pincode=pincode;
        this.landmark=landmark;
        this.address=address;
    }
}
export class Work{
    pincode:any;
    landmark:any;
    address:any;
    constructor(pincode:any,landmark:any,address:any){
        this.pincode=pincode;
        this.landmark=landmark;
        this.address=address;
    }
}
export class Other{
    pincode:any;
    landmark:any;
    address:any;
    constructor(pincode:any,landmark:any,address:any){
        this.pincode=pincode;
        this.landmark=landmark;
        this.address=address;
    }
}
export class ViewAddressModel {
    user_id: any;
    constructor(user_id: any) {
        this.user_id = user_id;
    }
}