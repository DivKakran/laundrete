import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PricingComponent} from './pricing/pricing.component';
import { HomeComponent } from './home/home.component';
import { MyHomeComponent } from './my-home/my-home.component';
import { MultipleLaundryComponent } from './multiple-laundry/multiple-laundry.component';
import {TermsAndConditionComponent} from './terms-and-condition/terms-and-condition.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {FaqComponent} from './faq/faq.component';
import {ServicePageComponent} from './service-page/service-page.component';
import {TrackOrderComponent} from './track-order/track-order.component';
import {MyOrderComponent} from './my-order/my-order.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {LoginComponent} from './login/login.component';
import {LoginOtpComponent} from './login-otp/login-otp.component';
import {LoginSuccessComponent} from './login-success/login-success.component';
import {ViewAddressComponent} from './view-address/view-address.component';
import {AddAddressComponent} from './add-address/add-address.component';
import {RegistrationComponent} from './registration/registration.component';
import {DateAndTimeComponent} from './date-and-time/date-and-time.component';
import {ReferAndEarnComponent} from './refer-and-earn/refer-and-earn.component';
import {PaymentOptionComponent} from './payment-option/payment-option.component';
import {ThankYouComponent} from './thank-you/thank-you.component';
import {NewHomeComponent} from './new-home/new-home.component';
import {BecomeAPartnerComponent} from './become-apartner/become-apartner.component';
import {BlogRedirectComponent} from './blog-redirect/blog-redirect.component';
import {RedirectionWebComponent} from './redirection-web/redirection-web.component';
import {RedirectionWebFaqComponent} from './redirection-web-faq/redirection-web-faq.component';
import {RedirectionWebPrivacyComponent} from './redirection-web-privacy/redirection-web-privacy.component';

import {AuthGuard} from './guards/auth-guard.service';
const routes: Routes = [
  // { path: 'barkha-home', component: HomeComponent },
  // { path: '', component:MyHomeComponent},
  { path: '', data:{title:"Best Laundry & Dry cleaning service in Gurgaon, Delhi, Noida - Launderette" , metatags:{descripton:'Launderette is a Laundry & Dry-cleaning marketplace, providing on-demand laundry & dry cleaning service in your area - in 100+ locations across Delhi, Gurgaon, Noida, Mumbai, among others. We are the best online dry cleaning & laundry service provider, working with premium brands and 5-star hotels to deliver your clothes at a convenient time and place.' ,
  keywords:'Online laundry service in Gurgaon, best dry cleaners in Gurgaon, dry cleaners in Delhi, best dry cleaners in Delhi, online laundry Gurgaon, Online Laundry Service, Laundry in Noida, Dry cleaning service in Noida'}}, component:NewHomeComponent },

  { path : 'multiple-laundry',data:{title:"Find Multiple Laundry & Dry cleaning Store  - Launderette" , metatags:{descripton:'launderette - Find Multiple Laundry & Dry cleaning Store in your near locations' ,
  keywords:'Multiple laundry, Multiple Dry Cleaning store'}}, component:MultipleLaundryComponent , canActivate: [AuthGuard]},
  { path: 'pricing', component: PricingComponent },
  { path: 'terms-and-condition' , data:{title:"Terms and Condition - Launderette" , metatags:{descripton:'Terms and Condition - Launderette' ,
  keywords:'Terms and Condition - Launderette'}} , component:TermsAndConditionComponent},
  { path: 'privacy-policy' , data:{title:"Privacy Policy - Launderette" , metatags:{descripton:'Privacy Policy - Launderette' ,
  keywords:'Privacy Policy - Launderette'}} , component:PrivacyPolicyComponent},
  { path: 'faq' , data:{title:"Faq - Launderette" , metatags:{descripton:'Faq - Launderette' ,
  keywords:'Faq- Launderette'}} ,component:FaqComponent },
  { path: 'service-select', component:ServicePageComponent},
  { path: 'track-order', component:TrackOrderComponent},
  { path: 'my-order' , component:MyOrderComponent},
  { path: 'user-profile', component:UserProfileComponent},
  { path: 'login' , component:LoginComponent},
  { path: 'login-otp', component:LoginOtpComponent},
  { path: 'login-success',component:LoginSuccessComponent},
  { path: 'view-address', component:ViewAddressComponent},
  { path: 'add-address', component:AddAddressComponent},
  { path: 'login-detail', component:RegistrationComponent},
  { path: 'date-time' ,component:DateAndTimeComponent},
  { path: 'refer-earn',component:ReferAndEarnComponent},
  { path: 'payment-option', component:PaymentOptionComponent},
  { path: 'thank-you' , component:ThankYouComponent},
  { path: 'trackorder', component:TrackOrderComponent},
  { path:'become-a-partner' ,data:{title:"Become A laundry & Dry Cleaning Partner With Launderette" , metatags:{descripton:'Submit your query & Become A laundry & Dry Cleaning Partner With Launderette' ,
  keywords:'become a laundry partner'}} , component: BecomeAPartnerComponent},
  { path:'blog/:id' , component: BlogRedirectComponent} ,
  {path:'blog' , component: BlogRedirectComponent} , 
  {path:'terms.html' , component: RedirectionWebComponent},
  {path:'faq.html' , component:RedirectionWebFaqComponent},
  {path:'privacy.html' , component: RedirectionWebPrivacyComponent},
  { path: "**",redirectTo:""}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
