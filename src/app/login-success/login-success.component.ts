import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import {ChangeHeaderService} from '../changeHeaderService';
import {Router} from '@angular/router';
@Component({
  selector: 'app-login-success',
  templateUrl: './login-success.component.html',
  styleUrls: ['./login-success.component.css']
})
export class LoginSuccessComponent implements OnInit {
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false,processHeader:true};
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private serviceHeaderChange:ChangeHeaderService, private router:Router) { }

  loginSuccessfully(){
    localStorage.setItem('hideContinueButton' , "true");
    window.location.href='/view-address';
  }
  retrieveServiceFromCart='';
  // retrieveServiceFromCart = localStorage.getItem('serviceName');
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveItemsFromCart='';
  retrieveServiceFromCartArray;
  // retrieveItemsFromCart = localStorage.getItem('itemList');
  retrieveItemsFromCartArray;
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
  ngOnInit() {
    if(localStorage.getItem('hideContinueButton') == "true"){
      this.router.navigate(['service-select']);
    }
    this.retrieveServiceFromCart = localStorage.getItem('serviceName');
    this.retrieveItemsFromCart = localStorage.getItem('itemList');
    this.serviceHeaderChange.changeHeader(this.headerValue);
    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCartArray);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCartArray);
    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
    this.serviceHeaderChange.setGetACallCSS(false);
    this.retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
    this.retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
  }

}
