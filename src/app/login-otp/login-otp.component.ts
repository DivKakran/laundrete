import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import {VerifyOtpModel,RegitrataionModel,RequestOtpModel,FbRegistrationModel} from './loginOtpModel';
import {LoginOtpService} from './loginOtpService';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
import { ChangeHeaderService } from '../changeHeaderService';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-login-otp',
  templateUrl: './login-otp.component.html',
  styleUrls: ['./login-otp.component.css'],
  providers:[LoginOtpService]
})
export class LoginOtpComponent implements OnInit {
  submittedOtp = false;
  otpConcat = '';
  otp1 = '';
  otp2 = '';
  otp3 = '';
  otp4 = '';
  otp5 = '';
  userMobileNumber='';
  // userMobileNumber =  localStorage.getItem('mobileNumber');
  verifyOtpResponse;
  // registeredUserName = localStorage.getItem('userName');
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false,processHeader:true};
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private loginOtpService:LoginOtpService,private toastr: ToastrService,
  private router:Router,private serviceHeaderChange:ChangeHeaderService) { }

  movetoNext(current: KeyboardEvent, nextFieldID) {
    if (current.keyCode === 32 || current.keyCode == 9) {
        current.preventDefault();
    }
    else {
      document.getElementById(nextFieldID).focus();
    }
  }
  disableButtonOnClickSubmitOtp = false;
  onSubmitOtp(otp:FormGroup) {
    this.disableButtonOnClickSubmitOtp = true;
    if(this.localStorage.getItem('userExistence')){
      if(this.localStorage.getItem('facebookId')){
        this.submittedOtp = true;
       // this.otpConcat = otp.otp1 + otp.otp2 + otp.otp3 + otp.otp4 + otp.otp5;
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "FB", "1234", this.localStorage.getItem('userFacebookId'));
        if (this.localStorage.getItem('userFacebookId')) {
            this.loginOtpService.verifyOtpService(verifyOtpModel).subscribe(response => {
            this.verifyOtpResponse = response;
              if (response && response["body"]) {
                if (response["body"].status == '1') {
                  this.localStorage.setItem('userLogin','true');
                  this.localStorage.setItem('userName',response["body"].userDetails.name);
                  this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                  // this.registeredUserName=this.localStorage.setItem('FbNumber',response["body"].userDetails.mobile);
                  this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                  this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                  this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                  this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                  this.router.navigate(['/login-success']);
                }
                if(response["body"].status == "0"){
                  this.showError(response["body"].message);
                  setTimeout(()=>{ 
                    otp.reset();
                  }, 5000);
                }
              }
            })
        }
      }
      else if(this.localStorage.getItem('googleId')){
        this.submittedOtp = true;
        // this.otpConcat = otp.otp1 + otp.otp2 + otp.otp3 + otp.otp4 + otp.otp5;
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "GOOGLE_PLUS", "1234", this.localStorage.getItem('userGoogleId'));
        if (this.localStorage.getItem('userGoogleId')) {
          this.loginOtpService.verifyOtpService(verifyOtpModel).subscribe(response => {
          this.verifyOtpResponse = response;
            if (response && response["body"]) {
              if (response["body"].status == '1') {
                this.localStorage.setItem('userLogin','true');
                this.localStorage.setItem('userName',response["body"].userDetails.name);
                this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                // this.registeredUserName=this.localStorage.setItem('googleNumber',response["body"].userDetails.mobile);
                this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                this.router.navigate(['/login-success']);
              }
              if(response["body"].status == "0"){
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                  otp.reset();
                }, 5000);
              }
            }
          })
        }
      }
      else{
        this.submittedOtp = true;
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let verifyOtpModel = new VerifyOtpModel(this.otpConcat, "MOBILE", "1234",this.userMobileNumber);
        if (this.userMobileNumber) {
          this.loginOtpService.verifyOtpService(verifyOtpModel).subscribe(response => {
            this.verifyOtpResponse = response;
            if (response && response["body"]) {
              if (response["body"].status == '1') { 
                this.localStorage.setItem('userLogin','true');
                this.localStorage.setItem('userName',response["body"].userDetails.name);
                this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                this.router.navigate(['/login-success']);
              }
              if(response["body"].status == "0"){
                this.disableButtonOnClickSubmitOtp = false;
                this.showError(response["body"].message);
                setTimeout(()=>{ 
                  otp.reset();
                }, 5000);
              }
            }
          })
        }
      }
    }
    else{
      if(this.localStorage.getItem('fbRegisteredUser')){
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let registration = new FbRegistrationModel(this.otpConcat ,1234, this.localStorage.getItem('fbUserMobileNumber'),
          this.localStorage.getItem('fbUserName') , this.localStorage.getItem('fbUserEmail') , 'WEB',this.localStorage.getItem('userFacebookId'),
          "FB");
            this.loginOtpService.registerUser(registration).subscribe(response =>{
              if(response && response["body"]){
                if(response["body"].status=="1"){
                  this.localStorage.setItem('userLogin','true');
                  this.localStorage.setItem('userId', response["body"].userDetails.user_id);
                  this.localStorage.setItem('userName',response["body"].userDetails.name);
                  this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
                  this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
                  this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
                  this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
                  this.router.navigate(['/login-success']);
                }
                else{
                  this.showError(response["body"].message);
                  setTimeout(()=>{ 
                  otp.reset();
                  }, 5000);
                }
              }
            })
        }
      else if(this.localStorage.getItem('googleRegisteredUser')){
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let registration = new FbRegistrationModel(this.otpConcat ,1234,this.localStorage.getItem('googleUserMobileNumber'),
        this.localStorage.getItem('googleUserName') , this.localStorage.getItem('googleUserEmail') , 'WEB',this.localStorage.getItem('userGoogleId'),
        "GOOGLE_PLUS");
        this.loginOtpService.registerUser(registration).subscribe(response =>{
          if(response && response["body"]){
            if(response["body"].status=="1"){
              this.localStorage.setItem('userLogin','true');
              this.localStorage.setItem('userId', response["body"].userDetails.user_id);
              this.localStorage.setItem('userName',response["body"].userDetails.name);
              this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
              this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
              this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
              this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
              this.router.navigate(['/login-success']);
              }
            else{
              this.showError(response["body"].message);
              setTimeout(()=>{ 
              otp.reset();
              }, 5000);
            }  
          }
        })
      }
      else{
        this.otpConcat = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
        let registration = new RegitrataionModel(this.otpConcat ,1234, this.localStorage.getItem('mobile'),
        this.localStorage.getItem('fullName') , this.localStorage.getItem('emailAddress') , 'MOBILE');
        this.loginOtpService.registerUser(registration).subscribe(response =>{
          if(response && response["body"]){
            if(response["body"].status=="1"){
            this.localStorage.setItem('userLogin','true');
            this.localStorage.setItem('userId', response["body"].userDetails.user_id);
            this.localStorage.setItem('userName',response["body"].userDetails.name);
            this.localStorage.setItem('creditPoint',response["body"].userDetails.creditPoints);
            this.localStorage.setItem('referCode',response["body"].userDetails.referCode);
            this.localStorage.setItem('referralAmountText',response["body"].userDetails.referralAmountText);
            this.localStorage.setItem('referralToLink',response["body"].userDetails.referralToLink);
            this.router.navigate(['/login-success']);
            }
            else{
              this.showError(response["body"].message);
              setTimeout(()=>{ 
              otp.reset();
              }, 5000);
            }
          }
        })
      }  
    }
  }
  showError(Error) {
    this.toastr.error(Error);
  }
  showSuccess(Error) {
    this.toastr.success(Error);
  }
  resendOtp(otp:FormGroup){
    setTimeout(()=>{ 
      otp.reset();
      }, 5000);
    
    if(this.localStorage.getItem('userFacebookId')){
      let requestOtp = new RequestOtpModel(this.localStorage.getItem('facebookResponseMobile'),"1234");
      this.loginOtpService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
            if(response["body"].status=="1"){
              this.localStorage.setItem('fbRegisteredUser','true');
              this.showSuccess(response["body"].message);
            }
            else{
              this.showError(response["body"].message);
            }
        }
      })
    }
    else if(this.localStorage.getItem('userGoogleId')){
      let requestOtp = new RequestOtpModel(this.localStorage.getItem('googleResponseMobile'),"1234");
      this.loginOtpService.requestOtpService(requestOtp).subscribe(response=>{
      if(response && response["body"]){
        if(response["body"].status=="1"){
          this.localStorage.setItem('googleRegisteredUser','true');
          this.showSuccess(response["body"].message);
        }
        else{
          this.showError(response["body"].message);
        }
      }
      })
    }
    else{
      let requestOtp = new RequestOtpModel(this.localStorage.getItem('mobileNumber') , 1234);
      this.loginOtpService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
          if(response["body"].status=="1"){ 
            this.showSuccess(response["body"].message);
            // localStorage.setItem('mobile',this.registerModel.mobile);
            // localStorage.setItem('emailAddress',this.registerModel.emailAddress);
            // localStorage.setItem('fullName',this.registerModel.fullName);
          }   
          else{
            this.showError(response["body"].message);
          }     
        }
      })
    } 
  }
  retrieveServiceFromCart='';
  // retrieveServiceFromCart = localStorage.getItem('serviceName');
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveServiceFromCartArray;
  retrieveItemsFromCart='';
  // retrieveItemsFromCart = localStorage.getItem('itemList');
  retrieveItemsFromCartArray;
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
  ngOnInit() {
    if(localStorage.getItem('userLogin')){
      this.router.navigate(['service-select']);
    }
    this.userMobileNumber =  localStorage.getItem('mobileNumber');
    this.retrieveServiceFromCart = localStorage.getItem('serviceName');
    this.retrieveItemsFromCart = localStorage.getItem('itemList');
    this.serviceHeaderChange.changeHeader(this.headerValue);
    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCartArray);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCartArray);
    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
    this.serviceHeaderChange.setGetACallCSS(false);


    // this.retrieveServiceFromCartArray = JSON.parse(localStorage.getItem('serviceName'));
    // this.retrieveItemsFromCartArray = JSON.parse(localStorage.getItem('itemList'));

    // this.retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
    // this.retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
  }

}
