import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest  } from '@angular/common/http';

@Injectable()
export class LoginOtpService{
    private verifyOtpUrl = environment.baseApiURl+'login/verifyotp';
    private registerServiceUrl = environment.baseApiURl + 'login/register';
    private requestOtp = environment.baseApiURl + 'login/requestotp';
    constructor(private http:HttpClient){}
    verifyOtpService(userOtp){
        var formdata = new FormData();
        formdata.append("otp" , userOtp.otp);
        formdata.append("login_type" , userOtp.login_type);
        formdata.append("uuid_value" , "1234");
        formdata.append("login_value" , userOtp.login_value);
        let headers = new HttpHeaders({'Authorization':'Basic YWRtaW46MTIzNA=='});
        return this.http.request(new HttpRequest(
            'POST',
            this.verifyOtpUrl,
            formdata,
            {
                headers:headers
            }
        ))

    }
    registerUser(registerModel) {
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.registerServiceUrl,
            registerModel,
            {
                headers: headers
            }
        ))
    }
    requestOtpService(requestOtpModel) {
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.requestOtp,
            requestOtpModel,
            {
                headers: headers
            }
        ))
    }
}    