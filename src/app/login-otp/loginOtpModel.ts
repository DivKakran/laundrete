export class VerifyOtpModel {
    otp: any;
    login_type: any;
    uuid_value: any;
    login_value: any;
    constructor(otp: any, login_type: any, uuid_value: any, login_value: any) {
        this.otp = otp;
        this.login_type = login_type;
        this.uuid_value = uuid_value;
        this.login_value = login_value;
    }
}
export class RegitrataionModel {
    otp: any;
    uuid_value: any;
    mobile: any;
    name: any;
    email: any;
    login_type: any;
    constructor(otp, uuid_value, mobile, name, email, login_type) {
        this.otp = otp;
        this.uuid_value = uuid_value;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.login_type = login_type
    }
}
export class RequestOtpModel {
    mobile: any;
    uuid_value: any;
    constructor(mobile, uuid_value) {
        this.mobile = mobile;
        this.uuid_value = uuid_value;
    }
}
export class FbRegistrationModel {
    otp: any;
    uuid_value: any;
    mobile: any;
    name: any;
    email: any;
    device_type: any;
    social_id: any;
    login_type: any;
    constructor(otp: any, uuid_value: any, mobile: any, name: any, email: any, device_type: any,
        social_id: any, login_type: any) {
        this.otp = otp;
        this.uuid_value = uuid_value;
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.device_type = device_type;
        this.social_id = social_id;
        this.login_type = login_type;
    }

}