import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject, AfterViewChecked} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import { SelectService } from './selectService';
import { HomeService, ServiceItem } from './serviceModel';
import { Router} from '@angular/router';
@Component({
  selector: 'app-service-page',
  templateUrl: './service-page.component.html',
  styleUrls: ['./service-page.component.css'],
  providers: [SelectService]
})
export class ServicePageComponent implements OnInit , AfterViewChecked{

  // selectedPostcode = localStorage.getItem("postcode");
  selectedPostcode = '';

  serviceList: any = [];
  serviceSelectData;
  serviceItemListData;
  serviceSubCategory = [];
  collapsed = false;
  selectedTab;
  totalItems = 0;
  duplicateHomeServiceArray = [];
  typeOfRate;
  applyOpacityBackground = false;
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private header: ChangeHeaderService, private selectService: SelectService,
     private router:Router) { 
     this.header.totalRate.subscribe(r =>{
       this.rate = <number>r;
     });
    //  Opacity css //
    //  this.header.opacity.subscribe((res) =>{
    //   this.applyOpacityBackground = res;
    //  });
     this.header.t.subscribe(r => {
       this.addNumber = <number>r;
     })
     this.header.setCount.subscribe((res) => {
      this.duplicateHomeServiceArray.filter((r , i) =>{
        for (let key in r){
          if(key == res["service"]){
            r[key].filter((r , i) =>{
              if(r.name == res['categoryName']){
                r.sub_category.filter((r ,i) =>{
                  if(r.name == res['subcategoryName']){
                    r.item_list.filter((r ,i)=>{
                      if(r.name == res['name']){
                        r.weight = 0;
                      }
                    });
                  }
                });
              }
            });
          }
        }
      });
      // end duplicate array filter
     });
   }
  headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: true, dropdownHeader: false,processHeader:false};

  _newTestLId = '';
  resolvingLaundryIssue(){
    if(localStorage.getItem('laundryPincodeList') == 'Premium'){
      this._newTestLId = '';
    }else{
      this._newTestLId = this.localStorage.getItem("laundryIdMain");
    }
    let _temp = { 
      postcode: this.localStorage.getItem("postcode") , //110064
      laundry_id: this._newTestLId
    };
    return _temp;
  }
  
  async serviceSelectService() { 
    if (this.selectedPostcode) { 
     let _temp = await this.resolvingLaundryIssue();
      // let HomeServiceModel = new HomeService(this.selectedPostcode);
      this.selectService.serviceHomeService(_temp).subscribe(response => {
        if (response && response["body"]) { 
          if(response["body"].status=="1"){ 
            this.localStorage.setItem('laundry_settings',JSON.stringify(response["body"].laundry_settings)); 
            this.localStorage.setItem('laundry_pincodes',response["body"].laundry_pincodes);
            this.serviceSelectData = response["body"].serviceList;
            this.serviceList = response["body"].serviceList;
            response["body"].serviceList.filter((res) => {
              // create duplicate array with same length of service
              let val = {};
              val[res.name] = [];
              this.duplicateHomeServiceArray.push(val);
            });

              this.selectedSubTab = 0;
              this.isCollapsed = false;
              this.serviceItem();
              this.selectedTab = 0;
            }
        }
      })
    }
  }
  newArray = [];
  dArray = [];
  openTabIfItemLengthOne:any;
  serviceItem() {
    if (this.serviceSelectData) {
      let serviceItemModel = new ServiceItem(this.serviceSelectData[0].id, this.selectedPostcode , localStorage.getItem("laundryIdMain"));
      this.selectService.serviceItemServive(serviceItemModel).subscribe(response => {
        if (response && response["body"]) {
          // open tab if one item
          if(response["body"].itemList[0].sub_category && response["body"].itemList[0].sub_category.length == 1){
            // alert(response["body"].itemList[0].sub_category[0].name);
            this.openTabIfItemLengthOne = response["body"].itemList[0].sub_category[0].name;
            
          }
          this.serviceItemListData = response["body"].itemList;
          this.duplicateHomeServiceArray.filter((res, i) => {
            for (let key in this.duplicateHomeServiceArray[i]) {
              if (key == this.serviceSelectData[0].name) {
                this.duplicateHomeServiceArray[i][key] = this.serviceItemListData;
                this.dArray = this.duplicateHomeServiceArray[i][key];
              }
            }
          });  
        
          for (var i = 0; i < this.serviceItemListData.length; i++) {
            this.serviceSubCategory[i] = response["body"].itemList[i].sub_category.length;
            let subCategoryRateType = response["body"].itemList[i].sub_category
            for(let j=0;j<subCategoryRateType.length;j++){
              let rateTypeOfSubcategory = subCategoryRateType[j].name;
              if(rateTypeOfSubcategory=="Select Laundry by Kg"){
                this.typeOfRate = 'PER_KG';
              }
            }
          }
        }
      })
    }
  }

  
  hiding = false;
  hide() {
    if (this.hiding == false) {
      this.hiding = true;
    } else {
      this.hiding = false;
    }
  }
  selectedSubTab;
  selectSubTab(serviceId, index, name, rateType) {
    // checking item length

    // chekingh item length end
    this.typeOfRate = rateType;
    this.selectedSubTab = index;
    let serviceItemModel = new ServiceItem(serviceId, this.selectedPostcode , localStorage.getItem("laundryIdMain"));
    this.selectService.serviceItemServive(serviceItemModel).subscribe(response => {
      if (response && response["body"]) {
        // checking item length
        if(response["body"].itemList[0].sub_category && response["body"].itemList[0].sub_category.length == 1){
          // alert(response["body"].itemList[0].sub_category[0].name);
          this.openTabIfItemLengthOne = response["body"].itemList[0].sub_category[0].name;    
        }
        // chekingh item length end
        this.serviceItemListData = response["body"].itemList;
        this.women = 1;
        this.duplicateHomeServiceArray.filter((res, i) => {
          for (let key in this.duplicateHomeServiceArray[i]) {
            if (key == name) {
              if (this.duplicateHomeServiceArray[i][key].length == 0) {
                this.duplicateHomeServiceArray[i][key] = this.serviceItemListData;
              }
              this.dArray = this.duplicateHomeServiceArray[i][key];
            }
          }
        });
        this.duplicateHomeServiceArray.filter((res, i) => {
          for (let key in this.duplicateHomeServiceArray[i]) {
            if (key == name) {
              this.dArray = this.duplicateHomeServiceArray[i][key];
            }
          }
        });
      }
    });
    // if(this.dArray && serviceId == 12){
    //   document.getElementById('check').click(); 
    // }
  }
  

  selectTab(index) {
    this.isCollapsed = true;
    this.selectedTab = index;
  }
  itemName;
  indexNum;
  sum;
  idByClick(item, i) {
    this.itemName = item;
    this.indexNum = i;
    this.sum = this.itemName + '-' + this.indexNum;
  }
  receive(id) {
  }
  women = 1;
  changeGenderStyle(item) {
    this.women = item;
  }

  addNumber = 0;
  removeNumber = 0;
  rate = 0;
  totalCountNumber = 0;
  addOrder(id, service, subCategory,rateType) {
    id.weight++;
    this.addNumber++;
    this.rate += +id.rate;
    this.header.incCount(this.addNumber);
    this.header.getRate(this.rate);
    id.count = id.weight;
    id.service = service.service_name;
    id.categoryName = service.name;
    id.subcategoryName = subCategory.name;
    id.subcategoryId = subCategory.id;
    id.serviceId = service.service_id;
    id.categoryId = service.id;
    if(id.service == "Laundry By Kg" || id.service == "Laundry By Kg Wash & Fold" || id.service == "Laundry By Kg Wash & Iron"){
      id.itemType = "PER_KG";
    }
    else{
      id.itemType = "PER_PIECE";
    }
    this.header.getServiceName(service);
    this.header.getSubcategoryItemList(id);
    this.header.getSubcategoryList(subCategory);
  }

  removeOrder(id) {
    this.header.removeObjectOnClick(id);
    if (id.weight > 0) {
      id.weight--;
      this.addNumber--;
      this.rate -= id.rate;
      this.header.decCount(this.addNumber);
      this.header.getRate(this.rate);
    }
    else {
      id = 0;
    }
  }
  isCollapsed = false;

  collapse(isCollapsed ) { 
    if (isCollapsed.t == true) {
      isCollapsed.t = false;
      this.isCollapsed = false;
    }
    else {
      isCollapsed.t = true;
      this.isCollapsed = true;
    }
  }

  ngOnInit() {
    
    this.selectedPostcode = localStorage.getItem("postcode");

    this.header.setGetACallCSS(false);
    this.header.incCount(0);
    this.header.getRate(0);
    this.header.getServiceName(null);
    this.header.getSubcategoryItemList(null);

    this.header.countItem.subscribe(res => {
      this.addNumber = <number>res;
    })
    this.header.changeHeader(this.headerValue);
    if (this.localStorage.getItem("postcode")) {
      this.serviceSelectService();
    }
    this.header.displaySavedCart(false);
    this.localStorage.removeItem('itemList');
    this.localStorage.removeItem('serviceName');
    this.localStorage.removeItem('totalPrice');
    this.localStorage.removeItem('totalCloths');
    this.localStorage.removeItem('serviceComment');
  }

  onDropDown = true;
  ngAfterViewChecked(){
    // if(this.onDropDown && document.getElementById('check')!=null){
    //   document.getElementById('check').click();
    //   this.onDropDown = false;
    // }
  }
}
