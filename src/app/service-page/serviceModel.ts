export class HomeService{
    postcode:any;
    constructor(postcode:any){
        this.postcode = postcode;
    }
}

export class ServiceItem{
    service_id:any;
    postcode:any;
    laundry_id:any;
    constructor(service_id:any,postcode:any , lId){
        this.service_id = service_id;
        this.postcode = postcode;
        this.laundry_id = lId;
    }
}