import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class SelectService {
    //private serviceHomeUrl = environment.baseApiURl + 'home/home';
    private serviceLaundryUrl = environment.baseApiURl + 'home/laundryDetails';
    private serviceItemUrl = environment.baseApiURl + 'serviceItems/service';

    constructor(private http: HttpClient) {
    }

        serviceHomeService(selectedPostcode) {
        let headers = new HttpHeaders({ "Content-Type": "application/json" }).append('Authorization', 'Basic YWRtaW46MTIzNA==');
        return this.http.request(new HttpRequest(
            'POST',
            this.serviceLaundryUrl,
            selectedPostcode,
            {
                headers: headers
            }
        ))
    }

    serviceItemServive(serviceItem) {
        var formData = new FormData();
        formData.append("service_id", serviceItem.service_id);
        formData.append("postcode", serviceItem.postcode);
        formData.append("laundry_id" , serviceItem.laundry_id)
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.serviceItemUrl,
            formData,
            {
                headers: headers
            }
        ))
    }
}