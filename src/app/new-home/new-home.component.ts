import { Title, Meta } from '@angular/platform-browser';
import { ChangeHeaderService } from './../changeHeaderService';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {HomeService} from  '../home/homeService';
import { HeaderService } from '../header/headerService';
import {Router} from '@angular/router';
import {LocationByPostcode} from '../header/headerServicesModel'

@Component({
  selector: 'app-new-home',
  templateUrl: './new-home.component.html',
  styleUrls: ['./new-home.component.css'] ,
  providers: [HomeService , HeaderService]
})
export class NewHomeComponent implements OnInit {
  baseImageUrl: any;
  sorryPopup = false;
  serviceAreas = false;
  requestCallDetail = {'fullName':'' , 'mobile':''};
  disableButton = false;
  linkButtonDeactivate = false;
  locationByPostcodeResponse: any;
  searchText: any = '';
  loadAPI: Promise<any>;
  
  headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: false, dropdownHeader: false,processHeader:false};
  constructor(private homeService:HomeService ,private toastr: ToastrService,
  private headerService: HeaderService , private router: Router , private changeHeader: ChangeHeaderService , private title: Title , private meta: Meta) {
      this.title.setTitle("Best Laundry & Dry cleaning service in Gurgaon, Delhi, Noida - Launderette");
      this.meta.addTags([{ name:"description" , content: 'Launderette is a Laundry & Dry-cleaning marketplace, providing on-demand laundry & dry cleaning service in your area - in 100+ locations across Delhi, Gurgaon, Noida, Mumbai, among others. We are the best online dry cleaning & laundry service provider, working with premium brands and 5-star hotels to deliver your clothes at a convenient time and place.' } ,
      {name:'keywords' , content: 'Online laundry service in Gurgaon, best dry cleaners in Gurgaon, dry cleaners in Delhi, best dry cleaners in Delhi, online laundry Gurgaon, Online Laundry Service, Laundry in Noida, Dry cleaning service in Noida'}]);
    }

  private loadScript() {
    var dynamicScripts = ["assets/js/custom.js" , "assets/js/slick.js"];
    for (var i = 0; i < dynamicScripts .length; i++) {
        let node = document.createElement('script');
        node.src = dynamicScripts [i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
  ngOnInit() {
    this.changeHeader.changeHeader(this.headerValue);
    this.loadAPI = new Promise((resolve) => {
      this.loadScript();
      resolve(true);
    });
    this.baseImageUrl = environment.imageUrl;
    let cTime = new Date().getTime();
  }
  showError(Error) {
    this.toastr.error(Error);
  }
  showSuccess(success) {
    this.toastr.success(success);
  }
  onSubmitRequestForm(){
    if(this.requestCallDetail.fullName=="" && this.requestCallDetail.mobile==""){
      this.showError('Please Enter Details');
    }
    else if(this.requestCallDetail.fullName=="" && this.requestCallDetail.mobile.length<10){
      this.showError('Please Enter Valid Mobile No.');
    }
    else if(this.requestCallDetail.fullName!="" && this.requestCallDetail.mobile.length<10){
      this.showError('Please Enter Valid Mobile No.');
    }
    else{
      // this.requestBtnText = "THANKS FOR CONNECTING!"
      this.disableButton = true;
      let requestModelData = {"mobile_number":this.requestCallDetail.mobile,"name":this.requestCallDetail.fullName };
      this.homeService.requestCallService(requestModelData).subscribe(response=>{
        if(response && response["body"]){
          if(response["body"].status=="1"){
            this.showSuccess(response["body"].message);
            // this.requestBtnText = "REQUEST A CALL";
            this.requestCallDetail.mobile = "";
            this.requestCallDetail.fullName = "";
            this.disableButton = false;
          }
          else{
            this.showError(response["body"].message);
            // this.requestBtnText = "REQUEST A CALL";
            this.requestCallDetail.mobile = "";
            this.requestCallDetail.fullName = "";
            this.disableButton = false;
          }
        }
      });
    }
  }

  placeOrder(number){
    // this.submitted = true;
    if (number == "" || number == undefined) {
      this.sorryPopup = true;
    } if (number) {    
      this.headerService.checkLocationByPostcode(number).subscribe(response => {
        this.locationByPostcodeResponse = response;
        if (response && response["body"]){
            if(response["body"].status=="1"){
              if(response["body"].laundaryList.length != 1){
                // removing data on click new pincode
                localStorage.removeItem('itemList');
                localStorage.removeItem('serviceName');
                localStorage.removeItem('totalPrice');
                localStorage.removeItem('totalCloths');
                localStorage.removeItem('serviceComment');

                localStorage.setItem("serviceArea" , "false");
                localStorage.setItem("postcode" , number);
                localStorage.setItem("cityName",response["body"].cityName);
                this.changeHeader.getUpdatedCityName(response["body"].cityName);
                window.location.href='/multiple-laundry';
                // this.router.navigate(["/multiple-laundry"]);
                // window.location.reload(true);
              }else{ 
                // removing data on click new pincode 
               
                localStorage.setItem('laundryTypeForGst' , "true"); 
               
                localStorage.removeItem('itemList');
                localStorage.removeItem('serviceName');
                localStorage.removeItem('totalPrice');
                localStorage.removeItem('totalCloths');
                localStorage.removeItem('serviceComment');

                localStorage.setItem("serviceArea" , "true");
                localStorage.setItem("cityName" , response["body"].cityName);
                localStorage.setItem("cityName",response["body"].cityName);
                localStorage.setItem("postcode" , number);
                localStorage.setItem('laundryPincodeList' , response["body"].laundaryList[0].type);

                localStorage.setItem("laundryIdForService" , response["body"].laundaryList[0].id );
                localStorage.setItem("laundryIdMain" , response["body"].laundaryList[0].id);

                if(response["body"].laundaryList[0].type == 'Premium'){
                  localStorage.setItem('forSlotId' , ''); 
                  localStorage.setItem('laundryIdMain' ,'');
                }else{ 
                  localStorage.setItem('forSlotId' , response["body"].laundaryList[0].id);
                  localStorage.setItem('typeOfLaundryOnSelect' , 'standard');
                }

                this.router.navigate(["/service-select"]);
              }
            }else{
            this.sorryPopup = true;
          }
        }
      },
      (error) => {
        // this.locationByPostcodeError = error;
      });
    }
  }
  serviceAreaResponse =[];
  serviceAvail() {
    this.sorryPopup   = false;
    this.serviceAreas = true;
    let data1 = "";
    this.headerService.checkServiceArea(data1).subscribe(response => {
      if (response && response["body"]) {
        this.serviceAreaResponse = response["body"].serviceList;
      }
    },
    (error) => { 
      // this.serviceAreaError = error; 
    })
  }
  getLink(numbe){ 
    this.linkButtonDeactivate = true;
    if(numbe.length<10){
      this.linkButtonDeactivate = false;
      this.toastr.error("Num isn't valid");
    }else if(numbe.length == 0){
      this.linkButtonDeactivate = false;
      this.toastr.error("Enter a number");
    }else{ 
      this.homeService.getALink(numbe).subscribe((res) => {
        this.linkButtonDeactivate = false;
        if(res && res['body'] && res['body'].status==1){
          this.toastr.success('Thank you connecting');
        }
      });
    }
  }

  serviceAreasPopup(postcode){
    this.serviceAreas = false;
    let locationByPostcode = new LocationByPostcode(postcode);
    this.headerService.checkLocationByPostcode(postcode).subscribe(response => {
       if (response && response["body"]) { 
        // removing cart when click new pincode
        localStorage.removeItem('itemList');
        localStorage.removeItem('serviceName');
        localStorage.removeItem('totalPrice');
        localStorage.removeItem('totalCloths');
        localStorage.removeItem('serviceComment');

        localStorage.setItem("cityName",response["body"].cityName);
        this.changeHeader.getUpdatedCityName(response["body"].cityName);
        localStorage.setItem("postcode" ,postcode);
         if(response["body"].laundaryList.length > 1){
          localStorage.setItem("serviceArea" , "false");
          this.router.navigate(["/multiple-laundry"]);
          window.location.reload(true);
         } else{    
          //  setting new requirement Id 
          localStorage.setItem('laundryId' , response["body"].laundaryList[0].id);
          localStorage.setItem("serviceArea" , "true");
          localStorage.setItem("laundryIdForService" , response["body"].laundaryList[0].id );
          localStorage.setItem("laundryIdMain" , response["body"].laundaryList[0].id);
          this.router.navigate(["/service-select"]);
         }
       }
    })  
 }


  addStickClass = false;
  @HostListener("window:scroll", [])
  onWindowScroll() {
    let client =  document.documentElement.scrollTop;
    const scrollHeight = 79.34;
    const initialHeight = 79.33;
    if(client >= scrollHeight){
      this.addStickClass = true;
    }
    else if(client <= initialHeight){
      this.addStickClass = false;
    }
  }
}
