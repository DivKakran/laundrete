import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()

export class PaymentService {
    private couponApplyUrl = environment.baseApiURl + 'referral/referralCheck';
    private creditCheckUrl = environment.baseApiURl + 'Referral/creditCheck';
    private promocodeListingUrl = environment.baseApiURl + 'Referral/getCouponList';
    private orderPlaceUrl = environment.baseApiURl + 'order/order';
    constructor(private http:HttpClient){}

    couponApplyService(textCoupon){
       console.log(textCoupon)
       let headers = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==')
       return this.http.request(new HttpRequest(
           'POST',
           this.couponApplyUrl,
           textCoupon,
           {
               headers:headers
           }
       ))
    }

    creditCheckService(userId){
        let headers = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.creditCheckUrl,
            userId,
            {
                headers:headers
            }
        ))
    }

    promocodeListingService(postcode){
        let headers = new HttpHeaders({'Authorization':'Basic YWRtaW46MTIzNA=='})
        return this.http.request(new HttpRequest(
            'POST',
            this.promocodeListingUrl,
            postcode,
            {
                headers:headers
            }
        ))
    }

    orderPlaceService(orderPlaceData){
        console.log('orderPlaceData service',orderPlaceData);
        let headers = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.orderPlaceUrl,
            orderPlaceData,
            {
                headers:headers
            }
        ))
    }
}