
export class ApplyCoupon{
    user_id:any;
    postcode:any;
    referral_code:any;
    cart_amount:any;
    laundry_id: any;
    constructor(user_id:any,postcode:any,referral_code:any,cart_amount:any , laundryId){
        this.user_id = user_id;
        this.postcode = postcode;
        this.referral_code = referral_code;
        this.cart_amount = cart_amount;
        this.laundry_id = laundryId;
    }
}

export class CreditCheck{
    user_id:any;
    constructor(user_id:any){
        this.user_id = user_id;
    }
}

export class PromocodeListing {
    postcode: any;
    laundry_id: any;
    constructor(postcode:any , lId){
        this.postcode = postcode;
        this.laundry_id = lId;
    }
}

export class ItemList {
    serviceId: any;
    serviceName: any;
    categoryId: any;
    categoryName: any;
    subCategoryId: any;
    subCategoryName: any;
    itemId: any;
    itemName: any;
    itemRate: any;
    itemCount: any;
    itemType: any;
    itemWeight: any;
    constructor(serviceId: any,serviceName: any,categoryId: any,categoryName: any,subCategoryId: any,
      subCategoryName: any,itemId: any,itemName: any,itemRate: any,itemCount: any,itemType: any,itemWeight: any){
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.categoryId = parseInt(categoryId);
        this.categoryName = categoryName;
        this.subCategoryId = parseInt(subCategoryId);
        this.subCategoryName = subCategoryName;
        this.itemId = itemId;
        this.itemName = itemName
        this.itemRate = parseInt(itemRate);
        this.itemCount = parseInt(itemCount);
        this.itemType = itemType;
        this.itemWeight = itemWeight;
    }
}

export class ServiceComment {
    serviceId: any;
    serviceName: any;
    comment: any;
    constructor(serviceId: any,serviceName: any,comment: any){
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.comment = comment;
    }
}

export class DeliveryDetails {
    pickupAddress: any;
    pickupLandmark: any;
    pickupPincode: any;
    deliveryAddress: any;
    deliveryLandmark: any;
    deliveryPincode: any;
    deliveryMode: any;
    pickupTime: any;
    pickupFromSlot: any;
    pickupToSlot: any;
    deliveryTime: any;
    deliverFromSlot: any;
    deliverToSlot: any;
    alternateNumber: any;
    constructor( pickupAddress: any,pickupLandmark: any,pickupPincode: any,deliveryAddress: any,deliveryLandmark: any,
      deliveryPincode: any,deliveryMode: any,pickupTime: any,pickupFromSlot: any,pickupToSlot: any,deliveryTime: any,
      deliverFromSlot: any,deliverToSlot: any,alternateNumber: any){
        this.pickupAddress = pickupAddress;
        this.pickupLandmark = pickupLandmark;
        this.pickupPincode = pickupPincode;
        this.deliveryAddress = deliveryAddress;
        this.deliveryLandmark = deliveryLandmark;
        this.deliveryPincode = deliveryPincode;
        this.deliveryMode = deliveryMode;
        this.pickupTime = pickupTime;
        this.pickupFromSlot = parseInt(pickupFromSlot);
        this.pickupToSlot = parseInt(pickupToSlot);
        this.deliveryTime = deliveryTime;
        this.deliverFromSlot = parseInt(deliverFromSlot);
        this.deliverToSlot = parseInt(deliverToSlot);
        this.alternateNumber = alternateNumber;
    }
}

export class PaymentDetails {
    price: any;
    credit: any;
    delivery: any;
    couponCode: any;
    discount: any;
    express_delivery: any;
    amtPayable: any;
    paymentMode: any;
    CGST: any;
    SGST: any;
    IGST: any;
    coupon_type: any;
    coupon_value: any;
    min_cart_value: any;
    discount_upto: any;
    constructor( price: any,credit: any,delivery: any,couponCode: any,discount: any,express_delivery: any,
      amtPayable: any,paymentMode: any,CGST: any,SGST: any,IGST: any,coupon_type: any,coupon_value: any,
      min_cart_value: any,discount_upto: any){
        this.price = price;
        this.credit = credit;
        this.delivery = delivery;
        this.couponCode = couponCode;
        this.discount = discount;
        this.express_delivery = express_delivery;
        this.amtPayable = amtPayable;
        this.paymentMode = paymentMode;
        this.CGST = parseInt(CGST);
        this.SGST = parseInt(SGST);
        this.IGST = parseInt(IGST);
        this.coupon_type = coupon_type;
        this.coupon_value = coupon_value;
        this.min_cart_value = min_cart_value; 
        this.discount_upto = discount_upto;
    }
}

export class OrderPlace {
    user_id: any;
    postcode: any;
    order_timestamp: any;
    credit_before_gst: any;
    itemList: ItemList[];
    serviceComment: ServiceComment[];
    deliveryDetails: DeliveryDetails;
    paymentDetails: PaymentDetails;
    laundry_id: any;
    constructor( user_id: any,postcode: any,order_timestamp: any,credit_before_gst: any,itemList: ItemList[],
      serviceComment: ServiceComment[],deliveryDetails: DeliveryDetails,paymentDetails: PaymentDetails , laundryId){
        this.user_id = user_id;
        this.postcode = postcode;
        this.order_timestamp = ""+order_timestamp;
        this.credit_before_gst = credit_before_gst;
        this.itemList = itemList;
        this.serviceComment = serviceComment;
        this.deliveryDetails = deliveryDetails;
        this.paymentDetails = paymentDetails;
        this.laundry_id = laundryId;
    }
}



