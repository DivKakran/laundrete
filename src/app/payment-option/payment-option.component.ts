import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import {ApplyCoupon, CreditCheck,PromocodeListing,OrderPlace,PaymentDetails,ServiceComment,ItemList,DeliveryDetails} from './paymentModel';
import {PaymentService} from './paymentService';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
@Component({
  selector: 'app-payment-option',
  templateUrl: './payment-option.component.html',
  styleUrls: ['./payment-option.component.css'],
  providers: [PaymentService]
})
export class PaymentOptionComponent implements OnInit {

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private serviceHeaderChange:ChangeHeaderService , private paymentService:PaymentService,
    private toastr: ToastrService , private router:Router) { }
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false, processHeader:true};
  coupenTextModel = {textCoupon:''};
  checkCreditModel = {checkCredit:''};
  retrieveServiceFromCart ='';
  // retrieveServiceFromCart = localStorage.getItem('serviceName');
  retrieveServiceFromCartArray;
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveItemsFromCart ='';
  // retrieveItemsFromCart = localStorage.getItem('itemList');
  retrieveItemsFromCartArray;
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
  serviceCommentArray='';
  // serviceCommentArray = localStorage.getItem('serviceComment');
  retrieveServiceCommentArray;
  // retrieveServiceCommentArray = JSON.parse(this.serviceCommentArray);
  submittedPaymentForm = false;
  discountBtnText = "Apply";
  editable = false;
  discountValue = 0;
  totalDiscountValue;
  discountValueAvailable = false;
  promoCodeClicked = false;
  promoTermsClicked = false;
  promoTermsCondition;
  promocodeListingResponse;
  paymentMode = "COD";
  totalPrice='';
  totalItems='';
  laundarySettings='';
  // totalPrice = localStorage.getItem('totalPrice');
  // totalItems = localStorage.getItem('totalCloths');
  // laundarySettings = localStorage.getItem('laundry_settings');
  couponType='';
  couponCode = '';
  couponValue = '';
  discountUpto='';
  discountMinCartValue;
  placeOrderLoader = false;
  // addressDetails = localStorage.getItem('selectedAddressDetails');
  // selectedAddressDetails = JSON.parse(this.addressDetails);
  // laundarySettingValue = JSON.parse(this.laundarySettings);
  laundarySettingValue;
  // minCartValue = this.laundarySettingValue.min_cart_amount;
  minCartValue;
  totalAmount;
  minCartAmount;
  expressDeliveryAmount = 0;
  // totalAmount = Number(this.totalPrice)
  // minCartAmount = Number(this.minCartValue)
  amountCalculationWithDiscount(){
    if(Number(this.totalPrice) < this.minCartAmount){
      if(this.deliveryDetail.serviceType == "EXPRESS"){  

        this.expPer = (JSON.parse(localStorage.getItem('laundry_settings'))).express_percent;
        // var discountInclude = this.grandTotal-this.discountValue+this.expressDeliveryAmount; 

        var discountInclude = this.grandTotal-this.discountValue;

      }else{ 
        if(this.totalPrice == this.grandTotal){
          var discountInclude = this.grandTotal-this.discountValue+parseInt(this.deliveryAmount.delivery_amt);  
        }else{
          var discountInclude = this.grandTotal-this.discountValue;  
        }  
      }
      // let discountInclude = this.grandTotal-this.discountValue; 

      this.cgstAmount = (discountInclude*this.taxValues.cgst)/100;
      this.totalCgstAmount = this.cgstAmount.toFixed(2)
      this.sgstAmount = (discountInclude*this.taxValues.sgst)/100;
      this.totalSgstAmount = this.sgstAmount.toFixed(2)
      this.igstAmount = (discountInclude*this.taxValues.igst)/100;
      this.totalIgstAmount = this.igstAmount.toFixed(2)
      if(Number(this.taxValues.igst)==0){
       this.amountPayable = Number(discountInclude)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
      else{
       this.amountPayable = Number(discountInclude)+Number(this.totalIgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
    }
    else if(Number(this.totalPrice) > this.minCartAmount){
     let discountInclude = this.grandTotal-this.discountValue
     this.cgstAmount = (discountInclude*this.taxValues.cgst)/100;
     this.totalCgstAmount = this.cgstAmount.toFixed(2)
     this.sgstAmount = (discountInclude*this.taxValues.sgst)/100;
     this.totalSgstAmount = this.sgstAmount.toFixed(2)
     this.igstAmount = (discountInclude*this.taxValues.igst)/100;
     this.totalIgstAmount = this.igstAmount.toFixed(2)
     if(Number(this.taxValues.igst)==0){
       this.amountPayable = Number(discountInclude)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
     }
     else{
       this.amountPayable = Number(discountInclude)+Number(this.totalIgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
     }
    }  
  }

  amountCalculationWithoutDiscount(){ 
    if(Number(this.totalPrice) < this.minCartAmount){   
      if(this.deliveryDetail.serviceType == "EXPRESS"){
        this.grandTotal = Number(this.totalPrice) + this.expressDeliveryAmount;
      }else{ 
        this.grandTotal = Number(this.totalPrice) + Number(this.deliveryAmount.delivery_amt);
      }
      // this.grandTotal = Number(this.totalPrice)+Number(this.deliveryAmount.delivery_amt)

      this.cgstAmount = (this.grandTotal*this.taxValues.cgst)/100;
      this.totalCgstAmount = this.cgstAmount.toFixed(2)
      this.sgstAmount = (this.grandTotal*this.taxValues.sgst)/100;
      this.totalSgstAmount = this.sgstAmount.toFixed(2)
      this.igstAmount = (this.grandTotal*this.taxValues.igst)/100;
      this.totalIgstAmount = this.igstAmount.toFixed(2)
      if(Number(this.taxValues.igst)==0){
      this.amountPayable = Number(this.grandTotal)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
      this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
      else{
      this.amountPayable = Number(this.grandTotal)+Number(this.totalIgstAmount);
      this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
    }
    else if(Number(this.totalPrice) > this.minCartAmount){
      if(this.deliveryDetail.serviceType == "EXPRESS"){
        this.grandTotal = Number(this.totalPrice) + this.expressDeliveryAmount;
      }else{
        this.grandTotal = Number(this.totalPrice);
      }
      this.cgstAmount = (this.grandTotal*this.taxValues.cgst)/100;
      this.totalCgstAmount = this.cgstAmount.toFixed(2)
      this.sgstAmount = (this.grandTotal*this.taxValues.sgst)/100;
      this.totalSgstAmount = this.sgstAmount.toFixed(2)
      this.igstAmount = (this.grandTotal*this.taxValues.igst)/100;
      this.totalIgstAmount = this.igstAmount.toFixed(2)
      if(Number(this.taxValues.igst)==0){
      this.amountPayable = Number(this.grandTotal)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
      this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
      else{
      this.amountPayable = Number(this.grandTotal)+Number(this.totalIgstAmount);
      this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
    }
  }
  couponApply(val){ 
    this.submittedPaymentForm = true;
    if(this.coupenTextModel.textCoupon!='' && this.discountBtnText=="Apply"){
    let couponAppliedModel = new ApplyCoupon(this.localStorage.getItem('userId'),this.localStorage.getItem('postcode'),
     this.coupenTextModel.textCoupon,this.localStorage.getItem('totalPrice') , localStorage.getItem('laundryId'));
     this.paymentService.couponApplyService(couponAppliedModel).subscribe(response=>{ 
       if(response && response["body"]){
         if(response["body"].status=="1"){
           this.discountBtnText = "Cancel";
           this.editable = true;
           this.discountValueAvailable = true;
           this.showSuccess(response["body"].message);
           this.couponType = response["body"].coupon_type;
           this.couponCode = this.coupenTextModel.textCoupon;
           if(response["body"].credit_points == 0){
            this.couponValue = response["body"].discount_value;
           }else{
            this.couponValue = response["body"].credit_points;
           }
           this.discountUpto = response["body"].discount_upto;
           this.discountMinCartValue = response["body"].min_cart_value;
          //  =================== FLAT Discount==============================//
           if(response["body"].coupon_type == "FLAT"){
            if(response["body"].credit_points == 0){
              this.discountValue = response["body"].discount_value;
            }else{
              this.discountValue = response["body"].credit_points;
            }
            this.totalDiscountValue = Number(this.discountValue).toFixed(2)
            if(this.creditPointAvailable){
              this.amountCalWithBothDiscount(); 
            }
            else{
              this.amountCalculationWithDiscount();
            }
           }
          //  ========================= Percentage Discount=====================//
           else if(response["body"].coupon_type == "PERCENTAGE"){
            let discountPercentage = response["body"].discount_value;
            let minCartValue = response["body"].min_cart_value;
            let discountUpto = response["body"].discount_upto;
            if(Number(this.totalPrice) > minCartValue){ 
             let cartValuePercentage = (Number(this.totalPrice)*discountPercentage)/100;
             if(discountUpto > cartValuePercentage){
               this.discountValue = cartValuePercentage;
               this.totalDiscountValue = Number(this.discountValue).toFixed(2)

              //  ================  If cart percentage is less============//
              if(this.creditPointAvailable){ 
                this.amountCalWithBothDiscount(); 
               }
               else{
               this.amountCalculationWithDiscount();
               }
             }
             else if(discountUpto < cartValuePercentage){
               this.discountValue = discountUpto;
               this.totalDiscountValue = Number(this.discountValue).toFixed(2)

              //  ======================== If discount upto value is less============//
              if(this.creditPointAvailable){
                this.amountCalWithBothDiscount(); 
               }
               else{
               this.amountCalculationWithDiscount();
               }
             }
            }
           }
         }
         else{
           this.showError(response["body"].message);
           setTimeout(()=>{
             this.coupenTextModel.textCoupon = ''
           },5000)
         }
       }
     });
    }
    // ======================  Cancel Button==================================//
    else if(this.coupenTextModel.textCoupon!='' && this.discountBtnText=="Cancel"){ 
      this.discountBtnText = "Apply";
      this.editable = false;
      this.coupenTextModel.textCoupon = '';
      this.discountValueAvailable = false;
      this.couponValue = '';
      // this.amountCalculationWithoutDiscount();
      if(this.creditPointAvailable){
        let creditWithDiscountTotal = this.grandTotal;
        let creditPercentCalc = (creditWithDiscountTotal*this.creditPercentage)/100;
        if(Number(this.creditPoints)>creditPercentCalc){
          this.totalCreditPoints = creditPercentCalc.toFixed(2)
          this.amountCalculationWithCredit();
        }
        else if(Number(this.creditPoints)<creditPercentCalc){
          this.totalCreditPoints = Number(this.creditPoints).toFixed(2)
          this.amountCalculationWithCredit();
        }
        // this.amountCalculationWithCredit();
      }
      else{ 
        this.amountCalculationWithoutDiscount();
      }
    }
  }
  // ========== checkbox method======================//
  checkboxValue = false;
  creditPoints;
  totalCreditPoints : any = "0";
  creditPercentage;
  creditPointAvailable = false;
  selectedAddressDetails='';
  // selectedAddressDetails = localStorage.getItem('selectedAddressDetails');
  taxValues;
  // taxValues = JSON.parse(this.selectedAddressDetails);
  laundrySettings='';
  // laundrySettings = localStorage.getItem('laundry_settings');
  deliveryAmount;
  // deliveryAmount = JSON.parse(this.laundrySettings);
 
  creditCheck(){
   if(this.checkboxValue == false){
     this.checkboxValue = true;
     let creditPointData = new CreditCheck(this.localStorage.getItem('userId'));
     this.paymentService.creditCheckService(creditPointData).subscribe(response=>{
       if(response && response["body"]){
         if(response["body"].status=="1"){
           if(response["body"].credit_points == "0"){
            this.showError(response["body"].message);
           }else{
            this.creditPoints = response["body"].credit_points;
            // this.totalCreditPoints = Number(this.creditPoints).toFixed(2)
            if(Number(this.creditPoints)>0){
            this.creditPercentage = response["body"].credit_max_percent;
           
            this.creditPointAvailable = true;
            // =========================   Credit Points calculation=================//
            if(this.discountValueAvailable){
              let creditWithDiscountTotal = this.grandTotal-this.discountValue;
              let creditPercentCalc = (creditWithDiscountTotal*this.creditPercentage)/100;             
             // this.totalCreditPoints = Number(this.creditPoints).toFixed(2)
              // this.amountCalWithBothDiscount();
              if(Number(this.creditPoints)>creditPercentCalc){
                this.totalCreditPoints = creditPercentCalc.toFixed(2)
                this.amountCalWithBothDiscount();
              }
              else if(Number(this.creditPoints)<creditPercentCalc){
                this.totalCreditPoints = Number(this.creditPoints).toFixed(2)
                this.amountCalWithBothDiscount();
              }
            }
            else{
              let creditWithDiscountTotal = this.grandTotal;
              let creditPercentCalc = (creditWithDiscountTotal*this.creditPercentage)/100;
              if(Number(this.creditPoints)>creditPercentCalc){
                this.totalCreditPoints = creditPercentCalc.toFixed(2)
                this.amountCalculationWithCredit();
              }
              else if(Number(this.creditPoints)<creditPercentCalc){
                this.totalCreditPoints = Number(this.creditPoints).toFixed(2)
                this.amountCalculationWithCredit();
              }
            }
          } 
          else{
            this.totalCreditPoints = 0;
          }
         }}
         else{
           this.showError(response["body"].message);
         }
       }
     });
   }
   else{ 
     this.checkboxValue = false;
     this.creditPointAvailable = false;
     this.totalCreditPoints = '';
    //  this.amountCalculationWithoutDiscount();
     if(this.discountValueAvailable){
       this.amountCalculationWithDiscount();
     }
     else{
      this.amountCalculationWithoutDiscount();
     }
   }
  }
  // end of credit check
  showError(Error) {
    this.toastr.error(Error);
  }
  showSuccess(Error) {
    this.toastr.success(Error);
  }
  
  grandTotal;
  cgstAmount;
  totalCgstAmount;
  sgstAmount;
  totalSgstAmount;
  igstAmount;
  totalIgstAmount;
  amountPayable;
  totalAmountPayable;

  deliveryDetail: any;
  expPer: any;
  ngOnInit() { 
    if(localStorage.getItem('itemList') == undefined || localStorage.getItem('totalCloths') == undefined){
      this.router.navigate(['']);
    }
    this.deliveryDetail = JSON.parse(localStorage.getItem('placeOrderDeliveryDetail'));
    if(this.deliveryDetail.serviceType == "EXPRESS"){
      this.expPer = (JSON.parse(localStorage.getItem('laundry_settings'))).express_percent;
    }
    this.retrieveServiceFromCart = localStorage.getItem('serviceName');
    this.retrieveItemsFromCart = localStorage.getItem('itemList');
    this.serviceCommentArray = localStorage.getItem('serviceComment');
    this.totalPrice = localStorage.getItem('totalPrice');
    this.totalItems = localStorage.getItem('totalCloths');
    this.laundarySettings = localStorage.getItem('laundry_settings');
    this.selectedAddressDetails = localStorage.getItem('selectedAddressDetails');
    this.laundrySettings = localStorage.getItem('laundry_settings');
    this.taxValues = JSON.parse(this.selectedAddressDetails);
    this.deliveryAmount = JSON.parse(this.laundrySettings);
    this.retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
    this.retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
    this.retrieveServiceCommentArray = JSON.parse(this.serviceCommentArray);
    this.laundarySettingValue = JSON.parse(this.laundarySettings);
    this.minCartValue = this.laundarySettingValue.min_cart_amount;
    this.totalAmount = Number(this.totalPrice)
    this.minCartAmount = Number(this.minCartValue)
    if(Number(this.totalPrice) < this.minCartAmount){ 
      // old total
      // this.grandTotal = Number(this.totalPrice)+Number(this.deliveryAmount.delivery_amt);
      // new total
      this.grandTotal = Number(this.totalPrice)+Number(this.deliveryAmount.delivery_amt);
     
      if((JSON.parse(localStorage.getItem('placeOrderDeliveryDetail'))).serviceType == "EXPRESS"){
        this.expressDeliveryAmount = ((Number(this.totalPrice)) * (this.expPer/100)) +
         Number(this.deliveryAmount.delivery_amt);

        this.grandTotal = Number(this.totalPrice)+this.expressDeliveryAmount;
      }else{
        // this.grandTotal = Number(this.totalPrice);
        // alert(Number(this.deliveryAmount.delivery_amt));
      }
      this.cgstAmount = (this.grandTotal * this.taxValues.cgst)/100; 
      this.totalCgstAmount = this.cgstAmount.toFixed(2);
      this.sgstAmount = (this.grandTotal*this.taxValues.sgst)/100;
      this.totalSgstAmount = this.sgstAmount.toFixed(2); 

      // this.igstAmount = ((this.grandTotal+Number(this.deliveryAmount.delivery_amt))*this.taxValues.igst)/100;
      this.igstAmount = ((this.grandTotal)*this.taxValues.igst)/100;
      this.totalIgstAmount = this.igstAmount.toFixed(2); 
      if(Number(this.taxValues.igst)==0){ 
        this.amountPayable = Number(this.grandTotal)+Number(this.totalCgstAmount)+
        Number(this.totalSgstAmount);
        this.totalAmountPayable = this.amountPayable.toFixed(2);
      }
      else{ 
        // this.amountPayable = Number(this.grandTotal)+Number(this.totalIgstAmount)+ 
        // Number(this.expressDeliveryAmount) + Number(this.deliveryAmount.delivery_amt);
        // this.totalAmountPayable = this.amountPayable.toFixed(2);
        this.amountPayable = Number(this.grandTotal)+Number(this.totalIgstAmount);
        this.totalAmountPayable = this.amountPayable.toFixed(2);
      }
    } 
    else if(Number(this.totalPrice) >= this.minCartAmount){ 
      // old total
      if((JSON.parse(localStorage.getItem('placeOrderDeliveryDetail'))).serviceType == "EXPRESS"){
        this.grandTotal = Number(this.totalPrice);
        this.expressDeliveryAmount = Number(this.totalPrice) * (this.expPer/100);
      }else{
        this.grandTotal = Number(this.totalPrice);
      }
      if((JSON.parse(localStorage.getItem('placeOrderDeliveryDetail'))).serviceType == "EXPRESS"){
      // this.cgstAmount = ((this.grandTotal
      //   +( (Number(this.totalPrice)) * (this.expPer/100) ) +
      //   Number(this.deliveryAmount.delivery_amt)) * this.taxValues.cgst)/100;
        this.cgstAmount = (this.grandTotal+this.expressDeliveryAmount)*this.taxValues.cgst/100
      }else{
        this.cgstAmount = (this.grandTotal*this.taxValues.cgst)/100;;
      }
      this.totalCgstAmount = this.cgstAmount.toFixed(2)
      if((JSON.parse(localStorage.getItem('placeOrderDeliveryDetail'))).serviceType == "EXPRESS"){
        // this.sgstAmount =  ((this.grandTotal
        //   +((Number(this.totalPrice)) * (this.expPer/100)) +
        //   Number(this.deliveryAmount.delivery_amt))*this.taxValues.sgst)/100;
        this.grandTotal = Number(this.totalPrice)+this.expressDeliveryAmount;
        this.sgstAmount = (this.grandTotal)*this.taxValues.sgst/100
      }else{
        this.sgstAmount =  (this.grandTotal*this.taxValues.sgst)/100;
      }
      this.totalSgstAmount = this.sgstAmount.toFixed(2); 
      this.igstAmount = ((this.grandTotal)*this.taxValues.igst)/100;
      this.totalIgstAmount = this.igstAmount.toFixed(2)
      if(Number(this.taxValues.igst)==0){ 

        // this.amountPayable = Number(this.grandTotal)+Number(this.totalCgstAmount)+
        // Number(this.totalSgstAmount)+Number(this.totalIgstAmount)+ Number(this.expressDeliveryAmount);

        this.amountPayable = Number(this.grandTotal)+Number(this.totalCgstAmount)+
        Number(this.totalSgstAmount)+Number(this.totalIgstAmount);

        this.totalAmountPayable = this.amountPayable.toFixed(2)

      }
      else{
      this.amountPayable = Number(this.grandTotal)+Number(this.totalIgstAmount);
      this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
    }
    this.serviceHeaderChange.changeHeader(this.headerValue);
    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCartArray);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCartArray);
    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
    this.serviceHeaderChange.setGetACallCSS(false);
  }

  openPromoCode(){
    this.promoCodeClicked = true;
    let promocodeList = new PromocodeListing(this.localStorage.getItem('postcode') , 
    this.localStorage.getItem('laundryId'));
    this.paymentService.promocodeListingService(promocodeList).subscribe(response=>{
      if(response && response["body"]){
        if(response["body"].status=="1"){
          this.promocodeListingResponse = response["body"].list;
        }
        else{
          this.showError(response["body"].message);
        }
      }
    });
  }

  promoApply(promocodeName){ 
    this.coupenTextModel.textCoupon = promocodeName;
    this.promoCodeClicked = false;
    this.couponApply(promocodeName);
  }
  closePromoCode(){
    this.promoCodeClicked = false;
  }

  amountCalWithBothDiscount(){
    if(Number(this.totalPrice) < this.minCartAmount){
      let discountInclude = this.grandTotal-this.discountValue-this.totalCreditPoints
      this.cgstAmount = (discountInclude*this.taxValues.cgst)/100;
      this.totalCgstAmount = this.cgstAmount.toFixed(2)
      this.sgstAmount = (discountInclude*this.taxValues.sgst)/100;
      this.totalSgstAmount = this.sgstAmount.toFixed(2)
      this.igstAmount = (discountInclude*this.taxValues.igst)/100;
      this.totalIgstAmount = this.igstAmount.toFixed(2)
      if(Number(this.taxValues.igst)==0){
       this.amountPayable = Number(discountInclude)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
      else{
       this.amountPayable = Number(discountInclude)+Number(this.totalIgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
    }
    else if(Number(this.totalPrice) > this.minCartAmount){
     let discountInclude = this.grandTotal-this.discountValue-this.totalCreditPoints
     this.cgstAmount = (discountInclude*this.taxValues.cgst)/100;
     this.totalCgstAmount = this.cgstAmount.toFixed(2)
     this.sgstAmount = (discountInclude*this.taxValues.sgst)/100;
     this.totalSgstAmount = this.sgstAmount.toFixed(2)
     this.igstAmount = (discountInclude*this.taxValues.igst)/100;
     this.totalIgstAmount = this.igstAmount.toFixed(2)
     if(Number(this.taxValues.igst)==0){
       this.amountPayable = Number(discountInclude)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
     }
     else{
       this.amountPayable = Number(discountInclude)+Number(this.totalIgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
     }
    } 
  }

  amountCalculationWithCredit(){
    if(Number(this.totalPrice) < this.minCartAmount){
      let creditPointInclude = this.grandTotal-this.totalCreditPoints
      this.cgstAmount = (creditPointInclude*this.taxValues.cgst)/100;
      this.totalCgstAmount = this.cgstAmount.toFixed(2)
      this.sgstAmount = (creditPointInclude*this.taxValues.sgst)/100;
      this.totalSgstAmount = this.sgstAmount.toFixed(2)
      this.igstAmount = (creditPointInclude*this.taxValues.igst)/100;
      this.totalIgstAmount = this.igstAmount.toFixed(2)
      if(Number(this.taxValues.igst)==0){
       this.amountPayable = Number(creditPointInclude)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
      else{
       this.amountPayable = Number(creditPointInclude)+Number(this.totalIgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
      }
    }
    else if(Number(this.totalPrice) > this.minCartAmount){
     let creditPointInclude = this.grandTotal-this.totalCreditPoints
     this.cgstAmount = (creditPointInclude*this.taxValues.cgst)/100;
     this.totalCgstAmount = this.cgstAmount.toFixed(2)
     this.sgstAmount = (creditPointInclude*this.taxValues.sgst)/100;
     this.totalSgstAmount = this.sgstAmount.toFixed(2)
     this.igstAmount = (creditPointInclude*this.taxValues.igst)/100;
     this.totalIgstAmount = this.igstAmount.toFixed(2)
     if(Number(this.taxValues.igst)==0){
       this.amountPayable = Number(creditPointInclude)+Number(this.totalCgstAmount)+Number(this.totalSgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
     }
     else{
       this.amountPayable = Number(creditPointInclude)+Number(this.totalIgstAmount);
       this.totalAmountPayable = this.amountPayable.toFixed(2)
     }
    }  
  }

  promoTermsClick(promoTermsCondition){
    this.promoTermsClicked = true;
    this.promoTermsCondition = promoTermsCondition;
  }

  closePromoTerms(){
    this.promoTermsClicked = false;
  }

  setPaymentMode(val){
    this.paymentMode = val;
  }

  objItem;
  objComment;
  itemArrayModel=[];
  commentArrayModel = [];

  placeOrder(){ 
    this.retrieveItemsFromCartArray.filter((res,i) => {
       let obj = new ItemList(res.serviceId , res.service, res.categoryId , res.categoryName, res.subcategoryId,
        res.subcategoryName , res.id , res.name, res.rate, res.count, res.itemType , res.weight);
        this.itemArrayModel.push(obj);       
    });
    
    this.retrieveServiceCommentArray.filter((res,i) => {
       let objComment = new ServiceComment(res.serviceId , res.serviceName , res.comment);
       this.commentArrayModel.push(objComment);   
    });

    this.placeOrderLoader = true;
    if(Number(this.totalPrice) > this.minCartAmount){ 
      if(this.expressDeliveryAmount!=0){
        var orderPlaceData = new OrderPlace(this.localStorage.getItem('userId'),this.localStorage.getItem('postcode'),
          Date.now(),"1",this.itemArrayModel,this.commentArrayModel,new DeliveryDetails(this.taxValues.address,this.taxValues.landmark,this.taxValues.pincode,this.taxValues.address,
          this.taxValues.landmark,this.taxValues.pincode,this.deliveryDetail.serviceType,this.deliveryDetail.pickUpTime,
          this.deliveryDetail.pickUpFromSlot,this.deliveryDetail.pickUpToSlot,
          this.deliveryDetail.deliveryTime,this.deliveryDetail.deliveryFromSlot,this.deliveryDetail.deliveryToSlot,this.localStorage.getItem('alterNumber')),new PaymentDetails(this.totalPrice,this.totalCreditPoints,
          '0',this.couponCode,
          this.discountValue,this.expressDeliveryAmount, this.totalAmountPayable,this.paymentMode,this.taxValues.cgst,this.taxValues.sgst,this.taxValues.igst,
          this.couponType ,this.couponValue,(JSON.parse(localStorage.getItem('laundry_settings'))).min_cart_amount,this.discountUpto) , (JSON.parse(localStorage.getItem('laundry_settings'))).lId);
      }else{
        if(this.totalAmount > this.minCartAmount && this.expressDeliveryAmount==0){
          var orderPlaceData = new OrderPlace(this.localStorage.getItem('userId'),this.localStorage.getItem('postcode'),
            Date.now(),"1",this.itemArrayModel,this.commentArrayModel,new DeliveryDetails(this.taxValues.address,this.taxValues.landmark,this.taxValues.pincode,this.taxValues.address,
            this.taxValues.landmark,this.taxValues.pincode,this.deliveryDetail.serviceType,this.deliveryDetail.pickUpTime,
            this.deliveryDetail.pickUpFromSlot,this.deliveryDetail.pickUpToSlot,
            this.deliveryDetail.deliveryTime,this.deliveryDetail.deliveryFromSlot,this.deliveryDetail.deliveryToSlot,this.localStorage.getItem('alterNumber')),new PaymentDetails(this.totalPrice,this.totalCreditPoints,
            '0',this.couponCode,
            this.discountValue,"", this.totalAmountPayable,this.paymentMode,this.taxValues.cgst,this.taxValues.sgst,this.taxValues.igst,
            this.couponType ,this.couponValue,(JSON.parse(localStorage.getItem('laundry_settings'))).min_cart_amount,this.discountUpto) , (JSON.parse(localStorage.getItem('laundry_settings'))).lId);
          }else{
            var orderPlaceData = new OrderPlace(this.localStorage.getItem('userId'),this.localStorage.getItem('postcode'),
            Date.now(),"1",this.itemArrayModel,this.commentArrayModel,new DeliveryDetails(this.taxValues.address,this.taxValues.landmark,this.taxValues.pincode,this.taxValues.address,
            this.taxValues.landmark,this.taxValues.pincode,this.deliveryDetail.serviceType,this.deliveryDetail.pickUpTime,
            this.deliveryDetail.pickUpFromSlot,this.deliveryDetail.pickUpToSlot,
            this.deliveryDetail.deliveryTime,this.deliveryDetail.deliveryFromSlot,this.deliveryDetail.deliveryToSlot,this.localStorage.getItem('alterNumber')),new PaymentDetails(this.totalPrice,this.totalCreditPoints,
            (JSON.parse(localStorage.getItem('laundry_settings'))).delivery_amt,this.couponCode,
            this.discountValue,"", this.totalAmountPayable,this.paymentMode,this.taxValues.cgst,this.taxValues.sgst,this.taxValues.igst,
            this.couponType ,this.couponValue,(JSON.parse(localStorage.getItem('laundry_settings'))).min_cart_amount,this.discountUpto) , (JSON.parse(localStorage.getItem('laundry_settings'))).lId);
          }
        }

        this.paymentService.orderPlaceService(orderPlaceData).subscribe(response=>{
          if(response && response["body"]){
            if(response && response["body"].status=="1"){
              this.localStorage.removeItem('totalCloths');
              this.placeOrderLoader = false;
              this.router.navigate(['/thank-you']);
              this.localStorage.setItem('placeOrderId',response["body"].orderId)
            }
            else{
              this.placeOrderLoader = false;
            }
          }
        })
    }
    else{
      if(this.expressDeliveryAmount!=0){
        var orderPlaceData = new OrderPlace(this.localStorage.getItem('userId'),this.localStorage.getItem('postcode'),
        Date.now(),"1",this.itemArrayModel,this.commentArrayModel,new DeliveryDetails(this.taxValues.address,this.taxValues.landmark,this.taxValues.pincode,this.taxValues.address,
        this.taxValues.landmark,this.taxValues.pincode,this.deliveryDetail.serviceType ,this.deliveryDetail.pickUpTime,this.deliveryDetail.pickUpFromSlot,
        this.deliveryDetail.pickUpToSlot,this.deliveryDetail.deliveryTime,this.deliveryDetail.deliveryFromSlot,
        this.deliveryDetail.deliveryToSlot,this.localStorage.getItem('alterNumber')),new PaymentDetails(this.totalPrice,this.totalCreditPoints,
        '0.0',this.couponCode, this.discountValue,this.expressDeliveryAmount, this.totalAmountPayable,this.paymentMode,this.taxValues.cgst,this.taxValues.sgst,this.taxValues.igst,
        this.couponType ,this.couponValue,this.minCartAmount,this.discountUpto) , (JSON.parse(localStorage.getItem('laundry_settings'))).lId);
      }else{
        var orderPlaceData = new OrderPlace(this.localStorage.getItem('userId'),this.localStorage.getItem('postcode'),
        Date.now(),"1",this.itemArrayModel,this.commentArrayModel,new DeliveryDetails(this.taxValues.address,this.taxValues.landmark,this.taxValues.pincode,this.taxValues.address,
        this.taxValues.landmark,this.taxValues.pincode,this.deliveryDetail.serviceType ,this.deliveryDetail.pickUpTime,this.deliveryDetail.pickUpFromSlot,
        this.deliveryDetail.pickUpToSlot,this.deliveryDetail.deliveryTime,this.deliveryDetail.deliveryFromSlot,
        this.deliveryDetail.deliveryToSlot,this.localStorage.getItem('alterNumber')),new PaymentDetails(this.totalPrice,this.totalCreditPoints,
        this.deliveryAmount.delivery_amt,this.couponCode, this.discountValue,this.expressDeliveryAmount, this.totalAmountPayable,this.paymentMode,this.taxValues.cgst,this.taxValues.sgst,this.taxValues.igst,
        this.couponType ,this.couponValue,this.minCartAmount,this.discountUpto) , (JSON.parse(localStorage.getItem('laundry_settings'))).lId);
      }

      this.paymentService.orderPlaceService(orderPlaceData).subscribe(response=>{
        if(response && response["body"]){
          if(response && response["body"].status=="1"){

            let updateCredit = parseInt(localStorage.getItem('creditPoint'))-parseInt(this.totalCreditPoints);
            localStorage.setItem('creditPoint' ,  String(updateCredit));
            this.serviceHeaderChange.headerCredit(updateCredit);

            this.placeOrderLoader = false;
            this.localStorage.removeItem('totalCloths');
            this.router.navigate(['/thank-you']);
            this.localStorage.setItem('placeOrderId',response["body"].orderId)
          }
          else{
            this.placeOrderLoader = false;
          }
        }
      });
    }
  }
}
