import { BecomeAPartnerComponent } from './become-apartner.component';
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable({providedIn: "root"})
export class BecomeAPartnerService{
    private becomePartnerUrl = environment.baseApiURl+'webpage/contact_us_email';
    constructor(private _http: HttpClient){}
    submitBecomePartner(data){
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA=='}).append('Content-Type','application/json');
        return this._http.request(new HttpRequest(
            'POST',
            this.becomePartnerUrl,
            data,
            {
                headers: headers
            }
        ));
    }
}