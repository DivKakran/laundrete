import { Meta , Title} from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import {BecomePartnerModel} from './model';
import {BecomeAPartnerService} from './service'
import { ToastrService } from 'ngx-toastr';
import {ChangeHeaderService} from '../changeHeaderService';
@Component({
  selector: 'app-become-apartner',
  templateUrl: './become-apartner.component.html',
  styleUrls: ['./become-apartner.component.css']
})
export class BecomeAPartnerComponent implements OnInit {

  phone:Number;
  email:String;
  name :String;
  buttonclick = false;
  headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: false, dropdownHeader: false,processHeader:false};
  constructor(private bService: BecomeAPartnerService , private changeHeader: ChangeHeaderService, 
    private toaster: ToastrService , private meta: Meta , private title: Title) { 
      this.title.setTitle("Become A laundry & Dry Cleaning Partner With Launderette");
      this.meta.addTags([{name:"description" , content: 'Submit your query & Become A laundry & Dry Cleaning Partner With Launderette' }
      ,{ name:"keywords" ,content: 'become a laundry partner' } ]);
    }

  ngOnInit() {
    this.changeHeader.changeHeader(this.headerValue);
  }
  submitConnect(data){ 
    let obj = new BecomePartnerModel(data.txt_tel , data.txt_email , data.txt_tel);
    if(this.validateFields(data)){
      this.bService.submitBecomePartner(obj).subscribe((res) => {
        this.buttonclick = true;
        if(res && res['body'] && res['body'].status==1){
          this.buttonclick = false;
          this.toaster.success(res['body'].message)
        }
      });
    }
  }
  validateFields(obj){  
    if(obj.txt_tel == undefined){
      this.toaster.error("Please provide phone num");
      return false;
    }else if(obj.txt_tel.length<10){
      this.toaster.error("Phone num isn't valid");
      return false;
    }else if(obj.txt_email == undefined){
      this.toaster.error("Provide email address");
      return false;
    }else if(!this.validateEmail(obj.txt_email)){
      this.toaster.error("Email isn't valid");
      return false;
    }else if(obj.txt_laundry == undefined){
      this.toaster.error("Provide organisation name");
      return false;
    }else{
      return true;
    }
  }
  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}
