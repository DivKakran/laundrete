export class BecomePartnerModel{
    txt_tel:   string;
    txt_email: string;
    txt_name: string;
    txt_comm: string;
    txt_city: string
    constructor(tel , email , name){
        this.txt_tel = tel;
        this.txt_email = email;
        this.txt_name = name;
        this.txt_comm = "Connect with us";
        this.txt_city = "Delhi"
    }
}