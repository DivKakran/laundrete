import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';


@Injectable()
export class RegisterService {
    private requestOtp = environment.baseApiURl + 'login/requestotp';
    constructor(private http:HttpClient){}
    
    requestOtpService(requestOtpModel) {
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.requestOtp,
            requestOtpModel,
            {
                headers: headers
            }
        ))
    }
}    