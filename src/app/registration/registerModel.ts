export class RequestOtpModel {
    mobile: any;
    uuid_value: any;
    constructor(mobile, uuid_value) {
        this.mobile = mobile;
        this.uuid_value = uuid_value;
    }
}
export class RequestOtpFBModel {
    mobile: any;
    name: any;
    email: any;
    uuid_value: any;
    uuid: any;
    login_type: any;
    social_id: any;
    device_type: any;
    constructor(mobile: any, name: any, email: any, uuid_value: any, uuid: any, login_type: any,
        social_id: any, device_type: any) {
        this.mobile = mobile;
        this.name = name;
        this.email = email;
        this.uuid_value = uuid_value;
        this.uuid = uuid;
        this.login_type = login_type;
        this.social_id = social_id;
        this.device_type = device_type;

    }
}