import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import {RequestOtpModel,RequestOtpFBModel} from './registerModel';
import {RegisterService} from './registerService';
import { ToastrService } from 'ngx-toastr';
import { ChangeHeaderService } from '../changeHeaderService';
import {Router} from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [RegisterService]
})
export class RegistrationComponent implements OnInit {
  registerModel;
  // registerModel = {fullName:'',emailAddress:'',mobile:localStorage.getItem('mobileNumber')};
  submittedRegisteredData = false;
  facebookModel;
  // facebookModel = {fullName:localStorage.getItem('fbUserName'),
  //   emailAddress:localStorage.getItem('fbUserEmail'),mobile:'' };
  googleModel;
  // googleModel = {fullName:localStorage.getItem('googleUserName'),
  //   emailAddress:localStorage.getItem('googleUserEmail'),mobile:''};
  userFacebookId;
  userGoogleId;
    // userFacebookId = localStorage.getItem('userFacebookId');
    // userGoogleId = localStorage.getItem('userGoogleId');
  // headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:true ,dropdownHeader:false};
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false,processHeader:true};
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private registerService:RegisterService, private toastr: ToastrService,
    private serviceHeaderChange:ChangeHeaderService, private router:Router) { }
  onSubmitRegisterData(val){
    if(this.userFacebookId){
      this.submittedRegisteredData = true;
    if(this.facebookModel.mobile){
      let requestOtp = new RequestOtpFBModel(this.facebookModel.mobile,this.facebookModel.fullName,
      this.facebookModel.emailAddress,"1234","1234","FB",this.userFacebookId,"WEB");
      this.registerService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
            if(response["body"].status=="1"){
              this.localStorage.setItem('fbRegisteredUser','true');
              this.localStorage.setItem('fbUserMobileNumber',this.facebookModel.mobile);
              this.showSuccess(response["body"].message);
              this.router.navigate(['/login-otp']);
            }
            else{
              this.showError(response["body"].message);
            }
        }
      })
    }
  }
  else if(this.userGoogleId){
    this.submittedRegisteredData = true;
    if(this.googleModel.mobile){
      let requestOtp = new RequestOtpFBModel(this.googleModel.mobile,this.googleModel.fullName,
      this.googleModel.emailAddress,"1234","1234","GOOGLE_PLUS",this.userGoogleId,"WEB");
      this.registerService.requestOtpService(requestOtp).subscribe(response=>{
      if(response && response["body"]){
        if(response["body"].status=="1"){
          this.localStorage.setItem('googleRegisteredUser','true');
          this.localStorage.setItem('googleUserMobileNumber',this.googleModel.mobile);
          this.showSuccess(response["body"].message);
          this.router.navigate(['/login-otp']);
        }
        else{
          this.showError(response["body"].message);
        }
      }
    })
  }
}
else{
    this.submittedRegisteredData = true;
      let requestOtp = new RequestOtpModel(this.registerModel.mobile , 1234);
      this.registerService.requestOtpService(requestOtp).subscribe(response=>{
        if(response && response["body"]){
          if(response["body"].status=="1"){
            this.showSuccess(response["body"].message);
            this.localStorage.setItem('mobile',this.registerModel.mobile);
            this.localStorage.setItem('emailAddress',this.registerModel.emailAddress);
            this.localStorage.setItem('fullName',this.registerModel.fullName);
            this.router.navigate(['/login-otp']);
          }   
          else{
            this.showError(response["body"].message);
          }     
        }        
      }) 
    }
  }  
  showError(Error) {
    this.toastr.error(Error);
  }
  showSuccess(Error) {
    this.toastr.success(Error);
  }
  retrieveServiceFromCart='';
  // retrieveServiceFromCart = localStorage.getItem('serviceName');
  //---------Comment After json end error----------
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveItemsFromCart='';
  // retrieveItemsFromCart = localStorage.getItem('itemList');

  //---------Comment After json end error----------
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
  retrieveServiceFromCartArray:any;
  retrieveItemsFromCartArray:any;
  ngOnInit() {
    this.registerModel = {fullName:'',emailAddress:'',mobile:localStorage.getItem('mobileNumber')};

    this.facebookModel = {fullName:localStorage.getItem('fbUserName'),
    emailAddress:localStorage.getItem('fbUserEmail'),mobile:'' };

    this.googleModel = {fullName:localStorage.getItem('googleUserName'),
    emailAddress:localStorage.getItem('googleUserEmail'),mobile:''};

    this.userFacebookId = localStorage.getItem('userFacebookId');
    this.userGoogleId   = localStorage.getItem('userGoogleId');

    this.retrieveServiceFromCart = JSON.parse(localStorage.getItem('serviceName'));
    this.retrieveItemsFromCart   = JSON.parse(localStorage.getItem('itemList'));

    this.serviceHeaderChange.changeHeader(this.headerValue);

    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCart);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCart);

    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
    this.serviceHeaderChange.setGetACallCSS(false);

   
  }
}
