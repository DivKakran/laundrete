import { Component, OnInit } from '@angular/core';
import {ChangeHeaderService} from '../changeHeaderService'
import {Router , ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-blog-redirect',
  templateUrl: './blog-redirect.component.html',
  styleUrls: ['./blog-redirect.component.css']
})
export class BlogRedirectComponent implements OnInit {

  constructor(private chnge: ChangeHeaderService , private ac: ActivatedRoute) { 
    
  }

  ngOnInit() {
    this.ac.params.subscribe((res) => { 
      if(res['id'] == undefined){
        window.location.href = "http://blog.launderette.staging.birdapps.org/";
      }else{
        window.location.href = "http://blog.launderette.staging.birdapps.org/"+res['id'];
      }
    });
    this.chnge.blogActive('check');
  }
}
