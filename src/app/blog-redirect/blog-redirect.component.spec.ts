import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogRedirectComponent } from './blog-redirect.component';

describe('BlogRedirectComponent', () => {
  let component: BlogRedirectComponent;
  let fixture: ComponentFixture<BlogRedirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogRedirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
