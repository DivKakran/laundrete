import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class ViewAddressService {
    private getProfileUrl = environment.baseApiURl + 'profile/profile';
    private proceedApi    = environment.baseApiURl + 'proceed/laundry';
    constructor(private http: HttpClient) {
    }

    getUserProfile(userId) {
        let headers = new HttpHeaders({ "Content-Type": "application/json" }).append('Authorization', 'Basic YWRtaW46MTIzNA==');
        return this.http.request(new HttpRequest(
            'POST',
            this.getProfileUrl,
            userId,
            {
                headers: headers
            }
        ))

    }
    getLaundryDetail(pincode , id){
        let data ={postcode:pincode , laundry_id: id };
        let headers = new HttpHeaders({ "Content-Type": "application/json" }).append('Authorization', 'Basic YWRtaW46MTIzNA==');
        return this.http.request(new HttpRequest(
            'POST',
            this.proceedApi,
            data,
            {
                headers: headers
            }
        ))
    }
}