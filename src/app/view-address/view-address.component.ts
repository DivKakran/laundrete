import { localeData } from 'moment';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import {ViewAadressModel} from './viewAddressModel';
import {ViewAddressService} from './viewAddressService';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
@Component({
  selector: 'app-view-address',
  templateUrl: './view-address.component.html',
  styleUrls: ['./view-address.component.css'],
  providers: [ViewAddressService]
})
export class ViewAddressComponent implements OnInit {
  showAddressOff='';
  user_id='';
  // user_id = localStorage.getItem("userId");
  userProfileData;
  alternate={altNumber:''};
  multiplePincodes='';
  // multiplePincodes = localStorage.getItem('laundry_pincodes');
 
  laundryPincodeArray;
  // laundryPincodeArray = this.multiplePincodes.split(",");
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private serviceHeaderChange:ChangeHeaderService,private addressService:ViewAddressService,
    private toastr: ToastrService, private router:Router) { }
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false, processHeader:true};
  getUserProfileService() {
    let userProfileModelData = new ViewAadressModel(this.localStorage.getItem("userId"));
    this.addressService.getUserProfile(userProfileModelData).subscribe(response => {
      if (response && response["body"]) {
        this.userProfileData = response["body"].addressDetails;
        if(response["body"].addressDetails.HOME && response["body"].addressDetails.WORK && response["body"].addressDetails.OTHER){
          if(response["body"].addressDetails.HOME.address!=''){
            this.showAddressOff = 'home';
          }
          if(response["body"].addressDetails.WORK.address!=''){
            this.showAddressOff = 'work';
          }
          if(response["body"].addressDetails.OTHER.address!=''){
            this.showAddressOff = 'other';
          }
        }  
      }
    });
  }
  showWarning(Error) {
    this.toastr.warning(Error);
  }
  changeShowAddressOff(val){
    this.showAddressOff=val;
  }
  pincodeMatched = false;
  viewAddressDone(showAddress){
    this.localStorage.setItem('alterNumber',this.alternate.altNumber);
    if(showAddress==''){
      this.showWarning('Please Enter Address to Proceed');
    }
    else{
      // this.router.navigate(['/date-time']);
      if(showAddress=='home'){ 
        this.laundryPincodeArray.filter((res)=>{
          if(res == this.userProfileData.HOME.pincode){
            this.userProfileData.HOME.addressType = showAddress;
            this.localStorage.setItem('selectedAddress',showAddress);
            this.localStorage.setItem('selectedAddressDetails',JSON.stringify(this.userProfileData.HOME));
            if(localStorage.getItem("laundryTypeForGst") == "true"){
              this.addressService.getLaundryDetail(this.userProfileData.HOME.pincode  , localStorage.getItem('laundryIdMain')).subscribe((res ) => {
                if(res && res['body']){
                  let initialSetting = JSON.parse(localStorage.getItem('laundry_settings'));
                  let secondCgst = JSON.parse(localStorage.getItem('selectedAddressDetails'));
                  // updating
                  secondCgst.cgst = res['body'].laundry_settings.cgst;
                  secondCgst.sgst = res['body'].laundry_settings.sgst;
                  secondCgst.igst = res['body'].laundry_settings.igst;

                  initialSetting.express_percent = res['body'].laundry_settings.express_percent;
                  initialSetting.cgst = res['body'].laundry_settings.cgst;
                  initialSetting.sgst = res['body'].laundry_settings.sgst;
                  initialSetting.igst = res['body'].laundry_settings.igst;
                  initialSetting.min_cart_amount = res['body'].laundry_settings.min_cart_amount;
                  initialSetting.delivery_amt    = res['body'].laundry_settings.delivery_amt;
                  initialSetting.lId = res['body'].laundry_settings.laundry_id;
                  if(localStorage.getItem('typeOfLaundryOnSelect') == "PREMIUM"){ 
                    localStorage.setItem('laundryId' , res['body'].laundry_settings.laundry_id);
                    localStorage.setItem('forSlotId' , '');
                    // localStorage.setItem('laundryIdMain' , res['body'].laundry_settings.laundry_id);
                  }
                  
                  localStorage.setItem('laundryId' , res['body'].laundry_settings.laundry_id);
                  // localStorage.setItem('laundryIdMain' , res['body'].laundry_settings.laundry_id);

                  localStorage.setItem('laundry_settings' , JSON.stringify(initialSetting));
                  localStorage.setItem('selectedAddressDetails' , JSON.stringify(secondCgst));
                  // console.log('initial setting now' , secondCgst);
                }
              });
            }else{
              this.addressService.getLaundryDetail(this.userProfileData.HOME.pincode  ,
                localStorage.getItem('laundryId')).subscribe((res ) => {
                  if(res && res['body']){
                    let initialSetting = JSON.parse(localStorage.getItem('laundry_settings'));
                    initialSetting.cgst = res['body'].laundry_settings.cgst;
                    initialSetting.sgst = res['body'].laundry_settings.sgst;
                    initialSetting.igst = res['body'].laundry_settings.igst;
                    initialSetting.min_cart_amount = res['body'].laundry_settings.min_cart_amount;
                    initialSetting.delivery_amt    = res['body'].laundry_settings.delivery_amt;
                    initialSetting.lId = res['body'].laundry_settings.laundry_id;
                    localStorage.setItem('laundry_settings' , JSON.stringify(initialSetting));
                    console.log('initial setting now' , initialSetting);
                  }
              });
            }
            this.router.navigate(['/date-time']);
            this.pincodeMatched=true;
          }
        });
        if(!this.pincodeMatched){
          this.showWarning("Laundry service is not available in " + this.userProfileData.HOME.pincode + " postcode for Pickup/Delivery");
        }
      }
      else if(showAddress=='work'){ 
        this.laundryPincodeArray.filter((res)=>{ console.log(res);
          if(res == this.userProfileData.WORK.pincode){
            this.userProfileData.WORK.addressType = showAddress;
            this.localStorage.setItem('selectedAddress',showAddress);
            this.localStorage.setItem('selectedAddressDetails',JSON.stringify(this.userProfileData.WORK));
            if(localStorage.getItem("laundryTypeForGst") == "true"){  
              this.addressService.getLaundryDetail(this.userProfileData.WORK.pincode  , localStorage.getItem('laundryIdMain')).subscribe((res ) => {
                if(res && res['body']){   
                  let initialSetting = JSON.parse(localStorage.getItem('laundry_settings'));

                  let secondCgst = JSON.parse(localStorage.getItem('selectedAddressDetails'));
                  // updating
                  secondCgst.cgst = res['body'].laundry_settings.cgst;
                  secondCgst.sgst = res['body'].laundry_settings.sgst;
                  secondCgst.igst = res['body'].laundry_settings.igst;

                  initialSetting.express_percent = res['body'].laundry_settings.express_percent;
                  initialSetting.cgst = res['body'].laundry_settings.cgst;
                  initialSetting.sgst = res['body'].laundry_settings.sgst;
                  initialSetting.igst = res['body'].laundry_settings.igst;
                  initialSetting.min_cart_amount = res['body'].laundry_settings.min_cart_amount;
                  initialSetting.delivery_amt    = res['body'].laundry_settings.delivery_amt;
                  initialSetting.lId = res['body'].laundry_settings.laundry_id;
                  if(localStorage.getItem('typeOfLaundryOnSelect') == "PREMIUM"){ 
                    localStorage.setItem('laundryId' , res['body'].laundry_settings.laundry_id);
                    localStorage.setItem('forSlotId' , '');
                  }

                  localStorage.setItem('laundryId' , res['body'].laundry_settings.laundry_id);

                  localStorage.setItem('laundry_settings' , JSON.stringify(initialSetting));
                  localStorage.setItem('selectedAddressDetails' , JSON.stringify(secondCgst));
                  // console.log('initial setting now' , initialSetting);
                }
              });
            }else{ 
              this.addressService.getLaundryDetail(this.userProfileData.WORK.pincode  ,
                localStorage.getItem('laundryIdMain')).subscribe((res ) => {
                  if(res && res['body']){
                    console.log('daata backend' , res['body']);
                    let initialSetting = JSON.parse(localStorage.getItem('laundry_settings'));
                    initialSetting.cgst = res['body'].laundry_settings.cgst;
                    initialSetting.sgst = res['body'].laundry_settings.sgst;
                    initialSetting.igst = res['body'].laundry_settings.igst;
                    initialSetting.min_cart_amount = res['body'].laundry_settings.min_cart_amount;
                    initialSetting.delivery_amt    = res['body'].laundry_settings.delivery_amt;
                    initialSetting.lId = res['body'].laundry_settings.laundry_id;

                    localStorage.setItem('laundryId' , res['body'].laundry_settings.laundry_id)

                    localStorage.setItem('laundry_settings' , JSON.stringify(initialSetting));
                    console.log('initial setting now' , initialSetting);
                  }
              });
            }
            this.router.navigate(['/date-time']);
            this.pincodeMatched=true;
          }
        });
        if(!this.pincodeMatched){ 
          this.showWarning("Laundry service is not available in " + this.userProfileData.WORK.pincode + " postcode for Pickup/Delivery");
        }
      }
      else if(showAddress=='other'){
        this.laundryPincodeArray.filter((res)=>{
          if(res == this.userProfileData.OTHER.pincode){
            this.userProfileData.OTHER.addressType = showAddress;
            this.localStorage.setItem('selectedAddress',showAddress);
            this.localStorage.setItem('selectedAddressDetails',JSON.stringify(this.userProfileData.OTHER));
            if(localStorage.getItem("laundryTypeForGst") == "true"){  
              this.addressService.getLaundryDetail(this.userProfileData.OTHER.pincode  , localStorage.getItem('laundryIdMain')).subscribe((res ) => {
                if(res && res['body']){
                  let initialSetting = JSON.parse(localStorage.getItem('laundry_settings'));

                  let secondCgst = JSON.parse(localStorage.getItem('selectedAddressDetails'));
                  // updating
                  secondCgst.cgst = res['body'].laundry_settings.cgst;
                  secondCgst.sgst = res['body'].laundry_settings.sgst;
                  secondCgst.igst = res['body'].laundry_settings.igst;

                  initialSetting.cgst = res['body'].laundry_settings.cgst;
                  initialSetting.express_percent = res['body'].laundry_settings.express_percent;
                  initialSetting.sgst = res['body'].laundry_settings.sgst;
                  initialSetting.igst = res['body'].laundry_settings.igst;
                  initialSetting.min_cart_amount = res['body'].laundry_settings.min_cart_amount;
                  initialSetting.delivery_amt    = res['body'].laundry_settings.delivery_amt;
                  initialSetting.lId = res['body'].laundry_settings.laundry_id;
                  if(localStorage.getItem('typeOfLaundryOnSelect') == "PREMIUM"){ 
                    localStorage.setItem('laundryId' , res['body'].laundry_settings.laundry_id);
                    localStorage.setItem('forSlotId' , '');
                  }

                  localStorage.setItem('laundryId' , res['body'].laundry_settings.laundry_id);

                  localStorage.setItem('laundry_settings' , JSON.stringify(initialSetting));
                  localStorage.setItem('selectedAddressDetails' , JSON.stringify(secondCgst));
                  // console.log('initial setting now' , initialSetting);
                  console.log('data check' , res['body']);
                }
              });
            }else{
              this.addressService.getLaundryDetail(this.userProfileData.OTHER.pincode  ,
                localStorage.getItem('laundryId')).subscribe((res ) => {
                  if(res && res['body']){
                    let initialSetting = JSON.parse(localStorage.getItem('laundry_settings'));
                    initialSetting.cgst = res['body'].laundry_settings.cgst;
                    initialSetting.sgst = res['body'].laundry_settings.sgst;
                    initialSetting.igst = res['body'].laundry_settings.igst;
                    initialSetting.min_cart_amount = res['body'].laundry_settings.min_cart_amount;
                    initialSetting.delivery_amt    = res['body'].laundry_settings.delivery_amt
                    initialSetting.lId = res['body'].laundry_settings.laundry_id;
                    localStorage.setItem('laundry_settings' , JSON.stringify(initialSetting));
                    // console.log('initial setting now' , initialSetting);
                    console.log('else data check' , res['body']);
                  }
              });
            }
            this.router.navigate(['/date-time']);
            this.pincodeMatched=true;
          }
        });
        if(!this.pincodeMatched){
          this.showWarning("Laundry service is not available in " + this.userProfileData.OTHER.pincode + " postcode for Pickup/Delivery");
        }
      }  
    }
  }
  retrieveServiceFromCart='';
  // retrieveServiceFromCart = localStorage.getItem('serviceName');
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveServiceFromCartArray;
  // retrieveItemsFromCart = localStorage.getItem('itemList');
  retrieveItemsFromCart='';
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
  retrieveItemsFromCartArray;
  ngOnInit() { 
    this.retrieveServiceFromCart = localStorage.getItem('serviceName');
    this.retrieveItemsFromCart   = localStorage.getItem('itemList');
    this.user_id = localStorage.getItem("userId");
    this.retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
    this.retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
    this.multiplePincodes = localStorage.getItem('laundry_pincodes');
    this.laundryPincodeArray = this.multiplePincodes.split(","); console.log('yahoo' , this.laundryPincodeArray);
    this.serviceHeaderChange.changeHeader(this.headerValue);
    this.serviceHeaderChange.setGetACallCSS(false);
    if (this.localStorage.getItem("userId")) {
      this.getUserProfileService();
    }
    this.serviceHeaderChange.userNameDisplay(this.localStorage.getItem('userName'));
    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCartArray);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCartArray);
    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
  }
  navigate(){
    if(this.showAddressOff == ''){
      this.router.navigate(['/add-address'] , {queryParams: {type: 'home'}});
    }else{
      this.router.navigate(['/add-address'] , {queryParams: {type: this.showAddressOff}});
    }
  }
}
