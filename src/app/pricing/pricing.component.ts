import { LOCAL_STORAGE , WINDOW} from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { HostListener } from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import { PricingService } from './pricingService';
import { LocationByPostcode } from './pricingModel';
import { Router } from '@angular/router';
@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css'],
  providers: [PricingService]
})
export class PricingComponent implements OnInit {

  addStickClass = false;
  serviceAreas = false;
  serviceAreaResponse;
  searchText = '';
  cityName;
  
  headerValue = { homeHeader: false, pricingHeader: true, serviceHeader: false, dropdownHeader: false, processHeader: false };
  constructor(private changeHeader: ChangeHeaderService, private pricingService: PricingService,
    private router: Router) { }

  serviceAvail() {
    this.serviceAreas = true;
    let data1 = "";
    this.pricingService.checkServiceArea(data1).subscribe(response => {
      this.serviceAreaResponse = response;
      if (response && response["body"]) {
        this.serviceAreaResponse = response["body"].serviceList;
      }
    });
  }
  serviceAreasPopup(postcode) {
    this.serviceAreas = false;
    let locationByPostcode = new LocationByPostcode(postcode);
    this.pricingService.checkLocationByPostcode(postcode).subscribe(response => {
      if (response && response["body"]) {
        this.cityName = response["body"].cityName;
        localStorage.setItem("cityName", this.cityName);
        localStorage.setItem("postcode", postcode);
        this.router.navigate(["/service-select"]);
      }
    });
  }

  sorryClose() {
    this.serviceAreas = false;
  }
  ngOnInit() {
    this.changeHeader.changeHeader(this.headerValue);
  }
  @HostListener("window:scroll", [])
  onWindowScroll() {
    let windows = window.innerHeight;
    let position = document.documentElement.scrollTop + windows;
    const scrollHeight = 695;
    const initialHeight = 694;
    if (position >= scrollHeight) {
      this.addStickClass = true;
    }
    else if (position <= initialHeight) {
      this.addStickClass = false;
    }
  }
}
