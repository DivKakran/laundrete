import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'locationPostcode'
})
export class LocationFilter implements PipeTransform {  
  transform(serviceArea: any[], args: any): any {
    return serviceArea.filter(text => {
      if(text && text.name && isNaN(args)){
          return text.name.toLowerCase().includes(args);
      }
      else if(text && text.name && !isNaN(args)){
        return text.postcode.toLowerCase().includes(args);
      }
    })
  }
}

