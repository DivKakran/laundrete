import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()

export class PricingService {
    private serviceAreasUrl = environment.baseApiURl + 'location/serviceArea';
    private locationByPostcodeUrl = environment.baseApiURl + 'location/locationbypostcode';
    constructor(private http: HttpClient) { }

    checkServiceArea(pincode) {
        var formdata = new FormData();
        formdata.append("postcode", pincode);
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' })
        return this.http.request(new HttpRequest(
            'POST',
            this.serviceAreasUrl,
            formdata, {
                headers: headers
            }
        ))
    }
    checkLocationByPostcode(postcode) {
        var formdata = new FormData();
        formdata.append("postcode", postcode);
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' });
        return this.http.request(new HttpRequest(
            'POST',
            this.locationByPostcodeUrl,
            formdata,
            {
                headers: headers
            }
        ))
    }
}