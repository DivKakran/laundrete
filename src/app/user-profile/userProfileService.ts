import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class UserProfileService {
    private getProfileUrl = environment.baseApiURl + 'profile/profile';
    private imageUpdataUrl = environment.baseApiURl + 'profile/updateprofilepicture';
    private userProfileUpdateUrl = environment.baseApiURl + 'profile/profileupdate';
    constructor(private http: HttpClient) {
    }

    getUserProfile(userId) {
        let headers = new HttpHeaders({ "Content-Type": "application/json" }).append('Authorization', 'Basic YWRtaW46MTIzNA==');
        return this.http.request(new HttpRequest(
            'POST',
            this.getProfileUrl,
            userId,
            {
                headers: headers
            }
        ))
    }

    UpdateImage(imageData) {
        var formData = new FormData();
        formData.append('user_id', imageData.user_id);
        formData.append('picturefile', imageData.picturefile);
        return this.http.request(new HttpRequest(
            'POST',
            this.imageUpdataUrl,
            formData
        ))
    }
    updateProfile(updatedUserProfile){
        let headers = new HttpHeaders({'Authorization': 'Basic YWRtaW46MTIzNA=='});
        return this.http.request(new HttpRequest(
            'POST',
            this.userProfileUpdateUrl,
            updatedUserProfile,
            {
                headers:headers
            }
        ))
    }
}