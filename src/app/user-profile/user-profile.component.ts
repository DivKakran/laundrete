import { LOCAL_STORAGE , WINDOW} from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import { UserProfileModel, UpdateImageModel, UpdateUserProfile,AddressDetails,Home,Work,Other } from './userProfileModel';
import { UserProfileService } from './userProfileService';
import { ToastrService } from 'ngx-toastr';
import { Ng2ImgMaxService } from 'ng2-img-max';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [UserProfileService]
})
export class UserProfileComponent implements OnInit {

  constructor(@Inject(WINDOW) private window: Window, @Inject(LOCAL_STORAGE) private localStorage: any, private header: ChangeHeaderService, private profileService: UserProfileService,
    private toastr: ToastrService , private ng2ImgMax: Ng2ImgMaxService) { }
  headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: false, dropdownHeader: true };
  profileModel = { fullName: '', email: '', phone: ''};
  homeAddressModel={home_address:'',home_landmark:'',home_pincode:''};
  workAddressModel={work_address:'',work_landmark:'',work_pincode:''};
  otherAddressModel={other_address:'',other_landmark:'',other_pincode:''};
  updateProfileSubmitted = false;
  editable = true;
  fileToUpload: File = null;
  url: any = '';
  fileType;
  allowedExtensions;
  showAddressOff = 'home';
  user_id = '';
  email_verify;

  getUserProfileService() {
    let userProfileModelData = new UserProfileModel(this.localStorage.getItem("userId"));
    this.profileService.getUserProfile(userProfileModelData).subscribe(response => {
      if (response && response["body"]) {
        this.profileModel.fullName = response['body'].userDetails.name;
        this.profileModel.email = response['body'].userDetails.email;
        this.profileModel.phone = response['body'].userDetails.mobile;
        this.picture_url = response["body"].userDetails.picture_url;
        this.email_verify = response["body"].userDetails.email_verify;
        if(response["body"].addressDetails.HOME){
          this.homeAddressModel.home_address =  response["body"].addressDetails.HOME.address;
          this.homeAddressModel.home_landmark = response["body"].addressDetails.HOME.landmark;
          this.homeAddressModel.home_pincode = response["body"].addressDetails.HOME.pincode;
        }
        if(response["body"].addressDetails.WORK){
          this.workAddressModel.work_address =  response["body"].addressDetails.WORK.address;
          this.workAddressModel.work_landmark = response["body"].addressDetails.WORK.landmark;
          this.workAddressModel.work_pincode = response["body"].addressDetails.WORK.pincode;
        }
        if(response["body"].addressDetails.OTHER){
          this.otherAddressModel.other_address =  response["body"].addressDetails.OTHER.address;
          this.otherAddressModel.other_landmark = response["body"].addressDetails.OTHER.landmark;
          this.otherAddressModel.other_pincode = response["body"].addressDetails.OTHER.pincode;
        }
      }
    })
  }
  // Image Upload
  readUrl(event: any, fileUrl) {
    //  this.fileUrl = fileUrl;
    if (event.target.files && event.target.files[0]) {
      this.fileToUpload = event.target.files[0];
      this.ng2ImgMax.compressImage(this.fileToUpload , 0.02048).subscribe((result) =>{
        // this.fileToUpload = new File([result], result.name); console.log(this.fileToUpload);
        // this.getImagePreview(this.uploadedImage);
      })
      this.allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.url = event.target.result;
        if (!this.allowedExtensions.exec(fileUrl)) {
          this.fileType = 'video';
          return false;
        } else {
          this.fileType = 'image';
          this.updateUserImage();
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  picture_url;

  updateUserImage() {
    let userImageModel = new UpdateImageModel(this.user_id, this.fileToUpload);
    this.profileService.UpdateImage(userImageModel).subscribe(response => {
      if (response && response["body"]) {
        this.picture_url = response["body"].picture_url;
      }
    })
  }

  showWarning(error) {
    this.toastr.warning(error);
  }
  showError(error){
    this.toastr.error(error);
  }
  showSuccess(message){
    this.toastr.success(message);
  }
  checkValidations(val){
    if(this.profileModel.fullName==''){
      this.showWarning('Please Enter User Name');
    }
    else if(this.profileModel.email==''){
      this.showWarning('Please Enter User Email');
    }
    else if(this.homeAddressModel.home_address=='' && this.homeAddressModel.home_pincode!=''){
      this.showWarning('Please Enter Address for Home');
      return false;
    }
    else if(this.homeAddressModel.home_address!=='' && this.homeAddressModel.home_pincode==''){
      this.showWarning('Please Enter Pincode for Home');
      return false;
    }
    else if(this.workAddressModel.work_address=='' && this.workAddressModel.work_pincode!=''){
      this.showWarning('Please Enter Address for Work');
      return false;
    }
    else if(this.workAddressModel.work_address!=='' && this.workAddressModel.work_pincode==''){
      this.showWarning('Please Enter Pincode for Work');
      return false;
    }
    else if(this.otherAddressModel.other_address=='' && this.otherAddressModel.other_pincode!=''){
      this.showWarning('Please Enter Address for Other');
      return false;
    }
    else if(this.otherAddressModel.other_address!=='' && this.otherAddressModel.other_pincode==''){
      this.showWarning('Please Enter Pincode for Other');
      return false;
    }
    else if(this.homeAddressModel.home_pincode=='' && this.workAddressModel.work_pincode=='' &&
     this.otherAddressModel.other_pincode==''){
      this.showWarning('Please Enter Address to Proceed');
      return false;
    }
    else{
      return true;
    }
  }

  saveProfile(val){
    this.updateProfileSubmitted = true;
    if(this.checkValidations(val)){
      let updatedUserData = new UpdateUserProfile(this.profileModel.fullName,new AddressDetails(
        new Home(this.homeAddressModel.home_pincode,this.homeAddressModel.home_landmark,
          this.homeAddressModel.home_address),new Work(this.workAddressModel.work_pincode,this.workAddressModel.work_landmark,
          this.workAddressModel.work_address),new Other(this.otherAddressModel.other_pincode,this.otherAddressModel.other_landmark,
          this.otherAddressModel.other_address)),
          this.localStorage.getItem('userId'),this.profileModel.email,this.profileModel.phone
      );
      this.profileService.updateProfile(updatedUserData).subscribe(response=>{
        if(response && response["body"]){
          if(response["body"].status=="1"){
            this.showSuccess(response["body"].message);
            setTimeout(()=>{
              this.editable = true;
              this.window.location.reload(true)
            },2000);
          }
          else{
            this.showError(response["body"].message);
          }
        }
      })
    }
  }

  editAbleClick() {
    this.editable = false;
  }

  cancelProfileClick() {
    this.editable = true;
  }

  changeShowAddressOff(val){
    this.showAddressOff=val;
  }

  ngOnInit() {
    this.user_id = localStorage.getItem("userId");

    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
    if (this.localStorage.getItem("userId")) {
      this.getUserProfileService();
    }
  }

}
