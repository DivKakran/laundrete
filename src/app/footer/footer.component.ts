import { Component, OnInit } from '@angular/core';
import {ChangeHeaderService} from '../changeHeaderService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  callRequestCSSApply = false;
  linkDownloadCss = false;
  constructor(private addCss:ChangeHeaderService , private route: Router) { }
 
  ngOnInit() {
    this.addCss.setGetACall.subscribe(res=>{
      this.callRequestCSSApply = <boolean>res;
      console.log('this.callRequestCSSApply',this.callRequestCSSApply)
    })
    this.addCss.setDownLoadLink.subscribe(res=>{
      this.linkDownloadCss = <boolean>res;
      console.log('this.linkDownloadCss',this.linkDownloadCss);
    });
  }
  navigate(val){
    window.location.href = "/"+val; 
    // this.route.navigate([val]);
  }
}
