import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'timeConvertor'})
export class TimeConvertorPipe implements PipeTransform {
 transform(z): string {
    if(z<12)
    {
        return z+" AM"
    }
    else if(z==12)
    {
        return z+" PM"
    }
    else if(z>12)
    {
        return z-12+" PM"
    }
    else if(z==24)
    {
        return z-12+" AM"
    }
 }
}