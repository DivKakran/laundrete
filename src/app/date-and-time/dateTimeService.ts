import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class DateTimeService {
    private timeSlotUrl = environment.baseApiURl + 'slot/slot';

    constructor(private http:HttpClient){}

    dateTimeService(postcode , id){ 
        let headers = new HttpHeaders({ 'Authorization': 'Basic YWRtaW46MTIzNA==' }).append('Content-Type','application/json')
        if(id==null){
            return this.http.request(new HttpRequest(
                'POST',
                this.timeSlotUrl,
                postcode,
                {
                    headers:headers
                }
            ))
        } else{
            // postcode.laundry_id = id;
            return this.http.request(new HttpRequest(
                'POST',
                this.timeSlotUrl,
                postcode, 
                {
                    headers:headers
                }
            ));
        }
    }
}