import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , AfterViewInit, Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import { DateTimeModel } from './dateTimeModel';
import { DateTimeService } from './dateTimeService';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'app-date-and-time',
  templateUrl: './date-and-time.component.html',
  styleUrls: ['./date-and-time.component.css'],
  providers: [DateTimeService]
})
export class DateAndTimeComponent implements OnInit, AfterViewInit{
  secondSlotDisplay = false;
  deliveryType;
  pickupSlots;
  deliverySlot;
  normalHours;
  expressHours;
  showDeliveryTime = true;
  loadAPI: Promise<any>;

  pickSlotNotClicked = true;

  dropOffTime: any;
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private serviceHeaderChange:ChangeHeaderService, private slotService:DateTimeService,
    private router:Router , private toaster: ToastrService) { }
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false, processHeader:true};
  retrieveServiceFromCart = '';
  retrieveServiceFromCartArray;
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveItemsFromCart = '';
  retrieveItemsFromCartArray;
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);

  secondSlotActive(){
    this.secondSlotDisplay = true;
  }
  setDeliveryType(val){
    this.deliveryType = val;
  }
  a='';

  mainTimeSlotLatestArray = [];
  mainTimeSlotLatestArrayDelivery = [];

  orgDeliveryDay ;

  updatingSevenDayPickUp = [];

  dateAndTime(){
    let slotData = new DateTimeModel(this.localStorage.getItem('postcode'));
    
    this.slotService.dateTimeService(slotData , localStorage.getItem('laundryIdForTimeSlot')).subscribe(response=>{
      if(response && response["body"]){
        this.onclickService = new Object(response["body"].DELIVERY);

        this.normalHours = response["body"].laundry_settings.normal_hours;
        this.expressHours = response["body"].laundry_settings.express_hours;
        if(response["body"].PICK_UP.length>0){
          this.pickupSlots = response["body"].PICK_UP; 
          response["body"].PICK_UP.filter((res , i) => {
            if(i< 7){
              this.updatingSevenDayPickUp.push(res);
            }
          });
          this.datePickUpDay = response["body"].PICK_UP[0].date;
          response["body"].PICK_UP.filter((res , index) => { 
            // this.datePickUpDay = res.date; 
            let obj = res.MORNING;
            if(obj.length>0){
              let obj2 = obj.concat(res.AFTERNOON);
              this.currentDateObject = obj2.concat(res.EVENING);
              this.mainTimeSlotLatestArray.push(this.currentDateObject);
            }
          });
          this.removingDateIfNoSlotAvailable(this.mainTimeSlotLatestArray , response["body"].DELIVERY);
          // let obj = this.pickupSlots[0].MORNING;
          // if(obj.length>0){
          //   let obj2 = obj.concat(this.pickupSlots[0].AFTERNOON);
          //   this.currentDateObject = obj2.concat(this.pickupSlots[0].EVENING);
          //   this.mainTimeSlotLatestArray.push( this.currentDateObject);
          //   console.log('--------------',this.mainTimeSlotLatestArray);
          // }
                 
          for(let k=0; k<response["body"].PICK_UP.length;k++){
            let pickUp = response["body"].PICK_UP[k].MORNING;
            for(let j=0; j<pickUp.length;j++){
              let mroningSlot = pickUp[j].service_from
            //  this.a= this.tConvert(mroningSlot+':00')
            }
          }
          for(let k=0; k<response["body"].PICK_UP.length;k++){
            let pickUp = response["body"].PICK_UP[k].AFTERNOON;
            for(let j=0; j<pickUp.length;j++){
              let mroningSlot = pickUp[j].service_from
              // this.a= this.tConvert(mroningSlot+':00')
            }
          }
          for(let k=0; k<response["body"].PICK_UP.length;k++){
            let pickUp = response["body"].PICK_UP[k].EVENING;
            for(let j=0; j<pickUp.length;j++){
              let mroningSlot = pickUp[j].service_from
              this.a+= this.tConvert(mroningSlot+':00')
            }
          }
        }

        if(response["body"].DELIVERY.length>0){

          this.deliverySlot = response["body"].DELIVERY;
          

          // this.orgDeliveryDay = response["body"].DELIVERY;
          response["body"].DELIVERY.filter((res , i) => {
            if(i>1 && i<=8){
              // this.deliveryDateArray.push(res);
            }
          }); 
          this.deliverySlot.filter((res , i) => {
            res.setDisable = false;
          })
          response["body"].DELIVERY.filter((res , index) => {
            let obj = res.MORNING;
            if(obj.length>=0){
              let obj2 = obj.concat(res.AFTERNOON);
              this.currentDateObject = obj2.concat(res.EVENING);
              this.mainTimeSlotLatestArrayDelivery.push( this.currentDateObject);
            }
          }); 
          let obj = this.deliverySlot[0].AFTERNOON;
          if(obj.length>0){
            this.currentDateObject2  = obj.concat(this.deliverySlot[0].EVENING);
          }
        }
      }
    });
  }

  onclickService: any;

  currentDateObject
  currentDateObject2;

  getTimeSlot(val){   
    this.currentDateObject = val;
  }
  test(d){
    this.showDeliveryTime = true;
  }
  laundrySetting: any;
  laundryIdForTimeSlot: any;

  gapForDeliveryDate: any;

  dateDeletedIfTimeSlotFull = 0;

  ngOnInit() {
    this.laundryIdForTimeSlot = localStorage.getItem('laundryIdForTimeSlot');
    this.laundrySetting = JSON.parse(localStorage.getItem('laundry_settings'));
    this.dropOffTime = (JSON.parse(localStorage.getItem('laundry_settings'))).normal_hours_gap;
    this.setUpGapForDelivery(moment().add(this.laundrySetting.normal_hours ,'hours').date());

    this.setUpDateTime(this.laundrySetting);
    this.retrieveServiceFromCart = localStorage.getItem('serviceName');
    this.retrieveItemsFromCart = localStorage.getItem('itemList');
    this.retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
    this.retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
    this.serviceHeaderChange.setGetACallCSS(false);
    this.dateAndTime();
    this.serviceHeaderChange.changeHeader(this.headerValue);
    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCartArray);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCartArray);
    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
  }
  async ngAfterViewInit(){
    this.loadAPI = new Promise((resolve) => {
      // this.loadScripts();
      resolve(true);
    });

  }
  private loadScript(scriptUrl: string) {
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptUrl;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    });
  }
  submited(){
    let deliveryDetail = { "pickUpTime"       : this.datePickUpDay, 
                           "pickUpFromSlot"   : this.selectTimePickUpFrom, 
                           "pickUpToSlot"     : this.selectTimePickUpTo, 
                           "deliveryTime"     : this.selectDeliverDate, 
                           "deliveryFromSlot" : this.selectTimeDeliveryFrom, 
                           "deliveryToSlot"   : this.selectTimeDeliveryTo,
                           "serviceType"      : this.serviceType
                         }
    if(this.selectTimePickUpFrom==0){
      this.toaster.error("Select Pick Up Time slot");
    } else if(!this.selectDeliverDate){
      this.toaster.error("Select Delivery Date");
    } else if(!this.selectTimeDeliveryFrom || this.selectTimeDeliveryFrom  == ''){
      this.toaster.error("Select Delivery Slot");
    }else{
      localStorage.setItem("placeOrderDeliveryDetail" , JSON.stringify(deliveryDetail));
      this.router.navigate(['/payment-option']);
    }
  }
   tConvert (time) {
    // time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time]; 
    // if (time.length > 1) { 
    //   time = time.slice (1);  
    //   time[5] = +time[0] < 12 ? 'AM' : 'PM'; 
    //   time[0] = +time[0] % 12 || 12; 
    // }
    // return time.join (''); 
  
    if(time<12)
        {
          return time+" AM"
        }
        else if(time==12)
        {
          return time+" PM"
        }
        else if(time>12)
        {
          return time-12+" PM"
        }
        else if(time==24)
        {
          return time-12+" AM"
        }
  }  
  // a =  this.tConvert ('15:00:00');

  // ngAfterViewInit(){
  //   // alert()
  // }


  public loadScripts() {        
    var isFound = false;
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
        if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("loader")) {
            isFound = true;
        }
    }

    if (!isFound) {
        var dynamicScripts = ["assets/js/jquery.scrolling-tabs.js" , "assets/js/st-demo.js"];

        for (var i = 0; i < dynamicScripts .length; i++) {
            let node = document.createElement('script');
            node.src = dynamicScripts [i];
            node.type = 'text/javascript';
            node.async = false;
            node.charset = 'utf-8';
            document.getElementsByTagName('body')[0].appendChild(node);
        }

    }
  }

  // addinf classes on select time slots
  selectPickUpDay:number = 0;
  datePickUpDay;

  selectTimePickUpFrom = 0;
  selectTimePickUpTo;
  selectedPickUpTime;

  // for class
  setDateTimeable = true; 

  clickPickUpDay(i , date){ 
    // setting delivery date slot null after selecting pick up day again
    this.selectDeliverDate = '';
    this.selectTimeDeliveryFrom = '';
    this.selectDeliveryDay = 0;

    this.selectedPickUpTime = 100;
    this.selectPickUpDay = i;
    this.datePickUpDay = date;
    let cDate = new Date(date).getDate();
    if(new Date().getDate() == cDate){
      this.setDateTimeable = true;
    }else{ 
      this.setDateTimeable = false;
    }
  }

  settingDisable:any = 7;
  settingActiveOnSelect = 0;

  deliveryDateArray = [];
  convert(t, y){ 
    if(parseInt(t) < parseInt(y)){
     return true;
    }else{
      return false;
      // return this.deliveryDateArray[0].date == "2018-12-29";
      // if(this.deliveryDateArray[0].date == "2018-12-29"){
      //   return true
      // }else{
      //   return false;
      // }
    }
  }

  calculatingHourGap(){ 
    let a = new Date(this.datePickUpDay); 
    if(!this.isExpress){
      a.setDate(a.getDate() + this.laundrySetting.normal_hours/24);
    }else{
      a.setDate(a.getDate() + this.laundrySetting.express_hours/24);
    }
    return a;   
  }

  makingAvailableDeliveryTimeSlot(toTimeOfPickUp){ 
    if(this.serviceType = "express"){ 
      let selectedPickUpDay = this.datePickUpDay; 
      this.deliveryDateArray = []; 
      this.deliverySlot.filter((res ,i ) => {
        res.setDisable = false;
        if(new Date(selectedPickUpDay).getDate() < new Date(res.date).getDate()){
          this.deliveryDateArray.push(res);
        }
      });
    }else {
      this.deliveryDateArray = []; 
      this.deliverySlot.filter((res ,i ) => {
        res.setDisable = false;
        if(i>2 &&  i<=9){
          this.deliveryDateArray.push(res);
        }
      });
    }
  }
  async clickPickUpTimeSlot(i , from , to){ 
  // this.makingAvailableDeliveryTimeSlot(to);
  // end last editing

   this.disableIssueResolved = 1;

    this.deliveryDateArray = [];
    this.selectDeliverTime = 100; 

    this.pickSlotNotClicked = false; 

     // setting delivery date slot null after selecting pick up day again
     this.selectDeliverDate = '';
     this.selectTimeDeliveryFrom = '';
     this.selectDeliveryDay = 0;

    this.selectedPickUpTime = i;
    this.selectTimePickUpFrom = from; 
    this.selectTimePickUpTo = to;
    
    let hourGap = await this.calculatingHourGap();
    
    this.deliverySlot.filter((res ,i ) => {
      let _temp = new Date(res.date); 
      if(_temp< hourGap){
        res.setDisable = true;
        // this.settingActiveOnSelect = ++i;
        // this.settingDisable = i+ +this.laundrySetting.delivery_days; 
        this.settingDisable = i+ +7; 
      } else {
        if(i <= this.settingDisable){
          res.setDisable = false; 
          this.deliveryDateArray.push(res); 
          this.selectDeliverDate = this.deliveryDateArray[0].date;
        }else{
          res.setDisable = true;
        }
      }
    }); 
  }
  // delivery time slot
   selectDeliveryDay: number = 0;

   selectTimeDeliveryFrom;
   selectTimeDeliveryTo;
   selectDeliverDate;
   selectDeliverTime;

   afterClicking = true;
  clickDeliveryDay(i , date){ 
    this.selectDeliveryDay = i;
    this.selectDeliverDate = date; 
    if(this.disableIssueResolved!=0){
      if(this.deliveryDateArray[0].date == date){
        this.afterClicking = true; 
      }else{   
        this.afterClicking = false;
      }
    }else{ 
      return true;
    }
  }
  clickDeliveryTimeSlot(i , from , to){
    this.selectTimeDeliveryFrom = from;
    this.selectTimeDeliveryTo = to;
    this.selectDeliverTime = i;
  }

  isExpress = false;
  serviceType = "NORMAL";
  
  removingDateIfNoSlotAvailable(val , deliverySlot){ 
    let valu = val[0];
    val[0].filter((res) => {
      if(res.service_from < this.currentTimeHour){
        
      }
    });
    if((val[0])[val[0].length-1].service_from < this.currentTimeHour){
      this.dateDeletedIfTimeSlotFull = 1;
      this.datePickUpDay = this.pickupSlots[1].date;
      this.currentTimeHour = ''; 
      this.changeDeliveryDate = true;
      this.updatingSevenDayPickUp = [];
      this.pickupSlots.filter((res , i) => {
        if(i!=0 && i< 8){
          this.updatingSevenDayPickUp.push(res);
        }
      });
      this.deliveryDateArray = []; 
      deliverySlot.filter((res ,i ) => {
        res.setDisable = false;
        if(i>2 &&  i<=9){
          this.deliveryDateArray.push(res);
        }
      });
    }else{
      deliverySlot.filter((res , i) => {
        if(i>1 && i<=8){
          this.deliveryDateArray.push(res);
        }
      }); 
    }
    
  }
  changeDeliveryDate = false;


  disableIssueResolved = 0;
  typeOfService(val){ 
    this.disableIssueResolved = 0;
    this.pickSlotNotClicked = true; 
    if(val == 'express'){
      let date = new Date().getHours();
      this.isExpress = true;
      this.serviceType = "EXPRESS";
      //setting default tyming
      this.selectPickUpDay = 0;

      if(this.dateDeletedIfTimeSlotFull == 0){
        this.datePickUpDay = this.pickupSlots[0].date;
      }else{
        this.datePickUpDay = this.pickupSlots[1].date;
      }
      if(new Date() == new Date(this.updatingSevenDayPickUp[0].date)){
        this.setUpDateTime(this.laundrySetting);
      }

      this.setDateTimeable = true;

      this.selectedPickUpTime=100;

      this.settingDisable = 7;

      this.selectTimePickUpFrom = 0;


      // this.datePickUpDay = new Date().setDate();


      // this.settingActiveOnSelect = 1;

      this.deliveryDateArray = [];
      this.selectDeliverTime = 100;

      if(this.changeDeliveryDate){ 
        this.onclickService.filter((res ,i ) => { 
          res.setDisable = false;
          if(i>1 &&  i<=8){
            this.deliveryDateArray.push(res);
          }
        });
      }else{
        this.onclickService.filter((res ,i ) => {
          res.setDisable = false;
          if(i>0 &&  i<=7){
            this.deliveryDateArray.push(res);
          }
        });
      }
      if(new Date() == new Date(this.updatingSevenDayPickUp[0].date)){
        this.currentTimeHour =  date + +this.laundrySetting.express_hours_gap; 
      }
    } 
    // main else
    else{
      this.selectPickUpDay = 0;
      if(new Date() == new Date(this.updatingSevenDayPickUp[0].date)){
        this.setUpDateTime(this.laundrySetting);
      }
      this.setDateTimeable = true;
      this.selectedPickUpTime=100;
      this.settingDisable = 7;
      this.selectTimePickUpFrom = 0;
      
      this.deliveryDateArray = [];
      this.selectDeliverTime = 100;
      if(this.changeDeliveryDate){
        this.deliverySlot.filter((res ,i ) => {
          res.setDisable = false;
          if(i>2 &&  i<=9){
            this.deliveryDateArray.push(res);
          }
        });
      }else{
       this.deliverySlot.filter((res ,i ) => {
          res.setDisable = false;
          if(i>1 &&  i<=8){
            this.deliveryDateArray.push(res);
          }
        });
      }
     this.isExpress = false;
     this.serviceType = "NORMAL";
    }
  }


  currentTimeHour;
  setUpDateTime(val){
    let date = new Date().getHours(); 
    
    this.currentTimeHour =  date + +val.normal_hours_gap;
  }
  gapHours: any;
  
  setUpGapForDelivery(gapDate){
    this.gapForDeliveryDate = gapDate;
    this.gapHours = gapDate - new Date().getDate();
  }
}
