import { Component , OnInit } from '@angular/core';
import {ChangeHeaderService} from './changeHeaderService';
import {SEOService} from './seo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  check = false;
  // applyOpacityBackground = true;
  constructor(private chnge: ChangeHeaderService , private seoService: SEOService){}
  ngOnInit(){
    this.seoService. addSeoData()
    this.chnge.blog.subscribe((res) => {
      this.check = <boolean>res;
    });
  }
  onActivate(event) {
    window.scroll(0,0);
  }
}
