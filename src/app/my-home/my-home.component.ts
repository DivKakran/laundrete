import { Component, OnInit, Inject } from '@angular/core';
import { LOCAL_STORAGE , WINDOW} from '@ng-toolkit/universal';
import{Router} from '@angular/router';
import { HeaderService } from '../header/headerService';
import { LocationByPostcode } from '../header/headerServicesModel';

@Component({
  selector: 'app-my-home',
  templateUrl: './my-home.component.html',
  styleUrls: ['./my-home.component.css'],
  providers: [HeaderService]
})
export class MyHomeComponent implements OnInit {
  pincodeText = '';
  submitted = false;
  sorryPopup = false;
  serviceAreas = false;
  serviceAreaResponse;
  serviceAreaError;
  locationByPostcodeResponse;
  locationByPostcodeError;
  searchText = "";
  cityName = '';
  pincodeTextModel = { pincodeText: '' };
  constructor(@Inject(WINDOW) private window:Window,@Inject(LOCAL_STORAGE) private localStorage:any,private router: Router, private headerService : HeaderService) { }
  ngOnInit() {}
  onSubmit(pincode) {
    this.submitted = true;
    if (pincode.pincodeText == "" || pincode.pincodeText == undefined) {
      this.sorryPopup = true;
    }
    if (pincode.pincodeText) {    
      let locationByPostcode = new LocationByPostcode(pincode.pincodeText);
      this.headerService.checkLocationByPostcode(pincode.pincodeText).subscribe(response => {
        this.locationByPostcodeResponse = response;
        if (response && response["body"]) {
          if(response["body"].status=="1"){
            console.log("response laundaryList :", response["body"].laundaryList.length);
            this.cityName = response["body"].cityName;            
            this.localStorage.setItem("cityName",this.cityName);
            this.localStorage.setItem("postcode" ,pincode.pincodeText);
            if(response["body"].laundaryList.length==1){
              this.router.navigate(["/service-select"]);
            }
            else{
              //move to multiple laundry
              this.router.navigate(["multiple-laundry"]);
            }
          }
        else{
          alert(response["body"].message);
          //this.sorryPopup = true;
          //console.log("response :", response["body"]);
        }
        }
      },
        (error) => {
          this.locationByPostcodeError = error;
        })
    }
  }
}
