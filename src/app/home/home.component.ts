import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, Inject} from '@angular/core';
import {ChangeHeaderService} from '../changeHeaderService';
import {HomeService} from './homeService';
import { GetInTouchFormModel, SubscriptionFormModel,RequestACall } from './homeServiceModel';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[HomeService]
})
export class HomeComponent implements OnInit {
  submittedGetInTouch = false;
  submittedSubscribeData = false;
  requestCallDetail = {fullName:'',mobile:''}
  subscribeModel = {emailAddress:''};
  requestBtnText = "REQUEST A CALL";
  disableButton = false;
  storeBlock = false;
  appStore = false;
  playStore = false;
  constructor(@Inject(WINDOW) private window: Window, 
  private header: ChangeHeaderService , 
  private homeService:HomeService ,
  private toastr: ToastrService) { }

  getInTouchModel = {fullName:'',mobile:'',email:'',city:'',comment:''};


  onSubmitGetInTouch(getInTouch:FormGroup){
    this.submittedGetInTouch = true;
    if(getInTouch.value.mobile=="0000000000" || getInTouch.value.mobile==""){
      this.showError('Mobile number is required');
    }
    else if(getInTouch.value.mobile.length>10 || getInTouch.value.mobile.length<10){
      this.showError('Mobile number should be 10 digit');
    }
    
    else{
      let getInTouchData = new GetInTouchFormModel(getInTouch.value.fullName,getInTouch.value.mobile,
        getInTouch.value.email,getInTouch.value.city,getInTouch.value.comment);
        this.homeService.getInTouchService(getInTouchData).subscribe(response=>{
          if(response && response["body"]){
            if(response["body"].status=="1"){
              this.showSuccess(response["body"].message);
              getInTouch.reset();
            }
            else{
              this.showError(response["body"].message);
              getInTouch.reset();
            }
          }
      })
    }
  }
  showSuccess(success) {
    this.toastr.success(success);
  }
  showError(Error) {
    this.toastr.error(Error);
  }

  onSubmitSubscribe(emailAddress){
    this.submittedSubscribeData = true;
         let subscribeModelData = new SubscriptionFormModel(this.subscribeModel.emailAddress);
         this.homeService.subscribeUser(subscribeModelData).subscribe(response=>{
          if(response["body"] && response["body"]){
              if(response["body"].status=="0"){
              this.showError(response["body"].message);
              }
              else{
              this.showSuccess(response["body"].message);
              }
            }
          },
          )
  }

  onSubmitRequestForm(requestForm){
    if(this.requestCallDetail.fullName=="" && this.requestCallDetail.mobile==""){
      this.showError('Please Enter Details');
    }
    else if(this.requestCallDetail.fullName=="" && this.requestCallDetail.mobile.length<10){
      this.showError('Please Enter Valid Mobile No.');
    }
    else if(this.requestCallDetail.fullName!="" && this.requestCallDetail.mobile.length<10){
      this.showError('Please Enter Valid Mobile No.');
    }
    else{
      this.requestBtnText = "THANKS FOR CONNECTING!"
      this.disableButton = true;
      let requestModelData = new RequestACall(this.requestCallDetail.mobile,this.requestCallDetail.fullName);
      this.homeService.requestCallService(requestModelData).subscribe(response=>{
        if(response && response["body"]){
          if(response["body"].status=="1"){
            this.showSuccess(response["body"].message);
            this.requestBtnText = "REQUEST A CALL";
            this.requestCallDetail.mobile = "";
            this.requestCallDetail.fullName = "";
            this.disableButton = false;
          }
          else{
            this.showError(response["body"].message);
            this.requestBtnText = "REQUEST A CALL";
            this.requestCallDetail.mobile = "";
            this.requestCallDetail.fullName = "";
            this.disableButton = false;
          }
        }
      })
    }
  }

  headerValue = {homeHeader:true, pricingHeader:false, serviceHeader:false , dropdownHeader:false,processHeader:false};
  detectDevice;
  // ==========================================================================================//
  detect(){
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
      this.detectDevice = "mobile device"
      if(navigator.userAgent.match(/Android/i)){
        // document.getElementsByClassName('play-store')[0].style.display = 'display'
        this.playStore = true;
      }
      
      else if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
        // alert(navigator.userAgent.match(/iPhone|iPad|iPod/i))
        this.appStore = true;
      }
    }
    else{
      this.detectDevice = "something else"  
      this.appStore = false;
      this.playStore = false;
    }
  }

  closeLink(){
    // alert('chl gya')
    document.getElementById('terminate').style.display='none'
    // ('.appLink').hide('slow');
    //   ('body').removeClass('appLink-add');
  }
  // =========================================================================================//
  ngOnInit() {
    this.header.changeHeader(this.headerValue);
    // for check
    this.header.incCount(0);
    this.header.getRate(0);
    this.header.setGetACallCSS(true);
    this.header.setDownloadLinkCss(true);
    this.detect();
    // alert(navigator.userAgent.match(/iPhone|iPad|iPod/i));
  }


}

// ==========================================================================================//


// ('.appLink').hide();
// $('body').removeClass('appLink-add');
// var isClosed = true;
// var isAndroid = false;
// var iOS = false;
// var isMobile = {
//    Android: function() {
//        return navigator.userAgent.match(/Android/i);
//    },
//    iOS: function() {
//        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
//    }
// };
// $( document ).ready(function() {    
//   if( isMobile.Android() ){ 
//     this.isAndroid = true;
//     $(".play-store").css("display", "block");      
//   }
//   else if( isMobile.iOS() ){ 
//     this.iOS = true;
//     $(".app-store").css("display", "block");       
//   }
//   else {
//     this.iOS = false;
//     this.isAndroid = false;
//     $(".play-store").css("display", "none");
//     $(".app-store").css("display", "none"); 
//     $('#top-app-store').hide();
//     $('#top-play-store').hide();
//   }
// });




// <script>
// $(document).ready(mob);
// $(window).resize(mob);
// function mob() {  
//    if($(window).width() <= 767 ){
//       var isMobile1 = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
//       if (isMobile1) {
//         if(isClosed==true){
//           $('.appLink').show('slow');
//           $('body').addClass('appLink-add');         }
//       }
//       else{
//         $('.appLink').hide();
//         $('body').removeClass('appLink-add');
//       }
//    }
//    else{
//        $('.appLink').hide();
//        $('body').removeClass('appLink-add');
//     }  
// }
// $('#closeLink').click(function(e) {
//   $('.appLink').hide('slow');
//   $('body').removeClass('appLink-add');
//   isClosed = false;
// });

// </script>