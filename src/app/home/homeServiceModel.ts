export class GetInTouchFormModel {
    txt_name: any;
    txt_tel: any;
    txt_email: any;
    txt_city: any;
    txt_comm: any;
    constructor(txt_name: any, txt_tel: any, txt_email: any, txt_city: any, txt_comm: any) {
        this.txt_name = txt_name;
        this.txt_tel = txt_tel;
        this.txt_email = txt_email;
        this.txt_city = txt_city;
        this.txt_comm = txt_comm;
    }
}

export class SubscriptionFormModel {
    txt_email: any;
    constructor(txt_email: any) {
        this.txt_email = txt_email;
    }
}

export class RequestACall{
    mobile_number:any;
    name:any;
    constructor(mobile_number:any,name:any){
        this.mobile_number = mobile_number;
        this.name = name;
    }
}