import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class HomeService {
    private getInTouchUrl = environment.baseApiURl + 'webpage/contact_us_email';
    private subscriptionUrl = environment.baseApiURl+'webpage/email';
    private requestCallUrl = environment.baseApiURl+'OrdersCall/ordersCallSave';
    private getLink         = environment.baseApiURl+'webpage/sendapplink';
    constructor(private http: HttpClient) {
    }

    getInTouchService(getInTouch) {
        var formData = new FormData();
        formData.append("txt_name", getInTouch.txt_name);
        formData.append("txt_tel", getInTouch.txt_tel);
        formData.append("txt_email", getInTouch.txt_email);
        formData.append("txt_city", getInTouch.txt_city);
        formData.append("txt_comm", getInTouch.txt_comm);
       return this.http.request(new HttpRequest(
            'POST',
            this.getInTouchUrl,
            formData
        ))
    }

    subscribeUser(email){
        var formdata = new FormData();
        formdata.append("txt_email" , email.txt_email);
        return this.http.request(new HttpRequest(
            'POST',
            this.subscriptionUrl,
            formdata
        
        ))
    }

    requestCallService(requestData){
        let headers = new HttpHeaders({'Content-Type':'application/json'}).append('Authorization','Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.requestCallUrl,
            requestData,
            {
                headers:headers
            }
        ))
    }
    getALink(number){
        let obj = {'txt_tel':number};
        return this.http.request(new HttpRequest( 'POST' , this.getLink  , obj ));
    }
}