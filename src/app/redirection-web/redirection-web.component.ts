import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-redirection-web',
  templateUrl: './redirection-web.component.html',
  styleUrls: ['./redirection-web.component.css']
})
export class RedirectionWebComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
    this.route.navigate(['/terms-and-condition']);
  }

}
