import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectionWebComponent } from './redirection-web.component';

describe('RedirectionWebComponent', () => {
  let component: RedirectionWebComponent;
  let fixture: ComponentFixture<RedirectionWebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectionWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectionWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
