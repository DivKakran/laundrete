import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import {LoginModel} from './loginModel';
import { LoginService } from './loginservice';
import {Router} from '@angular/router';
import {AuthService,FacebookLoginProvider,GoogleLoginProvider} from 'angular5-social-login';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[LoginService]
})
export class LoginComponent implements OnInit {

  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false ,dropdownHeader:false,processHeader:true};

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private serviceHeaderChange:ChangeHeaderService, private loginService:LoginService,
    private router:Router,private socialAuthService: AuthService,private toastr: ToastrService) { }
  loginModel = { mobile: "" };
  submittedLoginData = false;
  disableButton = false;
  LoginResponse;
  socialLoginResponse;
  facebookResponse;
  googleLoginResponse;
  googleResponse;
  FbLoginResponse;
  onSubmitLoginData(loginFormData){
    this.submittedLoginData = true;
    
    // let mobile = this.loginModel.mobile.toString();
    if(this.loginModel.mobile=="0000000000" || this.loginModel.mobile==""){
      this.showError('Mobile number is required');
    }
    else if(this.loginModel.mobile.length>10 || this.loginModel.mobile.length<10){
      this.showError('Mobile number should be 10 digit');
    }
    else{
      let loginData = new LoginModel('MOBILE', '1234', loginFormData.mobile);
      this.localStorage.setItem('mobileNumber',loginFormData.mobile);
      this.loginService.UserloginService(loginData).subscribe(response => {
        this.disableButton = true;
        this.LoginResponse = response;
        if (response && response["body"]){
          if (response["body"].status == '1') {
            this.localStorage.setItem('userExistence','true');
            this.router.navigate(['/login-otp']);
            this.disableButton = false;
          }
          else{ 
            // localStorage.setItem('mobileNumber',loginFormData.mobile);
            this.router.navigate(['/login-detail']);
            this.disableButton = false;
          }
        }
      });
    }
  }
  retrieveServiceFromCart='';
  // retrieveServiceFromCart = localStorage.getItem('serviceName');
  // retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
  retrieveItemsFromCart='';
  retrieveServiceFromCartArray;
  // retrieveItemsFromCart = localStorage.getItem('itemList');
  retrieveItemsFromCartArray;
  // retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);

  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } 
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.socialLoginResponse = userData;
        if(userData.provider=="facebook"){
        this.facebookResponse = userData;
        this.localStorage.setItem('userFacebookId',userData.id);
        let loginData = new LoginModel('FB', '1234', userData.id);
        this.loginService.UserloginService(loginData).subscribe(
        response=>{            
          if(response && response["body"]){
            this.FbLoginResponse = response["body"];
            this.localStorage.setItem('facebookResponseMobile',this.FbLoginResponse.mobile);
            if(response["body"].status=="0"){
              this.localStorage.setItem('fbUserName',userData.name);
              this.localStorage.setItem('fbUserEmail',userData.email);
              this.router.navigate(['/login-detail']);           
            }
            else{
              this.localStorage.setItem('userExistence','true');
              this.localStorage.setItem('facebookId','true');
              this.router.navigate(['/login-otp']);
            }
          }}         
        )
      }
      else{
        this.googleResponse = userData;
        this.localStorage.setItem('userGoogleId',userData.id);
        let loginData = new LoginModel('GOOGLE_PLUS', '1234', userData.id);
        this.loginService.UserloginService(loginData).subscribe(
        response=>{
          if(response && response["body"]){
            this.googleLoginResponse = response["body"];
            this.localStorage.setItem('googleResponseMobile',this.googleLoginResponse.mobile);
            if(response["body"].status=="0"){
              this.localStorage.setItem('googleUserName',userData.name);
              this.localStorage.setItem('googleUserEmail',userData.email);
              this.router.navigate(['/login-detail']);            
            }
            else{
              this.localStorage.setItem('userExistence','true');
              this.localStorage.setItem('googleId','true');
              this.router.navigate(['/login-otp']);
            }
          }
        }
        )
      }
      }
    );
  }

  showError(Error) {
    this.toastr.error(Error);
  }
  ngOnInit() {
    if(localStorage.getItem('userLogin')){
      this.router.navigate(['service-select']);
    }
    this.retrieveServiceFromCart = localStorage.getItem('serviceName');
    this.retrieveItemsFromCart = localStorage.getItem('itemList');
    this.retrieveServiceFromCartArray = JSON.parse(this.retrieveServiceFromCart);
    this.retrieveItemsFromCartArray = JSON.parse(this.retrieveItemsFromCart);
    this.serviceHeaderChange.changeHeader(this.headerValue);
    this.serviceHeaderChange.serviceListDisplay(this.retrieveServiceFromCartArray);
    this.serviceHeaderChange.itemListDisplay(this.retrieveItemsFromCartArray);
    this.serviceHeaderChange.totalPriceDisplay(this.localStorage.getItem('totalPrice'));
    this.serviceHeaderChange.totalClothsDisplay(this.localStorage.getItem('totalCloths'));
    this.serviceHeaderChange.setGetACallCSS(false);
    
  }

}
