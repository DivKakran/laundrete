import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest  } from '@angular/common/http';


@Injectable()
export class LoginService{
    private loginServiceUrl = environment.baseApiURl+'login/login';
    constructor(private http:HttpClient){}

    UserloginService(userData){
        var formdata = new FormData();
        formdata.append("login_type" , userData.login_type);
        formdata.append("uuid_value" , "1234");
        formdata.append("login_value" , userData.login_value);
        return this.http.request(new HttpRequest(
            'POST',
            this.loginServiceUrl,
            formdata
        ));
    }
}