export class LoginModel {
    login_type: any;
    uuid_value: any;
    login_value: any;
    constructor(login_type: any, uuid_value: any, login_value: any) {
        this.login_type = login_type;
        this.uuid_value = uuid_value;
        this.login_value = login_value;
    }
}