import { Injectable } from '@angular/core';
import { CanActivate , Router} from '@angular/router';
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private _route: Router){}
    canActivate(){
        if(localStorage.getItem("serviceArea") == "false"){
            return true;
        } else{ 
            this._route.navigate([""]);
            return false;
        }
    }
}