import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import {Router} from '@angular/router';
declare global {
  interface Window { dataLayer: any; }
}
@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.css']
})
export class ThankYouComponent implements OnInit {
  headerValue = { homeHeader: false, pricingHeader: false, serviceHeader: false, dropdownHeader: true };
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private header: ChangeHeaderService,
  private router: Router) { 
    this.loadAPI = new Promise((resolve) => {
      this.loadScript();
      resolve(true);
  });
  }
  placeOrderId='';

  loadAPI: Promise<any>;
  ngOnInit() {
    if(localStorage.getItem('itemList') == undefined){
      this.router.navigate(['']);
    }
    this.placeOrderId = localStorage.getItem('placeOrderId');
    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
  }
  redirect(val){ 
    this.localStorage.removeItem('itemList');
    this.localStorage.removeItem('serviceName');
    this.localStorage.removeItem('totalPrice');
    this.localStorage.removeItem('totalCloths');
    this.localStorage.removeItem('serviceComment');
    if(val == "track-order"){
      this.router.navigate([val]);
    }else{
      window.location.href='/'+val;
    }
  }

  public loadScript() {        
    var isFound = false;
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
      if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("loader")) {
        isFound = true;
      }
    }
    if (!isFound) {
      var dynamicScripts = ["../../assets/js/seoTagManager.js"];
      for (var i = 0; i < dynamicScripts .length; i++) {
        let node = document.createElement('script');
        node.src = dynamicScripts [i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
      }
    }
  }
}
