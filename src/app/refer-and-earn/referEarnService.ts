import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Injectable()
export class ReferEarnService {
    private emailInviteUrl = environment.baseApiURl + 'webpage/refer_and_earn';
    constructor(private http:HttpClient){}

    emailInviteService(invitedEmail){
        let headers = new HttpHeaders({'Content-Type': 'application/json' }).append('Authorization', 'Basic YWRtaW46MTIzNA==')
        return this.http.request(new HttpRequest(
            'POST',
            this.emailInviteUrl,
            invitedEmail,
            {
                headers:headers
            }
        ))
    }
}