import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ChangeHeaderService } from '../changeHeaderService';
import {ReferEarnEmailInvite} from './referEarnModel';
import {ReferEarnService} from './referEarnService';
import { ToastrService } from 'ngx-toastr';
import {Router,NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-refer-and-earn',
  templateUrl: './refer-and-earn.component.html',
  styleUrls: ['./refer-and-earn.component.css'],
  providers: [ReferEarnService]
})
export class ReferAndEarnComponent implements OnInit {
  // referCode = localStorage.getItem('referCode');
  // referralAmountText = localStorage.getItem('referralAmountText');
  // referralToLink = localStorage.getItem('referralToLink');
  referCode = '';
  referralAmountText = '';
  referralToLink = '';

  referEmail = {referEmailInvites:''};
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private header:ChangeHeaderService,private referEarnService:ReferEarnService,
    private toastr: ToastrService,private router:Router) { }
  headerValue = {homeHeader:false, pricingHeader:false, serviceHeader:false, dropdownHeader:true};

  referInvite(invitedEmail){
    
    let referEarnData = new ReferEarnEmailInvite(this.localStorage.getItem('userName'),this.referCode,invitedEmail,
    this.referralToLink);
    this.referEarnService.emailInviteService(referEarnData).subscribe(response=>{
      if(response && response["body"]){
        this.referEmail.referEmailInvites=''
        if(response["body"].status=="1"){
          this.showSuccess(response["body"].message);
        }
        else if(response["body"].status=="0"){
          this.showError(response["body"].message);
        }
      }
    }) 
  }

  copyToClipboard(val){
    let selBox = document.createElement('textarea');
    document.body.appendChild(selBox);
    selBox.value = val;
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  showError(error) {
    this.toastr.error(error);
  }
  showSuccess(message) {
    this.toastr.success(message);
  }
  ngOnInit() {
    this.referCode = localStorage.getItem('referCode');
    this.referralAmountText = localStorage.getItem('referralAmountText');
    this.referralToLink = localStorage.getItem('referralToLink');


    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
  }

}
