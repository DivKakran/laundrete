export class ReferEarnEmailInvite{
    sender_name:any;
    referral_code:any;
    email_ids:any;
    text_contents:any;
    constructor(sender_name:any,referral_code:any,email_ids:any,text_contents:any){
        this.sender_name = sender_name;
        this.referral_code = referral_code;
        this.email_ids = email_ids;
        this.text_contents = text_contents;
    }
}