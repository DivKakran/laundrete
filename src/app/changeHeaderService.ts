import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable()
export class ChangeHeaderService {
    data = new Subject();
    countItem = new Subject();
    totalRate = new Subject();
    serviceName = new Subject();
    ItemName = new Subject();
    subcategoryName = new Subject();
    removeObj = new Subject();
    showUserName = new Subject();
    showCart = new Subject();
    serviceList = new Subject();
    itemList = new Subject();
    totalPrice = new Subject();
    totalCloths = new Subject();
    opacity = new Subject<boolean>();
    updateCityName = new Subject();

    updateExistance = new Subject();

    rUserName   = new Subject();

    blog = new Subject();

    changingHeaderCredit = new Subject();

    // setting count in service when header delete
    t = new Subject();

    // setting count on delete
    setCount = new Subject();
    setGetACall = new Subject();
    setDownLoadLink = new Subject();
    changeHeader(val) {
        this.data.next(val);
    }
    incCount(val) {
        this.countItem.next(val);
    }
    decCount(val) {
        this.countItem.next(val);
    }
    getRate(val) {
        this.totalRate.next(val);
    }
    getServiceName(val) {
        this.serviceName.next(val);
    }
    getSubcategoryItemList(val) {
        this.ItemName.next(val);
    }
    getSubcategoryList(val) {
        this.subcategoryName.next(val);
    }
    removeObjectOnClick(val) {
        this.removeObj.next(val);
    }
    userNameDisplay(val){
        this.showUserName.next(val);
    }
    displaySavedCart(val){
        this.showCart.next(val);
    }
    serviceListDisplay(val){
        this.serviceList.next(val);
    }
    itemListDisplay(val){
        this.itemList.next(val);
    }
    totalPriceDisplay(val){
        this.totalPrice.next(val);
    }
    totalClothsDisplay(val){
        this.totalCloths.next(val);
    }
    setItemCountOnDelete(obj){
        this.setCount.next(obj);
    }
    // set count when delete from header
    set(val){
        this.t.next(val);
    }
    applyOpacity(val){
        this.opacity.next(val);
    }
    setGetACallCSS(val){
        this.setGetACall.next(val);
    }

    setDownloadLinkCss(val){
        this.setDownLoadLink.next(val);
    }
    getUpdatedCityName(cityName){
        this.updateCityName.next(cityName);
    }
    changingUserExistance(val){
        this.updateExistance.next(val);
    }
    getUserNameOnLogin(val){ 
        this.rUserName.next(val);
    }
    blogActive(val){
        this.blog.next(true);
    }
    headerCredit(val){
        this.changingHeaderCredit.next(val);
    }
}