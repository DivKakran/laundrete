import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectionWebFaqComponent } from './redirection-web-faq.component';

describe('RedirectionWebFaqComponent', () => {
  let component: RedirectionWebFaqComponent;
  let fixture: ComponentFixture<RedirectionWebFaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectionWebFaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectionWebFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
