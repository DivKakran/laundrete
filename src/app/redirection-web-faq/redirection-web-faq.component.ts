import { Component, OnInit } from '@angular/core';
import {Router , ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-redirection-web-faq',
  templateUrl: './redirection-web-faq.component.html',
  styleUrls: ['./redirection-web-faq.component.css']
})
export class RedirectionWebFaqComponent implements OnInit {

  constructor(private route: Router , private ac: ActivatedRoute) { }

  ngOnInit() {
    this.route.navigate(['faq']);
  }

}
