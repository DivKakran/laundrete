import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectionWebPrivacyComponent } from './redirection-web-privacy.component';

describe('RedirectionWebPrivacyComponent', () => {
  let component: RedirectionWebPrivacyComponent;
  let fixture: ComponentFixture<RedirectionWebPrivacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectionWebPrivacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectionWebPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
