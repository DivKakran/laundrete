import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-redirection-web-privacy',
  templateUrl: './redirection-web-privacy.component.html',
  styleUrls: ['./redirection-web-privacy.component.css']
})
export class RedirectionWebPrivacyComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
    this.route.navigate(['privacy-policy']);
  }

}
