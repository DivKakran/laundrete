import { Meta, Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import {ChangeHeaderService} from '../changeHeaderService';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {

  constructor(private header:ChangeHeaderService , private meta: Meta , private title: Title) { 
    this.title.setTitle("Privacy Policy - Launderette");
    this.meta.addTags([{ name:"description" , content: 'Privacy Policy - Launderette' } ,
    {name:"keywords" , content: 'Privacy Policy - Launderette' } ]);
  }
  headerValue = { homeHeader:false, pricingHeader:true, serviceHeader:false,dropdownHeader:false };
  ngOnInit() {
    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
  }

}
