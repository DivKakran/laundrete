import { Meta, Title } from '@angular/platform-browser';
import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import {ChangeHeaderService} from '../changeHeaderService';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-terms-and-condition',
  templateUrl: './terms-and-condition.component.html',
  styleUrls: ['./terms-and-condition.component.css']
})
export class TermsAndConditionComponent implements OnInit {

  constructor(@Inject(WINDOW) private window: Window, private header: ChangeHeaderService , private router:Router , private meta: Meta , private title: Title) { 
    this.title.setTitle("Terms and Condition - Launderette");
    this.meta.addTags([{ name:"description" , content: 'Terms and Condition - Launderette' } ,
    ,{ name:"keywords" , content: 'Terms and Condition - Launderette' }]);
  }
  headerValue = { homeHeader:false, pricingHeader:true, serviceHeader:false,dropdownHeader:false };
  ngOnInit() {
    this.header.changeHeader(this.headerValue);
    this.header.setGetACallCSS(false);
    this.router.events.subscribe((event: NavigationEnd) => {
      if (event instanceof NavigationEnd) {
        this.window.scrollTo(0 , 0);
      }
    });
    
  }

}
